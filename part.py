#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import numpy

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc

_LocalDBPath = "WORK/parts_database"

def getPath_LocalDB(path: str = None):
  if (path is None):
    try:
      return pyauto_base.fs.mkAbsolutePath(_LocalDBPath)
    except pyauto_base.fs.FSException:
      return None
  else:
    return pyauto_base.fs.mkAbsolutePath(path)

typeCapacitor = "capacitor"
typeFerriteBead = "ferrite_bead"

def thermal_voltage(T: float = 300.0) -> float:
  """
  :param T: in Kelvin
  :return: VT = kT/e
  """
  return ((1.38065e-23 * T) / 1.602176565e-19)

class PartModel(object):

  def __init__(self, PN=None):
    self.PN = PN
    self.mfr = None
    self.mfr_pn = None
    self.spice_path = ""
    self.spice_subckt = None

  def __str__(self):
    # yapf: disable
    return "{: >12}: {: <16}, {: <10}, {: <24} ({})".format(self.__class__.__name__, self.PN,
                                                            "NO_MFR" if (self.mfr == "") else self.mfr,
                                                            "NO_MPN" if (self.mfr_pn == "") else self.mfr_pn,
                                                            "NO_MODEL" if (self.spice_path == "") else "has model")
    # yapf: enable

  def _load_spice(self, fPath: str):
    dictRes = {}
    with open(fPath, "r") as fIn:
      for ln in fIn.readlines():
        if (ln.startswith("*")):
          continue
        if (ln.startswith(".SUBCKT")):
          dictRes["subckt"] = ln.split(" ")[1]
        if (ln[0] in ("R", "L", "C")):
          if ("passives" not in dictRes.keys()):
            dictRes["passives"] = {}
          tmp = ln.split()
          dictRes["passives"][tmp[0]] = float(tmp[3])
    return dictRes

  def load(self):
    path_mod = os.path.join(getPath_LocalDB(), "spice", self.spice_path)
    if (path_mod.lower().endswith(".mod")):
      return self._load_spice(path_mod)
    elif (path_mod.lower().endswith(".ckt")):
      return self._load_spice(path_mod)

class Capacitor(PartModel):

  def __init__(self, capacitance=math.nan, esl=math.nan, esr=math.nan, PN=None):
    super(Capacitor, self).__init__(PN)
    self.capacitance = capacitance
    self.esl = esl
    self.esr = esr

  def impedance(self, lstFreq):
    if (isinstance(lstFreq, list)):
      lstFreq = numpy.array(lstFreq)
    w = 2 * math.pi * lstFreq
    Z_cap = -1 / (w * self.capacitance)
    Z_esl = w * self.esl
    Z_esr = numpy.ones(len(lstFreq)) * self.esr
    Zo = numpy.sqrt(numpy.square(Z_cap + Z_esl) + numpy.square(Z_esr))
    return Zo

  def load(self):
    res = super(Capacitor, self).load()
    if ("C" in res["passives"].keys()):
      self.capacitance = res["passives"]["C"]
    elif ("C1" in res["passives"].keys()):
      self.capacitance = res["passives"]["C1"]
    elif ("C01" in res["passives"].keys()):
      self.capacitance = res["passives"]["C01"]

    if ("R" in res["passives"].keys()):
      self.esr = res["passives"]["R"]
    elif (("R1" in res["passives"].keys()) and ("R2" in res["passives"].keys())):
      self.esr = min(res["passives"]["R1"], res["passives"]["R2"])
    elif ("R03" in res["passives"].keys()):
      self.esr = res["passives"]["R03"]

    if ("L" in res["passives"].keys()):
      self.esl = res["passives"]["L"]
    elif ("L1" in res["passives"].keys()):
      self.esl = res["passives"]["L1"]
    elif ("L02" in res["passives"].keys()):
      self.esl = res["passives"]["L02"]

  def mkSpiceFixture(self, workDir):
    if (pyauto_base.misc.isEmptyString(self.mfr_pn)):
      return

    path_tpl = "./templates/fixture_cap.tpl"
    dict_tpl = {
        "title":
        self.mfr_pn,
        "includes": [
            pyauto_base.fs.mkAbsolutePath(
                os.path.join(getPath_LocalDB(), "spice", self.spice_path))
        ],
        "parts": ["XC1 in 0 {}".format(self.spice_subckt)]
    }
    path_out = os.path.join(workDir, "{}.cir".format(self.mfr_pn))
    pyauto_base.misc.templateWheezy(path_tpl, path_out, dictTpl=dict_tpl)

class FerriteBead(PartModel):

  def __init__(self, PN=None):
    super(FerriteBead, self).__init__(PN)

  def load(self):
    res = super(FerriteBead, self).load()
    if (res["subckt"] == ""):
      msg = "CANNOT find SUBCKT"
      logger.error(msg)
      raise RuntimeError(msg)

  def mkSpiceFixture(self, workDir):
    if (pyauto_base.misc.isEmptyString(self.mfr_pn)):
      return

    path_tpl = "./templates/fixture_fb.tpl"
    dict_tpl = {
        "title":
        self.mfr_pn,
        "includes": [
            pyauto_base.fs.mkAbsolutePath(
                os.path.join(getPath_LocalDB(), "spice", self.spice_path))
        ],
        "parts": ["XFB1 in 0 {}".format(self.spice_subckt)]
    }
    path_out = os.path.join(workDir, "{}.cir".format(self.mfr_pn))
    pyauto_base.misc.templateWheezy(path_tpl, path_out, dictTpl=dict_tpl)

class Diode(PartModel):

  def __init__(self, PN=None):
    super(Diode, self).__init__(PN)
    self.IFmax = math.nan
    self.VRmax = math.nan
    self.spice = {"IS": 1.0e-15, "N": 1.0, "RS": 0.0}

  def extractModel(self, Vmeas, Imeas):
    """
    based on (http://www.hindawi.com/journals/apec/2000/031390/abs/):
    "A new method for the extraction of diode parameters using a single exponential model"
    by S. Dib et al.
    """
    if (isinstance(Vmeas, numpy.ndarray)):
      pass
    elif (isinstance(Vmeas, list)):
      Vmeas = numpy.array([float(t) for t in Vmeas], dtype="float")
    else:
      logger.error("incorrect type for Vmeas (not a list)")
      return
    if (isinstance(Imeas, numpy.ndarray)):
      pass
    elif (isinstance(Imeas, list)):
      Imeas = numpy.array([float(t) for t in Imeas], dtype="float")
    else:
      logger.error("incorrect type for Imeas (not a list)")
      return

    #logger.debug(Vmeas)
    #logger.debug(Imeas)
    nVTmin = 0.01
    nVTmax = 0.2
    IS_mdl = 1.0e-9
    nVT_mdl = None
    RS_mdl = 0.01

    while True:
      logger.debug("nVT interval: [{}, {}]".format(nVTmin, nVTmax))
      nVT = numpy.arange(nVTmin, nVTmax, ((nVTmax - nVTmin) / 10.0))
      corr = []
      for tmp in nVT:
        v_mdl = (RS_mdl * Imeas) + (tmp * numpy.log(Imeas / IS_mdl))
        dv = (Vmeas - v_mdl)

        sol, res, rank, s = numpy.linalg.lstsq(
            numpy.vstack([Imeas, numpy.ones(len(Imeas))]).T, dv)
        corr.append(numpy.corrcoef((sol[0] * Imeas + sol[1]), dv)[0][1])
      #logger.debug(nVT)
      #logger.debug(corr)
      idx = numpy.argmax(corr)
      logger.debug("maximum found at {}; correlation: {}".format(nVT[idx], corr[idx]))
      if (math.fabs(nVTmin - nVTmax) < 0.001):
        nVT_mdl = nVT[idx]
        break

      if (idx == 0):
        nVTmin = nVTmin / 10.0
        nVTmax = nVT[idx]
      elif (idx == len(nVT)):
        nVTmin = nVT[idx]
        nVTmax = nVTmin + 0.1
      else:
        nVTmin = nVT[idx - 1]
        nVTmax = nVT[idx + 1]

    v_mdl = (RS_mdl * Imeas) + (nVT_mdl * numpy.log(Imeas / IS_mdl))
    dv = (Vmeas - v_mdl)
    linSol = numpy.linalg.lstsq(numpy.vstack([Imeas, numpy.ones(len(Imeas))]).T, dv)[0]
    logger.debug(nVT_mdl, linSol)

    self.spice["N"] = nVT_mdl / thermal_voltage()
    self.spice["RS"] = linSol[0] + RS_mdl
    self.spice["IS"] = IS_mdl / math.exp(linSol[1] / nVT_mdl)
    logger.info("SPICE model: {}".format(self.spice))

    #return [imeas, dv, imeas, (linSol[0] * imeas + linSol[1])]

  def getVICharacteristic(self, Id):
    import numpy

    if (isinstance(Id, numpy.ndarray)):
      pass
    elif (isinstance(Id, list)):
      Id = numpy.array([float(t) for t in Id], dtype="float")
    else:
      logger.error("incorrect type for current (not a list)")
      return

    #logger.debug(Id)
    Vd = (self.spice["RS"] * Id) \
        + (self.spice["N"] * thermal_voltage() * numpy.log(Id / self.spice["IS"]))
    Vd = [float(t) for t in Vd]
    return Vd

class MOSFET(PartModel):

  def __init__(self, PN=None):
    super(MOSFET, self).__init__(PN)
    self.V_GS_off = math.nan  # V
    self.V_GS_on = math.nan  # V
    self.I_D_on = math.nan  # A

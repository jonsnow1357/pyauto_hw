#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.Allegro
import pyauto_hw.CAD.DX
import pyauto_hw.CAD.HDL
import pyauto_hw.CAD.KiCad

def readNetlist(fPath: str) -> CADbase.Netlist:
  if (os.path.isdir(fPath) and fPath.endswith("allegro")):
    ntl = pyauto_hw.CAD.Allegro.readNetlist(fPath)
  elif (os.path.isdir(fPath) and fPath.endswith("packaged")):
    ntl = pyauto_hw.CAD.Allegro.readNetlist(fPath)
  elif (fPath.endswith("asc")):
    ntl = pyauto_hw.CAD.DX.readNetlist(fPath)
  elif (fPath.endswith("dialcnet.dat")):
    ntl = pyauto_hw.CAD.HDL.readNetlist_Concise(fPath)
  elif (fPath.endswith("net")):
    ntl = pyauto_hw.CAD.KiCad.readNetlist(fPath)
  else:
    msg = "UNRECOGNIZED path: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)

  if (ntl is None):
    msg = "CANNOT read netlist: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)

  return ntl

def readBom(fPath: str) -> CADbase.BOM:
  bom = CADbase.BOM()

  if (fPath.endswith("epicor.bom.csv")):
    bom.readCSV(fPath, CADbase.bomFileEpicor)
  elif (fPath.endswith("dx.csv")):
    bom.readCSV(fPath, CADbase.bomFileDX)
  elif (fPath.endswith("agile.csv")):
    bom.readCSV(fPath, CADbase.bomFileAgile)
  else:
    msg = "unknown BOM: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)

  return bom

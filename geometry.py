#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv

logger = logging.getLogger("lib")

_epsilon = (2 * sys.float_info.epsilon)
_round = 4

def isPositive(val, valId="", bZero=False):
  if (val < 0.0):
    msg = "INCORRECT {}: {}".format(valId, val)
    logger.error(msg)
    raise RuntimeError(msg)
  if (not bZero and (val == 0.0)):
    msg = "INCORRECT {}: {}".format(valId, val)
    logger.error(msg)
    raise RuntimeError(msg)

def setEpsilon(val=None):
  global _epsilon

  if (val is None):
    _epsilon = (2 * sys.float_info.epsilon)
  isPositive(val, "epsilon")
  _epsilon = val

def setRound(val=None):
  global _round

  if (val is None):
    _round = 4
  elif ((isinstance(val, int)) and (val > 0)):
    _round = val
  else:
    msg = "INCORRECT round: {}".format(val)
    logger.error(msg)
    raise RuntimeError(msg)

def degNormalize(deg):
  """
  normalize an angle in degrees to [0, 360.0)
  :param deg:
  """
  while (deg < 0.0):
    tmp = math.ceil(deg / 360.0)
    deg -= (tmp - 1) * 360.0
  while (deg >= 360.0):
    tmp = math.floor(deg / 360.0)
    deg -= tmp * 360.0

  if ((deg < 0.0) or (deg >= 360.0)):
    msg = "CANNOT convert {} deg to rad".format(deg)
    logger.error(msg)
    raise RuntimeError(msg)
  return deg

class Point2D(object):
  """
  represents a point.
  """

  def __init__(self, x, y):
    self.x = float(x)
    self.y = float(y)

  def __eq__(self, other):
    if (isinstance(other, self.__class__)):
      if ((self.x == other.x) and (self.y == other.y)):
        return True
      else:
        return False
    elif ((isinstance(other, list)) and (len(other) == 2)):
      if ((self.x == other[0]) and (self.y == other[1])):
        return True
      else:
        return False
    else:
      return False

  def __str__(self):
    return "{}: ({:g}, {:g})".format(self.__class__.__name__, self.x, self.y)

  def __add__(self, other):
    if (not isinstance(other, Point2D)):
      raise RuntimeError
    return Point2D((self.x + other.x), (self.y + other.y))

  def __sub__(self, other):
    if (not isinstance(other, Point2D)):
      raise RuntimeError
    return Point2D((self.x - other.x), (self.y - other.y))

  def toString(self):
    tmpx = round(self.x, _round)
    tmpy = round(self.y, _round)

    fmt = "x {{:.{}f}} y {{:.{}f}}".format(_round, _round)
    tmp = fmt.format(tmpx, tmpy)
    return tmp.format(self.x, self.y)

  def show(self):
    logger.info(self.toString())

  def distance(self, pt):
    if (not isinstance(pt, Point2D)):
      msg = "INCORRECT point: {}".format(pt)
      logger.error(msg)
      raise RuntimeError(msg)

    return math.sqrt((pt.y - self.y)**2 + (pt.x - self.x)**2)

class Line2D(object):
  """
  represents a line in general form: a*x + b*y + c = 0.
  """

  def __init__(self, **kwargs):
    self.a = None
    self.b = None
    self.c = None

    if (set(kwargs.keys()) == {"x", "y", "deg"}):
      pt = Point2D(kwargs["x"], kwargs["y"])
      self.__init_point_angle(pt, degNormalize(kwargs["deg"]))
    elif (set(kwargs.keys()) == {"pt", "deg"}):
      self.__init_point_angle(kwargs["pt"], degNormalize(kwargs["deg"]))
    elif (set(kwargs.keys()) == {"x1", "y1", "x2", "y2"}):
      pt1 = Point2D(kwargs["x1"], kwargs["y1"])
      pt2 = Point2D(kwargs["x2"], kwargs["y2"])
      self.__init_point_point(pt1, pt2)
    elif (set(kwargs.keys()) == {"pt1", "pt2"}):
      self.__init_point_point(kwargs["pt1"], kwargs["pt2"])
    else:
      msg = "CANNOT create {} with: {}".format(self.__class__.__name__, kwargs)
      logger.error(msg)
      raise RuntimeError(msg)

  def __init_point_angle(self, pt, deg):
    if (not isinstance(pt, Point2D)):
      msg = "INCORRECT point: {}".format(pt)
      logger.error(msg)
      raise RuntimeError(msg)

    deg = degNormalize(deg)

    if ((deg == 0.0) or (deg == 180.0)):
      self.a = 1.0
      self.b = 0.0
      self.c = -1.0 * pt.x
    elif ((deg == 90.0) or (deg == 270.0)):
      self.a = 0.0
      self.b = 1.0
      self.c = -1.0 * pt.y
    else:
      slope = math.tan((deg * math.pi) / 180.0)
      self.a = slope
      self.b = -1.0
      self.c = (pt.y - slope * pt.x)

  def __init_point_point(self, pt1, pt2):
    if (not isinstance(pt1, Point2D)):
      msg = "INCORRECT point: {}".format(pt1)
      logger.error(msg)
      raise RuntimeError(msg)
    if (not isinstance(pt2, Point2D)):
      msg = "INCORRECT point: {}".format(pt2)
      logger.error(msg)
      raise RuntimeError(msg)

    if (pt1.x == pt2.x):
      self.a = 1.0
      self.b = 0.0
      self.c = -1.0 * pt1.x
    elif (pt1.y == pt2.y):
      self.a = 0.0
      self.b = 1.0
      self.c = -1.0 * pt1.y
    else:
      slope = (pt2.y - pt1.y) / (pt2.x - pt1.x)
      self.a = slope
      self.b = -1.0
      self.c = (pt1.y - slope * pt1.x)

  def __str__(self):
    return "{}: {:g}*x + {:g}*y + {} = 0".format(self.__class__.__name__, self.a, self.b,
                                                 self.c)

  def hasPoint(self, pt):
    if (not isinstance(pt, Point2D)):
      msg = "INCORRECT point: {}".format(pt)
      logger.error(msg)
      raise RuntimeError(msg)

    res = self.a * pt.x + self.b * pt.y + self.c
    #logger.info("|{}| < {}".format(res, epsilon))
    if (math.fabs(res) <= _epsilon):
      return True
    return False

  def distance(self, pt):
    if (not isinstance(pt, Point2D)):
      msg = "INCORRECT point: {}".format(pt)
      logger.error(msg)
      raise RuntimeError(msg)

    raise NotImplementedError  # TODO:

class Circle2D(object):
  """
  represents a circle: (x - x_c)^2 + (y - y_c)^2 = r^2
  """

  def __init__(self, **kwargs):
    self._c = None
    self.r = None

    if (set(kwargs.keys()) == {"x", "y", "r"}):
      pt = Point2D(kwargs["x"], kwargs["y"])
      self.__init_point_radius(pt, kwargs["r"])
    elif (set(kwargs.keys()) == {"pt", "r"}):
      self.__init_point_radius(kwargs["pt"], kwargs["r"])
    elif (set(kwargs.keys()) == {"x1", "y1", "x2", "y2"}):
      pt1 = Point2D(kwargs["x1"], kwargs["y1"])
      pt2 = Point2D(kwargs["x2"], kwargs["y2"])
      self.__init_point_point(pt1, pt2)
    elif (set(kwargs.keys()) == {"pt1", "pt2"}):
      self.__init_point_point(kwargs["pt1"], kwargs["pt2"])
    else:
      msg = "CANNOT create {} with: {}".format(self.__class__.__name__, kwargs)
      logger.error(msg)
      raise RuntimeError(msg)

  def __init_point_radius(self, pt, r):
    if (not isinstance(pt, Point2D)):
      msg = "INCORRECT point: {}".format(pt)
      logger.error(msg)
      raise RuntimeError(msg)

    r = float(r)
    isPositive(r, "radius for Circle2D")

    self._c = pt
    self.r = r

  def __init_point_point(self, pt1, pt2):
    if (not isinstance(pt1, Point2D)):
      msg = "INCORRECT point: {}".format(pt1)
      logger.error(msg)
      raise RuntimeError(msg)
    if (not isinstance(pt2, Point2D)):
      msg = "INCORRECT point: {}".format(pt2)
      logger.error(msg)
      raise RuntimeError(msg)

    r = pt1.distance(pt2)
    isPositive(r, "radius for Circle2D")

    self._c = pt1
    self.r = r

  def __str__(self):
    return "{}: ({:g}, {:g}) r={:g}".format(self.__class__.__name__, self._c.x, self._c.y,
                                            self.r)

  @property
  def x(self):
    return self._c.x

  @property
  def y(self):
    return self._c.y

  @property
  def length(self):
    return (2.0 * math.pi * self.r)

  def hasPoint(self, pt):
    if (not isinstance(pt, Point2D)):
      msg = "INCORRECT point: {}".format(pt)
      logger.error(msg)
      raise RuntimeError(msg)

    res = ((pt.x - self.x)**2 + (pt.y - self.y)**2) - self.r**2
    #logger.info("|{}| < {}".format(res, epsilon))
    if (math.fabs(res) <= _epsilon):
      return True
    return False

  def pointAt(self, deg=0.0):
    """
    :param deg: angle from start point
    :return: the point situated on the circumference at <deg> angle from the x-axis direction (or counter-clockwise).
    """
    deg = degNormalize(deg)

    rad = (deg * math.pi) / 180.0
    #logger.info("rad = {}".format(rad))
    dy = self.r * math.sin(rad)
    dx = self.r * math.cos(rad)
    return Point2D((self.x + dx), (self.y + dy))

  def degAt(self, pt):
    if (not self.hasPoint(pt)):
      msg = "{} NOT ON {}".format(pt, self)
      logger.error(msg)
      raise RuntimeError(msg)

    if ((pt.x > self._c.x) and (pt.y >= self._c.y)):
      rad = math.acos((pt.x - self._c.x) / self.r)
      deg = (rad * 180.0) / math.pi
    elif ((pt.x <= self._c.x) and (pt.y > self._c.y)):
      rad = math.acos((self._c.x - pt.x) / self.r)
      deg = 180.0 - (rad * 180.0) / math.pi
    elif ((pt.x < self._c.x) and (pt.y <= self._c.y)):
      rad = math.acos((self._c.x - pt.x) / self.r)
      deg = 180.0 + (rad * 180.0) / math.pi
    else:
      rad = math.acos((pt.x - self._c.x) / self.r)
      deg = 360.0 - (rad * 180.0) / math.pi
    return degNormalize(deg)

class Segment2D(object):

  def __init__(self, **kwargs):
    self._p1 = None
    self._p2 = None

    if (set(kwargs.keys()) == {"x1", "y1", "x2", "y2"}):
      self._p1 = Point2D(kwargs["x1"], kwargs["y1"])
      self._p2 = Point2D(kwargs["x2"], kwargs["y2"])
    elif (set(kwargs.keys()) == {"pt1", "pt2"}):
      self._p1 = kwargs["pt1"]
      self._p2 = kwargs["pt2"]
    else:
      msg = "CANNOT create {} with: {}".format(self.__class__.__name__, kwargs)
      logger.error(msg)
      raise RuntimeError(msg)

  def __str__(self):
    return "{}: ({:g}, {:g}) ({:g}, {:g})".format(self.__class__.__name__, self._p1.x,
                                                  self._p1.y, self._p2.x, self._p2.y)

  @property
  def slope(self):
    """
    :return: the slope in degrees (normalized) of the segment
    """
    ln = Line2D(pt1=self._p1, pt2=self._p2)
    if (ln.a == 0):
      if (self._p1.x > self._p2.x):
        return 180.0
      else:
        return 0.0

    if (ln.b == 0):
      if (self._p1.y > self._p2.y):
        return 270.0
      else:
        return 90.0

    rad = math.atan(ln.a)
    deg = (rad * 180.0) / math.pi
    if (self._p2.x < self._p1.x):
      return degNormalize(deg + 180.0)
    else:
      return degNormalize(deg)

  @property
  def length(self):
    return self._p1.distance(self._p2)

  def middle(self):
    return Point2D((0.5 * (self._p1.x + self._p2.x)), (0.5 * (self._p1.y + self._p2.y)))

class Arc2D(object):
  """
  represents an arc following a positive angle from 'deg1' to 'deg2' (counter-clockwise)
  """

  def __init__(self, **kwargs):
    self._c = None
    self.r = None
    self.deg1 = None
    self.deg2 = None

    if (set(kwargs.keys()) == {"x", "y", "r", "deg1", "deg2"}):
      pt = Point2D(kwargs["x"], kwargs["y"])
      self.__init_point_radius_angle(pt, kwargs["r"], kwargs["deg1"], kwargs["deg2"])
    elif (set(kwargs.keys()) == {"pt", "r", "deg1", "deg2"}):
      self.__init_point_radius_angle(kwargs["pt"], kwargs["r"], kwargs["deg1"],
                                     kwargs["deg2"])
    elif (set(kwargs.keys()) == {"x1", "y1", "x2", "y2", "r"}):
      pt1 = Point2D(kwargs["x1"], kwargs["y1"])
      pt2 = Point2D(kwargs["x2"], kwargs["y2"])
      self.__init_point_point_radius(pt1, pt2, kwargs["r"])
    elif (set(kwargs.keys()) == {"pt1", "pt2", "r"}):
      self.__init_point_point_radius(kwargs["pt1"], kwargs["pt2"], kwargs["r"])
    else:
      msg = "CANNOT create {} with: {}".format(self.__class__.__name__, kwargs)
      logger.error(msg)
      raise RuntimeError(msg)

  def __init_point_radius_angle(self, pt, r, deg1, deg2):
    if (not isinstance(pt, Point2D)):
      msg = "INCORRECT point: {}".format(pt)
      logger.error(msg)
      raise RuntimeError(msg)

    r = float(r)
    isPositive(r, "radius for Arc2D")

    self._c = pt
    self.r = r
    self.deg1 = degNormalize(deg1)
    self.deg2 = degNormalize(deg2)

  def __init_point_point_radius(self, pt1, pt2, r):
    if (not isinstance(pt1, Point2D)):
      msg = "INCORRECT point(1): {}".format(pt1)
      logger.error(msg)
      raise RuntimeError(msg)
    if (not isinstance(pt2, Point2D)):
      msg = "INCORRECT point(2): {}".format(pt2)
      logger.error(msg)
      raise RuntimeError(msg)

    r = float(r)
    isPositive(r, "radius for Arc2D")

    seg = Segment2D(pt1=pt1, pt2=pt2)
    d1 = 0.5 * seg.length
    if (r < d1):
      msg = "CANNOT create {} with radius {}".format(self.__class__.__name__, r)
      logger.error(msg)
      raise RuntimeError(msg)

    pm = seg.middle()
    slope = seg.slope
    if (slope < 180.0):
      slope_p = slope + 90.0
    else:
      slope_p = slope - 90.0
    d2 = math.sqrt(r**2 - d1**2)
    cir_aux = Circle2D(pt=pm, r=d2)
    c1 = cir_aux.pointAt(slope_p)
    c2 = cir_aux.pointAt(slope_p + 180.0)
    #print("DBG", c1, c2)
    cir1 = Circle2D(pt=c1, r=r)
    cir2 = Circle2D(pt=c2, r=r)

    dp1c1 = cir1.degAt(pt1)
    dp2c1 = cir1.degAt(pt2)
    dp1c2 = cir2.degAt(pt1)
    dp2c2 = cir2.degAt(pt2)
    delta_c1 = dp2c1 - dp1c1
    delta_c2 = dp2c2 - dp1c2
    #print("DBG", dp1c1, dp2c1, dp1c2, dp2c2)
    if ((delta_c1 > 0) and (delta_c2 < 0)):
      self._c = c1
      self.deg1 = dp1c1
      self.deg2 = dp2c1
    elif ((delta_c1 < 0) and (delta_c2 > 0)):
      self._c = c2
      self.deg1 = dp1c2
      self.deg2 = dp2c2
    elif (((delta_c1 > 0) and (delta_c2 > 0)) or ((delta_c1 < 0) and (delta_c2 < 0))):
      if (delta_c1 < delta_c2):
        self._c = c1
        self.deg1 = dp1c1
        self.deg2 = dp2c1
      else:
        self._c = c2
        self.deg1 = dp1c2
        self.deg2 = dp2c2
    else:
      msg = "UNEXPECTED Arc2D"
      logger.error(msg)
      raise RuntimeError(msg)
    self.r = r

  def __str__(self):
    return "{}: ({:g}, {:g}) r={:g}, deg={:g}:{:g}".format(self.__class__.__name__,
                                                           self._c.x, self._c.y, self.r,
                                                           self.deg1, self.deg2)

  @property
  def x(self):
    return self._c.x

  @property
  def y(self):
    return self._c.y

  @property
  def length(self):
    if (self.deg2 < self.deg1):
      return ((360.0 - self.deg1 + self.deg2) / 360.0) * (2.0 * math.pi * self.r)
    else:
      return ((self.deg2 - self.deg1) / 360.0) * (2.0 * math.pi * self.r)

def rotate(pt, theta):
  """
  Rotates a Point2D around the origin with theta radians.
  :param pt: Point2D
  :param theta:
  :return: the new Point2D
  """
  res = Point2D(0, 0)
  res.x = pt.x * math.cos(theta) - pt.y * math.sin(theta)
  res.y = pt.x * math.sin(theta) + pt.y * math.cos(theta)
  return res

def intersection(ln1, ln2):
  """
  :param ln1: Line2D
  :param ln2: Line2D
  :return: the intersection point of 2 Line2D objects
  """
  if (not isinstance(ln1, Line2D)):
    msg = "INCORRECT line(1): {}".format(ln1)
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(ln2, Line2D)):
    msg = "INCORRECT line(2): {}".format(ln2)
    logger.error(msg)
    raise RuntimeError(msg)

  if ((ln1.b == 0) and (ln2.b == 0)):
    msg = "CANNOT determine intersection of parallel lines"
    logger.warning(msg)
    return None
  if ((ln1.a * ln2.b) == (ln2.a * ln1.b)):
    msg = "CANNOT determine intersection of parallel lines"
    logger.warning(msg)
    return None

  x = ((ln1.b * ln2.c) - (ln2.b * ln1.c)) / ((ln1.a * ln2.b) - (ln2.a * ln1.b))
  y = ((ln2.a * ln1.c) - (ln1.a * ln2.c)) / ((ln1.a * ln2.b) - (ln2.a * ln1.b))
  return Point2D(x, y)

def boundingBox(lstPts):
  """
  Determines the bounding box for a list of points
  :param lstPts: list of Point2D
  :return: [Point2D (lower left), Point2D (upper right)]
  """
  if (len(lstPts) == 0):
    return [None, None]
  if (len(lstPts) == 1):
    return [lstPts[0], lstPts[0]]

  # logger.info("boundingBox for {} pts".format(len(lstPts)))
  ll = Point2D(lstPts[0].x, lstPts[0].y)
  ur = Point2D(lstPts[0].x, lstPts[0].y)
  for pt in lstPts:
    if (pt.x < ll.x):
      ll.x = pt.x
    if (pt.x > ur.x):
      ur.x = pt.x
    if (pt.y < ll.y):
      ll.y = pt.y
    if (pt.y > ur.y):
      ur.y = pt.y
  return [ll, ur]

def guessGrid(lstPts):
  """
  Assuming the points are drawn on a grid, tries to guess the size of the grid.
  Basically determines the smallest, distance on x and y between points (greater than 0)
  :return: [dimX, dimY]
  """
  if (len(lstPts) < 2):
    return [None, None]

  # logger.info("guessGrid for {} pts".format(len(lstPts)))
  dimX = None
  dimY = None
  for pt1 in lstPts:
    for pt2 in lstPts:
      tmp_x = math.fabs(pt1.x - pt2.x)
      tmp_y = math.fabs(pt1.y - pt2.y)
      if (tmp_x > 0.0):
        if (dimX is None):
          dimX = tmp_x
        elif (tmp_x < dimX):
          dimX = tmp_x
      if (tmp_y > 0.0):
        if (dimY is None):
          dimY = tmp_y
        elif (tmp_y < dimY):
          dimY = tmp_y
  return [dimX, dimY]

def tiling_square(pt1, pt2, dx, dy=None, bAlternate=False):
  """
  :param pt1: top-left point
  :param pt2: bottom-right point
  :param dx: distance on x
  :param dy: distance on y
  :param bAlternate: [True|False] skips every second point
  :return: a list of points on a square tiling
  """
  if (not isinstance(pt1, Point2D)):
    msg = "INCORRECT point(1): {}".format(pt1)
    logger.error(msg)
    raise RuntimeError(msg)
  if (not isinstance(pt2, Point2D)):
    msg = "INCORRECT point(2): {}".format(pt2)
    logger.error(msg)
    raise RuntimeError(msg)
  if (pt2.x < pt1.x):
    msg = "INCORRECT points for tiling_square: {} -> {}".format(pt1, pt2)
    logger.error(msg)
    raise RuntimeError(msg)
  if (pt2.y > pt1.y):
    msg = "INCORRECT points for tiling_square: {} -> {}".format(pt1, pt2)
    logger.error(msg)
    raise RuntimeError(msg)
  if (dy is None):
    dy = dx
  isPositive(dx, "dx for tiling_square")
  isPositive(dy, "dy for tiling_square")

  lstPts = []
  gridColCnt = 0
  y = pt1.y
  while (y >= pt2.y):
    gridLnCnt = 0
    x = pt1.x
    while (x <= pt2.x):
      #print([gridLnCnt, gridColCnt])
      if (not bAlternate):
        #showPoint(x, y)
        lstPts.append(Point2D(x, y))
      else:
        if (((gridLnCnt + gridColCnt) % 2) == 0):
          #showPoint(x, y)
          lstPts.append(Point2D(x, y))
      x += dx
      gridLnCnt += 1
    y -= dy
    gridColCnt += 1

  return lstPts

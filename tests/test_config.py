#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.config"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.config

class Test_config(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def _test_DBCheck(self, cfg, strTable, nChecks):
    self.assertEqual(cfg.table, strTable)
    self.assertEqual(len(cfg.checks), nChecks)

  def _test_DBTransform(self, cfg, strSrc, strDst, strKey, nQueries):
    self.assertEqual(cfg.src, strSrc)
    self.assertEqual(cfg.dst, strDst)
    self.assertEqual(cfg.key, strKey)
    self.assertEqual(len(cfg.queries), nQueries)

  #@unittest.skip("")
  def test_PartsDBConfig(self):
    fPath = os.path.join(self.inFolder, "config", "test_read_partsdb.xml")
    cfg = pyauto_hw.config.PartsDBConfig()
    cfg.loadXmlFile(fPath)

    cfg.showInfo()
    self.assertEqual(len(cfg.lstDBChecks), 1)
    self.assertEqual(len(cfg.lstDBTransforms), 1)

    self._test_DBCheck(cfg.lstDBChecks[0], "table0", 2)
    self._test_DBTransform(cfg.lstDBTransforms[0], "0", "1", "3", 5)

  #@unittest.skip("")
  def test_ProjectConfig(self):
    fPath = os.path.join(self.inFolder, "config", "project")
    cfg = pyauto_hw.config.ProjectConfig(fPath)

    cfg.showInfo()
    self.assertEqual(cfg.name, "project1")
    self.assertEqual(len(cfg.boards), 2)

    self.assertEqual(cfg.boards[0].name, "board1")
    self.assertEqual(len(cfg.boards[0].vars), 1)
    self.assertIsNone(cfg.boards[0].sch)
    self.assertIsNotNone(cfg.boards[0].pcb)
    self.assertEqual(len(cfg.boards[0].pwr), 1)

    self.assertEqual(cfg.boards[0].pcb.stackup.nLayers, 6)
    self.assertEqual(cfg.boards[0].pcb.stackup.nVias, 1)
    self.assertEqual(cfg.boards[0].pcb.artwork.nLayers, 13)

    self.assertEqual(len(cfg.boards[0].pwr["board1"].netRemove), 1)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].netMerge), 2)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].netRename), 1)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].compRemove), 0)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].compAdd), 0)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].modelCases), 0)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].modelEfficiencies), 0)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].powerPorts), 0)
    self.assertEqual(len(cfg.boards[0].pwr["board1"].powerSense), 0)

    self.assertEqual(cfg.boards[1].name, "board2")
    self.assertEqual(len(cfg.boards[1].vars), 0)
    self.assertIsNone(cfg.boards[1].sch)
    self.assertIsNotNone(cfg.boards[1].pcb)
    self.assertEqual(len(cfg.boards[1].pwr), 1)

    self.assertEqual(cfg.boards[1].pcb.stackup.nLayers, 2)
    self.assertEqual(cfg.boards[1].pcb.stackup.nVias, 1)
    self.assertEqual(cfg.boards[1].pcb.artwork.nLayers, 9)

    self.assertEqual(len(cfg.boards[1].pwr["board2"].netRemove), 0)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].netMerge), 0)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].netRename), 0)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].compRemove), 2)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].compAdd), 1)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].modelCases), 0)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].modelEfficiencies), 0)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].powerPorts), 1)
    self.assertEqual(len(cfg.boards[1].pwr["board2"].powerSense), 0)

#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.standards.OpticalModule"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import json

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.standards.OpticalModule as opt_mod
import pyauto_base.misc

class Test_conversion(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_conversion(self):
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x00, 0x00]), 0.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x00, 0x01]), 0.004, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x00, 0xFF]), 0.996, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x01, 0x00]), 1.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x01, 0x01]), 1.004, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x19, 0x00]), 25.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x7D, 0x00]), 125.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x7F, 0xFF]), 127.996, delta=0.001)

    self.assertAlmostEqual(opt_mod.utils.convertTemp([0xFF, 0xFF]), -0.004, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0xFF, 0x00]), -1.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0xE7, 0x00]), -25.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0xD8, 0x00]), -40.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x80, 0x01]), -127.996, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertTemp([0x80, 0x00]), -128.0, delta=0.001)

    self.assertAlmostEqual(opt_mod.utils.convertVoltage([0x00, 0x00]), 0.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertVoltage([0x7F, 0xFF]), 3.2768, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertVoltage([0xFF, 0xFF]), 6.5535, delta=0.001)

    self.assertAlmostEqual(opt_mod.utils.convertBias([0x00, 0x00]), 0.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertBias([0x7F, 0xFF]), 65.534, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertBias([0xFF, 0xFF]), 131.07, delta=0.001)

    self.assertAlmostEqual(opt_mod.utils.convertPower([0x00, 0x00]), 0.0, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertPower([0x7F, 0xFF]), 3.2768, delta=0.001)
    self.assertAlmostEqual(opt_mod.utils.convertPower([0xFF, 0xFF]), 6.5535, delta=0.001)

# class Test_SFP(unittest.TestCase):
#   cwd = ""
#   lclDir = ""
#
#   @classmethod
#   def setUpClass(cls):
#     cls.cwd = os.getcwd()
#     cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
#     os.chdir(cls.lclDir)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   @classmethod
#   def tearDownClass(cls):
#     os.chdir(cls.cwd)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   def setUp(self):
#     print("")
#     self.inFolder = os.path.join(self.lclDir, "files")
#     self.outFolder = os.path.join(self.lclDir, "data_out")
#     if (not os.path.isdir(self.outFolder)):
#       os.makedirs(self.outFolder)
#
#   def tearDown(self):
#     pass
#
#   #@unittest.skip("")
#   def test_showInfo(self):
#     sfp = opt_mod.SFF_8472()
#     sfp.readJson(os.path.join(self.inFolder, "sfp_finisar.jsonl"))
#     sfp.parse_A0()
#     sfp.parse_A2()
#
#     sfp.showInfo_A0()
#     sfp.showInfo_A2()
#
#     self.assertEqual(sfp.infoBase["CRC"], "OK")
#     self.assertEqual(sfp.infoExt["CRC"], "OK")
#     self.assertEqual(sfp.infoDiag["CRC"], "OK")
#
#     self.assertEqual(sfp.infoBase["ModID"], "SFP/SFP+")
#     self.assertEqual(sfp.infoBase["VendorName"], "FINISAR CORP.   ")
#     self.assertEqual(sfp.infoBase["VendorPN"], "FTLX1472M3BCL   ")
#     self.assertEqual(sfp.infoExt["VendorSN"], "UMH04DL         ")
#     self.assertEqual(sfp.infoBase["Wavelength"], "1310nm")
#     self.assertEqual(sfp.infoBase["Transceiver"],
#                      ["10GBASE-LR", "OC-192 SR-1", "FC-800M,1200M-L-SM-LC"])
#
#     self.assertEqual(sfp.infoDiag["Flags"], [
#         "TX_Pwr_Low_Alarm", "RX_Pwr_Low_Alarm", "TX_Pwr_Low_Warn", "TX_Bias_Low_Warn",
#         "RX_Pwr_Low_Warn"
#     ])
#
#     sfp.readJson(os.path.join(self.inFolder, "sfp_opnext.jsonl"))
#     sfp.parse_A0()
#     sfp.parse_A2()
#
#     sfp.showInfo_A0()
#     sfp.showInfo_A2()
#
#     self.assertEqual(sfp.infoBase["CRC"], "OK")
#     self.assertEqual(sfp.infoExt["CRC"], "OK")
#     self.assertEqual(sfp.infoDiag["CRC"], "OK")
#
#     self.assertEqual(sfp.infoBase["ModID"], "SFP/SFP+")
#     self.assertEqual(sfp.infoBase["VendorName"], "OPNEXT INC      ")
#     self.assertEqual(sfp.infoBase["VendorPN"], "TRF5916AVLB400  ")
#     self.assertEqual(sfp.infoExt["VendorSN"], "C10C54800       ")
#     self.assertEqual(sfp.infoBase["Wavelength"], "1310nm")
#     self.assertEqual(sfp.infoBase["Transceiver"], ["OC-48 SR"])
#
#     self.assertEqual(sfp.infoDiag["Flags"], [
#         "TX_Pwr_Low_Alarm", "RX_Pwr_Low_Alarm", "TX_Pwr_Low_Warn", "TX_Bias_Low_Warn",
#         "RX_Pwr_Low_Warn"
#     ])
#
#     sfp.readJson(os.path.join(self.inFolder, "sfp_axcen.jsonl"))
#     sfp.parse_A0()
#     sfp.parse_A2()
#
#     sfp.showInfo_A0()
#     sfp.showInfo_A2()
#
#     self.assertEqual(sfp.infoBase["CRC"], "OK")
#     self.assertEqual(sfp.infoExt["CRC"], "OK")
#     self.assertEqual(sfp.infoDiag["CRC"], "FAIL")
#
#     self.assertEqual(sfp.infoBase["ModID"], "SFP/SFP+")
#     self.assertEqual(sfp.infoBase["VendorName"], "Axcen Photonics ")
#     self.assertEqual(sfp.infoBase["VendorPN"], "AXFE-R1S4-0M02  ")
#     self.assertEqual(sfp.infoExt["VendorSN"], "AX1232R200686   ")
#     self.assertEqual(sfp.infoBase["Wavelength"], "0nm")
#     self.assertEqual(sfp.infoBase["Transceiver"], ["100BASE-FX"])
#
# class Test_XFP(unittest.TestCase):
#   cwd = ""
#   lclDir = ""
#
#   @classmethod
#   def setUpClass(cls):
#     cls.cwd = os.getcwd()
#     cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
#     os.chdir(cls.lclDir)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   @classmethod
#   def tearDownClass(cls):
#     os.chdir(cls.cwd)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   def setUp(self):
#     print("")
#     self.inFolder = os.path.join(self.lclDir, "files")
#     self.outFolder = os.path.join(self.lclDir, "data_out")
#     if (not os.path.isdir(self.outFolder)):
#       os.makedirs(self.outFolder)
#
#   def tearDown(self):
#     pass
#
#   #@unittest.skip("")
#   def test_showInfo(self):
#     xfp = opt_mod.XFPModule()
#     xfp.readJson(os.path.join(self.inFolder, "xfp_fqd.jsonl"))
#     xfp.parse_A0()
#     #xfp.parse_Table0()
#     xfp.parse_Table1()
#
#     xfp.showInfo_A0()
#     #xfp.showInfo_Table0()
#     xfp.showInfo_Table1()
#
#     self.assertEqual(xfp.infoTable1["CRC_BASE"], "OK")
#     self.assertEqual(xfp.infoTable1["CRC_EXT"], "OK")
#
#     self.assertEqual(xfp.infoBase["ModID"], "XFP")
#     self.assertEqual(xfp.infoBase["Sig_Conditioner"], {
#         "Line Loopback": False,
#         "REFCLK Mode": "Async",
#         "XFI Loopback": False,
#         "Data Rate": 9.5
#     })
#
#     self.assertEqual(xfp.infoTable1["VendorName"], "FQD             ")
#     self.assertEqual(xfp.infoTable1["VendorPN"], "HS/FIM31020     ")
#     self.assertEqual(xfp.infoTable1["VendorSN"], "LU9022          ")
#     self.assertEqual(xfp.infoTable1["Wavelength"], "1536nm")
#
# class Test_CFP(unittest.TestCase):
#   cwd = ""
#   lclDir = ""
#
#   @classmethod
#   def setUpClass(cls):
#     cls.cwd = os.getcwd()
#     cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
#     os.chdir(cls.lclDir)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   @classmethod
#   def tearDownClass(cls):
#     os.chdir(cls.cwd)
#     logger.info("CWD: {}".format(os.getcwd()))
#
#   def setUp(self):
#     print("")
#     self.inFolder = os.path.join(self.lclDir, "files")
#     self.outFolder = os.path.join(self.lclDir, "data_out")
#     if (not os.path.isdir(self.outFolder)):
#       os.makedirs(self.outFolder)
#
#   def tearDown(self):
#     pass
#
#   #@unittest.skip("")
#   def test_showInfo(self):
#     cfp = opt_mod.CFPModule()
#     cfp.readJson(os.path.join(self.inFolder, "cfp_reflex.jsonl"))
#     cfp.parse_NVR1()
#
#     cfp.showInfo()
#
#     self.assertEqual(cfp.infoNVR1["CRC"], "OK")
#
#     self.assertEqual(cfp.infoNVR1["ModID"], "CXP/CFP")
#     self.assertEqual(cfp.infoNVR1["VendorName"], "Reflex Photonics")
#     self.assertEqual(cfp.infoNVR1["VendorPN"], "CF-X12-C11901-02")
#     self.assertEqual(cfp.infoNVR1["VendorSN"], "X000D005        ")
#     self.assertEqual(cfp.infoNVR1["Wavelength_Max"], "860.0000nm")
#     self.assertEqual(cfp.infoNVR1["Wavelength_Min"], "840.0000nm")
#     self.assertEqual(cfp.infoNVR1["Lanes"], {"Host": 10, "Network": 10})

class Test_parse(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_parse_SFP(self):
    dictCLIArgs = {"path": os.path.join(self.inFolder, "sfp_axcen.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    dictCLIArgs = {"path": os.path.join(self.inFolder, "sfp_finisar.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    dictCLIArgs = {"path": os.path.join(self.inFolder, "sfp_opnext.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

  def test_parse_XFP(self):
    dictCLIArgs = {"path": os.path.join(self.inFolder, "xfp_fqd.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

  def test_parse_CFP(self):
    dictCLIArgs = {"path": os.path.join(self.inFolder, "cfp_reflex.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

  def test_parse_QSFP(self):
    dictCLIArgs = {"path": os.path.join(self.inFolder, "qsfp_finisar1.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    dictCLIArgs = {"path": os.path.join(self.inFolder, "qsfp_finisar2.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    dictCLIArgs = {"path": os.path.join(self.inFolder, "qsfp_finisar3.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

  def test_parse_QSFPDD(self):
    dictCLIArgs = {"path": os.path.join(self.inFolder, "qsfpdd_innolight1.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    dictCLIArgs = {"path": os.path.join(self.inFolder, "qsfpdd_innolight2.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

    dictCLIArgs = {"path": os.path.join(self.inFolder, "qsfpdd_cig.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

  def test_parse_OSFP(self):
    dictCLIArgs = {"path": os.path.join(self.inFolder, "osfp_multilane.jsonl")}
    pyauto_base.misc.runScriptMainApp("pyauto_hw.standards.OpticalModule.parse",
                                      logger,
                                      dictCLIArgs=dictCLIArgs)

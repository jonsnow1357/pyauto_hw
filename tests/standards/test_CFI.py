#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.standards.CFI"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.standards.CFI

class Test_CFI(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

    self.cfi = pyauto_hw.standards.CFI.CFI()
    self.cfi.id_data = [
        "0x0051", "0x0052", "0x0059", "0x0001", "0x0000", "0x000a", "0x0001", "0x0000",
        "0x0000", "0x0000", "0x0000"
    ]
    self.cfi.if_data = [
        "0x0017", "0x0020", "0x0085", "0x0095", "0x0009", "0x000a", "0x000a", "0x0000",
        "0x0001", "0x0002", "0x0002", "0x0000"
    ]
    self.cfi.geom_data = [
        "0x001b", "0x0001", "0x0000", "0x000a", "0x0000", "0x0001", "0x00ff", "0x0003",
        "0x0000", "0x0002"
    ]

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_showInfo(self):
    rawData = not True
    self.cfi.parse(rawData)

    self.assertEqual(self.cfi.infoID["ascii"], "QRY")
    self.assertEqual(self.cfi.infoIf["VCCmax"], "0x0020")
    self.assertEqual(self.cfi.infoGeom["DevIf"], "x16-only async")
    self.cfi.showInfo()

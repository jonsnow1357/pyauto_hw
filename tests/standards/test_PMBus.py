#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.standards.PMBus"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.standards.PMBus as PMBus

class Test_PMBus(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_convert(self):
    self.assertEqual(PMBus.convert_to_L16u(4.75, exponent=-12), "0x4C00")
    self.assertEqual(PMBus.convert_to_L16u(4.75, exponent=-13), "0x9800")
    self.assertEqual(PMBus.convert_to_L16u(1.25, exponent=-12), "0x1400")

    self.assertEqual(PMBus.convert_from_L16u("0x4C00", exponent=-12), 4.75)
    self.assertEqual(PMBus.convert_from_L16u("0x9800", exponent=-13), 4.75)
    self.assertEqual(PMBus.convert_from_L16u("0x1400", exponent=-12), 1.25)

    self.assertAlmostEqual(PMBus.convert_from_L11("0x9807"), 0.0008544921875, delta=1e-12)
    self.assertAlmostEqual(PMBus.convert_from_L11("0xD280"), 10.0, delta=1e-12)

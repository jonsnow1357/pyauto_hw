#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.part"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_hw.part

class Test_part(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_Capacitor(self):
    cap = pyauto_hw.part.Capacitor(capacitance=22e-9, esl=1e-10, esr=12e-2)
    self.assertEqual(cap.capacitance, 22e-9)
    self.assertEqual(cap.esl, 1e-10)
    self.assertEqual(cap.esr, 12e-2)
    self.assertEqual(cap.PN, None)

    cap.impedance([1e6, 10e6, 100e6])

  # @unittest.skip("")
  def test_Capacitor_load(self):
    libPath = pyauto_hw.part.getPath_LocalDB()
    if ((libPath is None) or (not os.path.isdir(libPath))):
      self.skipTest("")

    mapPath = os.path.join(libPath, "spice", "parts.csv")
    if (not os.path.isfile(mapPath)):
      self.skipTest("")

    lstPN = []
    with open(mapPath, "r") as fIn:
      csvIn = csv.reader(fIn)
      for row in csvIn:
        if (row[1] == "test_mod"):
          cap0 = pyauto_hw.part.Capacitor(PN=row[1])
          cap0.spice_path = os.path.join(row[0], row[2], row[4])
        if (row[1] == "test_ckt"):
          cap1 = pyauto_hw.part.Capacitor(PN=row[1])
          cap1.spice_path = os.path.join(row[0], row[2], row[4])

    self.assertEqual(cap0.PN, "test_mod")
    cap0.load()
    self.assertEqual(cap0.capacitance, 8.36e-08)
    self.assertEqual(cap0.esl, 1.46e-10)
    self.assertEqual(cap0.esr, 2.62e-2)

    self.assertEqual(cap1.PN, "test_ckt")
    cap1.load()
    self.assertEqual(cap1.capacitance, 9.54792049014941e-05)
    self.assertEqual(cap1.esl, 5.79999992478264e-11)
    self.assertEqual(cap1.esr, 0.0050465427339077)

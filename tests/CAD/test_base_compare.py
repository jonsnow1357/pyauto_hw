#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD._base_compare"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.tests.tools as tt
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.tests.CAD.misc as misc

class Test_PinCompare(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath1 = os.path.join(self.inFolder, "base", "test_read_Component_pin1.pins.csv")
    fPath2 = os.path.join(self.inFolder, "base", "test_read_Component_pin2.pins.csv")

    pComp = CADbase.PinCompare()
    pComp.cp1 = CADbase.Component()
    pComp.cp2 = CADbase.Component()

    pComp.cp1.readCSV(fPath1)
    misc.chkComponent(self, pComp.cp1, "U?", "test_read_Component_pin1", 8, 0,
                      ["A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2"],
                      ["VCC", "GND", "IN0", "IN1", "IN2", "OUT0", "OUT1", "OUT2"], None,
                      None)
    pComp.cp2.readCSV(fPath2)
    misc.chkComponent(self, pComp.cp2, "U?", "test_read_Component_pin2", 8, 0,
                      ["A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3"],
                      ["VCC", "GND", "IN0", "IN1", "OUT0", "OUT1", "IO", "CTRL"], None,
                      None)

    pComp.compare()
    self.assertEqual(len(pComp.lstPinNo_del), 1)
    self.assertEqual(len(pComp.lstPinNo_add), 1)
    self.assertEqual(len(pComp.lstPinNo_chg), 2)
    self.assertCountEqual(pComp.lstPinNo_del, ["A1"])
    self.assertCountEqual(pComp.lstPinNo_add, ["C3"])
    self.assertCountEqual(pComp.lstPinNo_chg, ["B3", "C2"])

    pComp.showReport()
    pComp.showInfo()

class Test_NetCompare(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_Component2.pins.csv")

    nComp = CADbase.NetCompare()
    nComp.cp1 = CADbase.Component()
    nComp.cp2 = CADbase.Component()

    nComp.cp1.readCSV(fPath)
    nComp.cp1.delPinInfo("U?.1")
    nComp.cp1.getPinByNumber("2").netName = "GND"
    nComp.cp1.getPinByNumber("3").netName = "GND"
    nComp.cp1.getPinByNumber("4").netName = "GND"
    nComp.cp1.getPinByNumber("5").netName = "+3V3_ADC1"
    nComp.cp1.getPinByNumber("6").netName = "+3V3_REF"
    nComp.cp1.getPinByNumber("7").netName = "V1"
    nComp.cp1.getPinByNumber("8").netName = "V3"
    nComp.cp1.getPinByNumber("9").netName = "V4"
    nComp.cp1.getPinByNumber("10").netName = "V2"
    nComp.cp1.getPinByNumber("11").netName = "AS"
    nComp.cp1.getPinByNumber("12").netName = "CONVb"
    nComp.cp1.getPinByNumber("13").netName = "ALERT"
    nComp.cp1.getPinByNumber("14").netName = "SDA"
    nComp.cp1.getPinByNumber("15").netName = "SCL"
    nComp.cp1.getPinByNumber("16").netName = "GND"
    nComp.cp1.refresh()
    misc.chkComponent(self, nComp.cp1, "U?", "test_read_Component2", 15, 12,
                      [str(t) for t in range(2, 17)], [
                          "AGND_2", "AGND_3", "AGND_4", "AGND_16", "ALERT", "AS", "CONVST",
                          "REFIN", "SCL", "SDA", "VDD", "VIN1", "VIN2", "VIN3", "VIN4"
                      ], [
                          "+3V3_ADC1", "+3V3_REF", "GND", "V1", "V2", "V3", "V4", "AS",
                          "CONVb", "ALERT", "SCL", "SDA"
                      ], None)

    nComp.cp2.readCSV(fPath)
    nComp.cp2.getPinByNumber("1").netName = "GND"
    nComp.cp2.delPinInfo("U?.2")
    nComp.cp2.getPinByNumber("3").netName = "GND"
    nComp.cp2.getPinByNumber("4").netName = "GND"
    nComp.cp2.getPinByNumber("5").netName = "+3V3_ADC3"
    nComp.cp2.getPinByNumber("6").netName = "+2V5_REF"
    nComp.cp2.getPinByNumber("7").netName = "V1"
    nComp.cp2.getPinByNumber("8").netName = "V3"
    nComp.cp2.getPinByNumber("9").netName = "V4"
    nComp.cp2.getPinByNumber("10").netName = "V2"
    nComp.cp2.getPinByNumber("11").netName = "AS"
    nComp.cp2.getPinByNumber("12").netName = "CONVb"
    nComp.cp2.getPinByNumber("13").netName = "STOP"
    nComp.cp2.getPinByNumber("14").netName = "SDA"
    nComp.cp2.getPinByNumber("15").netName = "SCL"
    nComp.cp2.getPinByNumber("16").netName = "GND"
    nComp.cp2.refresh()
    misc.chkComponent(self, nComp.cp2, "U?", "test_read_Component2", 15, 12,
                      [str(t) for t in ([1] + list(range(3, 17)))], [
                          "AGND_1", "AGND_3", "AGND_4", "AGND_16", "ALERT", "AS", "CONVST",
                          "REFIN", "SCL", "SDA", "VDD", "VIN1", "VIN2", "VIN3", "VIN4"
                      ], [
                          "+3V3_ADC3", "+2V5_REF", "GND", "V1", "V2", "V3", "V4", "AS",
                          "CONVb", "STOP", "SCL", "SDA"
                      ], None)

    nComp.compare()
    self.assertEqual(len(nComp.lstPinNo_del), 1)
    self.assertEqual(len(nComp.lstPinNo_add), 1)
    self.assertEqual(len(nComp.lstPinNo_otherNet), 2)
    self.assertEqual(len(nComp.lstPinNo_fuzzyNet), 1)
    self.assertEqual(len(nComp.lstPinNo_sameNet), 11)
    self.assertCountEqual(nComp.lstPinNo_del, ["2"])
    self.assertCountEqual(nComp.lstPinNo_add, ["1"])

    nComp.showReport()
    nComp.showInfo()

    fOutPath = os.path.join(self.outFolder, "test_write_NetCompare.csv")
    nComp.writeReport(fOutPath)
    lstHash = [
        "e956fead69b50938c335e5b0889abb18874742dbf3d4bda3bb02b372d5d3cf7b",  # linux
        "76dcf439d5b708ac674436e11e7df0c8e51d716f650a36130aec1fdb2c98804c",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

class Test_BOMCompare(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath1 = os.path.join(self.inFolder, "CIS", "test_read_BOM_v1.bom")
    fPath2 = os.path.join(self.inFolder, "CIS", "test_read_BOM_v2.bom")

    cBOM = CADbase.BOMCompare()
    cBOM.bom1 = CADbase.BOM()
    cBOM.bom2 = CADbase.BOM()

    cBOM.bom1.readCSV(fPath1, CADbase.bomFileCIS)
    misc.chkBOM(self, cBOM.bom1, 17, 2, None, 5, None, 5, None)
    cBOM.bom2.readCSV(fPath2, CADbase.bomFileCIS)
    misc.chkBOM(self, cBOM.bom1, 17, 2, None, 5, None, 5, None)

    cBOM.compare()
    self.assertEqual(len(cBOM.lstVal_del), 1)
    self.assertEqual(len(cBOM.lstVal_add), 1)
    self.assertCountEqual(cBOM.lstVal_del, ["VAL2"])
    self.assertCountEqual(cBOM.lstVal_add, ["VAL6"])
    self.assertEqual(len(cBOM.lstPN_del), 1)
    self.assertEqual(len(cBOM.lstPN_add), 1)
    self.assertCountEqual(cBOM.lstPN_del, ["PN2"])
    self.assertCountEqual(cBOM.lstPN_add, ["PN6"])
    self.assertEqual(len(cBOM.lstRefDes_del), 2)
    self.assertEqual(len(cBOM.lstRefDes_add), 2)
    self.assertEqual(len(cBOM.lstRefDes_chg), 4)
    self.assertCountEqual(cBOM.lstRefDes_del, ["C1", "C2"])
    self.assertCountEqual(cBOM.lstRefDes_add, ["C10", "C11"])
    self.assertCountEqual(cBOM.lstRefDes_chg, ["R12", "R20", "U3", "U5"])

    cBOM.showInfo()
    cBOM.showReport()

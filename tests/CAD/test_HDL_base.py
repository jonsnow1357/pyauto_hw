#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.HDL"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.CAD.HDL

class Test_Library(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    dictLibs = pyauto_hw.CAD.HDL.getLibraries(
        os.path.join(self.inFolder, "HDL", "test_read_lib"))
    self.assertCountEqual(dictLibs.keys(), ["discrete", "ic"])

    lib = dictLibs["discrete"]
    lstCells = lib.getCellNames()
    self.assertCountEqual(lstCells, ["cap", "res"])
    logger.info(lib)

    lib = dictLibs["ic"]
    lstCells = lib.getCellNames()
    self.assertCountEqual(lstCells, ["conn_banks", "ic_8pin", "ic_12pin"])
    logger.info(lib)

class Test_Cell(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    lib = pyauto_hw.CAD.HDL.Library(
        "discrete", os.path.join(self.inFolder, "HDL", "test_read_lib", "discrete"))
    cell = lib.getCell("res")
    cell.read()
    self.assertEqual(cell.nPrimitives, 1)
    self.assertEqual(cell.nSymbols, 1)
    self.assertEqual(cell.nPTFs, 1)
    logger.info(cell)

    pr = cell.getPrimitive()
    self.assertEqual(pr.getProperty("PART_NAME"), "RES")
    self.assertEqual(pr.getProperty("BODY_NAME"), "RES")
    self.assertEqual(pr.getProperty("PHYS_DES_PREFIX"), "R")
    self.assertEqual(pr.getProperty("CLASS"), "DISCRETE")
    logger.info(pr)

    cp = cell.getComponent()
    self.assertEqual(cp.value, "RES")
    self.assertEqual(cp.refdes, "R?")
    self.assertEqual(cp.nPins, 2)
    self.assertEqual(cp.nBanks, 0)
    self.assertEqual(cp.nNets, 0)
    logger.info(cp)

    cell = lib.getCell("cap")
    cell.read()
    self.assertEqual(cell.nPrimitives, 1)
    self.assertEqual(cell.nSymbols, 1)
    self.assertEqual(cell.nPTFs, 0)
    logger.info(cell)

    pr = cell.getPrimitive()
    self.assertEqual(pr.getProperty("PART_NAME"), "CAP")
    self.assertEqual(pr.getProperty("BODY_NAME"), "CAP")
    self.assertEqual(pr.getProperty("PHYS_DES_PREFIX"), "C")
    self.assertEqual(pr.getProperty("CLASS"), "DISCRETE")
    logger.info(pr)

    lib = pyauto_hw.CAD.HDL.Library(
        "ic", os.path.join(self.inFolder, "HDL", "test_read_lib", "ic"))
    cell = lib.getCell("conn_banks")
    cell.read()
    self.assertEqual(cell.nPrimitives, 1)
    self.assertEqual(cell.nSymbols, 3)
    self.assertEqual(cell.nPTFs, 0)
    logger.info(cell)

    pr = cell.getPrimitive()
    self.assertEqual(pr.getProperty("PART_NAME"), "CONN_BANKS")
    self.assertEqual(pr.getProperty("BODY_NAME"), "CONN_BANKS")
    self.assertEqual(pr.getProperty("PHYS_DES_PREFIX"), "J")
    self.assertEqual(pr.getProperty("CLASS"), "IO")
    logger.info(pr)

    cp = cell.getComponent()
    self.assertEqual(cp.value, "CONN_BANKS")
    self.assertEqual(cp.refdes, "J?")
    self.assertEqual(cp.nPins, 12)
    self.assertEqual(cp.nBanks, 3)
    self.assertEqual(cp.nNets, 0)
    logger.info(cp)

    sym = cell.symbols["sym_1"]
    self.assertCountEqual(sym.pinNumbers, ["1", "2", "3", "4"])
    self.assertCountEqual(sym.getAllProperties().keys(), ["CDS_LMAN_SYM_OUTLINE"])

    sym = cell.symbols["sym_2"]
    self.assertCountEqual(sym.pinNumbers, ["P1", "P2", "P3", "P4"])
    self.assertCountEqual(sym.getAllProperties().keys(), ["CDS_LMAN_SYM_OUTLINE"])

    sym = cell.symbols["sym_3"]
    self.assertCountEqual(sym.pinNumbers, ["G1", "G2", "G3", "G4"])
    self.assertCountEqual(sym.getAllProperties().keys(), ["CDS_LMAN_SYM_OUTLINE"])

    cell = lib.getCell("ic_8pin")
    cell.read()
    self.assertEqual(cell.nPrimitives, 1)
    self.assertEqual(cell.nSymbols, 1)
    self.assertEqual(cell.nPTFs, 0)
    logger.info(cell)

    sym = cell.symbols["sym_1"]
    self.assertCountEqual(sym.pinNumbers, ["1", "2", "3", "4", "5", "6", "7", "8"])
    self.assertCountEqual(
        sym.getAllProperties().keys(),
        ["CDS_LMAN_SYM_OUTLINE", "$LOCATION", "C_90DEG", "C_180DEG", "C_270DEG"])

    pr = cell.getPrimitive()
    self.assertEqual(pr.getProperty("PART_NAME"), "IC_8PIN")
    self.assertEqual(pr.getProperty("BODY_NAME"), "IC_8PIN")
    self.assertEqual(pr.getProperty("PHYS_DES_PREFIX"), "U")
    self.assertEqual(pr.getProperty("CLASS"), "IC")
    logger.info(pr)

    cp = cell.getComponent()
    self.assertEqual(cp.value, "IC_8PIN")
    self.assertEqual(cp.refdes, "U?")
    self.assertEqual(cp.nPins, 8)
    self.assertEqual(cp.nBanks, 0)
    self.assertEqual(cp.nNets, 0)
    logger.info(cp)

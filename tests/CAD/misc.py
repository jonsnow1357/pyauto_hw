#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import random

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase

def mkStr(prefix, maxLen, lstLen, bNum=True):
  if (lstLen < 2):
    return pyauto_base.misc.getRndStr_Pat(prefix=prefix, maxLen=maxLen, bNum=bNum)
  else:
    res = set([])
    while (len(res) < lstLen):
      res.add(pyauto_base.misc.getRndStr_Pat(prefix=prefix, maxLen=maxLen, bNum=bNum))
    return list(res)

_lstRefdesPrefix = "RCLDQUJP"

def mkRefdes(n=1, prefix=None):
  """
  :param n: number of refdes
  :param prefix: prefix
  :return:
  - if n <= 1 return a string
  - if n > 1 return a list
  """
  if (prefix is None):
    return mkStr(random.choice(_lstRefdesPrefix), 4, n)
  else:
    return mkStr(prefix, 4, n)

def _mkVal(n=1):
  return mkStr("VAL_", 12, n)

def _mkMFR(n=1):
  return mkStr("MFR_", 12, n)

def _mkMfrPN(n=1):
  return mkStr("MFRPN_", 12, n)

def _mkPinName(n=1):
  return mkStr("PIN_", 12, lstLen=n)

def _mkNetName(n=1):
  return mkStr("NET_", 12, lstLen=n)

def _mkNetlistName(n=1):
  return mkStr("NETLIST_", 12, lstLen=n)

def _mkPN(n=1):
  return mkStr("PN_", 12, n)

def chkPin(tc, pin, strRefdes, strNo, strName, strType, strNetName):
  tc.assertEqual(type(pin), type(CADbase.Pin()))
  tc.assertEqual(pin.type, strType)
  tc.assertEqual(pin.refdes, strRefdes)
  tc.assertEqual(pin.number, strNo)
  tc.assertEqual(pin.name, strName)
  tc.assertEqual(pin.netName, strNetName)

def mkPin(tc, refdes=None, number=None, name=None, netName=None):
  if (refdes is None):
    refdes = mkRefdes()
  if (number is None):
    number = pyauto_base.misc.getRndStr(3, val="digits")
  if (name is None):
    name = _mkPinName()
  p_type = random.choice(CADct.lstPinTypes)
  if (netName is None):
    netName = _mkNetName()

  pin = CADbase.mkPin(strRefdes=refdes,
                      strNo=number,
                      strName=name,
                      strType=p_type,
                      strNetName=netName)

  logger.info(pin)
  chkPin(tc,
         pin,
         strRefdes=refdes,
         strNo=number,
         strName=name,
         strType=p_type,
         strNetName=netName)
  return pin

def chkBaseComponent(tc, cp, strRefdes, strVal):
  tc.assertEqual(type(cp), type(CADbase.BaseComponent()))
  tc.assertEqual(cp.refdes, strRefdes)
  tc.assertEqual(cp.value, strVal)

def mkBaseComponent(tc, refdes=None, value=None):
  if (refdes is None):
    refdes = mkRefdes()
  if (value is None):
    value = _mkVal()

  cp = CADbase.BaseComponent()
  cp.refdes = refdes
  cp.value = value

  logger.info(cp)
  chkBaseComponent(tc, cp, refdes, value)
  return cp

def chkComponent(tc, cp, strRefdes, strVal, nPins, nNets, lstPinNumbers, lstPinNames,
                 lstNetNames, strFootprint):
  tc.assertEqual(type(cp), type(CADbase.Component()))
  tc.assertEqual(cp.refdes, strRefdes)
  tc.assertEqual(cp.value, strVal)
  tc.assertEqual(cp.nPins, nPins)
  tc.assertEqual(cp.nNets, nNets)
  if (lstPinNumbers is not None):
    tc.assertCountEqual(cp.pinNumbers, lstPinNumbers)
  if (lstPinNames is not None):
    tc.assertCountEqual(cp.pinNames, lstPinNames)
  if (lstNetNames is not None):
    tc.assertCountEqual(cp.netNames, lstNetNames)
  if (strFootprint is not None):
    tc.assertEqual(cp.footprint, strFootprint)

def mkComponent(tc, refdes=None, value=None):
  if (refdes is None):
    refdes = mkRefdes()
  if (value is None):
    value = _mkVal()

  cp = CADbase.Component()
  cp.refdes = refdes
  cp.value = value
  if (refdes[0] in ("R", "C", "L")):
    lstPinNumbers = ["1", "2"]
  elif (refdes[0] == "D"):
    lstPinNumbers = ["A", "C"]
  elif (refdes[0] == "Q"):
    lstPinNumbers = ["1", "2", "3"]
  else:
    lstPinNumbers = [str(t) for t in range(random.randint(4, 60))]
  nPins = len(lstPinNumbers)
  lstPinNames = _mkPinName(n=random.randint(2, nPins))
  lstNetNames = _mkNetName(n=random.randint(2, nPins))
  for i in range(0, len(lstPinNumbers)):
    pinno = lstPinNumbers[i]
    if (i < len(lstPinNames)):
      name = lstPinNames[i]
    else:
      name = random.choice(lstPinNames)
    if (i < len(lstNetNames)):
      netName = lstNetNames[i]
    else:
      netName = random.choice(lstNetNames)
    pin = mkPin(tc, refdes=refdes, number=pinno, name=name, netName=netName)
    cp.addPin(pin)
  cp.refresh()

  logger.info(cp)
  chkComponent(tc, cp, refdes, value, nPins, len(lstNetNames), lstPinNumbers, lstPinNames,
               lstNetNames, "")
  return cp

def chkNet(tc, net, strName, nPins, nRefdes, lstPinInfo, lstRefdes):
  tc.assertEqual(type(net), type(CADbase.Net()))
  tc.assertEqual(net.name, strName)
  tc.assertEqual(net.nPins, nPins)
  tc.assertEqual(net.nRefdes, nRefdes)
  if (lstPinInfo is not None):
    tc.assertCountEqual(net.pinInfo, lstPinInfo)
  if (lstRefdes is not None):
    tc.assertCountEqual(net.refdes, lstRefdes)

def mkNet(tc, name=None, lstPinInfo=None):
  if (name is None):
    name = _mkNetName()
  if (lstPinInfo is None):
    nPins = random.randint(2, 10)
    lstPinNumbers = mkStr("", 3, nPins)
    lstRefdes = mkRefdes(n=random.randint(2, nPins))
    lstPinInfo = []
    for i in range(0, len(lstPinNumbers)):
      if (i < len(lstRefdes)):
        refdes = lstRefdes[i]
      else:
        refdes = random.choice(lstRefdes)
      pininfo = "{}.{}".format(refdes, lstPinNumbers[i])
      lstPinInfo.append(pininfo)
  else:
    lstRefdes = []
    for pininfo in lstPinInfo:
      tmp = pininfo.split(".")
      lstRefdes.append(tmp[0])
    lstRefdes = list(set(lstRefdes))  # remove duplicates

  net = CADbase.Net()
  net.name = name
  for pininfo in lstPinInfo:
    net.addPinInfo(pininfo)
  net.refresh()

  logger.info(net)
  chkNet(tc, net, name, len(lstPinInfo), len(lstRefdes), lstPinInfo, lstRefdes)
  return net

def chkNetlist(tc, ntl, strName, nComponents, nNets):
  tc.assertEqual(type(ntl), type(CADbase.Netlist()))
  tc.assertEqual(ntl.name, strName)
  tc.assertEqual(ntl.nComponents, nComponents)
  tc.assertEqual(ntl.nNets, nNets)

def mkNetlist(tc, name=None, nComps=None, nNets=None):
  if (name is None):
    name = _mkNetlistName()

  ntl = CADbase.Netlist()
  ntl.name = name

  if (nComps is None) or (nComps < 2):
    nComps = random.randint(10, 60)
  lstRefdes = set([])
  while (len(lstRefdes) < nComps):
    refdes = mkRefdes()
    lstRefdes.add(refdes)
  lstRefdes = list(lstRefdes)

  lstPinInfo = []
  for refdes in lstRefdes:
    cp = mkComponent(tc, refdes=refdes, value=_mkVal())
    cp.clearNets()
    ntl.addComponent(cp)
    lstPinInfo += ["{}.{}".format(cp.refdes, t) for t in cp.pinNumbers]

  lstNetNames = []
  net = mkNet(tc, name=_mkNetName(), lstPinInfo=[lstPinInfo[0], lstPinInfo[-1]])
  ntl.addNet(net)
  lstNetNames.append(net.name)
  lstPinInfo = lstPinInfo[1:-1]
  if ((nNets is None) or (nNets < 2)):
    while (len(lstPinInfo) > 10):
      nPins = random.randint(2, 10)
      idx1 = random.randint(0, (len(lstPinInfo) - nPins))
      idx2 = idx1 + nPins
      #print("DBG {}-{}, {}".format(idx1, idx2, len(lstPinInfo)))
      while (True):
        net = mkNet(tc, name=_mkNetName(), lstPinInfo=lstPinInfo[idx1:idx2])
        if (net.name not in lstNetNames):  # sometimes random nets have the same name
          break
      lstNetNames.append(net.name)
      ntl.addNet(net)
      lstPinInfo = lstPinInfo[0:idx1] + lstPinInfo[idx2:]
  else:
    nPins = len(lstPinInfo) // (nNets - 1)
    if (nPins < 2):
      msg = "CANNOT generate nets with {:d} pin(s)".format(nPins)
      logger.error(msg)
      raise RuntimeError(msg)
    for _ in range(nNets - 1):
      idx1 = random.randint(0, (len(lstPinInfo) - nPins))
      idx2 = idx1 + nPins
      #print("DBG {}-{}, {}".format(idx1, idx2, len(lstPinInfo)))
      while (True):
        net = mkNet(tc, name=_mkNetName(), lstPinInfo=lstPinInfo[idx1:idx2])
        if (net.name not in lstNetNames):  # sometimes random nets have the same name
          break
      lstNetNames.append(net.name)
      ntl.addNet(net)
      lstPinInfo = lstPinInfo[0:idx1] + lstPinInfo[idx2:]
  ntl.refresh()

  chkNetlist(tc, ntl, name, len(lstRefdes), len(lstNetNames))
  tc.assertCountEqual(ntl.refdes, lstRefdes)
  tc.assertCountEqual(ntl.netNames, lstNetNames)
  logger.info(ntl)
  return ntl

def chkBOMPart(tc, bomp, strVal, strPN, dnp, qty, lstRefdes):
  tc.assertEqual(type(bomp), type(CADbase.BOMPart()))
  tc.assertEqual(bomp.value, strVal)
  tc.assertEqual(bomp.pn, strPN)
  tc.assertEqual(bomp.dnp, dnp)
  tc.assertEqual(bomp.qty, qty)
  if (lstRefdes is not None):
    tc.assertCountEqual(bomp.refdes, lstRefdes)

def mkBOMPart(tc, val=None, pn=None, lstRefdes=None, bDNP=False):
  if (val is None):
    val = _mkVal()
  if (pn is None):
    pn = _mkPN()
  if (lstRefdes is None):
    lstRefdes = mkRefdes(n=random.randint(1, 20))

  part = CADbase.BOMPart()
  part.value = val
  part.pn = pn
  part.refdes = lstRefdes
  if (bDNP):
    part.dnp = True
  else:
    part.dnp = False

  chkBOMPart(tc, part, val, pn, bDNP, len(lstRefdes), lstRefdes)
  logger.info(part)
  return part

def chkBOM(tc, bom, nRefdes, nRefdes_DNP, lstRefdes, nVal, lstVal, nPN, lstPN):
  tc.assertEqual(type(bom), type(CADbase.BOM()))
  tc.assertEqual(bom.nRefdes, nRefdes)
  tc.assertEqual(bom.nRefdes_DNP, nRefdes_DNP)
  if (lstRefdes is not None):
    tc.assertCountEqual(bom.refdes, lstRefdes)
  tc.assertEqual(bom.nValues, nVal)
  if (lstVal is not None):
    tc.assertCountEqual(bom.values, lstVal)
  tc.assertEqual(bom.nPN, nPN)
  if (lstPN is not None):
    tc.assertCountEqual(bom.PNs, lstPN)

def mkBOM(tc, nPNs=None):
  bom = CADbase.BOM()
  if (nPNs is None):
    nPNs = random.randint(10, 40)

  lstPN = _mkPN(n=nPNs)
  lstVal = _mkVal(n=random.randint(2, (nPNs - 5)))
  lstRefdes = []
  lstRefdes_DNP = []
  for i in range(0, len(lstPN)):
    if (i < len(lstVal)):
      val = lstVal[i]
    else:
      val = random.choice(lstVal)
    refdes = []
    prefix = random.choice(_lstRefdesPrefix)
    while (True):
      refdes += mkRefdes(n=random.randint(2, 20), prefix=prefix)
      refdes = list(set(refdes) - set(lstRefdes))  # remove refdes already used
      if (len(refdes) > 2):
        break
    lstRefdes += refdes
    dnp = True if (random.randint(0, 8) == 0) else False
    if (dnp):
      lstRefdes_DNP += refdes
    part = mkBOMPart(tc, val=val, pn=lstPN[i], lstRefdes=refdes, bDNP=dnp)
    bom.addBOMPart(part)
  bom.refresh()

  chkBOM(tc, bom, len(lstRefdes), len(lstRefdes_DNP), lstRefdes, len(lstVal), lstVal,
         len(lstPN), lstPN)
  logger.info(bom)
  return bom

#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.DX"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.tests.tools as tt
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.DX as DX
import pyauto_hw.tests.CAD.misc as misc

class Test_DX(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readCSV(self):
    fPath = os.path.join(self.inFolder, "DX", "test_read_Component.csv")
    cp = DX.readCSV(fPath)
    misc.chkComponent(self, cp, "U?", "", 64, 0, None, None, None, "")

    pn = "B4"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self, pin, "U?", pn, "~VPP/_WP", CADct.pinTypeIN, "")

    pn = "F5"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self, pin, "U?", pn, "DQ12", CADct.pinTypeIO, "")

    pname = "~RP"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self, pin, "U?", "B5", pname, CADct.pinTypeIN, "")

  #@unittest.skip("")
  def test_readNetlist(self):
    fPath = os.path.join(self.inFolder, "DX", "test_read_Netlist.asc")
    ntl = DX.readNetlist(fPath)
    misc.chkNetlist(self, ntl, "", 732, 588)

    refdes = "R326"
    cp = ntl.getAllComponentsByRefdes()[refdes]
    misc.chkComponent(self, cp, refdes, "2001-7321", 2, 2, ["1", "2"], None,
                      ["$9N5025", "$9N6012"], "")
    pin = cp.getPinByNumber("1")
    misc.chkPin(self, pin, refdes, "1", "", CADct.pinTypeAnalog, "$9N5025")

    refdes = "U6"
    cp = ntl.getComponentByRefdes(refdes)
    misc.chkComponent(self, cp, refdes, "2001-9104", 7, 7, None, None, None, "")
    pin = cp.getPinByNumber("5")
    misc.chkPin(self, pin, refdes, "5", "", CADct.pinTypeAnalog, "$18N3348")

    netName = "CFG_LBMUX"
    net = ntl.getAllNetsByName()[netName]
    misc.chkNet(self, net, netName, 3, 3, ["U20.14", "R165.1", "U17.AH22"],
                ["U20", "R165", "U17"])
    pin = net.getPinByInfo("U17.AH22")
    misc.chkPin(self, pin, "U17", "AH22", "", CADct.pinTypeAnalog, netName)

    netName = "$18N3135"
    net = ntl.getNetByName(netName)
    misc.chkNet(self, net, netName, 4, 4, ["R202.1", "C281.1", "X4.1", "U10.27"],
                ["R202", "C281", "X4", "U10"])
    pin = net.getPinByInfo("X4.1")
    misc.chkPin(self, pin, "X4", "1", "", CADct.pinTypeAnalog, netName)

  #@unittest.skip("")
  def test_writeCSV(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_Component1.pins.csv")
    cp = CADbase.Component()
    cp.readCSV(fPath)
    misc.chkComponent(self, cp, "U?", "test_read_Component1", 81, 0, None, None, None, "")

    fOutPath = os.path.join(self.outFolder, "test_write_PinOut.csv")
    DX.writeCSV(fOutPath, cp)
    lstHash = [
        "cf7374c323a36c001f466131fce27246d6c62cd2f0e6079f95c84c40102a46fb",  # linux
        "a40ea4c06a2524f4df7516498b32abd86ce4d4ecb129bbd7591579f5a11c20ee",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

    fOutPath = os.path.join(self.outFolder, "test_write_PinOut_Short.csv")
    DX.writeCSV_Short(fOutPath, cp)
    lstHash = [
        "9ff5bfca79535afb0a1f4d3122a3aab118e8269c053bfbce50d76e848f49ff0e",  # linux
        "fbe89e83a0f886c8d0fe90d81d3efd4da8e9cf09e73b17bfb2f9faf76c476142",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

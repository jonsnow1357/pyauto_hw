#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.base"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import random

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.tests.tools as tt
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.tests.CAD.misc as misc

_nLoop = 100

class Test_RandomGen(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    lstRefdes = misc.mkRefdes(n=100)
    for refdes in lstRefdes:
      self.assertIsNotNone(re.match(r"[RCLDQUJP][1-9][0-9]*", refdes),
                           msg="INCORRECT refdes: {}".format(refdes))

class Test_Pin(unittest.TestCase):

  #@unittest.skip("")
  def test_base(self):
    pin = CADbase.Pin()

    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    self.assertEqual(pin.refdes, "U?")
    self.assertEqual(pin.number, "1")
    self.assertEqual(pin.name, "")
    self.assertEqual(pin.nameAlt, {})
    self.assertEqual(pin.params, {
        "bank": "",
        "side": "",
        "order": None,
        "signaling": "",
        "voltage": ""
    })
    self.assertEqual(pin.paramsCAD, {"net": ""})

    for _ in range(_nLoop):
      pin = misc.mkPin(self)

    pin.setInfo("U1.A1")
    pin.name = "PIN_NAME"
    pin.nameAlt = {"other1": "PIN_NAME_ALT1", "other2": "PIN_NAME_ALT2"}
    pin.netName = "PIN_NET"
    tmp = pyauto_base.misc.getRndStr(4)
    pin.bank = tmp
    self.assertEqual(pin.bank, tmp)
    tmp = random.choice(CADct.lstPinSides)
    pin.side = tmp
    self.assertEqual(pin.side, tmp)
    tmp = pyauto_base.misc.getRndStr(4)
    pin.signaling = tmp
    self.assertEqual(pin.signaling, tmp)
    tmp = pyauto_base.misc.getRndStr(4)
    pin.voltage = tmp
    self.assertEqual(pin.voltage, tmp)
    logger.info(pin)

    for pt in CADct.lstPinTypes:
      pin.type = pt
      self.assertEqual(pin.type, pt)
    pin.type = CADct.pinTypeAnalog

    for pt in CADct.PinTypeConv.dictFromCIS.keys():
      pin.setType_CIS(pt)
      self.assertIn(pin.type, CADct.lstPinTypes)
    pin.type = CADct.pinTypeAnalog

    for pt in CADct.PinTypeConv.dictFromAltera.keys():
      pin.setType_Altera(pt)
      self.assertIn(pin.type, CADct.lstPinTypes)
    pin.type = CADct.pinTypeAnalog

    pin = CADbase.Pin("U2.6")
    for pt in CADct.PinTypeConv.dictFromCIS.keys():
      pin.setType_CIS(pt)
      self.assertEqual(pin.type, CADct.PinTypeConv.dictFromCIS[pt])
    for pt in CADct.PinTypeConv.dictFromAltera.keys():
      pin.setType_Altera(pt)
      self.assertEqual(pin.type, CADct.PinTypeConv.dictFromAltera[pt])
    logger.info(pin)

  #@unittest.skip("")
  def test_getInfo(self):
    pin = CADbase.Pin("U3.A1")
    pin.netName = "a\\b\\c\\d\\"
    self.assertEqual(pin.getNetName_CIS(), "abcdb")
    pin.netName = "testing35"
    self.assertEqual(pin.getNetName_CIS(), "testing[35]")
    logger.info(pin)

    pin = CADbase.Pin("U4.6")
    pin.name = "name1"
    pin.netName = "name2"
    self.assertEqual(pin.getInfo_AlteraTcl(0), "set_location_assignment PIN_6 -to name1")
    self.assertEqual(pin.getInfo_AlteraTcl(1), "set_location_assignment PIN_6 -to name2")
    self.assertEqual(pin.getInfo_AlteraTcl(2), "set_location_assignment PIN_6 -to name[2]")
    logger.info(pin)

    pin = CADbase.Pin("U5.A1")
    self.assertEqual(pin.getInfo_AlteraPinFile(), None)
    pin.name = "name"
    self.assertEqual(pin.getInfo_AlteraPinFile(), None)
    pin.setType_Altera("input")
    self.assertEqual(pin.getInfo_AlteraPinFile(), None)
    pin.signaling = "LVDS"
    self.assertEqual(pin.getInfo_AlteraPinFile(), None)
    pin.voltage = "2.5"
    self.assertEqual(pin.getInfo_AlteraPinFile(), None)
    pin.bank = "1"
    self.assertEqual(pin.getInfo_AlteraPinFile(), "name : A1 : input : LVDS : 2.5 : 1")
    logger.info(pin)

    pin = CADbase.Pin("U6.6")
    pin.name = "IO"
    pin.netName = "name2"
    pin.bank = "bank3a"
    self.assertEqual(pin.getName_AlteraPin(), "IO_BANK3A_6")
    pin.nameAlt["opt"] = "namealt5"
    self.assertEqual(pin.getName_AlteraPin(), "IO_BANK3A_6/NAMEALT5")
    pin.nameAlt["cfg"] = "namecfg5"
    self.assertEqual(pin.getName_AlteraPin(), "IO_BANK3A_6/NAMEALT5/NAMECFG5")
    pin.nameAlt["alteraTXRXCh"] = "DIFFIO_TX_B35p"
    pin.nameAlt["alteraLVDS"] = "DIFFOUT_B35p"
    self.assertEqual(pin.getName_AlteraPin(), "IO_BANK3A_6/NAMEALT5/NAMECFG5/TX_B35P")
    pin.nameAlt["alteraDQS8"] = "DQ4B"
    pin.nameAlt["alteraDQS16"] = "DQ2B"
    pin.nameAlt["alteraDQS32"] = "DQ3B"
    self.assertEqual(pin.getName_AlteraPin(),
                     "IO_BANK3A_6/NAMEALT5/NAMECFG5/TX_B35P/DQ2B/DQ3B/DQ4B")

    pin.nameAlt["opt"] = "RUP2A"
    self.assertEqual(pin.getName_AlteraPin(),
                     "RUP2A/IO_BANK3A_6/NAMECFG5/TX_B35P/DQ2B/DQ3B/DQ4B")

    pin.name = "VCC"
    self.assertEqual(pin.getName_AlteraPin(), "VCC_6")
    pin.name = "GND"
    self.assertEqual(pin.getName_AlteraPin(), "GND_6")
    pin.name = "VREF"
    self.assertEqual(pin.getName_AlteraPin(), "VREF_6")

    pin.name = "NC"
    self.assertEqual(pin.getName_AlteraPin(), "NC_6")
    logger.info(pin)

    pin = CADbase.Pin("U7.A1")
    pin.name = "some/,random,name"
    self.assertEqual(pin.getName_AlteraPin(), "SOME//RANDOM/NAME")
    logger.info(pin)

  #@unittest.skip("")
  def test_guessType(self):
    pin = CADbase.Pin("U1.A1")
    pin.name = "GND"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeGnd)

    pin = CADbase.Pin("U1.A1")
    pin.name = "SGND0"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeGnd)

    pin = CADbase.Pin("U1.A1")
    pin.name = "VSS"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeGnd)

    pin = CADbase.Pin("U1.A1")
    pin.name = "TVSS4"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeGnd)

    pin = CADbase.Pin("U1.A1")
    pin.name = "VCC"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypePwr)

    pin = CADbase.Pin("U1.A1")
    pin.name = "AVCC1"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypePwr)

    pin = CADbase.Pin("U1.A1")
    pin.name = "VDD"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypePwr)

    pin = CADbase.Pin("U1.A1")
    pin.name = "RVDD3"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypePwr)

    pin = CADbase.Pin("U1.A1")
    pin.name = "VCC"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypePwr)

    pin = CADbase.Pin("U1.A1")
    pin.name = "RST"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeIN)

    pin = CADbase.Pin("U1.A1")
    pin.name = "CORE_RST"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeIN)

    pin = CADbase.Pin("U1.A1")
    pin.name = "CLK"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeIN)

    pin = CADbase.Pin("U1.A1")
    pin.name = "REFCLK"
    self.assertEqual(pin.type, CADct.pinTypeAnalog)
    pin.guessType()
    self.assertEqual(pin.type, CADct.pinTypeIN)

class Test_BaseComponent(unittest.TestCase):

  #@unittest.skip("")
  def test_base(self):
    cp = CADbase.BaseComponent()

    self.assertEqual(cp.refdes, "U?")
    self.assertEqual(cp.value, "")

    for _ in range(_nLoop):
      misc.mkBaseComponent(self)

class Test_Component(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def _Component_change(self, cp):
    lstPinNumbers = cp.pinNumbers
    nPins = cp.nPins

    logger.info("delete pin")
    pinNo = random.choice(lstPinNumbers)
    lstPinNumbers.remove(pinNo)
    cp.delPinInfo("{}.{}".format(cp.refdes, pinNo))
    cp.refresh()
    self.assertEqual(cp.nPins, (nPins - 1))
    self.assertCountEqual(cp.pinNumbers, lstPinNumbers)
    # some pins can have the same name, so we don't test pin names
    # some pins can have the same net, so we don't test net names

    lstNetNames = cp.netNames
    nNets = cp.nNets

    logger.info("delete net")
    netName = random.choice(lstNetNames)
    lstNetNames.remove(netName)
    cp.delNet(netName)
    cp.refresh()
    self.assertEqual(cp.nPins, (nPins - 1))  # should not change nPins
    self.assertEqual(cp.nNets, (nNets - 1))
    self.assertCountEqual(cp.netNames, lstNetNames)

  #@unittest.skip("")
  def test_base(self):
    cp = CADbase.Component()

    self.assertEqual(cp.refdes, "U?")
    self.assertEqual(cp.value, "")
    self.assertEqual(cp.paramsCAD, {
        "footprint": "",
        "symbol": "",
        "mfr_pn": "",
        "local_pn": ""
    })
    self.assertEqual(cp.paramsDB, {})
    self.assertEqual(cp.nPins, 0)
    self.assertEqual(cp.nNets, 0)
    self.assertEqual(cp.pinNumbers, [])
    self.assertEqual(cp.pinNames, [])
    self.assertEqual(cp.netNames, [])

    for _ in range(_nLoop):
      cp = misc.mkComponent(self)
      self._Component_change(cp)

    cp = CADbase.Component()
    cp.value = "COMP_VALUE"
    pin = CADbase.Pin("U?.1")
    pin.bank = "1"
    cp.addPin(pin)
    pin = CADbase.Pin("U?.2")
    pin.bank = "2"
    cp.addPin(pin)
    cp.refresh()
    self.assertEqual(cp.nBanks, 2)
    self.assertCountEqual(cp.banks, ["1", "2"])
    logger.info(cp)

  #@unittest.skip("")
  def test_CSV_base(self):
    cp1 = misc.mkComponent(self)
    print(cp1)
    fPath = os.path.join(self.outFolder, "test_component.pins.csv")
    cp1.writeCSV(fPath)

    cp2 = CADbase.Component()
    cp2.readCSV(fPath)

    self.assertEqual(cp1.nPins, cp2.nPins)
    for pn, pin1 in cp1.getAllPinsByNumber().items():
      pin2 = cp2.getPinByNumber(pn)
      self.assertEqual(pin1.type, pin2.type)
      self.assertEqual(pin1.number, pin2.number)
      self.assertEqual(pin1.name, pin2.name)

  #@unittest.skip("")
  def test_CSV_complex(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_Component1.pins.csv")
    cp = CADbase.Component()
    cp.readCSV(fPath)
    misc.chkComponent(self, cp, "U?", "test_read_Component1", 81, 0, None, None, None, "")
    self.assertEqual(cp.nBanks, 3)
    self.assertCountEqual(cp.banks, ["1", "2", "3"])

    pn = "B5"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self,
                pin,
                strRefdes="U?",
                strNo=pn,
                strName="PF5",
                strType=CADct.pinTypeIO,
                strNetName="")
    self.assertEqual(pin.bank, "2")
    self.assertEqual(pin.side, "left")
    self.assertEqual(pin.getFullName(),
                     "PF5/TIM0_CDTI2_2/TIM0_CDTI2_5/USB_VBUSEN_0/PRS_CH2_1")

    pn = "F9"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self,
                pin,
                strRefdes="U?",
                strNo=pn,
                strName="PB6",
                strType=CADct.pinTypeIO,
                strNetName="")
    self.assertEqual(pin.bank, "1")
    self.assertEqual(pin.side, "left")
    self.assertEqual(pin.getFullName(), "PB6/US2_CS_1")

    pname = "VSS_A4"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self,
                pin,
                strRefdes="U?",
                strNo="A4",
                strName=pname,
                strType=CADct.pinTypeGnd,
                strNetName="")
    self.assertEqual(pin.bank, "3")
    self.assertEqual(pin.side, "bot")
    self.assertEqual(pin.getFullName(), "VSS_A4")

    fOutPath = os.path.join(self.outFolder, "test_write_Component.pins.csv")
    cp.writeCSV(fOutPath)
    lstHash = [
        "d73b18e60888cc2057c9f4712b9e77728e2cc6b99063e17ac89eb0601f1823f2",  # linux
        "fcab93c197be440f8d62e790811122917a98575804476daaa4e2bf804a7c9472",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

  #@unittest.skip("")
  def test_JSON_base(self):
    cp1 = misc.mkComponent(self)
    print(cp1)
    fPath = os.path.join(self.outFolder, "test_component.pins.json")
    cp1.writeJSON(fPath)

    cp2 = CADbase.Component()
    cp2.readJSON(fPath)

    self.assertEqual(cp1.refdes, cp2.refdes)
    self.assertEqual(cp1.value, cp2.value)
    self.assertEqual(cp1.nPins, cp2.nPins)
    for pn, pin1 in cp1.getAllPinsByNumber().items():
      pin2 = cp2.getPinByNumber(pn)
      self.assertEqual(pin1.type, pin2.type)
      self.assertEqual(pin1.number, pin2.number)
      self.assertEqual(pin1.name, pin2.name)

class Test_Net(unittest.TestCase):

  def _Net_change(self, net):
    lstPinInfo = net.pinInfo
    nPins = net.nPins

    logger.info("delete pin")
    pinInfo = random.choice(lstPinInfo)
    lstPinInfo.remove(pinInfo)
    net.delPinInfo(pinInfo)
    net.refresh()
    self.assertEqual(net.nPins, (nPins - 1))
    self.assertCountEqual(net.pinInfo, lstPinInfo)
    # some net can have multiple pins on same refdes, so we don't test refdes

  #@unittest.skip("")
  def test_base(self):
    net = CADbase.Net()

    self.assertEqual(net.id, "")
    self.assertEqual(net.name, "")
    self.assertEqual(net.paramsCAD, {})
    self.assertEqual(net.nPins, 0)
    self.assertEqual(net.nRefdes, 0)
    self.assertEqual(net.pinInfo, [])
    self.assertEqual(net.refdes, [])

    for _ in range(_nLoop):
      net = misc.mkNet(self)
      self._Net_change(net)

class Test_Netlist(unittest.TestCase):

  def _Netlist_change(self, ntl):
    lstNetNames = ntl.netNames
    nNets = ntl.nNets

    logger.info("delete net")
    netName = random.choice(lstNetNames)
    lstNetNames.remove(netName)
    ntl.delNet(netName)
    ntl.refresh()
    self.assertEqual(ntl.nNets, (nNets - 1))
    self.assertCountEqual(ntl.netNames, lstNetNames)

    lstNetNames = ntl.netNames
    nNets = ntl.nNets

    logger.info("rename net")
    netName = random.choice(lstNetNames)
    ntl.renameNet(netName, "NET_RENAMED")
    lstNetNames.remove(netName)
    lstNetNames.append("NET_RENAMED")
    ntl.refresh()
    self.assertEqual(ntl.nNets, nNets)
    self.assertCountEqual(ntl.netNames, lstNetNames)

    lstNetNames = ntl.netNames
    nNets = ntl.nNets

    if (len(lstNetNames) > 1):
      logger.info("merge 2 nets")
      netName1 = random.choice(lstNetNames)
      netName2 = netName1
      while (netName2 == netName1):
        netName2 = random.choice(lstNetNames)
      finalName = min([netName1, netName2], key=len)
      if (netName1 == finalName):
        otherName = netName2
      else:
        otherName = netName1
      lstNetNames.remove(otherName)
      lstPinInfo = ntl.getNetByName(netName1).pinInfo + ntl.getNetByName(netName2).pinInfo
      ntl.mergeNet([netName1, netName2])
      ntl.refresh()
      self.assertEqual(ntl.nNets, (nNets - 1))
      self.assertCountEqual(ntl.netNames, lstNetNames)
      self.assertCountEqual(ntl.getNetByName(finalName).pinInfo, lstPinInfo)

    lstRefdes = ntl.refdes
    nComponents = ntl.nComponents

    logger.info("delete component")
    cRefdes = random.choice(lstRefdes)
    lstRefdes.remove(cRefdes)
    ntl.delComponent(cRefdes)
    ntl.refresh()
    self.assertEqual(ntl.nComponents, (nComponents - 1))
    self.assertCountEqual(ntl.refdes, lstRefdes)

  #@unittest.skip("")
  def test_base(self):
    ntl = CADbase.Netlist()

    self.assertEqual(ntl.name, "")
    self.assertEqual(ntl.nComponents, 0)
    self.assertEqual(ntl.nNets, 0)
    self.assertEqual(ntl.refdes, [])
    self.assertEqual(ntl.netNames, [])

    for _ in range(_nLoop):
      ntl = misc.mkNetlist(self)
      self._Netlist_change(ntl)

  #@unittest.skip("")
  def test_merge2nets(self):
    ntl = CADbase.Netlist()

    cp = CADbase.Component("R1")
    pin = CADbase.Pin("R1.1")
    cp.addPin(pin)
    pin = CADbase.Pin("R1.2")
    cp.addPin(pin)
    ntl.addComponent(cp)

    cp = CADbase.Component("R2")
    pin = CADbase.Pin("R2.1")
    cp.addPin(pin)
    pin = CADbase.Pin("R2.2")
    cp.addPin(pin)
    ntl.addComponent(cp)

    net = CADbase.Net("NET_1")
    net.addPinInfoList(["R1.1", "R2.1"])
    ntl.addNet(net)

    net = CADbase.Net("NET_2")
    net.addPinInfoList(["R1.2", "R2.2"])
    ntl.addNet(net)

    ntl.refresh()
    logger.info(ntl)

    ntl.mergeNet(ntl.netNames)

    ntl.refresh()
    logger.info(ntl)
    logger.info(ntl.getNetByName(ntl.netNames[0]))

    for _ in range(_nLoop):
      ntl.name = misc._mkNetlistName()
      ntl = misc.mkNetlist(self, nComps=2, nNets=2)

      ntl.mergeNet(ntl.netNames)

      ntl.refresh()
      logger.info(ntl)
      logger.info(ntl.getNetByName(ntl.netNames[0]))

'''
class Test_other(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if(not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  @unittest.skip("")
  def test_KiCad(self):
    self.assertEqual(type(CADbase.KiCad.getLibRules().pinTextOffset), type([]))
    self.assertEqual(type(CADbase.KiCad.getLibRules().pinLength), type([]))
    self.assertEqual(type(CADbase.KiCad.getLibRules().lineThickness), type([]))
    CADbase.KiCad.getLibRules().showInfo()
    CADbase.KiCad.getModRules().showInfo()

    dcmFile = CADbase.KiCad.libSymbol()

    self.assertEqual(type(dcmFile.libFields), type([]))
    self.assertEqual(type(dcmFile.libFPList), type([]))
    self.assertEqual(type(dcmFile.libAlias), type([]))
    self.assertEqual(type(dcmFile.libDraw), type([]))
    self.assertEqual(type(dcmFile.doc), type([]))

    modFile = CADbase.KiCad.modFootprint()

    self.assertEqual(type(modFile.libFields), type([]))
    self.assertEqual(type(modFile.libText), type([]))
    self.assertEqual(type(modFile.libD), type([]))
    self.assertEqual(type(modFile.libShape3D), type([]))
    self.assertEqual(type(modFile.doc), type([]))
    self.assertEqual(type(modFile.libPads), type({}))

    modFile_Pad = CADbase.KiCad.modFootprint_Pad()

    self.assertEqual(type(modFile_Pad.libFields), type([]))
'''

#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD._HDL_sch"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.HDL as HDL
import pyauto_hw.tests.CAD.misc as misc

class Test_HDL(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readNetlist_BodyOrdered(self):
    fPath = os.path.join(self.inFolder, "HDL", "test_read_Netlist_dialbonl.dat")
    ntl = HDL.readNetlist_BodyOrdered(fPath)
    misc.chkNetlist(self, ntl, "", 56, 35)

    refdes = "R150"
    cp = ntl.getAllComponentsByRefdes()[refdes]
    misc.chkComponent(self, cp, refdes, "RESISTOR_0201-64K9", 2, 2, ["1", "2"], None,
                      ["+1.1V", "UNNAMED_7_ADJVOLTAGESTEPDOWNTPS82085_I14_FB"], "")
    pin = cp.getPinByNumber("1")
    misc.chkPin(self, pin, refdes, "1", "", CADct.pinTypeAnalog, "+1.1V")

    refdes = "U23"
    cp = ntl.getComponentByRefdes(refdes)
    misc.chkComponent(self, cp, refdes, "TPS82085_SON08", 8, 4, None, None, None, "")
    pin = cp.getPinByNumber("5")
    misc.chkPin(self, pin, refdes, "5", "", CADct.pinTypeAnalog, "DGND")

    netName = "EPCS_SPI_CS"
    net = ntl.getAllNetsByName()[netName]
    misc.chkNet(self, net, netName, 2, 2, ["R156.1", "U22.W19"], ["R156", "U22"])
    pin = net.getPinByInfo("U22.W19")
    misc.chkPin(self, pin, "U22", "W19", "", CADct.pinTypeAnalog, netName)

    netName = "VCCA_GXB"
    net = ntl.getNetByName(netName)
    misc.chkNet(self, net, netName, 5, 4,
                ["C123.1", "C124.1", "FL9.1", "U22.E19", "U22.M20"],
                ["C123", "C124", "FL9", "U22"])
    pin = net.getPinByInfo("FL9.1")
    misc.chkPin(self, pin, "FL9", "1", "", CADct.pinTypeAnalog, netName)

  #@unittest.skip("")
  def test_readNetlist_Concise(self):
    fPath = os.path.join(self.inFolder, "HDL", "test_read_Netlist_dialcnet.dat")
    ntl = HDL.readNetlist_Concise(fPath)
    misc.chkNetlist(self, ntl, "", 103, 74)

    refdes = "R24"
    cp = ntl.getAllComponentsByRefdes()[refdes]
    misc.chkComponent(self, cp, refdes, "0R_1P_0201-RES_0201", 2, 2, ["1", "2"], None,
                      ["+3V3", "+3V3_VCCT"], "")
    pin = cp.getPinByNumber("1")
    misc.chkPin(self, pin, refdes, "1", "", CADct.pinTypeAnalog, "+3V3")

    refdes = "U3"
    cp = ntl.getComponentByRefdes(refdes)
    misc.chkComponent(self, cp, refdes, "EFM32XG360-CSP81-EFM32XG360-CSA", 39, 31, None,
                      None, None, "")
    pin = cp.getPinByNumber("H3")
    misc.chkPin(self, pin, refdes, "H3", "", CADct.pinTypeAnalog, "TX_PWR")

    netName = "SCL"
    net = ntl.getAllNetsByName()[netName]
    misc.chkNet(self, net, netName, 2, 2, ["J2.5", "U3.C8"], ["J2", "U3"])
    pin = net.getPinByInfo("J2.5")
    misc.chkPin(self, pin, "J2", "5", "", CADct.pinTypeAnalog, netName)

    netName = "OSA_OUT+"
    net = ntl.getNetByName(netName)
    misc.chkNet(self, net, netName, 7, 7,
                ["C25.1", "L5.2", "L6.1", "L7.1", "R17.1", "R18.1", "TP1.1"],
                ["C25", "L5", "L6", "L7", "R17", "R18", "TP1"])
    pin = net.getPinByInfo("L5.2")
    misc.chkPin(self, pin, "L5", "2", "", CADct.pinTypeAnalog, netName)

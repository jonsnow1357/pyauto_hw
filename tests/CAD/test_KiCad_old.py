#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD._KiCAD_sch"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import copy

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.CAD._KiCad_old as KiCad

class Test_KiCad_old(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readSymbols_libFile(self):
    fPath = os.path.join(self.inFolder, "KiCad_old", "test_read_generic.lib")
    symbols = KiCad.readSymbols_libFile(fPath)
    self.assertEqual(len(symbols), 60)

    sym = symbols["BATTERY"]
    self.assertEqual(sym.getName(), "BATTERY")
    self.assertEqual(sym.getRef(), "BT")
    self.assertFalse(sym.hasPowerFlag())

    sym = symbols["RES-V"]
    self.assertEqual(sym.getName(), "RES-V")
    self.assertEqual(sym.getRef(), "R")
    self.assertFalse(sym.hasPowerFlag())
    self.assertEqual(len(sym.libFields), 5)
    self.assertEqual(sym.libFields[0], "F0 \"R\" 100 50 40 H V C CNN")
    self.assertEqual(sym.libFields[1], "F1 \"RES-V\" 100 -50 40 H V C CNN")
    self.assertEqual(len(sym.libDraw), 3)
    self.assertEqual(sym.libDraw[0], "S -30 100 30 -100 0 1 10 N")
    self.assertEqual(sym.libDraw[1], "X ~ 1 0 150 50 D 40 40 1 1 P")
    self.assertEqual(sym.libDraw[2], "X ~ 2 0 -150 50 U 40 40 1 1 P")
    self.assertEqual(len(sym.libFPList), 0)
    self.assertEqual(len(sym.doc), 0)

  #@unittest.skip("")
  def test_writeSymbols_libFile(self):
    fPath1 = os.path.join(self.outFolder, "test_write_generic.lib")
    with open(fPath1, "w"):
      pass
    fPath2 = os.path.join(self.outFolder, "test_write_generic.dcm")
    with open(fPath2, "w"):
      pass

    fPath = os.path.join(self.inFolder, "KiCad_old", "test_read_generic.lib")
    symsSrc = KiCad.readSymbols_libFile(fPath)
    symsDst = KiCad.readSymbols_libFile(fPath1)
    self.assertEqual(len(symsSrc), 60)
    self.assertEqual(len(symsDst), 0)

    symsDst = copy.deepcopy(symsSrc)
    symsDst["RES-H"].setName("RES-H_COPY")
    KiCad.writeSymbols_libFile(fPath1, symsDst)

    self.assertTrue(os.path.isfile(fPath1), "file DOES NOT exist: {}".format(fPath1))
    self.assertTrue(os.path.isfile(fPath2), "file DOES NOT exist: {}".format(fPath2))
    #cannot check hash, file has a timestamp included

  #@unittest.skip("")
  def test_readFootprints_modFile(self):
    fPath = os.path.join(self.inFolder, "KiCad_old", "test_read_led.mod")
    modules = KiCad.readFootprints_modFile(fPath)
    self.assertEqual(len(modules), 6)

    mod = modules["LED-0603"]
    self.assertEqual(mod.getName(), "LED-0603")
    self.assertEqual(len(mod.libFields), 8)
    self.assertEqual(mod.libFields[0], "Po 0 0 0 15 49BFA1B8 00000000 ~~")
    self.assertEqual(len(mod.libText), 2)
    self.assertEqual(mod.libText[0], "T0 0 -400 300 300 0 35 N V 21 N\"LED-0603\"")
    self.assertEqual(len(mod.libD), 22)
    self.assertEqual(mod.libD[0], "DS 177 -177 177 177 26 21")
    self.assertEqual(len(mod.libPads["1"].libFields), 5)
    self.assertEqual(mod.libPads["1"].libFields[0], "Sh \"1\" R 314 314 0 0 0")

    mod = modules["LED-3MM"]
    self.assertEqual(mod.getName(), "LED-3MM")
    self.assertEqual(len(mod.libShape3D), 4)
    self.assertEqual(mod.libShape3D[1], "Sc 1.000000 1.000000 1.000000")
    #logger.info(mod.doc)
    self.assertEqual(len(mod.doc), 3)
    self.assertEqual(mod.doc[1], "Cd LED 3mm - Lead pitch 100mil (2,54mm)")

  #@unittest.skip("")
  def test_writeFootprints_modFile(self):
    fPath1 = os.path.join(self.outFolder, "test_write_led.mod")
    with open(fPath1, "w"):
      pass
    fPath2 = os.path.join(self.outFolder, "test_write_led.mdc")
    with open(fPath2, "w"):
      pass

    fPath = os.path.join(self.inFolder, "KiCad_old", "test_read_led.mod")
    modsSrc = KiCad.readFootprints_modFile(fPath)
    modsDst = KiCad.readFootprints_modFile(fPath1)
    self.assertEqual(len(modsSrc), 6)
    self.assertEqual(len(modsDst), 0)

    modsDst = copy.deepcopy(modsSrc)
    modsDst["LED-0805"].setName("LED-0805_COPY")
    modsDst["LED-0805_COPY"] = modsDst["LED-0805"]
    del (modsDst["LED-0805"])
    KiCad.writeFootprints_modFile(fPath1, modsDst)

    self.assertTrue(os.path.isfile(fPath1), "file DOES NOT exist: {}".format(fPath1))
    self.assertTrue(os.path.isfile(fPath2), "file DOES NOT exist: {}".format(fPath2))
    #cannot check hash, file has a timestamp included

#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD._base_BOM"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import random
import copy

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.tests.CAD.misc as misc

_nLoop = 100

class Test_BOMPart(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    part = CADbase.BOMPart()

    self.assertEqual(part.value, "")
    self.assertEqual(part.pn, "")
    self.assertEqual(part.refdes, [])
    self.assertEqual(part.getMfrInfo(), [])
    self.assertEqual(part.paramsDB, {})
    self.assertFalse(part.dnp)
    self.assertEqual(part.mfrCnt, 0)

    val = misc._mkVal()
    part.value = val
    self.assertEqual(part.value, val)
    self.assertEqual(part.id, val)
    part.value = None
    pn = misc._mkPN()
    part.pn = pn
    self.assertEqual(part.pn, pn)
    self.assertEqual(part.id, pn)
    part.value = val
    self.assertEqual(part.id, "{}/{}".format(val, pn))

    nMfr = random.randint(2, 20)
    for i in range(nMfr):
      mfr = misc._mkMFR()
      mfrpn = misc._mkMfrPN()
      part.addMfrInfo(mfr=mfr, mfrPN=mfrpn)
      self.assertEqual(part.mfrCnt, (i + 1))
      self.assertEqual(part.getMfrInfo()[-1][0], mfr)
      self.assertEqual(part.getMfrInfo()[-1][1], mfrpn)
    part.clrMfrInfo()
    self.assertEqual(part.mfrCnt, 0)

    nMfr = random.randint(2, 20)
    for i in range(nMfr):
      mfr = misc._mkMFR()
      mfrpn = misc._mkMfrPN()
      part.addMfrInfo(mfr=mfr, mfrPN=mfrpn, distr=mfr, distrPN=mfrpn)
      self.assertEqual(part.mfrCnt, (i + 1))
      self.assertEqual(part.getMfrInfo()[-1][0], mfr)
      self.assertEqual(part.getMfrInfo()[-1][1], mfrpn)
      self.assertEqual(part.getMfrInfo()[-1][2], mfr)
      self.assertEqual(part.getMfrInfo()[-1][3], mfrpn)
    part.clrMfrInfo()
    self.assertEqual(part.mfrCnt, 0)

    for _ in range(_nLoop):
      misc.mkBOMPart(self)

class Test_BOM(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    bom = CADbase.BOM()

    self.assertEqual(bom.nRefdes, 0)
    self.assertEqual(bom.nRefdes_DNP, 0)
    self.assertEqual(bom.refdes, [])
    self.assertEqual(bom.nPN, 0)
    self.assertEqual(bom.PNs, [])
    self.assertEqual(bom.nValues, 0)
    self.assertEqual(bom.values, [])

    for _ in range(_nLoop):
      misc.mkBOM(self)

  #@unittest.skip("")
  def test_readCSV(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_BOM1.csv")

    bomFile = CADbase.BOMFileCfg()
    bomFile.setReqNames("Value", "Reference")
    bomFile.setOptNames(None, None, "MFP")

    bom = CADbase.BOM()
    bom.readCSV(fPath, bomFile)

    lstRefdes = [
        "C1", "C10", "C11", "C14", "C15", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9",
        "CON1", "D1", "D2", "D3", "D4", "D5", "D6", "D7", "F1", "FB1", "ICSP1", "ICSP2",
        "J1", "JP2", "P1", "P2", "P3", "P4", "Q1", "R1", "R10", "R11", "R12", "R13", "R14",
        "R15", "R16", "R17", "R2", "R3", "R4", "R5", "R6", "R7", "R8", "R9", "SW1", "U1",
        "U2", "U3", "U4", "U5", "VR1", "VR2", "X1", "X2"
    ]
    lstValues = [
        "0.1 uF", "0R", "10K", "1K", "1M", "1N4148W-7-F", "1uF", "2.2uF", "20pF", "22R",
        "47uF", "500mA", "510R", "ATMEGA16U2-MU", "ATMEGA328P", "Analog Header",
        "BARREL_JACK", "BLM21PG221SN1D", "CG0603MLC-05E", "CSTCE16MOV53-R0", "DIODE",
        "Digital Header 01", "Digital Header 02", "FDN340P", "FOXSLF/160-20", "LD1117S50TR",
        "LED", "LMV358IDGKR", "LP2985-33DBVR", "M20-9980346", "NO_POP", "Power Header",
        "RESET_SWITCH", "USB_TYPE_B"
    ]
    lstPNs = [
        "1N4148W-7-F", "292304-1", "6.94106E+11", "ATMEGA16U2-MU", "ATMEGA328P-PU",
        "BLM21PG221SN1D", "C0805C104K1RACAUTO", "C0805C105K8RACAUTO", "C0805C200G5GACTU",
        "C2012X7R1C225K125AB", "CG0603MLC-05E", "CRCW080510K0JNEA", "CRCW08051K00JNEA",
        "CRCW08051M00FKEA", "CSTCE16M0V53-R0", "ERJ-6GEY0R00V", "EVQ-Q2U02W", "FDN340P",
        "FOOTPRINT ONLY", "FOXSLF/160-20", "LD1117S50TR", "LG R971-KN-1", "LMV358IDGKR",
        "LP2985-33DBVR", "M20-9980346", "MAL215371479E3", "MF-MSMF050-2", "MRA4007T3G",
        "NPPC061KFXC-RC", "NPPC081KFXC-RC", "NPPC101KFXC-RC", "RC0805FR-07510RL",
        "RC0805JR-0722RL"
    ]
    misc.chkBOM(self, bom, 59, 0, lstRefdes, 34, lstValues, 33, lstPNs)

  #@unittest.skip("")
  def test_writeCSV_byRefdes(self):
    bom1 = misc.mkBOM(self)
    fPath = os.path.join(self.outFolder, "test_write_refdes.BOM.csv")
    bom1.writeCSV_byRefdes(fPath)

    nRefdes = bom1.nRefdes
    nRefdes_DNP = bom1.nRefdes_DNP
    lstRefdes = bom1.refdes
    nVal = bom1.nValues
    lstValues = bom1.values
    nPN = bom1.nPN
    lstPNs = bom1.PNs

    bom2 = CADbase.BOM()
    bom2.readCSV(fPath)
    misc.chkBOM(self, bom2, nRefdes, nRefdes_DNP, lstRefdes, nVal, lstValues, nPN, lstPNs)

  #@unittest.skip("")
  def test_writeCSV_byVal(self):
    bom1 = misc.mkBOM(self)
    fPath = os.path.join(self.outFolder, "test_write_val.BOM.csv")
    bom1.writeCSV_byVal(fPath)

    nRefdes = bom1.nRefdes
    nRefdes_DNP = bom1.nRefdes_DNP
    lstRefdes = bom1.refdes
    nVal = bom1.nValues
    lstValues = bom1.values
    nPN = bom1.nPN
    lstPNs = bom1.PNs

    bom2 = CADbase.BOM()
    bom2.readCSV(fPath)
    misc.chkBOM(self, bom2, nRefdes, nRefdes_DNP, lstRefdes, nVal, lstValues, nPN, lstPNs)

  #@unittest.skip("")
  def test_writeCSV_byPN(self):
    bom1 = misc.mkBOM(self)
    fPath = os.path.join(self.outFolder, "test_write_pn.BOM.csv")
    bom1.writeCSV_byPN(fPath)

    nRefdes = bom1.nRefdes
    nRefdes_DNP = bom1.nRefdes_DNP
    lstRefdes = bom1.refdes
    nVal = bom1.nValues
    lstValues = bom1.values
    nPN = bom1.nPN
    lstPNs = bom1.PNs

    bom2 = CADbase.BOM()
    bom2.readCSV(fPath)
    misc.chkBOM(self, bom2, nRefdes, nRefdes_DNP, lstRefdes, nVal, lstValues, nPN, lstPNs)

class Test_BOMFileCfg(unittest.TestCase):

  #@unittest.skip("")
  def test_base(self):
    bomFile = CADbase.BOMFileCfg()

    self.assertEqual(bomFile.delimiter, ",")
    self.assertEqual(bomFile.nRows_skip, 0)

    self.assertEqual(bomFile.value, "value")
    self.assertEqual(bomFile.refdes, "refdes")
    self.assertEqual(bomFile.qty, "qty")
    self.assertEqual(bomFile.DNP, "DNP")
    self.assertEqual(bomFile.MPN, "MFR_PN")

    bomFile = CADbase.BOMFileCfg()
    str1 = pyauto_base.misc.getRndStr(16)
    str2 = pyauto_base.misc.getRndStr(16)
    bomFile.setReqNames(str1, str2)
    self.assertEqual(bomFile.value, str1)
    self.assertEqual(bomFile.refdes, str2)
    self.assertEqual(bomFile.qty, "qty")
    self.assertEqual(bomFile.DNP, "DNP")
    self.assertEqual(bomFile.MPN, "MFR_PN")

    bomFile = CADbase.BOMFileCfg()
    str1 = pyauto_base.misc.getRndStr(16)
    str2 = pyauto_base.misc.getRndStr(16)
    str3 = pyauto_base.misc.getRndStr(16)
    bomFile.setOptNames(str1, str2, str3)
    self.assertEqual(bomFile.value, "value")
    self.assertEqual(bomFile.refdes, "refdes")
    self.assertEqual(bomFile.qty, str1)
    self.assertEqual(bomFile.DNP, str2)
    self.assertEqual(bomFile.MPN, str3)

class Test_CAD(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_CIS(self):
    fPath = os.path.join(self.inFolder, "CIS", "test_read_BOM.BOM")

    bom = CADbase.BOM()
    bomFile = copy.deepcopy(CADbase.bomFileCIS)
    bomFile.DNP = None
    bom.readCSV(fPath, bomFile)

    misc.chkBOM(self, bom, 5405, 0, None, 213, None, 219, None)

    val = "0.1uF"
    lstParts = bom.getAllPartsByValue()[val]
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "N220-061", False, 1771, None)

    val = "8.2pF"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "N220-054", False, 2, ["C2533", "C2549"])

    pn = "N220-072"
    lstParts = bom.getAllPartsByPN()[pn]
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "270pF", pn, False, 2, ["C2571", "C2587"])

    pn = "N221-035"
    lstParts = bom.getPartsByPN(pn)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "2200pF", pn, False, 7,
                    ["C330", "C331", "C334", "C335", "C338", "C339", "C342"])

    refdes = "C2549"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "8.2pF", "N220-054", False, 2, [refdes, "C2533"])

    refdes = "U26"
    part = bom.getPartByRefdes(refdes)
    misc.chkBOMPart(self, part, "MT48LC4M32B2B5-7 IT:G", "N284-047", False, 4,
                    [refdes, "U24", "U25", "U27"])

  #@unittest.skip("")
  def test_CIS_PartReport(self):
    fPath = os.path.join(self.inFolder, "CIS", "test_read_PartReport.PRP")

    bom = CADbase.BOM()
    bom.readCSV(fPath, CADbase.bomFilePRP)

    misc.chkBOM(self, bom, 5526, 522, None, 209, None, 215, None)

    val = "0.1uF"
    lstParts = bom.getAllPartsByValue()[val]
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "N220-061", False, 1717, None)

    val = "DNP"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "DNP", True, 522, None)

    pn = "N220-072"
    lstParts = bom.getAllPartsByPN()[pn]
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "270pF", pn, False, 2, ["C2571", "C2587"])

    pn = "N220-055"
    lstParts = bom.getPartsByPN(pn)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "10pF", pn, False, 14, [
        "C30", "C31", "C32", "C33", "C19", "C20", "C21", "C22", "C23", "C24", "C25", "C26",
        "C27", "C28"
    ])

    refdes = "F4"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "DLW31SN601SQ2", "N253-002", False, 4,
                    [refdes, "F10", "F12", "F13"])

    refdes = "U14"
    part = bom.getPartByRefdes(refdes)
    misc.chkBOMPart(self, part, "NC7WZ241", "N264-024", False, 3, [refdes, "U20", "U61"])

  #@unittest.skip("")
  def test_HDL(self):
    fPath = os.path.join(self.inFolder, "HDL", "test_read_BOM.csv")

    bom = CADbase.BOM()
    bom.readCSV(fPath, CADbase.bomFileHDL)

    misc.chkBOM(self, bom, 180, 2, None, 37, None, 41, None)

    val = "10uF"
    lstParts = bom.getAllPartsByValue()[val]
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "CAPACITOR_0402-10UF,10V,20%", False, 6,
                    ["C31", "C119", "C123", "C125", "C126", "C129"])

    val = "220R@100MHZ"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "EMI_FILTER_0603-220R,2.5A", False, 1, ["FL11"])

    pn = "CONN_PCB_SFP"
    lstParts = bom.getAllPartsByPN()[pn]
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "CONN_PCB_SFP", pn, True, 1, ["J10"])

    pn = "EMI_FILTER_0402-600R,500MA"
    lstParts = bom.getPartsByPN(pn)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "600R@100MHz", pn, False, 3, ["FL9", "FL10", "FL12"])

    refdes = "U26"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "MAX3736HETE+", "MAX3736HETE+", False, 1, [refdes])

    refdes = "Y4"
    part = bom.getPartByRefdes(refdes)
    misc.chkBOMPart(self, part, "?", "TCXO_31.25MHZ_4.6PPM", False, 1, [refdes])

  #@unittest.skip("")
  def test_DX(self):
    fPath = os.path.join(self.inFolder, "DX", "test_read_BOM1.csv")

    bom = CADbase.BOM()
    bom.readCSV(fPath, CADbase.bomFileDX)

    misc.chkBOM(self, bom, 733, 70, None, 71, None, 75, None)

    val = "1K21"
    lstParts = bom.getAllPartsByValue()[val]
    self.assertEqual(len(lstParts), 2)
    for part in lstParts:
      if (part.dnp):
        misc.chkBOMPart(self, part, val, "2001-7420", True, 2, ["R323", "R335"])
      else:
        misc.chkBOMPart(self, part, val, "2001-7420", False, 13, [
            "R101", "R241", "R242", "R264", "R300", "R319", "R320", "R334", "R343", "R348",
            "R351", "R356", "R8"
        ])

    val = "470pF"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "2001-7501", False, 2, ["C178", "C185"])

    pn = "2001-7455"
    lstParts = bom.getAllPartsByPN()[pn]
    self.assertEqual(len(lstParts), 2)
    for part in lstParts:
      if (part.dnp):
        misc.chkBOMPart(self, part, "10K", pn, True, 2, ["R105", "R316"])
      else:
        misc.chkBOMPart(
            self, part, "10K", pn, False, 10,
            ["R159", "R181", "R204", "R206", "R222", "R357", "R36", "R4", "R42", "R43"])

    pn = "2001-7733"
    lstParts = bom.getPartsByPN(pn)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "BLUE", pn, False, 1, ["D8"])

    refdes = "J1"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "QTE-040-01-F-D-A", "2001-8843", False, 2, [refdes, "J2"])

    refdes = "U12"
    part = bom.getPartByRefdes(refdes)
    misc.chkBOMPart(self, part, "MT29F8G08ADADAH4", "2001-9141", True, 1, [refdes])

    fPath = os.path.join(self.inFolder, "DX", "test_read_BOM2.csv")

    bomFile = copy.deepcopy(CADbase.bomFileDX)
    bomFile.MPN = "MPN"
    bom.readCSV(fPath, bomFile)

    misc.chkBOM(self, bom, 474, 22, None, 135, None, 147, None)

    val = "6.8pF"
    lstParts = bom.getAllPartsByValue()[val]
    self.assertEqual(len(lstParts), 2)
    for part in lstParts:
      if (part.dnp):
        misc.chkBOMPart(self, part, val, "C1608C0G2A6R8D080AA", True, 2, ["C26", "C137"])
      else:
        misc.chkBOMPart(self, part, val, "C1608C0G2A6R8D080AA", False, 1, ["C138"])

    val = "95Ohm@1000 Mhz"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "2743021447", False, 6,
                    ["FB1", "FB2", "FB4", "FB6", "FB7", "FB8"])

    pn = "CL31B105KCHNNNE"
    lstParts = bom.getAllPartsByPN()[pn]
    self.assertEqual(len(lstParts), 2)
    for part in lstParts:
      if (part.dnp):
        misc.chkBOMPart(self, part, "1uF", pn, True, 1, ["C67"])
      else:
        misc.chkBOMPart(self, part, "1uF", pn, False, 5,
                        ["C65", "C66", "C68", "C69", "C245"])

    pn = "LM7321MF"
    lstParts = bom.getPartsByPN(pn)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], "LM7321MF", pn, False, 2, ["U9", "U10"])

    refdes = "D5"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "SMV1213-004LF", "SMV1213-004LF", False, 1, [refdes])

    refdes = "D6"
    part = bom.getPartByRefdes(refdes)
    misc.chkBOMPart(self, part, "PESD3V3S4UD", "PESD3V3S4UD", False, 2, [refdes, "D7"])

  #@unittest.skip("")
  def test_Epicor_BOM(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_BOM.epicor.csv")

    bom = CADbase.BOM()
    bom.readCSV(fPath, CADbase.bomFileEpicor)

    misc.chkBOM(self, bom, 1315, 0, None, 132, None, 0, None)

    val = "2001-8129"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "", False, 4, ["FB15", "FB29", "FB44", "FB54"])

    val = "2001-8437"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "", False, 3, ["U11", "U18", "U20"])

    refdes = "U47"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "2001-8320", "", False, 2, [refdes, "U50"])

    refdes = "D12"
    part = bom.getPartByRefdes(refdes)
    misc.chkBOMPart(self, part, "2001-7702", "", False, 8,
                    [refdes, "D8", "D10", "D14", "D16", "D22", "D35", "D36"])

  #@unittest.skip("")
  def test_Epicor_MethodTracker(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_BOM.epicor_MT.csv")

    bomFile = CADbase.BOMFileCfg()
    bomFile.setReqNames("Part", "Mtl")
    bomFile.setOptNames("Qty/Parent", None)
    bomFile.qty_check = False

    bom = CADbase.BOM()
    bom.readCSV(fPath, bomFile)

    misc.chkBOM(self, bom, 1760, 0, None, 157, None, 0, None)

    val = "2001-7494"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "", False, 16, None)

    val = "2001-8111"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "", False, 7, None)

  #@unittest.skip("")
  def test_Agile_BOM(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_BOM.agile.csv")

    bom = CADbase.BOM()
    bom.readCSV(fPath, CADbase.bomFileAgile)

    misc.chkBOM(self, bom, 72, 0, None, 13, None, 0, None)

    val = "612-0389-001"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "", False, 2, ["C11", "C12"])

    val = "610-0632-001"
    lstParts = bom.getPartsByValue(val)
    self.assertEqual(len(lstParts), 1)
    misc.chkBOMPart(self, lstParts[0], val, "", False, 3, ["R10", "R12", "R14"])

    refdes = "U1"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "631-0487-001", "", False, 1, [refdes])

    refdes = "MTG8"
    part = bom.getAllPartsByRefdes()[refdes]
    misc.chkBOMPart(self, part, "500-1516-001", "", False, 5,
                    ["MTG5", "MTG6", "MTG7", refdes, "MTG9"])

  #@unittest.skip("")
  # def test_KiCad(self):
  #   fPath = os.path.join(self.inFolder, "KiCad", "test_read_BOM1.csv")
  #
  #   bom = CADbase.BOM()
  #   bom.readCSV(fPath, CADbase.bomFileKiCad)
  #
  #   misc.chkBOM(self, bom, 44, 0, None, 23, None, 0, None)
  #
  #   val = "0.1uF"
  #   lstParts = bom.getAllPartsByValue()[val]
  #   self.assertEqual(len(lstParts), 1)
  #   misc.chkBOMPart(self, lstParts[0], val, "", False, 3, ["C1", "C2", "C3"])
  #
  #   val = "HDR_ARDUINO_DIG1"
  #   lstParts = bom.getPartsByValue(val)
  #   self.assertEqual(len(lstParts), 1)
  #   misc.chkBOMPart(self, lstParts[0], val, "", False, 1, ["J1"])
  #
  #   refdes = "R3"
  #   part = bom.getAllPartsByRefdes()[refdes]
  #   misc.chkBOMPart(self, part, "2.2k", "", False, 2, [refdes, "R1"])
  #
  #   refdes = "U2"
  #   part = bom.getPartByRefdes(refdes)
  #   misc.chkBOMPart(self, part, "TXS0104ED", "", False, 1, [refdes])
  #
  #   # TODO: handle DNS
  #   '''
  #   fPath = os.path.join(self.inFolder, "KiCad", "test_read_BOM2.csv")
  #
  #   bom.readCSV(fPath, CADbase.bomFileKiCad)
  #
  #   misc.chkBOM(self, bom, 22, 6, None, 8, None, 0, None)
  #
  #   val = "10090099-S094VLF"
  #   lstParts = bom.getAllPartsByValue()[val]
  #   self.assertEqual(len(lstParts), 1)
  #   misc.chkBOMPart(self, lstParts[0], val, "", False, 1, ["J2"])
  #
  #   val = "0R"
  #   lstParts = bom.getPartsByValue(val)
  #   self.assertEqual(len(lstParts), 2)
  #   for part in lstParts:
  #     if(part.dnp):
  #       misc.chkBOMPart(self, part, val, "", True, 5,
  #                       ["R1", "R2", "R4", "R6", "R7"])
  #     else:
  #       misc.chkBOMPart(self, part, val, "", False, 6,
  #                       ["R3", "R5", "R8", "R9", "R10", "R11"])
  #
  #   refdes = "J5"
  #   part = bom.getAllPartsByRefdes()[refdes]
  #   misc.chkBOMPart(self, part, "10118194-0001LF", "", False, 1, [refdes])
  #
  #   refdes = "C2"
  #   part = bom.getPartByRefdes(refdes)
  #   misc.chkBOMPart(self, part, "100nF", "", False, 5,
  #                   [refdes, "C1", "C3", "C4", "C5"])
  #   '''

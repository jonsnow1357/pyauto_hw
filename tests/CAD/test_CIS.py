#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.CIS"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.tests.tools as tt
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.CIS as CIS
import pyauto_hw.tests.CAD.misc as misc

class Test_CIS(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_ComponentCSV(self):
    fPath = os.path.join(self.inFolder, "CIS", "test_read_Component.csv")
    cp = CIS.readCSV(fPath)
    misc.chkComponent(self, cp, "U?", "", 144, 127, None, None, None, "")

    pn = "27"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self, pin, "U?", pn, "PA8", CADct.pinTypeAnalog, "PA8")

    pn = "119"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self, pin, "U?", pn, "PB4", CADct.pinTypeAnalog, "ECRSDV")

    pname = "PA0"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self, pin, "U?", "23", pname, CADct.pinTypeAnalog, "PA0")

    fOutPath = os.path.join(self.outFolder, "test_write_Component.csv")
    CIS.writeCSV(fOutPath, cp)
    lstHash = [
        "028bdcd912b89144fd82c6deec6d06b2d4fb4bdd7add5ecff07e82a1b2dcdf75",  # linux
        "72e3e7edf130a20d809f0d7fbdf2c8f3074ca70103d4a85b1202b2c6c1b8587b",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

  #@unittest.skip("")
  def test_readNetlist(self):
    fPath = os.path.join(self.inFolder, "CIS", "test_read_Netlist.NET")
    ntl = CIS.readNetlist(fPath)
    misc.chkNetlist(self, ntl, "", 5620, 4609)

    refdes = "R1314"
    cp = ntl.getAllComponentsByRefdes()[refdes]
    misc.chkComponent(self, cp, refdes, "NTSD1XH103FPB", 2, 2, ["1", "2"], None,
                      ["VREF_ADC", "AMBIENT_TEMP"], "RESAR250W45L3000T300H200B")
    pin = cp.getPinByNumber("1")
    misc.chkPin(self, pin, refdes, "1", "", CADct.pinTypeAnalog, "VREF_ADC")

    refdes = "U71"
    cp = ntl.getComponentByRefdes(refdes)
    misc.chkComponent(self, cp, refdes, "MAX1617A", 11, 10, None, None, None,
                      "qsop16_150w68h")
    pin = cp.getPinByNumber("6")
    misc.chkPin(self, pin, refdes, "6", "", CADct.pinTypeAnalog, "N17836988")

    netName = "AMBIENT_TEMP"
    net = ntl.getAllNetsByName()[netName]
    misc.chkNet(self, net, netName, 4, 4, ["C3083.1", "R1307.2", "R1314.2", "U181.15"],
                ["C3083", "R1307", "R1314", "U181"])
    pin = net.getPinByInfo("U181.15")
    misc.chkPin(self, pin, "U181", "15", "ADIN", CADct.pinTypeIN, netName)

    netName = "P1_CLDDS_SPISEL"
    net = ntl.getNetByName(netName)
    misc.chkNet(self, net, netName, 2, 2, ["U112.15", "U35.88"], ["U112", "U35"])
    pin = net.getPinByInfo("U35.88")
    misc.chkPin(self, pin, "U35", "88", "VCMPST2/CN69/RF", CADct.pinTypeIO, netName)

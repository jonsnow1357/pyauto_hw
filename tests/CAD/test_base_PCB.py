#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD._base_PCB"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.tests.tools as tt
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase

class Test_PCBLayer(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerCu, strName="TOP", thickness=0.7)

    self.assertEqual(ly.name, "TOP")
    self.assertEqual(ly.thickness.magnitude, 0.7)
    self.assertEqual(ly.thickness.units, "millimeter")
    logger.info(ly)

    ly = CADbase.PCBLayer(strType=CADct.pcbLayerDiel, thickness=3.6, unit="mils")
    self.assertEqual(ly.name, "")
    self.assertEqual(ly.thickness.magnitude, 3.6)
    self.assertIn(ly.thickness.units, ("thou", "mil"))
    self.assertEqual(ly.material, None)
    ly.material = CADct.pcbMaterial_FR4
    self.assertEqual(ly.material, CADct.pcbMaterial_FR4)
    logger.info(ly)

class Test_PCBVia(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    via = CADbase.PCBVia(drillSize=0.25)

    self.assertEqual(via.drillSize.magnitude, 0.25)
    self.assertEqual(via.drillSize.units, "millimeter")
    self.assertEqual(via.startLy, "TOP")
    self.assertEqual(via.stopLy, "BOT")
    self.assertEqual(via.fill, False)
    self.assertEqual(via.laser, False)
    logger.info(via)

    via = CADbase.PCBVia(drillSize=10, unit="mils")
    self.assertEqual(via.drillSize.magnitude, 10.0)
    self.assertIn(via.drillSize.units, ("thou", "mil"))
    logger.info(via)

class Test_PCBStackup(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    stk = CADbase.PCBStackup()

    self.assertEqual(stk.validate(), False)
    self.assertEqual(stk.nLayers, 0)
    self.assertEqual(stk.nVias, 0)
    self.assertEqual(stk.unit.units, "millimeter")
    self.assertEqual(stk.thickness_total.magnitude, 0.0)
    self.assertEqual(stk.thickness_total.units, "millimeter")
    stk.showInfo()

    stk = CADbase.PCBStackup(nCu=2, unit="thou")
    self.assertEqual(stk.validate(), True)
    self.assertEqual(stk.nLayers, 2)
    self.assertEqual(stk.nVias, 0)
    self.assertEqual(stk.unit.units, "thou")
    self.assertAlmostEqual(float(stk.thickness_total.magnitude), 6.29921259842, delta=1e-10)
    self.assertEqual(stk.thickness_total.units, "thou")
    stk.showInfo()

    stk = CADbase.PCBStackup()
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerSMask, thickness=0.01)
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerCu, thickness=0.025, strName="TOP")
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerDiel, thickness=1.0)
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerCu, thickness=0.025, strName="BOT")
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerSMask, thickness=0.01)
    stk.addLayer(ly)
    via = CADbase.PCBVia(drillSize=12)
    stk.addVia(via)
    self.assertEqual(stk.validate(), True)
    self.assertEqual(stk.nLayers, 2)
    self.assertEqual(stk.nVias, 1)
    self.assertEqual(stk.unit.units, "millimeter")
    self.assertAlmostEqual(stk.thickness_total.magnitude, 1.07)
    self.assertEqual(stk.thickness_total.units, "millimeter")
    self.assertAlmostEqual(stk.thickness_noSMask.magnitude, 1.05)
    self.assertEqual(stk.thickness_noSMask.units, "millimeter")
    stk.showInfo()

    fOutPath = os.path.join(self.outFolder, "test_write_stackup_mm.svg")
    stk.writeSVG(fOutPath)
    lstHash = [
        "69742ae397c80bbe8dd9ca020733c8ec9171af6ace59712f71ed33c380df5853",  # linux py2
        "8e86bd37a37c896b7c479ef7510d905acd6d254b1f16d7f727978adfdba0865c",  # linux py3
        "7a2f58d13f0b5dbcd70e741cece5ac601c6a52998c0008211aa2611c74513afa",  # win py2
        "8e30f5d408ec45be113a26dd2e18eb1794b4b33daf5a692d70a382d3c1effa90",  # win py3
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

    stk = CADbase.PCBStackup(unit="mils")
    self.assertEqual(stk.nLayers, 0)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerSMask,
                          thickness=0.5,
                          unit=str(stk.unit.units))
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerCu,
                          thickness=0.7,
                          strName="TOP",
                          unit=str(stk.unit.units))
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerDiel,
                          thickness=39.0,
                          unit=str(stk.unit.units))
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerCu,
                          thickness=0.7,
                          strName="BOT",
                          unit=str(stk.unit.units))
    stk.addLayer(ly)
    ly = CADbase.PCBLayer(strType=CADct.pcbLayerSMask,
                          thickness=0.5,
                          unit=str(stk.unit.units))
    stk.addLayer(ly)
    via = CADbase.PCBVia(drillSize=10, unit=str(stk.unit.units))
    stk.addVia(via)
    self.assertEqual(stk.validate(), True)
    self.assertEqual(stk.nLayers, 2)
    self.assertEqual(stk.nVias, 1)
    self.assertIn(stk.unit.units, ("thou", "mil"))
    self.assertAlmostEqual(stk.thickness_total.magnitude, 41.4)
    self.assertIn(stk.thickness_total.units, ("thou", "mil"))
    self.assertAlmostEqual(stk.thickness_noSMask.magnitude, 40.4)
    self.assertIn(stk.thickness_noSMask.units, ("thou", "mil"))
    stk.showInfo()

    fOutPath = os.path.join(self.outFolder, "test_write_stackup_mils.svg")
    stk.writeSVG(fOutPath)
    lstHash = [
        "128c894a01c7d172d68d863d69c8fca9be17cfb6d1c6ba4f3237d2df86f7250f",  # linux py2
        "09a540e5d5e105866ddcb0c9e197e0406631c9aad9804ab111bdf231193e2e3c",  # linux py3
        "a800d000985abf9fd692448c95e0cb3f7bd4033b2b8282f62f8342fbdd08fd4d",  # win py2
        "bea92716fafe2881a5f1483d7393507da0ba804f22fb38bea356c50443e39136",  # win py3
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

class Test_PCBArtworkLayer(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    ly = CADbase.PCBArtworkLayer(strType=CADct.pcbArtCu, strName="TOP")

    self.assertEqual(ly.type, CADct.pcbArtCu)
    self.assertEqual(ly.name, "TOP")
    self.assertEqual(ly.opts, [])
    self.assertEqual(ly.content, [])
    logger.info(ly)

class Test_PCBArtwork(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    art = CADbase.PCBArtwork()

    self.assertEqual(art.nLayers, 0)
    art.showInfo()

    ly = CADbase.PCBArtworkLayer(strType="Cu", strName="TOP")
    ly.content = "TOP"
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="Cu", strName="L01")
    ly.content = "L01"
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="Cu", strName="L02")
    ly.content = "L02"
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="Cu", strName="BOT")
    ly.content = "BOTTOM"
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="SMT", strName="SMT", opts="positive")
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="SMB", strName="SMB", opts="negative")
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="SPT", strName="SPT")
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="SPB", strName="SPB")
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="SKT", strName="SKT", opts="auto_brd")
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="SKB", strName="SKB", opts="auto")
    art.addLayer(ly)
    ly = CADbase.PCBArtworkLayer(strType="DOC", strName="DRL", opts="line_10")
    ly.content = [
        "MANUFACTURING/NCDRILL_LEGEND", "MANUFACTURING/NCDRILL_FIGURE",
        "MANUFACTURING/NCLEGEND-1-4"
    ]
    art.addLayer(ly)
    self.assertEqual(art.nLayers, 11)
    art.showInfo()

    fOutPath = os.path.join(self.outFolder, "test_write_allegro_script.txt")
    art.writeAllegroScript(fOutPath)
    lstHash = [
        "e9f9e571b447718c0906b5635606a32438852971daa79e11a822b5162fc3d18f",  # linux
        "aafe9bfc59c0e3d45178d36956a339c8cc27e71b5f0e055b5d66df1adf28ab72",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

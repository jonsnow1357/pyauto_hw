#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.Allegro"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.Allegro as Allegro
import pyauto_hw.tests.CAD.misc as misc

class Test_Allegro(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readNetlist(self):
    fPath = os.path.join(self.inFolder, "Allegro", "test_read_Netlist", "packaged")
    ntl = Allegro.readNetlist(fPath)
    misc.chkNetlist(self, ntl, "", 428, 221)

    refdes = "R17"
    cp = ntl.getAllComponentsByRefdes()[refdes]
    misc.chkComponent(self, cp, refdes, "910R0", 2, 2, ["1", "2"], None,
                      ["N16932379", "N16488021"], "SM0402")
    pin = cp.getPinByNumber("1")
    misc.chkPin(self, pin, refdes, "1", "", CADct.pinTypeAnalog, "N16932379")

    refdes = "U10"
    cp = ntl.getComponentByRefdes(refdes)
    misc.chkComponent(self, cp, refdes, "Si53306-B-GM", 17, 11, None, None, None,
                      "QFN50P300X300X100-17L24T175X175")
    pin = cp.getPinByNumber("4")
    misc.chkPin(self, pin, refdes, "4", "", CADct.pinTypeAnalog, "GND")

    netName = "RF_PLL_LOCK_DET"
    net = ntl.getAllNetsByName()[netName]
    misc.chkNet(self, net, netName, 2, 2, ["J6.15", "U7.30"], ["J6", "U7"])
    pin = net.getPinByInfo("J6.15")
    misc.chkPin(self, pin, "J6", "15", "", CADct.pinTypeAnalog, netName)

    netName = "N16165558"
    net = ntl.getNetByName(netName)
    misc.chkNet(self, net, netName, 5, 5, ["C151.2", "R83.2", "R86.2", "R93.2", "U23.2"],
                ["C151", "R83", "R86", "R93", "U23"])
    pin = net.getPinByInfo("U23.2")
    misc.chkPin(self, pin, "U23", "2", "", CADct.pinTypeAnalog, netName)

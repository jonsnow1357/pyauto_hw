#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.Altera"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_base.tests.tools as tt
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.Altera as Altera
import pyauto_hw.tests.CAD.misc as misc

class Test_Quartus(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readPin_Quartus(self):
    fPath = os.path.join(self.inFolder, "Altera", "test_read_1.pin")
    cp = Altera.readPin(fPath)
    misc.chkComponent(self, cp, "U?", "EP2S30F672C4", 672, 420, None, None, None, "")

    pn = "AC19"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self, pin, "U?", pn, "MCU2_DATA[6]", CADct.pinTypeIO, "MCU2_DATA6")
    self.assertEqual(pin.getFullName(), "MCU2_DATA[6]")

    pn = "B23"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self, pin, "U?", pn, "DF2_CFG_DCLK", CADct.pinTypeOUT, "DCLK_D2")
    self.assertEqual(pin.getFullName(), "DF2_CFG_DCLK")

    pname = "DBG_CS"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self, pin, "U?", "N22", pname, CADct.pinTypeIN, "DBG_CS")
    self.assertEqual(pin.getFullName(), "DBG_CS")

    fPath = os.path.join(self.inFolder, "Altera", "test_read_2.pin")
    cp = Altera.readPin(fPath)
    misc.chkComponent(self, cp, "U?", "EP4CGX30CF23I7", 484, 0, None, None, None, "")

    pn = "A20"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self, pin, "U?", pn, "F1_REV_BOARD[2]", CADct.pinTypeIN, "")
    self.assertEqual(pin.getFullName(), "F1_REV_BOARD[2]")

    pn = "K1"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self, pin, "U?", pn, "PHY1_SGMII_RX(n)", CADct.pinTypeOUT, "")
    self.assertEqual(pin.getFullName(), "PHY1_SGMII_RX(n)")

    pname = "F1_FPGA_ALIVE_LED"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self, pin, "U?", "D22", pname, CADct.pinTypeOUT, "")
    self.assertEqual(pin.getFullName(), "F1_FPGA_ALIVE_LED")

  #@unittest.skip("")
  def test_readPinOut(self):
    fPath = os.path.join(self.inFolder, "Altera", "test_read_PinOut1.txt")
    cp = Altera.readPinOut(fPath, "F256")
    misc.chkComponent(self, cp, "U?", "EP2C5-F256", 256, 0, None, None, None, "")

    pn = "E1"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self, pin, "U?", pn, "IO", CADct.pinTypeIO, "")
    self.assertEqual(pin.bank, "B1")
    self.assertEqual(pin.getFullName(), "IO/DPCLK0/DQS0L/DPCLK0/DQS0L/LVDS5p")
    self.assertEqual(pin.getName_AlteraPin(), "IO_B1_E1/LVDS5P/DPCLK0/DQS0L")

    pn = "L6"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self, pin, "U?", pn, "VCCD_PLL1", CADct.pinTypePwr, "")
    self.assertEqual(pin.bank, "B1")
    self.assertEqual(pin.getFullName(), "VCCD_PLL1")
    self.assertEqual(pin.getName_AlteraPin(), "VCCD_PLL1_L6")

    pname = "nCONFIG"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self, pin, "U?", "J5", pname, CADct.pinTypeIN, "")
    self.assertEqual(pin.bank, "B1")
    self.assertEqual(pin.getFullName(), "nCONFIG/nCONFIG")
    self.assertEqual(pin.getName_AlteraPin(), "NCONFIG")

    fPath = os.path.join(self.inFolder, "Altera", "test_read_PinOut2.txt")
    cp = Altera.readPinOut(fPath, "F1152")
    misc.chkComponent(self, cp, "U?", "5AGXFB1-F1152", 1152, 0, None, None, None, "")

    pn = "AJ31"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self, pin, "U?", pn, "GXB_TX_L0n", CADct.pinTypeOUT, "")
    self.assertEqual(pin.bank, "GXB_L0")
    self.assertEqual(pin.getFullName(), "GXB_TX_L0n")
    self.assertEqual(pin.getName_AlteraPin(), "GXB_TX_L0N")

    pn = "AL7"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self, pin, "U?", pn, "IO", CADct.pinTypeIO, "")
    self.assertEqual(pin.bank, "4A")
    self.assertEqual(pin.getFullName(), "IO/DQ8B/DQ16B/DIFFOUT_B155n/DIFFIO_RX_B155n/DATA5")
    self.assertEqual(pin.getName_AlteraPin(), "IO_4A_AL7/DATA5/RX_B155N/DQ16B/DQ8B")

    pname = "nCONFIG"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self, pin, "U?", "C33", pname, CADct.pinTypeIN, "")
    self.assertEqual(pin.bank, "8A")
    self.assertEqual(pin.getFullName(), "nCONFIG/nCONFIG")
    self.assertEqual(pin.getName_AlteraPin(), "NCONFIG")

  #@unittest.skip("")
  def test_writeQsf(self):
    fPath = os.path.join(self.inFolder, "base", "test_read_Component1.pins.csv")
    cp = CADbase.Component()
    cp.readCSV(fPath)
    misc.chkComponent(self, cp, "U?", "test_read_Component1", 81, 0, None, None, None, "")

    fOutPath = os.path.join(self.outFolder, "test_write.qsf")
    Altera.writeQsf(fOutPath, cp)
    lstHash = [
        "c400bbf1ded9c49accd2c33761540778035b47f9123f34be58afebc6622e9a1d",  # linux
        "535585cada7c455b813e83c2cadbc6bdf34183b7100f6e96412f3375189fac26",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

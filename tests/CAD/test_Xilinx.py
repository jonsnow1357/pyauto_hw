#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.CAD.Xilinx"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.Xilinx as Xilinx
import pyauto_hw.tests.CAD.misc as misc

class Test_Vivado(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_readPin(self):
    fPath = os.path.join(self.inFolder, "Xilinx", "test_read_1.rpt")
    cp = Xilinx.readPin(fPath)
    misc.chkComponent(self, cp, "U?", "xc7z100", 900, 315, None, None, None, "")

    pn = "A27"
    pin = cp.getAllPinsByNumber()[pn]
    misc.chkPin(self, pin, "U?", pn, "PS_DDR_DQ9_502", CADct.pinTypeAnalog, "DDR_DQ[9]")
    self.assertEqual(pin.getFullName(), "PS_DDR_DQ9_502")

    pn = "D3"
    pin = cp.getPinByNumber(pn)
    misc.chkPin(self, pin, "U?", pn, "IO_L16N_T2_33", CADct.pinTypeAnalog, "DSP_SPI_CS_L")
    self.assertEqual(pin.getFullName(), "IO_L16N_T2_33")

    pname = "PS_MIO27_501"
    lstPin = cp.getPinsByName(pname)
    self.assertEqual(len(lstPin), 1)
    pin = lstPin[0]
    misc.chkPin(self, pin, "U?", "G20", pname, CADct.pinTypeAnalog, "MIO[27]")
    self.assertEqual(pin.getFullName(), "PS_MIO27_501")

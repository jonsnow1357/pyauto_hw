#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.geometry"""

#import site #http://docs.python.org/library/site.html
import datetime
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import unittest
import random

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.misc
import pyauto_hw.geometry as geo

delta = 1e-12

class Test_geometry_base(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    geo.isPositive(0.5)
    with self.assertRaises(RuntimeError):
      geo.isPositive(0.0)
    with self.assertRaises(RuntimeError):
      geo.isPositive(-1.0)
    geo.isPositive(1.0)
    geo.isPositive(0.0, bZero=True)

    for i in range(-4, 5):
      self.assertEqual(geo.degNormalize(0.0 + (360.0 * i)), 0.0)
      self.assertEqual(geo.degNormalize(45.0 + (360.0 * i)), 45.0)
      self.assertEqual(geo.degNormalize(-213.0 + (360.0 * i)), (360.0 - 213.0))
      self.assertEqual(geo.degNormalize(360.0 + (360.0 * i)), 0.0)
      tmp = pyauto_base.misc.getRndFloat(0.0, 360.0)
      self.assertAlmostEqual(geo.degNormalize(tmp + (360.0 * i)), tmp, delta=delta)
      tmp = pyauto_base.misc.getRndFloat(-360.0, 0.0)
      self.assertAlmostEqual(geo.degNormalize(tmp + (360.0 * i)), (360.0 + tmp),
                             delta=delta)

    p = geo.Point2D(0.1, 0.2)
    logger.info(p)
    self.assertEqual(p, p)
    self.assertEqual(p, [0.1, 0.2])
    self.assertNotEqual(p, geo.Point2D(0, 0))
    self.assertNotEqual(p, [0.11, 0.25])

    p = geo.Point2D(1.0, 1.0)
    p2 = geo.Point2D(1.5, 1.0)
    self.assertAlmostEqual(p.distance(p2), 0.5, delta=delta)
    p2 = geo.Point2D(1.0, 2.0)
    self.assertAlmostEqual(p.distance(p2), 1.0, delta=delta)
    p2 = geo.Point2D(1.0, -0.3)
    self.assertAlmostEqual(p.distance(p2), 1.3, delta=delta)
    p2 = geo.Point2D(-0.2, 1.0)
    self.assertAlmostEqual(p.distance(p2), 1.2, delta=delta)
    p2 = geo.Point2D(0.0, 0.0)
    self.assertAlmostEqual(p.distance(p2), math.sqrt(2.0), delta=delta)

  #@unittest.skip("")
  def test_Line2D(self):
    ln = geo.Line2D(x=0.5, y=1.0, deg=45.0)
    self.assertAlmostEqual(ln.a, 1.0, delta=delta)
    self.assertAlmostEqual(ln.b, -1.0, delta=delta)
    self.assertAlmostEqual(ln.c, 0.5, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(pt=geo.Point2D(0.5, 1.0), deg=45.0)
    self.assertAlmostEqual(ln.a, 1.0, delta=delta)
    self.assertAlmostEqual(ln.b, -1.0, delta=delta)
    self.assertAlmostEqual(ln.c, 0.5, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(x=1.5, y=random.random(), deg=0.0)
    self.assertAlmostEqual(ln.a, 1.0, delta=delta)
    self.assertAlmostEqual(ln.b, 0.0, delta=delta)
    self.assertAlmostEqual(ln.c, -1.5, delta=delta)
    logger.info(ln)
    ln = geo.Line2D(x=1.5, y=random.random(), deg=180.0)
    self.assertAlmostEqual(ln.a, 1.0, delta=delta)
    self.assertAlmostEqual(ln.b, 0.0, delta=delta)
    self.assertAlmostEqual(ln.c, -1.5, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(x=random.random(), y=-1.6, deg=90.0)
    self.assertAlmostEqual(ln.a, 0.0, delta=delta)
    self.assertAlmostEqual(ln.b, 1.0, delta=delta)
    self.assertAlmostEqual(ln.c, 1.6, delta=delta)
    logger.info(ln)
    ln = geo.Line2D(x=random.random(), y=-1.6, deg=270.0)
    self.assertAlmostEqual(ln.a, 0.0, delta=delta)
    self.assertAlmostEqual(ln.b, 1.0, delta=delta)
    self.assertAlmostEqual(ln.c, 1.6, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(x1=2.0, y1=1.0, x2=1.0, y2=2.0)
    self.assertAlmostEqual(ln.a, -1.0, delta=delta)
    self.assertAlmostEqual(ln.b, -1.0, delta=delta)
    self.assertAlmostEqual(ln.c, 3.0, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(pt1=geo.Point2D(2.0, 1.0), pt2=geo.Point2D(1.0, 2.0))
    self.assertAlmostEqual(ln.a, -1.0, delta=delta)
    self.assertAlmostEqual(ln.b, -1.0, delta=delta)
    self.assertAlmostEqual(ln.c, 3.0, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(x1=-2.3, y1=random.random(), x2=-2.3, y2=random.random())
    self.assertAlmostEqual(ln.a, 1.0, delta=delta)
    self.assertAlmostEqual(ln.b, 0.0, delta=delta)
    self.assertAlmostEqual(ln.c, 2.3, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(x1=random.random(), y1=0.6, x2=random.random(), y2=0.6)
    self.assertAlmostEqual(ln.a, 0.0, delta=delta)
    self.assertAlmostEqual(ln.b, 1.0, delta=delta)
    self.assertAlmostEqual(ln.c, -0.6, delta=delta)
    logger.info(ln)

    ln = geo.Line2D(pt1=geo.Point2D(0.0, 0.0), pt2=geo.Point2D(1.0, 1.0))
    self.assertTrue(ln.hasPoint(geo.Point2D(0.0, 0.0)))
    self.assertTrue(ln.hasPoint(geo.Point2D(0.5, 0.5)))
    self.assertTrue(ln.hasPoint(geo.Point2D(1.0, 1.0)))
    self.assertFalse(ln.hasPoint(geo.Point2D(0.1, 0.2)))
    self.assertFalse(ln.hasPoint(geo.Point2D(0.500001, 0.5)))

  #@unittest.skip("")
  def test_Circle2D(self):
    cir = geo.Circle2D(x=0.0, y=0.0, r=0.5)
    self.assertAlmostEqual(cir.x, 0.0, delta=delta)
    self.assertAlmostEqual(cir.y, 0.0, delta=delta)
    self.assertAlmostEqual(cir.r, 0.5, delta=delta)
    self.assertAlmostEqual(cir.length, math.pi, delta=delta)
    logger.info(cir)

    cir = geo.Circle2D(pt=geo.Point2D(2.2, -0.5), r=1.3)
    self.assertAlmostEqual(cir.x, 2.2, delta=delta)
    self.assertAlmostEqual(cir.y, -0.5, delta=delta)
    self.assertAlmostEqual(cir.r, 1.3, delta=delta)
    logger.info(cir)

    cir = geo.Circle2D(x1=1.0, y1=1.0, x2=2.0, y2=3.0)
    self.assertAlmostEqual(cir.x, 1.0, delta=delta)
    self.assertAlmostEqual(cir.y, 1.0, delta=delta)
    self.assertAlmostEqual(cir.r, math.sqrt(5.0), delta=delta)
    logger.info(cir)

    cir = geo.Circle2D(pt1=geo.Point2D(-1.0, -0.5), pt2=geo.Point2D(1.0, 0.5))
    self.assertAlmostEqual(cir.x, -1.0, delta=delta)
    self.assertAlmostEqual(cir.y, -0.5, delta=delta)
    self.assertAlmostEqual(cir.r, math.sqrt(5.0), delta=delta)
    logger.info(cir)

    pt = geo.Circle2D(x=0.0, y=0.0, r=1.0).pointAt()
    self.assertAlmostEqual(pt.x, 1.0, delta=delta)
    self.assertAlmostEqual(pt.y, 0.0, delta=delta)
    pt.show()
    pt = geo.Circle2D(x=0.0, y=0.0, r=1.0).pointAt(90.0)
    self.assertAlmostEqual(pt.x, 0.0, delta=delta)
    self.assertAlmostEqual(pt.y, 1.0, delta=delta)
    pt.show()
    pt = geo.Circle2D(x=0.0, y=0.0, r=1.0).pointAt(180.0)
    self.assertAlmostEqual(pt.x, -1.0, delta=delta)
    self.assertAlmostEqual(pt.y, 0.0, delta=delta)
    pt.show()
    pt = geo.Circle2D(x=0.0, y=0.0, r=1.0).pointAt(270.0)
    self.assertAlmostEqual(pt.x, 0.0, delta=delta)
    self.assertAlmostEqual(pt.y, -1.0, delta=delta)
    pt.show()
    pt = geo.Circle2D(x=0.0, y=0.0, r=1.5).pointAt(45.0)
    self.assertAlmostEqual(pt.x, (1.5 / math.sqrt(2.0)), delta=delta)
    self.assertAlmostEqual(pt.y, (1.5 / math.sqrt(2.0)), delta=delta)
    pt.show()

    cir = geo.Circle2D(x=0.0, y=0.0, r=1.0)
    self.assertTrue(cir.hasPoint(geo.Point2D(0.0, 1.0)))
    self.assertTrue(cir.hasPoint(geo.Point2D(1.0, 0.0)))
    self.assertTrue(cir.hasPoint(geo.Point2D(0.0, -1.0)))
    self.assertTrue(cir.hasPoint(geo.Point2D(-1.0, 0.0)))
    self.assertFalse(cir.hasPoint(geo.Point2D(0.5, 0.5)))
    self.assertFalse(cir.hasPoint(geo.Point2D(0.000001, 1.0)))
    pt = geo.Point2D(1.0, 0.0)
    self.assertAlmostEqual(cir.degAt(pt), 0.0, delta=delta)
    pt = geo.Point2D((0.5 * math.sqrt(2.0)), (0.5 * math.sqrt(2.0)))
    self.assertAlmostEqual(cir.degAt(pt), 45.0, delta=delta)
    pt = geo.Point2D(0.0, 1.0)
    self.assertAlmostEqual(cir.degAt(pt), 90.0, delta=delta)
    pt = geo.Point2D((-0.5 * math.sqrt(2.0)), (0.5 * math.sqrt(2.0)))
    self.assertAlmostEqual(cir.degAt(pt), 135.0, delta=delta)
    pt = geo.Point2D(-1.0, 0.0)
    self.assertAlmostEqual(cir.degAt(pt), 180.0, delta=delta)
    pt = geo.Point2D((-0.5 * math.sqrt(2.0)), (-0.5 * math.sqrt(2.0)))
    self.assertAlmostEqual(cir.degAt(pt), 225.0, delta=delta)
    pt = geo.Point2D(0.0, -1.0)
    self.assertAlmostEqual(cir.degAt(pt), 270.0, delta=delta)
    pt = geo.Point2D((0.5 * math.sqrt(2.0)), (-0.5 * math.sqrt(2.0)))
    self.assertAlmostEqual(cir.degAt(pt), 315.0, delta=delta)

  #@unittest.skip("")
  def test_Segment2D(self):
    seg = geo.Segment2D(x1=0, y1=0, x2=1, y2=0)
    self.assertAlmostEqual(seg.slope, 0.0, delta=delta)
    self.assertAlmostEqual(seg.length, 1.0, delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, 0.5, delta=delta)
    self.assertAlmostEqual(pt.y, 0.0, delta=delta)
    logger.info(seg)

    seg = geo.Segment2D(x1=0, y1=0, x2=0, y2=1)
    self.assertAlmostEqual(seg.slope, 90.0, delta=delta)
    self.assertAlmostEqual(seg.length, 1.0, delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, 0.0, delta=delta)
    self.assertAlmostEqual(pt.y, 0.5, delta=delta)
    logger.info(seg)

    seg = geo.Segment2D(x1=0, y1=0, x2=-1, y2=0)
    self.assertAlmostEqual(seg.slope, 180.0, delta=delta)
    self.assertAlmostEqual(seg.length, 1.0, delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, -0.5, delta=delta)
    self.assertAlmostEqual(pt.y, 0.0, delta=delta)
    logger.info(seg)

    seg = geo.Segment2D(x1=0, y1=0, x2=0, y2=-1)
    self.assertAlmostEqual(seg.slope, 270.0, delta=delta)
    self.assertAlmostEqual(seg.length, 1.0, delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, 0.0, delta=delta)
    self.assertAlmostEqual(pt.y, -0.5, delta=delta)
    logger.info(seg)

    seg = geo.Segment2D(pt1=geo.Point2D(0, 0), pt2=geo.Point2D(1, 1))
    self.assertAlmostEqual(seg.slope, 45.0, delta=delta)
    self.assertAlmostEqual(seg.length, math.sqrt(2.0), delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, 0.5, delta=delta)
    self.assertAlmostEqual(pt.y, 0.5, delta=delta)
    logger.info(seg)

    seg = geo.Segment2D(pt1=geo.Point2D(0, 0), pt2=geo.Point2D(-1, 1))
    self.assertAlmostEqual(seg.slope, 135.0, delta=delta)
    self.assertAlmostEqual(seg.length, math.sqrt(2.0), delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, -0.5, delta=delta)
    self.assertAlmostEqual(pt.y, 0.5, delta=delta)
    logger.info(seg)

    seg = geo.Segment2D(pt1=geo.Point2D(0, 0), pt2=geo.Point2D(-1, -1))
    self.assertAlmostEqual(seg.slope, 225.0, delta=delta)
    self.assertAlmostEqual(seg.length, math.sqrt(2.0), delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, -0.5, delta=delta)
    self.assertAlmostEqual(pt.y, -0.5, delta=delta)
    logger.info(seg)

    seg = geo.Segment2D(pt1=geo.Point2D(0, 0), pt2=geo.Point2D(1, -1))
    self.assertAlmostEqual(seg.slope, 315.0, delta=delta)
    self.assertAlmostEqual(seg.length, math.sqrt(2.0), delta=delta)
    pt = seg.middle()
    self.assertAlmostEqual(pt.x, 0.5, delta=delta)
    self.assertAlmostEqual(pt.y, -0.5, delta=delta)
    logger.info(seg)

  #@unittest.skip("")
  def test_Arc2D(self):
    arc = geo.Arc2D(x=1, y=1, r=1, deg1=0, deg2=90)
    self.assertAlmostEqual(arc.x, 1.0, delta=delta)
    self.assertAlmostEqual(arc.y, 1.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 0.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 90.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=delta)
    logger.info(arc)

    arc = geo.Arc2D(pt=geo.Point2D(-1, 0), r=1.5, deg1=45, deg2=135)
    self.assertAlmostEqual(arc.x, -1.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.5, delta=delta)
    self.assertAlmostEqual(arc.deg1, 45.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 135.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.75 * math.pi), delta=delta)
    logger.info(arc)

    arc = geo.Arc2D(x1=1, y1=0, x2=0, y2=1, r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 0.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 90.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=delta)
    logger.info(arc)

    arc = geo.Arc2D(x1=(0.5 * math.sqrt(2.0)),
                    y1=(0.5 * math.sqrt(2.0)),
                    x2=(-0.5 * math.sqrt(2.0)),
                    y2=(0.5 * math.sqrt(2.0)),
                    r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 45.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 135.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=delta)
    logger.info(arc)

    arc = geo.Arc2D(x1=0, y1=1, x2=-1, y2=0, r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 90.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 180.0, delta=(delta * 1e7))
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=(delta * 1e5))
    logger.info(arc)

    arc = geo.Arc2D(x1=(-0.5 * math.sqrt(2.0)),
                    y1=(0.5 * math.sqrt(2.0)),
                    x2=(-0.5 * math.sqrt(2.0)),
                    y2=(-0.5 * math.sqrt(2.0)),
                    r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 135.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 225.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=delta)
    logger.info(arc)

    arc = geo.Arc2D(x1=-1, y1=0, x2=0, y2=-1, r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 180.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 270.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=delta)
    logger.info(arc)

    arc = geo.Arc2D(x1=(-0.5 * math.sqrt(2.0)),
                    y1=(-0.5 * math.sqrt(2.0)),
                    x2=(0.5 * math.sqrt(2.0)),
                    y2=(-0.5 * math.sqrt(2.0)),
                    r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 225.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 315.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=delta)
    logger.info(arc)

    arc = geo.Arc2D(x1=0, y1=-1, x2=1, y2=0, r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 270.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 0.0, delta=(delta * 1e6))
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=(delta * 1e5))
    logger.info(arc)

    arc = geo.Arc2D(x1=(0.5 * math.sqrt(2.0)),
                    y1=(-0.5 * math.sqrt(2.0)),
                    x2=(0.5 * math.sqrt(2.0)),
                    y2=(0.5 * math.sqrt(2.0)),
                    r=1.0)
    self.assertAlmostEqual(arc.x, 0.0, delta=delta)
    self.assertAlmostEqual(arc.y, 0.0, delta=delta)
    self.assertAlmostEqual(arc.r, 1.0, delta=delta)
    self.assertAlmostEqual(arc.deg1, 315.0, delta=delta)
    self.assertAlmostEqual(arc.deg2, 45.0, delta=delta)
    self.assertAlmostEqual(arc.length, (0.5 * math.pi), delta=delta)
    logger.info(arc)

class Test_geometry_functions(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  def test_rotate(self):
    pt1 = geo.Point2D(1.0, 0.0)

    pt2 = geo.rotate(pt1, 0.0)
    self.assertAlmostEqual(pt2.x, 1.0, delta=delta)
    self.assertAlmostEqual(pt2.y, 0.0, delta=delta)
    pt2.show()

    pt2 = geo.rotate(pt1, (0.5 * math.pi))
    self.assertAlmostEqual(pt2.x, 0.0, delta=delta)
    self.assertAlmostEqual(pt2.y, 1.0, delta=delta)
    pt2.show()

    pt2 = geo.rotate(pt1, math.pi)
    self.assertAlmostEqual(pt2.x, -1.0, delta=delta)
    self.assertAlmostEqual(pt2.y, 0.0, delta=delta)
    pt2.show()

    pt2 = geo.rotate(pt1, (1.5 * math.pi))
    self.assertAlmostEqual(pt2.x, 0.0, delta=delta)
    self.assertAlmostEqual(pt2.y, -1.0, delta=delta)
    pt2.show()

    pt2 = geo.rotate(pt1, (0.25 * math.pi))
    self.assertAlmostEqual(pt2.x, 0.7071067811865476, delta=delta)
    self.assertAlmostEqual(pt2.y, 0.7071067811865476, delta=delta)
    pt2.show()

  #@unittest.skip("")
  def test_intersection(self):
    ln1 = geo.Line2D(x=1.0, y=1.0, deg=0.0)
    ln2 = geo.Line2D(x=4.3, y=-1.0, deg=90.0)
    logger.info(ln1)
    logger.info(ln2)
    pt = geo.intersection(ln1, ln2)
    self.assertAlmostEqual(pt.x, 1.0, delta=delta)
    self.assertAlmostEqual(pt.y, -1.0, delta=delta)
    pt.show()

    ln1 = geo.Line2D(x1=3.0, y1=0.0, x2=0.0, y2=3.0)
    ln2 = geo.Line2D(x1=-1.0, y1=1.0, x2=5.0, y2=4.0)
    logger.info(ln1)
    logger.info(ln2)
    pt = geo.intersection(ln1, ln2)
    self.assertAlmostEqual(pt.x, 1.0, delta=delta)
    self.assertAlmostEqual(pt.y, 2.0, delta=delta)
    pt.show()

  #@unittest.skip("")
  def test_boundingBox(self):
    lstPts = [
        geo.Point2D(-0.1, -0.5),
        geo.Point2D(0.4, 0.6),
        geo.Point2D(-1, -1),
        geo.Point2D(1, 1)
    ]
    [ll, ur] = geo.boundingBox(lstPts)
    self.assertEqual(ll.x, -1.0)
    self.assertEqual(ll.y, -1.0)
    self.assertEqual(ur.x, 1.0)
    self.assertEqual(ur.y, 1.0)
    ll.show()
    ur.show()

    lstPts = []
    for _ in range(int(100)):
      lstPts.append(geo.Point2D(random.random(), random.random()))
    lstPts.append(geo.Point2D(-1, -1))
    lstPts.append(geo.Point2D(1, 1))
    [ll, ur] = geo.boundingBox(lstPts)
    self.assertEqual(ll.x, -1.0)
    self.assertEqual(ll.y, -1.0)
    self.assertEqual(ur.x, 1.0)
    self.assertEqual(ur.y, 1.0)
    ll.show()
    ur.show()

  def test_guessGrid(self):
    lstPts = [
        geo.Point2D(-0.1, -0.5),
        geo.Point2D(0.4, 0.6),
        geo.Point2D(-1, -1),
        geo.Point2D(1, 1)
    ]
    [dx, dy] = geo.guessGrid(lstPts)
    self.assertEqual(dx, 0.5)
    self.assertEqual(dy, 0.4)

    lstPts = []
    [dx, dy] = geo.guessGrid(lstPts)
    self.assertIsNone(dx)
    self.assertIsNone(dy)
    for _ in range(int(100)):
      t0 = round(random.random(), 1)
      t1 = round(random.random(), 1)
      lstPts.append(geo.Point2D(t0, t1))
    [dx, dy] = geo.guessGrid(lstPts)
    self.assertAlmostEqual(dx, 0.1, delta=delta)
    self.assertAlmostEqual(dy, 0.1, delta=delta)

  #@unittest.skip("")
  def test_tiling_square(self):
    lst = geo.tiling_square(pt1=geo.Point2D(-1, 1), pt2=geo.Point2D(1, -1), dx=1)
    self.assertCountEqual(lst,
                          [[-1.0, 1.0], [0.0, 1.0], [1.0, 1.0], [-1.0, 0.0], [0.0, 0.0],
                           [1.0, 0.0], [-1.0, -1.0], [0.0, -1.0], [1.0, -1.0]])

    lst = geo.tiling_square(pt1=geo.Point2D(-1, 1),
                            pt2=geo.Point2D(1, -1),
                            dx=1,
                            bAlternate=True)
    self.assertCountEqual(lst,
                          [[-1.0, 1.0], [1.0, 1.0], [0.0, 0.0], [-1.0, -1.0], [1.0, -1.0]])

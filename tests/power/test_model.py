#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.power._model"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.power.constants as hwPwrCt
import pyauto_hw.power as hwPwr

class Test_PowerModel(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    mdl = hwPwr.PowerModel()
    self.assertEqual(mdl.name, None)
    # TODO:

class Test_PowerModelDB(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    mPath = os.path.join(self.inFolder, "power_db")
    pmdb = hwPwr.PowerModelDB()

    self.assertEqual(pmdb.path, None)
    self.assertEqual(pmdb._models, {})

    pmdb.path = mPath
    pmdb.load()
    logger.info("\n" + "\n".join(pmdb.getAllValues()))

    model = pmdb.get("test_read_model_RES")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelRes)

    model = pmdb.get("test_read_model_IND")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelInd)

    model = pmdb.get("test_read_model_FET")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelTR)

    model = pmdb.get("test_read_model_LDO")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelLDO)

    model = pmdb.get("test_read_model_SW_1_1")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelDCDC)

    model = pmdb.get("test_read_model_IC1")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelIC)

    model = pmdb.get("test_read_model_IC2")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelIC)

    model = pmdb.get("test_read_model_IC3")
    model.load()
    self.assertEqual(model.type, hwPwrCt.PwrModelIC)

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_hw.CAD.base as CADbase

def mkPowerTreeNetlist():
  ntl = CADbase.Netlist()

  cp = CADbase.Component("J1")
  pin = CADbase.Pin("J1.1")
  cp.addPin(pin)
  pin = CADbase.Pin("J1.2")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("SW1", "test_read_model_SW_1_1")
  pin = CADbase.Pin("SW1.IN")
  cp.addPin(pin)
  pin = CADbase.Pin("SW1.OUT")
  cp.addPin(pin)
  pin = CADbase.Pin("SW1.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("SW4", "test_read_model_SW_1_2")
  pin = CADbase.Pin("SW4.IN")
  cp.addPin(pin)
  pin = CADbase.Pin("SW4.OUT1")
  cp.addPin(pin)
  pin = CADbase.Pin("SW4.OUT2")
  cp.addPin(pin)
  pin = CADbase.Pin("SW4.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("SW2", "test_read_model_SW_1_1")
  pin = CADbase.Pin("SW2.IN")
  cp.addPin(pin)
  pin = CADbase.Pin("SW2.OUT")
  cp.addPin(pin)
  pin = CADbase.Pin("SW2.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("SW3", "test_read_model_SW_1_1")
  pin = CADbase.Pin("SW3.IN")
  cp.addPin(pin)
  pin = CADbase.Pin("SW3.OUT")
  cp.addPin(pin)
  pin = CADbase.Pin("SW3.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("LDO1", "test_read_model_LDO")
  pin = CADbase.Pin("LDO1.IN")
  cp.addPin(pin)
  pin = CADbase.Pin("LDO1.OUT")
  cp.addPin(pin)
  pin = CADbase.Pin("LDO1.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("L1", "test_read_model_IND")
  pin = CADbase.Pin("L1.1")
  cp.addPin(pin)
  pin = CADbase.Pin("L1.2")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("Q1", "test_read_model_FET")
  pin = CADbase.Pin("Q1.1")
  cp.addPin(pin)
  pin = CADbase.Pin("Q1.2")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("U1", "test_read_model_IC3")
  pin = CADbase.Pin("U1.VCC")
  cp.addPin(pin)
  pin = CADbase.Pin("U1.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("U2", "test_read_model_IC1")
  pin = CADbase.Pin("U2.VCC1")
  cp.addPin(pin)
  pin = CADbase.Pin("U2.VCC2")
  cp.addPin(pin)
  pin = CADbase.Pin("U2.VCC3")
  cp.addPin(pin)
  pin = CADbase.Pin("U2.VCC_CORE")
  cp.addPin(pin)
  pin = CADbase.Pin("U2.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  cp = CADbase.Component("U3", "test_read_model_IC2")
  pin = CADbase.Pin("U3.V+")
  cp.addPin(pin)
  pin = CADbase.Pin("U3.V-")
  cp.addPin(pin)
  pin = CADbase.Pin("U3.GND")
  cp.addPin(pin)
  ntl.addComponent(cp)

  net = CADbase.Net("P12V")
  net.addPinInfoList(["J1.1", "SW1.IN", "SW2.IN", "SW3.IN"])
  ntl.addNet(net)

  net = CADbase.Net("P5V")
  net.addPinInfoList(["SW1.OUT", "SW4.IN"])
  ntl.addNet(net)

  net = CADbase.Net("P2V5")
  net.addPinInfoList(["SW2.OUT", "LDO1.IN", "U2.VCC2"])
  ntl.addNet(net)

  net = CADbase.Net("P2V3")
  net.addPinInfoList(["LDO1.OUT", "U1.VCC"])
  ntl.addNet(net)

  net = CADbase.Net("P1V8")
  net.addPinInfoList(["SW3.OUT", "L1.1", "Q1.1"])
  ntl.addNet(net)

  net = CADbase.Net("+3V3")
  net.addPinInfoList(["SW4.OUT1", "U3.V+", "U2.VCC1"])
  ntl.addNet(net)

  net = CADbase.Net("-3.3V")
  net.addPinInfoList(["SW4.OUT2", "U3.V-"])
  ntl.addNet(net)

  net = CADbase.Net("+1V8_DEV_IO")
  net.addPinInfoList(["L1.2", "U2.VCC3"])
  ntl.addNet(net)

  net = CADbase.Net("+1V8_DEV_CORE")
  net.addPinInfoList(["Q1.2", "U2.VCC_CORE"])
  ntl.addNet(net)

  ntl.refresh()
  print(ntl)
  return ntl

#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.power._base"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest
import random

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.power as hwPwr
import pyauto_hw.tests.CAD.misc as CADmisc

_nLoop = 100

def _mkPortInfo() -> list:
  tmp = random.randint(1, 100)
  if (tmp <= 90):
    v = 0.0
    while (v != 0.0):
      v = round(random.uniform(-5.0, 5.0), 2)

    tags = CADmisc.mkStr("", 4, 4, bNum=False)

    if (v < 0.0):
      i = round(random.uniform(-2.0, -0.1), 3)
    else:
      i = round(random.uniform(0.1, 2.0), 3)

    return [v, tags, i]
  elif ((tmp > 90) and (tmp <= 95)):
    return ["POS", [], 0.0]
  elif ((tmp > 95) and (tmp <= 100)):
    return ["NEG", [], 0.0]

def _mkRailParams() -> list:
  v = 0.0
  while (v == 0.0):
    tmp1 = random.randint(-5, 5)
    tmp2 = random.randint(0, 99)
    v = tmp1 + (tmp2 / 100)
  tmp = random.randint(1, 3)
  if (tmp == 1):
    railName = "{:+.2f}".format(v).replace(".", "V")
  elif (tmp == 2):
    railName = "{:+.2f}".format(v).replace(".", "V").replace("+", "P").replace("-", "N")
  else:
    railName = "{:+.2f}V".format(v)
  nTags = random.randint(2, 4)
  tags = CADmisc.mkStr("TAG", 4, nTags)
  railName = "_".join([railName] + tags)

  i = 10.0 * random.random()
  if (v < 0.0):
    i *= -1.0

  t_0 = random.random() + 10.0

  return [railName, v, tags, i, t_0]

class Test_PowerPort(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    port = hwPwr.PowerPort()

    self.assertEqual(port.v, 0.0)
    self.assertEqual(port.i, 0.0)
    self.assertEqual(port.tags, [])
    self.assertEqual(port.refdes, None)
    self.assertEqual(port.railName, None)
    self.assertEqual(port.no, 0)
    self.assertEqual(port.id, 0)
    self.assertEqual(port.type, None)
    self.assertEqual(port.name, "0|None|0|None")

    # test fromList() only since fromName will be tested by PowerRail
    for _ in range(_nLoop):
      refdes = CADmisc.mkRefdes(1)
      v, tags, i = _mkPortInfo()
      port.fromList(refdes, [v, tags, i])

      self.assertAlmostEqual(port.v, v, delta=1e-9)
      self.assertAlmostEqual(port.i, i, delta=1e-9)
      self.assertEqual(port.tags, tags)
      self.assertEqual(port.refdes, refdes)
      self.assertEqual(port.railName, None)
      self.assertEqual(port.id, 0)
      self.assertEqual(port.type, None)
      logger.info(port)

class Test_PowerRail(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    pr = hwPwr.PowerRail("P3V3")

    self.assertEqual(pr.v, 3.3)
    self.assertEqual(pr.i, 0.0)
    self.assertEqual(pr.tags, [])
    self.assertEqual(pr.name, "P3V3")
    self.assertEqual(pr.t_0, 0.0)
    self.assertEqual(pr.t_on, 5.0)
    self.assertEqual(pr.startAfter, None)

    for _ in range(_nLoop):
      name, v, tags, i, t_0 = _mkRailParams()
      pr.fromName(name)
      pr.i = i
      pr.t_0 = t_0

      self.assertAlmostEqual(pr.v, v, delta=1e-9)
      self.assertEqual(pr.i, i)
      self.assertEqual(pr.tags, tags)
      self.assertEqual(pr.name, name)
      self.assertEqual(pr.t_0, t_0)
      self.assertEqual(pr.t_on, (t_0 + 5.0))
      self.assertEqual(pr.startAfter, None)
      logger.info(pr)

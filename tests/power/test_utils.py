#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.power._model"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.tests.power.tools as tt
import pyauto_hw.config
import pyauto_hw.utils
import pyauto_hw.power as hwPwr
import pyauto_hw.power.utils

class Test_PowerUtils(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_getDecouplingInfo1(self):
    mPath = os.path.join(self.inFolder, "power_db")
    ntl = tt.mkPowerTreeNetlist()

    pwrTree = hwPwr.PowerTree("PowerTree")
    pwrTree.modelPath = mPath
    pwrTree.cfg = {"ports": {"SW4": [["m", "P5V", "in", ""]]}}
    pwrTree.readNetlist(ntl)
    pwrTree.build()

    pwrTree.showTree()

    lstRails = pyauto_hw.power.utils.getDecouplingInfo(ntl)
    self.assertEqual(len(lstRails), 9)
    self.assertEqual(len(lstRails[0].decoupling), 0)

#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.power"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_hw.tests.power.tools as tt
import pyauto_hw.power as hwPwr
import pyauto_hw.power.constants as hwPwrCt

class Test_PowerLoad(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    load = hwPwr.PowerLoad()

    self.assertEqual(load.refdes, "U?")
    self.assertEqual(load.value, "")
    self.assertEqual(load.model, None)
    self.assertEqual(load.power, None)

    load.setInfo("U10", "IC_Value")
    self.assertEqual(load.refdes, "U10")
    self.assertEqual(load.value, "IC_Value")
    self.assertEqual(load.power, None)
    self.assertEqual(load.model, None)

  #@unittest.skip("")
  def test_read_model(self):
    load = hwPwr.PowerLoad()

    load.setInfo("U2", "test_read_model_IC1")
    self.assertEqual(load.refdes, "U2")
    self.assertEqual(load.value, "test_read_model_IC1")
    load.model = hwPwr.PowerModel()
    load.model.path = os.path.join(self.inFolder, "power_db", "test_read_model_IC1.json")
    load.model.load()

    lst_rails = load.model.get("max")
    self.assertEqual(len(lst_rails), 4)
    self.assertCountEqual([t[0] for t in lst_rails], [3.3, 2.5, 1.8, 1.8])
    for val in lst_rails:
      logger.info(val)
      if ((val[0] == 1.8) and (val[2] == 0.18)):
        self.assertEqual(val[1], ["IO"])
    lst_rails = load.model.get("typ")
    self.assertEqual(len(lst_rails), 4)
    self.assertCountEqual([t[0] for t in lst_rails], [3.3, 2.5, 1.8, 1.8])
    for val in lst_rails:
      logger.info(val)
      if ((val[0] == 1.8) and (val[2] == 2.25)):
        self.assertEqual(val[1], ["CORE"])
    lst_rails = load.model.get("min")
    self.assertEqual(len(lst_rails), 4)
    self.assertCountEqual([t[0] for t in lst_rails], [3.3, 2.5, 1.8, 1.8])
    for val in lst_rails:
      logger.info(val)
      if ((val[0] == 1.8) and (val[2] == 2.0)):
        self.assertEqual(val[1], ["CORE"])
    self.assertEqual(load.power, None)
    load.model.check()
    logger.info(load)

    load.setInfo("U12", "test_read_model_IC2")
    self.assertEqual(load.refdes, "U12")
    self.assertEqual(load.value, "test_read_model_IC2")
    load.model = hwPwr.PowerModel()
    load.model.path = os.path.join(self.inFolder, "power_db", "test_read_model_IC2.json")
    load.model.load()

    lst_rails = load.model.get("max")
    self.assertEqual(len(lst_rails), 2)
    self.assertCountEqual([t[0] for t in lst_rails], ["POS", "NEG"])
    for val in lst_rails:
      logger.info(val)
      if (val[0] == "POS"):
        self.assertEqual(val[2], 0.45)
    lst_rails = load.model.get("typ")
    self.assertEqual(len(lst_rails), 2)
    self.assertCountEqual([t[0] for t in lst_rails], ["POS", "NEG"])
    for val in lst_rails:
      logger.info(val)
      if (val[0] == "NEG"):
        self.assertEqual(val[2], -0.15)
    lst_rails = load.model.get("min")
    self.assertEqual(len(lst_rails), 2)
    self.assertCountEqual([t[0] for t in lst_rails], ["POS", "NEG"])
    for val in lst_rails:
      logger.info(val)
      if (val[0] == "NEG"):
        self.assertEqual(val[2], -0.1)
    self.assertEqual(load.power, None)
    load.model.check()
    logger.info(load)

class Test_PowerConverter(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    conv = hwPwr.PowerConverter()

    self.assertEqual(conv.refdes, "U?")
    self.assertEqual(conv.value, "")
    self.assertEqual(conv.model, None)
    self.assertEqual(conv.power, None)
    self.assertEqual(conv.process, False)

    conv.setInfo("U4", "LDO_Value")
    self.assertEqual(conv.refdes, "U4")
    self.assertEqual(conv.value, "LDO_Value")
    self.assertEqual(conv.power, None)
    self.assertEqual(conv.model, None)
    self.assertEqual(conv.process, False)

  #@unittest.skip("")
  def test_read_model(self):
    conv = hwPwr.PowerConverter()

    conv.setInfo("U?", "test_read_model_SW_1_1")
    self.assertEqual(conv.refdes, "U?")
    self.assertEqual(conv.value, "test_read_model_SW_1_1")
    conv.model = hwPwr.PowerModel()
    conv.model.path = os.path.join(self.inFolder, "power_db", "test_read_model_SW_1_1.json")
    conv.model.load()
    self.assertEqual(conv.model.type, hwPwrCt.PwrModelDCDC)
    self.assertEqual(conv.model.get(None)["inputs"], 1)
    self.assertEqual(conv.model.get(None)["outputs"], 1)
    self.assertCountEqual(conv.model.get(None)["map"], [[True]])
    self.assertEqual(conv.power, None)
    conv.model.check()
    logger.info(conv)

    conv.setInfo("U?", "test_read_model_SW_1_2")
    self.assertEqual(conv.refdes, "U?")
    self.assertEqual(conv.value, "test_read_model_SW_1_2")
    conv.model = hwPwr.PowerModel()
    conv.model.path = os.path.join(self.inFolder, "power_db", "test_read_model_SW_1_2.json")
    conv.model.load()
    self.assertEqual(conv.model.type, hwPwrCt.PwrModelDCDC)
    self.assertEqual(conv.model.get(None)["inputs"], 1)
    self.assertEqual(conv.model.get(None)["outputs"], 2)
    self.assertCountEqual(conv.model.get(None)["map"], [[True, True]])
    self.assertEqual(conv.power, None)
    conv.model.check()
    logger.info(conv)

    conv.setInfo("U?", "test_read_model_LDO")
    self.assertEqual(conv.refdes, "U?")
    self.assertEqual(conv.value, "test_read_model_LDO")
    conv.model = hwPwr.PowerModel()
    conv.model.path = os.path.join(self.inFolder, "power_db", "test_read_model_LDO.json")
    conv.model.load()
    self.assertEqual(conv.model.type, hwPwrCt.PwrModelLDO)
    self.assertEqual(conv.model.get(None)["inputs"], 1)
    self.assertEqual(conv.model.get(None)["outputs"], 1)
    self.assertCountEqual(conv.model.get(None)["map"], [[True]])
    self.assertEqual(conv.power, None)
    conv.model.check()
    logger.info(conv)

    conv.setInfo("Q?", "test_read_model_FET")
    self.assertEqual(conv.refdes, "Q?")
    self.assertEqual(conv.value, "test_read_model_FET")
    conv.model = hwPwr.PowerModel()
    conv.model.path = os.path.join(self.inFolder, "power_db", "test_read_model_FET.json")
    conv.model.load()
    self.assertEqual(conv.model.type, hwPwrCt.PwrModelTR)
    self.assertEqual(conv.power, None)
    conv.model.check()
    logger.info(conv)

    conv.setInfo("L?", "test_read_model_IND")
    self.assertEqual(conv.refdes, "L?")
    self.assertEqual(conv.value, "test_read_model_IND")
    conv.model = hwPwr.PowerModel()
    conv.model.path = os.path.join(self.inFolder, "power_db", "test_read_model_IND.json")
    conv.model.load()
    self.assertEqual(conv.model.type, hwPwrCt.PwrModelInd)
    self.assertEqual(conv.model.get(None)["i_max"], 100.0)
    self.assertEqual(conv.power, None)
    conv.model.check()
    logger.info(conv)

class Test_PowerTree(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_PowerTreeXML(self):
    mPath = os.path.join(self.inFolder, "power_db")
    fPath = os.path.join(self.inFolder, "test_read_PowerTree.xml")
    pwrTree = hwPwr.PowerTree("PowerTree")
    pwrTree.modelPath = mPath
    pwrTree.readXML(fPath)
    pwrTree.build()

    pwrTree.showTree()
    pwrTree.updatePower()
    pwrTree.showRails()
    pwrTree.showConverters()
    pwrTree.showLoads()

    self.assertAlmostEqual(pwrTree.getRail("+3V3").i, 1.25, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("-3.3V").i, -0.2, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P2V3").i, 0.5, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("+1V8_DEV_CORE").i, 2.4, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("+1V8_DEV_IO").i, 0.18, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P1V8").i, 2.58, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P2V5").i, 0.88, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P5V").i, 1.19625, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P12V").i, 1.335963542, delta=1e-9)

    self.assertAlmostEqual(pwrTree.getLoad("U1").power, 1.15, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getLoad("U2").power, 8.234, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getLoad("U3").power, 2.145, delta=1e-9)

    self.assertAlmostEqual(pwrTree.getConverter("L1").power, 3.24e-6, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("Q1").power, 0.0288, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("LDO1").power, 0.1, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW1").power, 1.4953125, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW2").power, 0.55, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW3").power, 1.161, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW4").power, 1.19625, delta=1e-9)

    self.assertAlmostEqual(pwrTree.totalPower, 16.06, delta=1e-2)

    fOutPath = os.path.join(self.outFolder, "test_write_PowerTree.xml")
    pwrTree.writeXML(fOutPath)
    #lstHash = [
    #    "90a32c8c92102aa5f7792897ad88afdf3391da183dd226c910d3025db59b3b1a",  # py2
    #    "05af8b8c1e050873186cc5e8f6596e7f6f9ebdc3f2f854b0e0e5bb659405eb3e",  # py3
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 1200)
    fOutPath = os.path.join(self.outFolder, "test_write_PowerTree.csv")
    pwrTree.writeReport_CSV(fOutPath)
    #lstHash = [
    #    "56effdb98ba377783ad8e2b0a61b3951805520202b6ccaf5d25ce9cdbc170641",  # linux py2
    #    "f8025229f72acfa82f29bc083b1fee6bbbeadc725434c86677e00b923c4ea91e",  # linux py3
    #    "0fa0d1f881466a9239c8f531657e7e6b292c27b917ee65aff940b34e9b871269",  # win
    #]
    #tt.chkHash_file(self, fOutPath, lstHash)
    self.assertTrue(os.path.isfile(fOutPath), "file DOES NOT exist: {}".format(fOutPath))
    self.assertGreater(os.path.getsize(fOutPath), 1200)

  #@unittest.skip("")
  def test_PowerTreeNetlist(self):
    mPath = os.path.join(self.inFolder, "power_db")
    ntl = tt.mkPowerTreeNetlist()
    pwrTree = hwPwr.PowerTree("PowerTree")
    pwrTree.modelPath = mPath
    pwrTree.cfg = {"ports": {"SW4": [["m", "P5V", "in", ""]]}}
    pwrTree.readNetlist(ntl)
    pwrTree.build()

    pwrTree.showTree()
    pwrTree.updatePower()
    pwrTree.showRails()
    pwrTree.showConverters()
    pwrTree.showLoads()

    self.assertAlmostEqual(pwrTree.getRail("+3V3").i, 1.25, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("-3.3V").i, -0.2, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P2V3").i, 0.5, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("+1V8_DEV_CORE").i, 2.4, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("+1V8_DEV_IO").i, 0.18, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P1V8").i, 2.58, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P2V5").i, 0.88, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P5V").i, 1.19625, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getRail("P12V").i, 1.335963542, delta=1e-9)

    self.assertAlmostEqual(pwrTree.getLoad("U1").power, 1.15, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getLoad("U2").power, 8.234, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getLoad("U3").power, 2.145, delta=1e-9)

    self.assertAlmostEqual(pwrTree.getConverter("L1").power, 3.24e-6, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("Q1").power, 0.0288, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("LDO1").power, 0.1, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW1").power, 1.4953125, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW2").power, 0.55, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW3").power, 1.161, delta=1e-9)
    self.assertAlmostEqual(pwrTree.getConverter("SW4").power, 1.19625, delta=1e-9)

    self.assertAlmostEqual(pwrTree.totalPower, 16.06, delta=1e-2)

  def _assert_PowerStartInfo(self, rail, t_0, t_on, strAfter):
    self.assertEqual(rail.t_0, t_0)
    self.assertEqual(rail.t_on, t_on)
    self.assertEqual(rail.startAfter, strAfter)

  #@unittest.skip("")
  def test_PowerStart(self):
    mPath = os.path.join(self.inFolder, "power_db")
    fPath = os.path.join(self.inFolder, "test_read_PowerTree.xml")
    pwrTree = hwPwr.PowerTree("PowerTree")
    pwrTree.modelPath = mPath
    pwrTree.readXML(fPath)
    pwrTree.build()

    pwrTree.showTree()
    pwrTree.updateStart()
    self._assert_PowerStartInfo(pwrTree.getRail("P12V"), 0.0, 5.0, None)
    self._assert_PowerStartInfo(pwrTree.getRail("P5V"), 5.0, 10.0, None)
    self._assert_PowerStartInfo(pwrTree.getRail("P2V5"), 5.0, 10.0, None)
    self._assert_PowerStartInfo(pwrTree.getRail("P1V8"), 5.0, 10.0, None)
    self._assert_PowerStartInfo(pwrTree.getRail("+3V3"), 10.0, 15.0, None)
    self._assert_PowerStartInfo(pwrTree.getRail("-3.3V"), 15.0, 20.0, "P2V3")
    self._assert_PowerStartInfo(pwrTree.getRail("P2V3"), 10.0, 15.0, None)
    self._assert_PowerStartInfo(pwrTree.getRail("+1V8_DEV_IO"), 5.0, 10.0, None)
    self._assert_PowerStartInfo(pwrTree.getRail("+1V8_DEV_CORE"), 15.0, 20.0, "P2V3")

    rail = pwrTree.getRail("+1V8_DEV_IO")
    rail.startAfter = "+3V3"
    with self.assertRaises(RuntimeError):
      pwrTree.updateStart()
    rail.startAfter = None

  #@unittest.skip("")
  def test_PowerTreeXML_case01(self):
    mPath = os.path.join(self.inFolder, "power_db")
    fPath = os.path.join(self.inFolder, "test_read_PowerTree_case01.xml")
    pwrTree = hwPwr.PowerTree("PowerTree")
    pwrTree.modelPath = mPath
    pwrTree.readXML(fPath)
    pwrTree.build()

    pwrTree.showTree()
    pwrTree.updatePower()
    pwrTree.showRails()
    pwrTree.showConverters()
    pwrTree.showLoads()

    self.assertEqual(pwrTree.getRail("P5V").i, 0.5)
    self.assertEqual(pwrTree.getRail("P5V_OUT").i, pwrTree.getRail("P5V").i)
    self.assertGreater(pwrTree.getRail("P12V_IN").i, 0.0)
    self.assertEqual(pwrTree.getRail("P12V").i, pwrTree.getRail("P12V_IN").i)

    self.assertGreater(pwrTree.getConverter("SW1").power, 0.0)

    self.assertGreater(pwrTree.totalPower, 0.0)

  #@unittest.skip("")
  def test_PowerTreeXML_case02(self):
    mPath = os.path.join(self.inFolder, "power_db")
    fPath = os.path.join(self.inFolder, "test_read_PowerTree_case02.xml")
    pwrTree = hwPwr.PowerTree("PowerTree")
    pwrTree.modelPath = mPath
    pwrTree.readXML(fPath)
    pwrTree.build()

    pwrTree.showTree()
    pwrTree.updatePower()
    pwrTree.showRails()
    pwrTree.showConverters()
    pwrTree.showLoads()

    self.assertEqual(pwrTree.getRail("P5V").i, 0.5)
    self.assertEqual(pwrTree.getRail("P5V_OUT").i, pwrTree.getRail("P5V").i)
    self.assertGreater(pwrTree.getRail("P12V_IN").i, 0.0)
    self.assertEqual(pwrTree.getRail("P12V").i, pwrTree.getRail("P12V_IN").i)

    self.assertGreater(pwrTree.getConverter("SW1").power, 0.0)

    self.assertGreater(pwrTree.totalPower, 0.0)

  #@unittest.skip("")
  def test_PowerTreeXML_case03(self):
    mPath = os.path.join(self.inFolder, "power_db")
    fPath = os.path.join(self.inFolder, "test_read_PowerTree_case03.xml")
    pwrTree = hwPwr.PowerTree("PowerTree")
    pwrTree.modelPath = mPath
    pwrTree.readXML(fPath)
    pwrTree.build()

    pwrTree.showTree()
    pwrTree.updatePower()
    pwrTree.showRails()
    pwrTree.showConverters()
    pwrTree.showLoads()

    self.assertEqual(pwrTree.getRail("P3V3_VIO").i, 0.5)
    self.assertGreater(pwrTree.getRail("P3V3_VCC").i, pwrTree.getRail("P3V3_VIO").i)
    self.assertGreater(pwrTree.getRail("P3V3").i, 0.0)
    self.assertEqual(pwrTree.getRail("P3V3_VCC").i, pwrTree.getRail("P3V3").i)

    self.assertGreater(pwrTree.getConverter("SW1").power, 0.0)

    self.assertGreater(pwrTree.totalPower, 0.0)

#!/usr/bin/env python
# template: unittest
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""unit testing for pyauto_hw.data.IDF"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import unittest

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.tests.tools as tt
import pyauto_hw.data.IDF

class Test_IDF(unittest.TestCase):
  cwd = ""
  lclDir = ""

  @classmethod
  def setUpClass(cls):
    cls.cwd = os.getcwd()
    cls.lclDir = os.path.dirname(os.path.realpath(__file__))  # folder where this file is
    os.chdir(cls.lclDir)
    logger.info("CWD: {}".format(os.getcwd()))

  @classmethod
  def tearDownClass(cls):
    os.chdir(cls.cwd)
    logger.info("CWD: {}".format(os.getcwd()))

  def setUp(self):
    print("")
    self.inFolder = os.path.join(self.lclDir, "files")
    self.outFolder = os.path.join(self.lclDir, "data_out")
    if (not os.path.isdir(self.outFolder)):
      os.makedirs(self.outFolder)

  def tearDown(self):
    pass

  #@unittest.skip("")
  def test_base(self):
    fPath = os.path.join(self.inFolder, "test_read1.emn")
    idf = pyauto_hw.data.IDF.read(fPath)

    self.assertEqual(len(idf.brd.header), 4)
    self.assertEqual(len(idf.brd.sections), 10)

    self.assertEqual(idf.brd.header[1].split()[0], "BOARD_FILE")

    sData = [
        ".BOARD_OUTLINE  MCAD", ".DRILLED_HOLES", ".PLACE_KEEPOUT      MCAD",
        ".PLACE_KEEPOUT      MCAD", ".ROUTE_OUTLINE  ECAD", ".PLACE_KEEPOUT  MCAD",
        ".PLACE_KEEPOUT  MCAD", ".PLACE_KEEPOUT  MCAD", ".PLACE_KEEPOUT  MCAD",
        ".PLACE_KEEPOUT  MCAD"
    ]
    for i, sect in enumerate(idf.brd.sections):
      self.assertEqual(sect[0], sData[i])

    fOutPath = os.path.join(self.outFolder, "test_write1.emn")
    pyauto_hw.data.IDF.write(fOutPath, idf)
    lstHash = [
        "b6c2cae06773a25a518fe91e6efc140f66ae2c1c8d5211bc7ba95764545156f5",  # linux
        "a1e72526f0bef7d5b3365369cfe8fc91ce1b32021412eae9540fb72a55ea8602",  # win
    ]
    tt.chkHash_file(self, fOutPath, lstHash)

    idf.changeOwner(pyauto_hw.data.IDF.ownerNone)
    for sect in idf.brd.sections:
      tmp = sect[0].split()
      if (len(tmp) > 1):
        self.assertEqual(tmp[1], pyauto_hw.data.IDF.ownerNone)

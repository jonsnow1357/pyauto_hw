#!/usr/bin/env python

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
#import re
#import time
#import datetime

import argparse

appDesc = "pyauto_hw"
parser = argparse.ArgumentParser(description=appDesc)
parser.add_argument("action", help="action", choices=("rates"))
#parser.add_argument("-f", "--cfg", default=appCfgPath,
#                    help="configuration file path")
#parser.add_argument("-l", "--list", action="store_true", default=False,
#                    help="list config file options")
#parser.add_argument("-x", "--extra",
#                    choices=("", ""),
#                    help="extra parameters")

cliArgs = vars(parser.parse_args())
#print("[{}] {}".format(modName, cliArgs))

#parser.print_help()

if (cliArgs["action"] == "rates"):
  import pyauto_hw.standards.commRates
  pyauto_hw.standards.commRates.show()

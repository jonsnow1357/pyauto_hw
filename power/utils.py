#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_hw.CAD.base as CADbase
#import pyauto_hw.power.constants as hwPwrCt
from ._base import *

def getDecouplingInfo(ntl):
  if (not isinstance(ntl, CADbase.Netlist)):
    msg = "INCORRECT Netlist type: {}".format(type(ntl))
    logger.error(msg)
    raise RuntimeError(msg)

  lstRails = []
  for netName in ntl.netNames:
    #print("DBG", netName)
    if (not CADbase.isPower(netName)):
      continue

    rail = PowerRail(netName)
    net = ntl.getNetByName(netName)
    #print("DBG", "process {}".format(netName))
    for refdes in net.refdes:
      if (re.match(CADct.regexPartCap, refdes) is None):
        continue

      cp = ntl.getComponentByRefdes(refdes)
      if (len(cp.netNames) > 2):
        continue

      bGND = False
      if (CADbase.isGround(cp.netNames[0])):
        bGND = True
      if (CADbase.isGround(cp.netNames[1])):
        bGND = True
      if (not bGND):
        continue

      #print("DBG", cp.refdes, cp.paramsCAD)
      if (("local_pn" in cp.paramsCAD)
          and (not pyauto_base.misc.isEmptyString(cp.paramsCAD["local_pn"]))):
        k = cp.paramsCAD["local_pn"]
      elif (("mfr_pn" in cp.paramsCAD)
            and (not pyauto_base.misc.isEmptyString(cp.paramsCAD["mfr_pn"]))):
        k = cp.paramsCAD["mfr_pn"]
      else:
        k = cp.value
      if (k in rail.decoupling.keys()):
        rail.decoupling[k] += 1
      else:
        rail.decoupling[k] = 1
    lstRails.append(rail)
  return lstRails

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import copy
import natsort

logger = logging.getLogger("lib")
from .constants import *
from ._base import *
from ._model import *
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config
import pyauto_base.convert
import pyauto_base.cli
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.part

_MAX_LOOP_CNT = 20

def _chkPowerPort(obj):
  if (not isinstance(obj, PowerPort)):
    msg = "INCORRECT PowerPort type: {}".format(type(obj))
    logger.error(msg)
    raise RuntimeError(msg)

def _chkPowerRail(obj):
  if (not isinstance(obj, PowerRail)):
    msg = "INCORRECT PowerRail type: {}".format(type(obj))
    logger.error(msg)
    raise RuntimeError(msg)

def _chkPowerLoad(obj):
  if (not isinstance(obj, PowerLoad)):
    msg = "INCORRECT PowerLoad type: {}".format(type(obj))
    logger.error(msg)
    raise RuntimeError(msg)

def _chkPowerConverter(obj):
  if (not isinstance(obj, PowerConverter)):
    msg = "INCORRECT PowerConverter type: {}".format(type(obj))
    logger.error(msg)
    raise RuntimeError(msg)

def _chkPowerReport(obj):
  if (not isinstance(obj, PowerReport)):
    msg = "INCORRECT PowerRail type: {}".format(type(obj))
    logger.error(msg)
    raise RuntimeError(msg)

def _updatePowerPort(port, portType=None, portCurrent=None, portNo=None):
  """
  method used for accounting purposes to keep track of all PowerPort changes
  """
  if (portType is not None):
    port.type = portType
  if (portCurrent is not None):
    if (isinstance(portCurrent, float)):
      port.i = portCurrent
    else:
      port.i = float(portCurrent)
  if (portNo is not None):
    port.no = portNo
  logger.debug("~~ update {}".format(port))

def _isMatch_v_loose(obj0, obj1):
  if (isinstance(obj0, float) and isinstance(obj1, float)):
    if (obj0 != obj1):
      return False
    return True
  elif (isinstance(obj0, str) and isinstance(obj1, float)):
    if ((obj0 == "POS") and (obj1 < 0.0)):
      return False
    elif ((obj0 == "NEG") and (obj1 > 0.0)):
      return False
    return True
  elif (isinstance(obj0, float) and isinstance(obj1, str)):
    if ((obj1 == "POS") and (obj0 < 0.0)):
      return False
    elif ((obj1 == "NEG") and (obj0 > 0.0)):
      return False
    return True
  else:
    msg = "UNEXPECTED voltages: {} ({}), {} ({})".format(obj0, type(obj0), obj1, type(obj1))
    logger.error(msg)
    raise RuntimeError(msg)

def _matchPower_v_loose(obj0, obj1):
  if (isinstance(obj0.v, float) and isinstance(obj1.v, float)):
    if (obj0.v != obj1.v):
      return 0
    return 100
  elif (isinstance(obj0.v, str) and isinstance(obj1.v, float)):
    if ((obj0.v == "POS") and (obj1.v < 0.0)):
      return 0
    elif ((obj0.v == "NEG") and (obj1.v > 0.0)):
      return 0
    return 100
  elif (isinstance(obj0.v, float) and isinstance(obj1.v, str)):
    if ((obj1.v == "POS") and (obj0.v < 0.0)):
      return 0
    elif ((obj1.v == "NEG") and (obj0.v > 0.0)):
      return 0
    return 100
  else:
    # yapf: disable
    msg = "UNEXPECTED rail voltages: {} ({}), {} ({})".format(obj0.v, type(obj0.v), obj1.v, type(obj1.v))
    logger.error(msg)
    raise RuntimeError(msg)
    # yapf: enable

def _matchPower_v_exact(obj0, obj1):
  if (isinstance(obj0.v, float) and isinstance(obj1.v, float)):
    if (obj0.v != obj1.v):
      return 0
    return 5
  elif (isinstance(obj0.v, str) and isinstance(obj1.v, float)):
    if ((obj0.v == "POS") and (obj1.v < 0.0)):
      return 0
    elif ((obj0.v == "NEG") and (obj1.v > 0.0)):
      return 0
    return 2
  elif (isinstance(obj0.v, float) and isinstance(obj1.v, str)):
    if ((obj1.v == "POS") and (obj0.v < 0.0)):
      return 0
    elif ((obj1.v == "NEG") and (obj0.v > 0.0)):
      return 0
    return 2
  else:
    # yapf: disable
    msg = "UNEXPECTED rail voltages: {} ({}), {} ({})".format(obj0.v, type(obj0.v), obj1.v, type(obj1.v))
    logger.error(msg)
    raise RuntimeError(msg)
    # yapf: disable

def _match_tags(lstTags0, lstTags1):
  tmp = (10 * len(set(lstTags0) & set(lstTags1)))
  if (tmp >= 100):
    raise NotImplementedError  # will NOT handle more than 10 tags
  return tmp

def _getStats_PortTypes(lstPorts):
  """
  gets the count of each type of PowerPort from a list
  """
  res = dict([(t, 0) for t in ([None] + list(lstPortAll))])
  for pp in lstPorts:
    res[pp.type] += 1
  return res

# yapf: disable
def _showDebug_matchArray(lstRows, lstCols, arr_match):
  sh = arr_match.shape()
  for i in range(0, sh[0]):
    row = arr_match.getRow(i)
    logger.debug("{:<36}".format(lstRows[i]) + "[" + ",".join(["{:3d}".format(t) for t in row]) + "]")
  for i in range(0, sh[1]):
    logger.debug("{:_<36}{}{}{}".format(lstCols[i],
                                        "".join(["____"] * i),
                                        "___|",
                                        "".join(["   |"] * (sh[1] - i - 1))))
# yapf: enable

def fail_PowerLoad(load, msg):
  tmp = "PowerLoad {}: {}".format(load.refdes, msg)
  logger.error(tmp)
  raise RuntimeError(tmp)

class PowerLoad(CADbase.BaseComponent):

  def __init__(self, strRefdes="U?", strValue=""):
    super(PowerLoad, self).__init__(strRefdes=strRefdes, strValue=strValue)
    self.model = None
    self.caseId = "max"
    self.power = None
    self.lstRailNames = []
    self.setPorts = set()

  # yapf: disable
  def _str_list(self):
    res = ["{}:".format(self.__class__.__name__),
           self.refdes, "({})".format(self.value),
           "-W" if (self.power is None) else "{:.3f}W".format(self.power)
           ]
    return res
  # yapf: enable

def fail_PowerConverter(conv, msg):
  tmp = "PowerConverter {},{}: {}".format(conv.refdes, conv.value, msg)
  logger.error(tmp)
  raise RuntimeError(tmp)

class PowerConverter(CADbase.BaseComponent):

  def __init__(self, strRefdes="U?", strValue=""):
    super(PowerConverter, self).__init__(strRefdes=strRefdes, strValue=strValue)
    self.model = None
    self.power = None
    self.process = False
    self.lstRailNames = []
    self.setPorts = set()
    self._efficiency = 0.0  # last calculated efficiency

  def _str_list(self):
    # yapf: disable
    res = ["{}:".format(self.__class__.__name__),
           "{},{} ({})".format(self.refdes, self.value,
                               "-" if (self.model is None) else self.model.type),
           "-W" if (self.power is None) else "{:.3f}W".format(self.power)
           ]
    # yapf: enable
    return res

  @property
  def description(self):
    mdl = self.model.get(None)
    attr = []
    if (self.model.type in (PwrModelDCDC, PwrModelSwitch)):
      attr.append("{:d}%".format(int(self._efficiency * 100)))
    if ("i_max" in mdl.keys()):
      attr.append("{}A".format(mdl["i_max"]))
    if (len(attr) > 0):
      return "{} ({})".format(self.model.type, ", ".join(attr))
    else:
      return self.model.type
      #attr.append("({})".format(",".join(attr)))

  def efficiency(self, vIn, iOut):
    if (self.model.type == PwrModelSwitch):
      self._efficiency = 0.99
    elif (self.model.type == PwrModelDCDC):
      self._efficiency = 0.8
    else:
      raise NotImplementedError
    return self._efficiency

class PowerReport(object):

  def __init__(self):
    self.dataLoad = []
    self.dataConv = []
    self.dataRail = []

class PowerTree(object):

  def __init__(self, name):
    self._modelDB = PowerModelDB()
    self._dictPorts = {}  # {portId: PowerPort, ... }
    self._dictRails = {}  # {railName: PowerRail, ...}
    self._dictLoads = {}  # {refdes: PowerLoad, ...}
    self._dictConv = {}  # {refdes: PowerConverter, ...}
    self._tree = pyauto_base.misc.StringTree()  # tree of PowerRails
    self._setGndNet = set()  # set of GND nets
    self._setIgnoreModel = set()  # list of models we should ignore
    self._cfg = {}  # extra configuration info to help parse the tree
    self.name = name
    self.ts = pyauto_base.misc.getTimestamp()
    self.totalPower = 0.0

    for tmp in ("ports_step0.txt", "ports_step1.txt", "ports_step2.txt", "ports_step3.txt",
                "ports_step4.txt", "ports_fail.txt"):
      try:
        pyauto_base.fs.rm(os.path.join("data_out", tmp))
      except RuntimeError:
        pass

    lib_path = pyauto_hw.part.getPath_LocalDB()
    if (lib_path is not None):
      self.modelPath = os.path.join(lib_path, "power")

  # yapf: disable
  def __str__(self):
    res = [
        "{}{}:".format(self.__class__.__name__, "" if (self.name is None) else (" " + self.name)),
        "{} rail(s),".format(len(self._dictRails)),
        "{} conv(s),".format(len(self._dictConv)),
        "{} load(s)".format(len(self._dictLoads)),
    ]
    return " ".join(res)
  # yapf: enable

  @property
  def modelPath(self):
    return self._modelDB.path

  @modelPath.setter
  def modelPath(self, path):
    self._modelDB.path = path

  @property
  def cfg(self):
    return self._cfg

  @cfg.setter
  def cfg(self, val):
    if (not isinstance(val, dict)):
      raise RuntimeError
    self._cfg = copy.deepcopy(val)

  def _getModel(self, modelName):
    mdl = self._modelDB.get(modelName)
    if (mdl is None):
      return

    mdl.load()
    # TODO: use value from schematic
    #if (mdl.type == PwrModelRes):
    #  mdl.update_ResValue(1e-6)
    return mdl

  def _clear(self):
    self._dictRails = {}
    self._dictConv = {}
    self._dictLoads = {}
    self._tree.clear()

  def _getPart_ModelType(self, refdes):
    try:
      conv = self._dictConv[refdes]
      return conv.model.type
    except KeyError:
      try:
        load = self._dictLoads[refdes]
        return load.model.type
      except KeyError:
        msg = "CANNOT get model type for {}".format(refdes)
        logger.error(msg)
        raise RuntimeError(msg)

  def _getPart_Value(self, refdes):
    try:
      conv = self._dictConv[refdes]
      return conv.value
    except KeyError:
      try:
        load = self._dictLoads[refdes]
        return load.value
      except KeyError:
        msg = "CANNOT get value for {}".format(refdes)
        logger.error(msg)
        raise RuntimeError(msg)

  def _getPortPairs_PowerConverter(self, conv):
    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    lstPortsIn = [pp for pp in lstConvPorts if (pp.type == PwrPortIn)]
    lstPortsOut = [pp for pp in lstConvPorts if (pp.type == PwrPortOut)]

    if (conv.model.type in lstModelsDiscrete):
      return [[lstPortsIn[0], lstPortsOut[0]]]

    res = []
    mdl = conv.model.get(None)
    try:
      for ppin in lstPortsIn:
        for ppout in lstPortsOut:
          # print("DBG", str(ppin))
          # print("DBG", str(ppout))
          # print("DBG", ppin.no, ppout.no, mdl["map"][ppin.no][ppout.no])
          if (mdl["map"][ppin.no][ppout.no]):
            res.append([ppin, ppout])
    except (IndexError, TypeError):
      msg = "CANNOT get port pair for {},{}".format(conv.refdes, conv.value)
      logger.error(msg)
      raise RuntimeError(msg)
    return res

  def _getRailConversion(self, railName1, railName2):
    res = []
    for conv in self._dictConv.values():
      for ppin, ppout in self._getPortPairs_PowerConverter(conv):
        set1 = (ppin.railName, ppout.railName)
        set2 = (railName1, railName2)
        if (set1 == set2):
          res.append(conv.refdes)
    return res

  def getRail(self, railName):
    if (railName not in self._dictRails.keys()):
      msg = "PowerRail {} DOES NOT exist".format(railName)
      logger.error(msg)
      raise RuntimeError(msg)

    return self._dictRails[railName]

  def getParents(self, rail):
    """
    :return: list of PowerRail
    """
    if (isinstance(rail, str)):
      lstRails = self._tree.getParents(rail)
    elif (isinstance(rail, PowerRail)):
      lstRails = self._tree.getParents(rail.name)
    else:
      msg = "INCORRECT type: {}".format(type(rail))
      logger.error(msg)
      raise RuntimeError(msg)

    return [self._dictRails[t] for t in lstRails]

  def getLoad(self, strRefdes):
    try:
      return self._dictLoads[strRefdes]
    except (KeyError, ValueError):
      msg = "NO PowerLoad {}".format(strRefdes)
      logger.error(msg)
      raise RuntimeError(msg)

  def getConverter(self, strRefdes):
    try:
      return self._dictConv[strRefdes]
    except (KeyError, ValueError):
      msg = "NO PowerConverter {}".format(strRefdes)
      logger.error(msg)
      raise RuntimeError(msg)

  def getPort(self, portId):
    return self._dictPorts[portId]

  def getConverters(self, rail, parentRail):
    """
    :return: list of [PowerConverter, ...]
    """
    _chkPowerRail(rail)
    _chkPowerRail(parentRail)

    lstRefdes1 = []
    for pp in [self._dictPorts[t] for t in parentRail.setPorts]:
      if (pp.type == PwrPortIn):
        lstRefdes1.append(pp.refdes)

    lstRefdes2 = []
    for pp in [self._dictPorts[t] for t in rail.setPorts]:
      if (pp.type == PwrPortOut):
        lstRefdes2.append(pp.refdes)

    lstRefdes = list(set(lstRefdes1) & set(lstRefdes2))
    res = []
    for refdes in lstRefdes:
      res.append(self._dictConv[refdes])
    return res

  def getAllRailNames(self):
    return list(self._dictRails.keys())

  def getAllRailNames_preorder(self):
    return self._tree.preorderNames()

  def getAllRailNames_postorder(self):
    return self._tree.postorderNames()

  def getAllIC(self):
    res = list(self._dictLoads.values())
    return res

  def getAllConverters(self):
    res = list(self._dictConv.values())
    return res

  def showLoads(self):
    print("== power load(s):")
    for ic in self._dictLoads.values():
      print(ic, "{} port(s)".format(len(ic.setPorts)))

  def showRails(self):
    print("== power rail(s):")
    for rail in self._dictRails.values():
      print(rail, "{} port(s)".format(len(rail.setPorts)))

  def showConverters(self):
    print("== power converter(s):")
    for conv in self._dictConv.values():
      print(conv, "{} port(s)".format(len(conv.setPorts)))

  def showTree(self):
    self._tree.showTree()

  def _dumpPorts(self, fName):
    fPath = os.path.join(pyauto_base.fs.mkOutFolder(), fName)
    with open(fPath, "w") as fOut:
      for p in self._dictPorts.values():
        fOut.write(str(p) + "\n")
        #fOut.write("PowerPort: {} {} {}({}) {}\n".format(p.id, p.railName, p.refdes, p.no, p.type))
    logger.info("-- {} written".format(fPath))

  def _showDebug_PowerPort(self, port, strVal, bDie=True):
    if (strVal in port.name):
      logger.debug(port)
    if (bDie):
      raise RuntimeError

  def _showDebug_PowerRail(self, rail, railName, bDie=True):
    if (rail.name == railName):
      logger.debug(rail)
      for ppid in rail.setPorts:
        logger.debug(self._dictPorts[ppid])
      if (bDie):
        raise RuntimeError

  def _showDebug_PowerLoad(self, load, refdes, bDie=True):
    if (load.refdes == refdes):
      logger.debug(load)
      for ppid in load.setPorts:
        logger.debug(self._dictPorts[ppid])
      if (bDie):
        raise RuntimeError

  def _showDebug_PowerConverter(self, conv, refdes, bDie=True):
    if (conv.refdes == refdes):
      logger.debug(conv)
      for ppid in conv.setPorts:
        logger.debug(self._dictPorts[ppid])
      if (bDie):
        raise RuntimeError

  def _createDummyLoad(self, railName, current):
    refdes = "UL{:03d}".format(len(self._dictLoads))
    load = PowerLoad(refdes, "dummy")
    load.model = PowerModel()
    load.model.type = PwrModelRes
    self.addPowerLoad(load)

    pp = PowerPort()
    pp.fromName(refdes, railName)
    _updatePowerPort(pp, PwrPortSupply, current)
    self.addPowerPort(pp)

  def addPowerRail(self, obj):
    _chkPowerRail(obj)
    if (obj.name in self._dictRails.keys()):
      msg = "DUPLICATE PowerRail: {}".format(obj.name)
      logger.warning(msg)
      return

    self._tree.addNode(obj.name, None)
    self._dictRails[obj.name] = obj
    logger.debug("~~ add {}".format(obj))

  def addPowerConverter(self, obj):
    _chkPowerConverter(obj)
    if (obj.refdes in self._dictConv.keys()):
      msg = "DUPLICATE PowerConverter: {}".format(obj.refdes)
      logger.warning(msg)
      return

    if (obj.model.type not in (list(lstModelsConverters) + list(lstModelsDiscrete))):
      # yapf: disable
      fail_PowerConverter(obj, "INCORRECT model {} for PowerConverter".format(obj.model.type))
      # yapf: enable

    self._dictConv[obj.refdes] = obj
    logger.debug("~~ add {}".format(obj))

  def addPowerLoad(self, obj):
    _chkPowerLoad(obj)
    if (obj.refdes in self._dictLoads.keys()):
      msg = "DUPLICATE PowerLoad: {}".format(obj.refdes)
      logger.warning(msg)
      return

    if (obj.model.type not in (list(lstModelsLoads) + [PwrModelRes])):
      fail_PowerLoad(obj, "INCORRECT model {} for PowerLoad".format(obj.model.type))

    self._dictLoads[obj.refdes] = obj
    logger.debug("~~ add {}".format(obj))

  def addPowerPort(self, port):
    _chkPowerPort(port)
    if (port.id == 0):
      port.id = len(self._dictPorts) + 1000

    try:
      conv = self._dictConv[port.refdes]
      conv.setPorts.add(port.id)
    except KeyError:
      try:
        load = self._dictLoads[port.refdes]
        load.setPorts.add(port.id)
      except KeyError:
        fail_PowerPort(port, "CANNOT add to tree (refdes)")

    try:
      rail = self._dictRails[port.railName]
      rail.setPorts.add(port.id)
    except KeyError:
      fail_PowerPort(port, "CANNOT add to tree (rail name)")

    self._dictPorts[port.id] = port
    logger.debug("~~ add {}".format(port))

  def delPowerPort(self, port):
    _chkPowerPort(port)

    try:
      rail = self._dictRails[port.railName]
      rail.setPorts.remove(port.id)
    except KeyError:
      pass
    try:
      conv = self._dictConv[port.refdes]
      conv.setPorts.remove(port.id)
    except KeyError:
      pass
    try:
      load = self._dictLoads[port.refdes]
      load.setPorts.remove(port.id)
    except KeyError:
      pass

    self._dictPorts.pop(port.id)
    logger.debug("~~ del {}".format(port))

  def _readXML_PowerRail(self, netName):
    if (not CADbase.isPower(netName)):
      return

    rail = PowerRail(netName)
    self.addPowerRail(rail)

  def _readXML_Discrete(self, refdes, value, lst_info, mdl):
    lst_rail_names = [t[0] for t in lst_info]

    if (len(lst_rail_names) > 2):
      msg = "MORE THAN 2 nets for component {},{}".format(refdes, value)
      logger.error(msg)
      raise RuntimeError(msg)

    if (len(lst_rail_names) < 2):
      raise NotImplementedError  # TODO: add dummy load

    conv = PowerConverter(strRefdes=refdes, strValue=value)
    conv.model = copy.deepcopy(mdl)
    self.addPowerConverter(conv)

    for railName in lst_rail_names:
      pp = PowerPort()
      pp.fromName(conv.refdes, railName)
      self.addPowerPort(pp)

  def _readXML_PowerLoad(self, refdes, value, lst_info, mdl):
    lst_rail_names = [t[0] for t in lst_info]

    # yapf: disable
    if (len(lst_rail_names) == 0):
      msg = "IGNORING PowerLoad {:>6},{:<20} (NO power rails)".format(refdes, value)
      logger.warning(msg)
      return
    # yapf: enable

    load = PowerLoad(strRefdes=refdes, strValue=value)
    load.model = copy.deepcopy(mdl)
    self.addPowerLoad(load)

    for rn in lst_rail_names:
      load.lstRailNames.append(rn)

  def _readXML_PowerConverter(self, refdes, value, lst_info, model):
    lst_rail_names = [t[0] for t in lst_info]

    # yapf: disable
    if (len(lst_rail_names) < 2):
      msg = "IGNORING PowerConverter {:>6},{:<20} (NO power rails)".format(refdes, value)
      logger.warning(msg)
      return
    # yapf: enable

    lst_cfg = []
    for info in lst_info:
      rail_name, port_type, port_no, port_i = info
      if (port_type is not None):
        lst_cfg.append(["m", rail_name, port_type, ("" if (port_no is None) else port_no)])
    if (len(lst_cfg) > 0):
      self._cfg["ports"][refdes] = lst_cfg

    conv = PowerConverter(strRefdes=refdes, strValue=value)
    conv.model = copy.deepcopy(model)
    self.addPowerConverter(conv)

    for rn in lst_rail_names:
      conv.lstRailNames.append(rn)

  def _readXML_part(self, elPart):
    refdes = None
    if (pyauto_base.misc.chkXMLElementHasAttr(elPart, "refdes", bWarn=False)):
      refdes = elPart.attrib["refdes"]
    pyauto_base.misc.chkXMLElementHasAttr(elPart, "value", bDie=True)
    value = elPart.attrib["value"]

    lst_part_info = []
    for el in elPart.findall("port"):
      pyauto_base.misc.chkXMLElementHasAttr(el, "rail", bDie=True)
      rail_name = el.attrib["rail"]
      port_type = None
      if (pyauto_base.misc.chkXMLElementHasAttr(el, "type", bWarn=False)):
        port_type = el.attrib["type"]
      port_no = None
      if (pyauto_base.misc.chkXMLElementHasAttr(el, "no", bWarn=False)):
        port_no = el.attrib["no"]
      current = None
      if (pyauto_base.misc.chkXMLElementHasAttr(el, "current", bWarn=False)):
        current = float(el.attrib["current"])
      lst_part_info.append([rail_name, port_type, port_no, current])

      if (pyauto_base.misc.chkXMLElementHasAttr(el, "startAfter", bWarn=False)):
        rail = self._dictRails[el.attrib["rail"]]
        if (rail.startAfter is None):
          rail.startAfter = el.attrib["startAfter"]
        else:
          msg = "DUPLICATE startAfter for rail {}".format(rail.name)
          logger.error(msg)
          raise RuntimeError(msg)

    mdl = self._getModel(value)
    if (mdl is None):
      if (refdes is None):  # this should only happen for PowerLoads
        _next = (len(self._dictLoads) + len(self._dictConv)) + 1
        refdes = "UL{:03d}".format(_next)
      msg = "model {} for {},{} NOT in database".format(value, refdes, value)
      logger.warning(msg)
      msg = "generating model {} for {},{}".format(value, refdes, value)
      logger.warning(msg)

      model_content = []
      for info in lst_part_info:
        pp = PowerPort()
        pp.fromName(refdes, info[0])
        model_content.append([pp.v, pp.tags, info[3]])
      mdl = mkModel_IC(value, {"max": model_content})

    if (mdl.type in lstModelsDiscrete):
      if (refdes is None):
        _next = (len(self._dictLoads) + len(self._dictConv)) + 1
        refdes = "UD{:03d}".format(_next)
      self._readXML_Discrete(refdes, value, lst_part_info, mdl)
    elif (mdl.type in lstModelsLoads):
      if (refdes is None):
        _next = (len(self._dictLoads) + len(self._dictConv)) + 1
        refdes = "UL{:03d}".format(_next)
      self._readXML_PowerLoad(refdes, value, lst_part_info, mdl)
    elif (mdl.type in list(lstModelsDiscrete + lstModelsConverters)):
      if (refdes is None):
        _next = (len(self._dictLoads) + len(self._dictConv)) + 1
        refdes = "UX{:03d}".format(_next)
      self._readXML_PowerConverter(refdes, value, lst_part_info, mdl)
    else:
      raise RuntimeError

  def readXML(self, fPath):
    import xml.etree.ElementTree as ET

    self._clear()
    self._modelDB.load(projectName=self.name)

    pyauto_base.fs.chkPath_File(fPath)
    logger.info("== reading {}".format(fPath))

    dom = ET.parse(fPath)
    root = dom.getroot()
    if (root.tag != "PowerTree"):
      msg = "INCORRECT PowerTree XML file"
      logger.error(msg)
      raise RuntimeError(msg)

    if (pyauto_base.misc.chkXMLElementHasAttr(root, "name", bWarn=False)):
      self.name = root.attrib["name"]

    logger.info(("== read power rails"))
    lst_rail_names = []
    for elPart in root.findall("part"):
      for elPort in elPart.findall("port"):
        pyauto_base.misc.chkXMLElementHasAttr(elPort, "rail", bDie=True)
        lst_rail_names.append(elPort.attrib["rail"])
    lst_rail_names = list(set(lst_rail_names))  # remove duplicates
    for net_name in lst_rail_names:
      self._readXML_PowerRail(net_name)

    self._cfg["ports"] = {}
    for elPart in root.findall("part"):
      self._readXML_part(elPart)

    self._dumpPorts("ports_step0.txt")

  def writeXML(self, fPath):
    import xml.etree.ElementTree as ET

    elRoot = ET.Element("PowerTree")

    for rail in self._dictRails.values():
      rail.process = False

    for k in sorted(self._dictLoads.keys()):
      load = self._dictLoads[k]
      elLoad = ET.SubElement(elRoot, "part")
      elLoad.set("refdes", load.refdes)
      if (not pyauto_base.misc.isEmptyString(load.value)):
        elLoad.set("value", load.value)
      lstLoadPorts = [self._dictPorts[t] for t in load.setPorts]
      for pp in lstLoadPorts:
        el = ET.SubElement(elLoad, "port")
        el.set("rail", pp.railName)
        el.set("current", str(pp.i))

    for k in sorted(self._dictConv.keys()):
      conv = self._dictConv[k]
      elLoad = ET.SubElement(elRoot, "part")
      elLoad.set("refdes", conv.refdes)
      if (not pyauto_base.misc.isEmptyString(conv.value)):
        elLoad.set("value", conv.value)
      lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
      for pp in lstConvPorts:
        el = ET.SubElement(elLoad, "port")
        el.set("rail", pp.railName)
        rail = self._dictRails[pp.railName]
        if (rail.startAfter is not None):
          if (not rail.process):
            el.set("startAfter", rail.startAfter)
            rail.process = True

    pyauto_base.misc.writeXML_pretty(fPath, elRoot, bEncoding=True)

  def _readNetlist_PowerRail(self, netName):
    if (not CADbase.isPower(netName)):
      return

    rail = PowerRail(netName)
    self.addPowerRail(rail)

  def _readNetlist_Discrete(self, cp, mdl):
    lst_rail_names = []
    for netName in cp.netNames:
      if (netName in self._dictRails.keys()):
        lst_rail_names.append(netName)

    if (len(lst_rail_names) > 2):
      msg = "MORE THAN 2 nets for component {},{}".format(cp.refdes, cp.value)
      logger.error(msg)
      raise RuntimeError(msg)

    if (len(lst_rail_names) < 2):
      return  # ignore components with one rail

    conv = PowerConverter(strRefdes=cp.refdes, strValue=cp.value)
    conv.model = copy.deepcopy(mdl)
    self.addPowerConverter(conv)

    for railName in lst_rail_names:
      pp = PowerPort()
      pp.fromName(conv.refdes, railName)
      self.addPowerPort(pp)

  def _readNetlist_PowerLoad(self, cp, mdl):
    lst_rail_names = []
    for netName in cp.netNames:
      if (netName in self._dictRails.keys()):
        lst_rail_names.append(netName)

    # yapf: disable
    if (len(lst_rail_names) == 0):
      msg = "IGNORING PowerLoad {:>6},{:<20} (NO power rails)".format(cp.refdes, cp.value)
      logger.warning(msg)
      return
    # yapf: enable

    load = PowerLoad(strRefdes=cp.refdes, strValue=cp.value)
    load.model = copy.deepcopy(mdl)
    if (("cases" in self._cfg.keys()) and (load.refdes in self._cfg["cases"].keys())):
      load.caseId = self._cfg["cases"][load.refdes]
      logger.debug("~~ change {},{} case to '{}'".format(load.refdes, load.value,
                                                         load.caseId))
    if (load.caseId not in load.model.cases):
      msg = "NO case '{}' in model for PowerLoad {},{}".format(load.caseId, cp.refdes,
                                                               cp.value)
      logger.error(msg)
      raise RuntimeError(msg)
    for rn in lst_rail_names:
      load.lstRailNames.append(rn)
    self.addPowerLoad(load)

  def _readNetlist_PowerConverter(self, cp, model):
    lst_rail_names = []
    for netName in cp.netNames:
      if (netName in self._dictRails.keys()):
        lst_rail_names.append(netName)

    # yapf: disable
    if (len(lst_rail_names) < 2):
      msg = "IGNORING PowerConverter {:>6},{:<20} (NO power rails)".format(cp.refdes, cp.value)
      logger.warning(msg)
      return
    # yapf: enable

    conv = PowerConverter(strRefdes=cp.refdes, strValue=cp.value)
    conv.model = copy.deepcopy(model)
    self.addPowerConverter(conv)

    for rn in lst_rail_names:
      conv.lstRailNames.append(rn)

  def _readNetlist_Component(self, cp):
    if (re.match(CADct.regexPartRes, cp.refdes) is not None):
      # TODO: maybe test for value instead ???
      if (not pyauto_base.misc.isEmptyString(cp.paramsCAD["footprint"])):
        modelName = "RES_{}".format(cp.paramsCAD["footprint"])
      else:
        modelName = cp.value
    else:
      if (not pyauto_base.misc.isEmptyString(cp.paramsCAD["mfr_pn"])):
        modelName = cp.paramsCAD["mfr_pn"]
      elif (not pyauto_base.misc.isEmptyString(cp.paramsCAD["local_pn"])):
        modelName = cp.paramsCAD["local_pn"]
      else:
        modelName = cp.value
      modelName = modelName.replace("/", "")  # forbidden in file names
      modelName = modelName.replace(":", "_")  # Windows does not allow ':' in file names
      #print("DBG", modelName, cp.refdes, cp.value, cp.paramsCAD)

    mdl = self._getModel(modelName)
    if (mdl is None):
      # yapf: disable
      msg = "IGNORING model {:<32} for {},{} (NOT in database)".format(modelName, cp.refdes, cp.value)
      logger.warning(msg)
      # yapf: enable
      if (modelName != ""):
        self._setIgnoreModel.add(modelName)
      return

    if (mdl.type in lstModelsDiscrete):
      self._readNetlist_Discrete(cp, mdl)
    elif (mdl.type == PwrModelIC):
      self._readNetlist_PowerLoad(cp, mdl)
    elif (mdl.type in lstModelsConverters):
      self._readNetlist_PowerConverter(cp, mdl)
    else:
      msg = "UNKNOWN model {} for {},{}".format(mdl.type, cp.refdes, cp.value)
      logger.error(msg)
      raise RuntimeError(msg)

  def readNetlist(self, ntl):
    if (not isinstance(ntl, CADbase.Netlist)):
      msg = "INCORRECT Netlist type: {}".format(type(ntl))
      logger.error(msg)
      raise RuntimeError(msg)

    self._clear()
    self._modelDB.load(projectName=self.name)

    logger.info(("== read power/gnd rails"))
    for net_name in ntl.netNames:
      self._readNetlist_PowerRail(net_name)
      if (re.match(CADct.regexNetGnd, net_name) is not None):
        self._setGndNet.add(net_name)
    logger.debug("-- GND nets: {}".format(self._setGndNet))

    logger.info(("== create power parts and load models"))
    for refdes, cp in ntl.getAllComponentsByRefdes().items():
      if (cp.value in self._setIgnoreModel):
        #msg = "component {},{} is ignored (cache)".format(cp.refdes, cp.value)
        #logger.warning(msg)
        continue
      if (re.match(CADct.regexPartCap, cp.refdes) is not None):
        #print("DBG", cp, cp.netNames)
        if (len(cp.netNames) == 1):
          continue  # ignore caps with one rail - probably from netlist modification
        set_tmp = set(cp.netNames) & self._setGndNet
        if (len(set_tmp) == 0):
          continue
        rail_name = list(set(cp.netNames) - set_tmp)[0]
        if (rail_name in self._dictRails.keys()):
          pr = self.getRail(rail_name)
          pn = cp.paramsCAD["local_pn"]
          if (pn in pr.decoupling.keys()):
            pr.decoupling[pn] += 1
          else:
            pr.decoupling[pn] = 1
        continue
      if (re.match(CADct.regexPartTP, cp.refdes) is not None):
        #msg = "component {},{} is ignored (refdes)".format(cp.refdes, cp.value)
        #logger.warning(msg)
        continue

      self._readNetlist_Component(cp)
    logger.info(self)
    self._dumpPorts("ports_step0.txt")

  def _check_models(self):
    logger.debug("~~ check models")

    for load in self._dictLoads.values():
      if (load.model is None):
        raise RuntimeError
    for conv in self._dictConv.values():
      if (conv.model is None):
        raise RuntimeError

  def _process_PowerConverter(self, conv, lstBrdCfg):
    """
    Create the correct number of PowerPorts from the model and map them to existing rails
    """
    if (conv.model.type in lstModelsDiscrete):
      return  # skip, they should have the proper number of PowerPorts

    logger.debug("_process_PowerConverter {},{}".format(conv.refdes, conv.value))

    mdl = conv.model.get()
    lst_ports = []
    for i in range(mdl["inputs"]):
      pp = PowerPort(PwrPortIn, refdes=conv.refdes, rail="NC")
      pp.no = i
      lst_ports.append(pp)
    for i in range(mdl["outputs"]):
      pp = PowerPort(PwrPortOut, refdes=conv.refdes, rail="NC")
      pp.no = i
      lst_ports.append(pp)
    if ("supply" in mdl.keys()):
      pp = PowerPort(PwrPortSupply, refdes=conv.refdes, rail="NC")
      pp.i = mdl["supply"][2]
      lst_ports.append(pp)

    if (len(lstBrdCfg) > 0):
      logger.info("-- brd changes for {},{}".format(conv.refdes, conv.value))
    # only delete nets / ports first
    for chg in lstBrdCfg:
      if (chg[0] == "dn"):
        logger.debug("delete net for {},{}".format(conv.refdes, conv.value))
        conv.lstRailNames.remove(chg[1])

    lst_rails = [self.getRail(rn) for rn in conv.lstRailNames]
    if (len(lst_rails) > len(lst_ports)):
      # yapf: disable
      fail_PowerConverter(conv, "TOO MANY power rails: {}".format([r.name for r in lst_rails]))
      # yapf: enable

    # yapf: disable
    arr_match = pyauto_base.misc.SparseMatrix(len(lst_rails), len(lst_ports))
    lst_arr_rows = [pr.name for pr in lst_rails]
    lst_arr_cols = ["_".join(["{:>3}".format(pp.type), "{:.3f}".format(pp.i)]) for pp in lst_ports]
    # yapf: enable

    for chg in lstBrdCfg:
      if (chg[0] == "dn"):
        continue  # already done
      elif (chg[0] == "m"):
        logger.debug("map net to port for {},{}".format(conv.refdes, conv.value))
        net_name, p_type, tmp = chg[1:]
        p_no = []
        if (tmp != ""):
          p_no = [int(t) for t in tmp.split(",")]
        bFound = False
        for i, pr in enumerate(lst_rails):
          for j, pp in enumerate(lst_ports):
            # yapf: disable
            if ((net_name == pr.name) and (p_type == pp.type) and ((p_no == []) or (pp.no in p_no))):
              arr_match.set(i, j, 100)
              bFound = True
            # yapf: enable
        if (not bFound):
          fail_PowerConverter(conv, "CANNOT map port {}".format(chg))
      else:
        fail_PowerConverter(conv, "UNEXPECTED operation {}".format(chg))

    _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)

    if (conv.model.type == PwrModelSwitch):
      if (arr_match.nValues == mdl["inputs"]):
        for i, pr in enumerate(lst_rails):
          for j, pp in enumerate(lst_ports):
            if ((i not in arr_match.rowsWithVal) and (pp.type == PwrPortOut)):
              arr_match.set(i, j, 100)
            if ((i == arr_match.rowsWithVal[0]) and (pp.type == PwrPortSupply)):
              # yapf: disable
              logger.warning("ASSUMING supply for {},{} is {}".format(conv.refdes, conv.value, pr.name))
              arr_match.set(i, j, 100)
              # yapf: enable
        _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)

    rows_wo_val = list(set(range(arr_match.nRows)).difference(set(arr_match.rowsWithVal)))
    cols_wo_val = list(set(range(arr_match.nCols)).difference(set(arr_match.colsWithVal)))
    ports_in_not_set = []
    ports_out_not_set = []
    for c in cols_wo_val:
      if (lst_ports[c].type == PwrPortIn):
        ports_in_not_set.append(c)
      if (lst_ports[c].type == PwrPortOut):
        ports_out_not_set.append(c)
    #print("DBG", rows_wo_val, cols_wo_val, ports_in_not_set, ports_out_not_set)
    # yapf: disable
    if ((len(ports_in_not_set) == 0) and ((len(ports_out_not_set) > 0)) and (len(rows_wo_val) == len(ports_out_not_set))):
      for i in range(len(rows_wo_val)):
        arr_match.set(rows_wo_val[i], ports_out_not_set[i], 100)
    elif ((len(rows_wo_val) == 2) and (len(ports_in_not_set) == 1) and (len(ports_out_not_set) == 1)):
      if (lst_rails[rows_wo_val[0]].v > lst_rails[rows_wo_val[1]].v):
        v_max_idx = rows_wo_val[0]
        v_min_idx = rows_wo_val[1]
      elif (lst_rails[rows_wo_val[1]].v > lst_rails[rows_wo_val[0]].v):
        v_max_idx = rows_wo_val[1]
        v_min_idx = rows_wo_val[0]
      else:
        fail_PowerConverter(conv, "CANNOT match all PowerPorts")

      if ((conv.model.type == PwrModelLDO)
          or (("buck" in mdl.keys()) and (mdl["buck"] is True))):
        arr_match.set(v_max_idx, ports_in_not_set[0], 100)
        arr_match.set(v_min_idx, ports_out_not_set[0], 100)
      elif (("boost" in mdl.keys()) and (mdl["boost"] is True)):
        arr_match.set(v_max_idx, ports_out_not_set[0], 100)
        arr_match.set(v_min_idx, ports_in_not_set[0], 100)
      else:
        _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)
        fail_PowerConverter(conv, "CANNOT match all PowerPorts for buck-boost - update cfg")
    elif ((len(rows_wo_val) == 1) and (len(cols_wo_val) > 0)):
      logger.warning("ASSUMING {} connected to remaining ports".format(
          lst_arr_rows[rows_wo_val[0]]))
      for i in rows_wo_val:
        for j in cols_wo_val:
          arr_match.set(i, j, 100)
    # yapf: enable
    _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)
    arr_match.keepMax_col()
    arr_match.keepMax_row()
    _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)

    if (arr_match.nValues != len(lst_ports)):
      fail_PowerConverter(conv, "CANNOT match all PowerPorts")

    rows_wo_val = list(set(range(arr_match.nRows)).difference(set(arr_match.rowsWithVal)))
    cols_wo_val = list(set(range(arr_match.nCols)).difference(set(arr_match.colsWithVal)))
    if ((len(rows_wo_val) != 0) or (len(cols_wo_val) != 0)):
      fail_PowerConverter(conv, "CANNOT match all PowerPorts")

    for av in arr_match.getAll():
      pr = lst_rails[av[0]]
      pp = lst_ports[av[1]]
      pp.setRailName(pr.name)
    for pp in lst_ports:
      self.addPowerPort(pp)
    conv.lstRailNames = []

  def _process_PowerLoad(self, load, lstBrdCfg):
    """
    Create the correct number of PowerPorts from the model and map them to existing rails
    """
    logger.debug("_process_PowerLoad {},{}".format(load.refdes, load.value))

    mdl = load.model.get(load.caseId)
    lst_ports = []
    for v in mdl:
      pp = PowerPort(PwrPortSupply, refdes=load.refdes, rail="NC")
      pp.v = v[0]
      pp.tags = v[1]
      pp.i = v[2]
      lst_ports.append(pp)
      # print("DBG", pp)

    if (len(lstBrdCfg) > 0):
      logger.info("-- port changes for {},{}".format(load.refdes, load.value))
    # only delete nets / ports first
    for chg in lstBrdCfg:
      if (chg[0] == "dn"):
        logger.debug("delete net {} for {},{}".format(chg[1], load.refdes, load.value))
        load.lstRailNames.remove(chg[1])
      elif (chg[0] == "dp"):
        logger.debug("delete port for {},{}".format(load.refdes, load.value))
        port_v, port_tags = chg[1:]
        port_tags = [t for t in port_tags.split("_") if (t != "")]
        bFound = False
        for pp in lst_ports:
          set_tmp = set(port_tags).difference(set(pp.tags))
          if (pp.v == port_v) and (len(set_tmp) == 0):
            lst_ports.remove(pp)
            bFound = True
            break
        if (not bFound):
          fail_PowerLoad(load, "CANNOT delete port {}".format(chg))
      elif (chg[0] == "m"):
        pass  # will be done later
      elif (chg[0] == "up"):
        pass  # will be done later
      else:
        fail_PowerLoad(load, "UNEXPECTED operation {}".format(chg))

    lst_rails = [self.getRail(rn) for rn in load.lstRailNames]
    if (len(lst_rails) > len(lst_ports)):
      fail_PowerLoad(load, "TOO MANY power rails: {}".format([r.name for r in lst_rails]))

    # yapf: disable
    arr_match = pyauto_base.misc.SparseMatrix(len(lst_rails), len(lst_ports))
    lst_arr_rows = [pr.name for pr in lst_rails]
    lst_arr_cols = ["_".join([str(t.v)] + t.tags) for t in lst_ports]
    # yapf: enable

    logger.debug("~~ match matrix: value and tags")
    for i in range(0, len(lst_rails)):
      for j in range(0, len(lst_ports)):
        max_row_val = arr_match.get(i, j)
        max_row_val += _matchPower_v_loose(lst_rails[i], lst_ports[j])
        max_row_val += _match_tags(lst_rails[i].tags, lst_ports[j].tags)
        arr_match.set(i, j, max_row_val)

    _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)
    arr_match.keepMax_col()
    arr_match.keepMax_row()
    _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)

    logger.debug("~~ match matrix: add mapped ports")
    max_row_val = [max(arr_match.getRow(i)) for i in range(arr_match.nRows)]
    for chg in lstBrdCfg:
      if (chg[0] == "dn"):
        continue  # already done
      elif (chg[0] == "dp"):
        continue  # already done
      elif (chg[0] == "m"):
        logger.debug("map net to port for {},{}".format(load.refdes, load.value))
        net_name, port_v, port_tags = chg[1:]
        port_tags = [t for t in port_tags.split("_") if (t != "")]
        bFound = False
        for i, pr in enumerate(lst_rails):
          for j, pp in enumerate(lst_ports):
            set_tmp = set(port_tags).intersection(set(pp.tags))
            # yapf: disable
            if ((net_name == pr.name) and (port_v == pp.v) and (len(set_tmp) == len(port_tags))):
              if (max_row_val[i] == 0):
                arr_match.set(i, j, (max_row_val[i] + 100))
              elif (max_row_val[i] == 100):
                arr_match.set(i, j, (max_row_val[i] + 10))
              elif (max_row_val[i] > 100):
                arr_match.set(i, j, max_row_val[i])
              bFound = True
            # yapf: enable
        if (not bFound):
          fail_PowerLoad(load, "CANNOT map port {}".format(chg))
      elif (chg[0] == "up"):
        logger.debug("external port update for {}".format(load.refdes))
        port_v, port_tags, port_i = chg[1:]
        port_tags = [t for t in port_tags.split("_") if (t != "")]
        for pp in lst_ports:
          if (port_v == pp.v) and (set(port_tags) == set(pp.tags)):
            if (port_i.endswith("A")):
              pp.i = pyauto_base.convert.value2float(port_i[:-1])
            else:
              pp.i = float(port_i)
            break
      else:
        fail_PowerLoad(load, "UNEXPECTED port operation {}".format(chg))

    _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)
    arr_match.keepMax_col()
    arr_match.keepMax_row()
    _showDebug_matchArray(lst_arr_rows, lst_arr_cols, arr_match)

    if (arr_match.nValues != len(lst_ports)):
      fail_PowerLoad(load, "CANNOT match all PowerPorts")

    rows_wo_val = list(set(range(arr_match.nRows)).difference(set(arr_match.rowsWithVal)))
    cols_wo_val = list(set(range(arr_match.nCols)).difference(set(arr_match.colsWithVal)))
    if ((len(rows_wo_val) != 0) or (len(cols_wo_val) != 0)):
      fail_PowerLoad(load, "CANNOT match all PowerPorts")

    for av in arr_match.getAll():
      pr = lst_rails[av[0]]
      pp = lst_ports[av[1]]
      pp.setRailName(pr.name)
    for pp in lst_ports:
      self.addPowerPort(pp)
    load.lstRailNames = []

  def _processComponents(self):
    logger.info("== process converters")
    for conv in self._dictConv.values():
      lstBrdCfg = []
      if (("ports" in self._cfg.keys()) and (conv.refdes in self._cfg["ports"].keys())):
        lstBrdCfg = self._cfg["ports"][conv.refdes]
      self._process_PowerConverter(conv, lstBrdCfg)
      self._checkPortCnt_conv(conv)
    self._dumpPorts("ports_step1.txt")

    logger.info("== process loads")
    for load in self._dictLoads.values():
      lstBrdCfg = []
      if (("ports" in self._cfg.keys()) and (load.refdes in self._cfg["ports"].keys())):
        lstBrdCfg = self._cfg["ports"][load.refdes]
      self._process_PowerLoad(load, lstBrdCfg)
      self._checkPortTypes_Load(load)
      self._checkPortCur_load(load)
    self._dumpPorts("ports_step2.txt")

  def _updatePowerRailPorts_AllOut(self, lstPorts):
    for port in lstPorts:
      if (port.type is not None):
        continue
      _updatePowerPort(port, portType=PwrPortOut)

  def _updatePowerRailPorts_AllIn(self, lstPorts):
    for port in lstPorts:
      if (port.type is not None):
        continue
      _updatePowerPort(port, portType=PwrPortIn)

  def _updatePowerRailPorts_Model_Out(self, lstPorts, mdlType):
    for port in lstPorts:
      if (port.type is not None):
        continue
      #refdes = port.name.split("|")[0]
      if (self._getPart_ModelType(port.refdes) == mdlType):
        _updatePowerPort(port, portType=PwrPortOut)

  def _updatePowerRailPorts_Model_In(self, lstPorts, mdlType):
    for port in lstPorts:
      if (port.type is not None):
        continue
      #refdes = port.name.split("|")[0]
      if (self._getPart_ModelType(port.refdes) == mdlType):
        _updatePowerPort(port, portType=PwrPortIn)

  def _updateStructure_PowerRail(self, rail):
    lstRailPorts = [self._dictPorts[t] for t in rail.setPorts]
    pp_types = _getStats_PortTypes(lstRailPorts)
    if (len(lstRailPorts) < 2):
      logger.warning("PowerRail {} has less than 2 ports".format(rail.name))
    if (pp_types[None] == 0):
      rail.process = True
      rail.processCnt = _MAX_LOOP_CNT
      return

    lst_refdes = [pp.refdes for pp in lstRailPorts]
    lst_refdes = list(set(lst_refdes))  # remove duplicates
    nRefdes = len(lst_refdes)
    lst_refdes_none = [pp.refdes for pp in lstRailPorts if (pp.type is None)]
    lst_refdes_none = list(set(lst_refdes_none))  # remove duplicates
    nRefdesNone = len(lst_refdes_none)

    part_types = dict([(t, 0) for t in ([None] + list(lstModelsAll))])
    lst_conv_val = []
    for refdes in lst_refdes:
      m_type = self._getPart_ModelType(refdes)
      part_types[m_type] += 1
      if (m_type in lstModelsConverters):
        lst_conv_val.append(self._getPart_Value(refdes))
    lst_conv_val = list(set(lst_conv_val))  # remove duplicates

    nRes = part_types[PwrModelRes]
    nInd = part_types[PwrModelInd]
    nFB = part_types[PwrModelFB]
    nTR = part_types[PwrModelTR]
    nConv = sum([part_types[m] for m in lstModelsConverters])

    # yapf: disable
    logger.debug("_updateStructure_PowerRail {}".format(rail.name))
    logger.debug("  {} ports {}".format(rail.name, pp_types))
    logger.debug("  {} parts {}".format(rail.name, part_types))
    logger.debug("  {} proc: {} n:{} n_conv:{} n_none:{}".format(rail.name,
                                                                 rail.processCnt, nRefdes, nConv, nRefdesNone))
    #self._showDebug_PowerRail(rail, rail.name, bDie=False)
    # yapf: enable

    rail.processCnt += 1
    # we already have some 'in' and 'out' ports
    if ((pp_types[PwrPortIn] != 0) or (pp_types[PwrPortOut] != 0)):
      if (pp_types[None] == 1):
        if ((pp_types[PwrPortIn] > 0) and (pp_types[PwrPortOut] == 0)):
          # the remaining 'none' port must be out
          self._updatePowerRailPorts_AllOut(lstRailPorts)
          return
        if ((pp_types[PwrPortIn] == 0) and (pp_types[PwrPortOut] > 0)):
          # the remaining 'none' port must be in
          self._updatePowerRailPorts_AllIn(lstRailPorts)
          return
        if ((pp_types[PwrPortIn] > 0) and (pp_types[PwrPortOut] > 0)):
          logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
          return

      if ((pp_types[PwrPortIn] > 0) and (pp_types[PwrPortOut] == 0)):
        if (nRefdesNone == 1):
          # all remaining ports are on one refdes, must be 'out'
          self._updatePowerRailPorts_AllOut(lstRailPorts)
          return
        if ((pp_types[None] == nTR) and (nTR < 3)):
          logger.warning("ASSUMING parallel transistors on {}".format(rail.name))
          self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelTR)
          return

      if ((pp_types[PwrPortIn] == 0) and (pp_types[PwrPortOut] > 0)):
        if (pp_types[None] == nRes):
          # all remaining ports are resistors, should be 'in'
          self._updatePowerRailPorts_AllIn(lstRailPorts)
          return
        if (pp_types[None] == nFB):
          # all remaining ports are ferrites, should be 'in'
          self._updatePowerRailPorts_AllIn(lstRailPorts)
          return
        if (pp_types[None] == nInd):
          # all remaining ports are inductors, should be 'in'
          self._updatePowerRailPorts_AllIn(lstRailPorts)
          return

      # no loads, only converters
      if (pp_types[PwrPortSupply] == 0):
        if ((pp_types[PwrPortIn] > 0) and (pp_types[PwrPortOut] == 0)):
          if (nRefdesNone == 1):
            # all remaining ports are on one refdes, must be 'out'
            self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelDCDC)
            return
          if ((pp_types[None] == nRes) and (nRes < 3)):
            logger.warning("ASSUMING parallel res on {}".format(rail.name))
            self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelRes)
            return
          if ((pp_types[None] == nFB) and (nFB < 3)):
            logger.warning("ASSUMING parallel ferrites on {}".format(rail.name))
            self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelFB)
            return
          if ((pp_types[None] == nInd) and (nInd < 3)):
            logger.warning("ASSUMING parallel ind on {}".format(rail.name))
            self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelInd)
            return
        if ((pp_types[PwrPortIn] == 0) and (pp_types[PwrPortOut] > 0)):
          if (pp_types[None] == nRes):
            logger.warning("ASSUMING parallel res on {}".format(rail.name))
            self._updatePowerRailPorts_Model_In(lstRailPorts, PwrModelRes)
            return
        if ((pp_types[PwrPortIn] > 0) and (pp_types[PwrPortOut] > 0)):
          logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
          return

      if (pp_types[None] > 1):
        logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
        return
    else:  # no 'in' or 'out' ports
      if (pp_types[PwrPortSupply] > 0):  # there are some loads
        if ((nConv > 0) and ((nRes + nFB + nInd + nTR) == 0)):  # only IC converters
          if (nConv == 1):  # one converter
            self._updatePowerRailPorts_AllOut(lstRailPorts)
            return
          elif (len(lst_conv_val) == 1):  # more converters but with the same value
            logger.warning("ASSUMING parallel conv on {}".format(rail.name))
            self._updatePowerRailPorts_AllOut(lstRailPorts)
            return
          else:
            logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
            return
        elif ((nConv == 0)
              and ((nRes + nFB + nInd + nTR) == 1)):  # only one discrete converter
          self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelRes)
          self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelFB)
          self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelInd)
          self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelTR)
          return
        elif ((nConv == 0) and ((nFB + nInd + nTR) == 0)
              and (nRes > 1)):  # multiple resistors
          if (rail.processCnt == (_MAX_LOOP_CNT - 2)):
            raise NotImplementedError
          else:
            logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
            return
        elif ((nConv == 0) and ((nRes + nInd + nTR) == 0)
              and (nFB > 1)):  # multiple ferrites
          if (rail.processCnt > (_MAX_LOOP_CNT // 2)):
            logger.warning("ASSUMING parallel ferrites on {}".format(rail.name))
            self._updatePowerRailPorts_Model_Out(lstRailPorts, PwrModelFB)
            return
          else:
            logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
            return
        else:
          logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
          return
      else:  # no loads
        logger.debug("_updateStructure_PowerRail {} NOT updated".format(rail.name))
        return

    self._showDebug_PowerRail(rail, rail.name, bDie=False)
    fail_PowerRail(rail, "PowerRail UNSUPPORTED topology")

  def _updateStructure_PowerConverter_Discrete(self, conv):
    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    pp_types = _getStats_PortTypes(lstConvPorts)

    # yapf: disable
    logger.debug("_updateStructure_PowerConverter_Discrete {},{}".format(conv.refdes, conv.value))
    logger.debug("  {} {}".format(conv.refdes, pp_types))
    # yapf: enable

    if (pp_types[None] == 0):
      if ((pp_types[PwrPortIn] == 1) and (pp_types[PwrPortOut] == 1)):
        ppin, ppout = self._getPortPairs_PowerConverter(conv)[0]
        self._tree.updateNode(ppout.railName, ppin.railName)
        #logger.info("update tree {} -> {}".format(railName_p, railName))
        conv.process = True
        return
      else:
        self._showDebug_PowerConverter(conv, conv.refdes, bDie=False)
        self._dumpPorts("ports_fail.txt")
        fail_PowerConverter(conv, "INCORRECT ports")

    if ((lstConvPorts[0].type is None) and (lstConvPorts[1].type is None)):
      return

    if ((lstConvPorts[0].type == PwrPortOut) and (lstConvPorts[1].type is None)):
      _updatePowerPort(lstConvPorts[1], portType=PwrPortIn)
    elif ((lstConvPorts[0].type == PwrPortIn) and (lstConvPorts[1].type is None)):
      _updatePowerPort(lstConvPorts[1], portType=PwrPortOut)
    elif ((lstConvPorts[0].type is None) and (lstConvPorts[1].type == PwrPortOut)):
      _updatePowerPort(lstConvPorts[0], portType=PwrPortIn)
    elif ((lstConvPorts[0].type is None) and (lstConvPorts[1].type == PwrPortIn)):
      _updatePowerPort(lstConvPorts[0], portType=PwrPortOut)
    else:
      self._showDebug_PowerConverter(conv, conv.refdes, bDie=False)
      self._dumpPorts("ports_fail.txt")
      fail_PowerConverter(conv, "INCORRECT ports")

  def _updateStructure_PowerConverter(self, conv):
    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    pp_types = _getStats_PortTypes(lstConvPorts)
    if (pp_types[None] == 0):
      mdl = conv.model.get()
      set_in_id = set([pp.no for pp in lstConvPorts if (pp.type == PwrPortIn)])
      set_out_id = set([pp.no for pp in lstConvPorts if (pp.type == PwrPortOut)])

      if (len(set_in_id) != mdl["inputs"]):
        self._dumpPorts("ports_fail.txt")
        msg = "INCORRECT in id(s) for {},{}".format(conv.refdes, conv.value)
        logger.error(msg)
        raise RuntimeError(msg)

      if (len(set_out_id) < mdl["outputs"]):
        if (mdl["inputs"] != 1):
          if (len(set_in_id) != mdl["inputs"]):
            self._dumpPorts("ports_fail.txt")
            msg = "INCORRECT in id(s) for {},{}".format(conv.refdes, conv.value)
            raise RuntimeError(msg)
        out_id = 0
        for pp in lstConvPorts:
          if (pp.type == PwrPortOut):
            _updatePowerPort(pp, portNo=out_id)
            out_id += 1

      for ppin, ppout in self._getPortPairs_PowerConverter(conv):
        self._tree.updateNode(ppout.railName, ppin.railName)
        #logger.info("update tree {} -> {}".format(ppin, ppout))
      conv.process = True
      return

    logger.debug("_updateStructure_PowerConverter {},{}".format(conv.refdes, conv.value))
    logger.debug("  {}".format(pp_types))
    #self._showDebug_PowerConverter(conv, conv.refdes, bDie=False)

    mdl = conv.model.get()
    if (pp_types[PwrPortIn] == mdl["inputs"]):
      for pp in lstConvPorts:
        if (pp.type is None):
          _updatePowerPort(pp, portType=PwrPortOut)
    elif (pp_types[PwrPortOut] == mdl["outputs"]):
      for pp in lstConvPorts:
        if (pp.type is None):
          _updatePowerPort(pp, portType=PwrPortIn)
    elif (pp_types[None] == 1):
      if (pp_types[PwrPortIn] == (mdl["inputs"] - 1)):
        for pp in lstConvPorts:
          if (pp.type is None):
            _updatePowerPort(pp, portType=PwrPortIn)
      elif (pp_types[PwrPortOut] == (mdl["outputs"] - 1)):
        for pp in lstConvPorts:
          if (pp.type is None):
            _updatePowerPort(pp, portType=PwrPortOut)
      else:
        raise RuntimeError
    else:
      logger.debug("_updateStructure_PowerConverter {},{} NOT updated".format(
          conv.refdes, conv.value))
      return

  def _updateStructure(self):
    for rail in self._dictRails.values():
      rail.process = False
      rail.processCnt = 0
    for conv in self._dictConv.values():
      conv.process = False
    logger.info("-- update structure")

    while (1):
      railCnt = 0
      for name, rail in self._dictRails.items():
        if (rail.process):
          continue
        self._updateStructure_PowerRail(rail)
        if (rail.processCnt > _MAX_LOOP_CNT):
          self._dumpPorts("ports_fail.txt")
          msg = "CANNOT update structure for rail {}".format(rail.name)
          logger.error(msg)
          raise RuntimeError(msg)
        railCnt += 1

      convCnt = 0
      # give priority to the discrete converters
      for refdes, conv in self._dictConv.items():
        if (conv.process):
          continue
        if (conv.model.type in lstModelsDiscrete):
          self._updateStructure_PowerConverter_Discrete(conv)
          convCnt += 1

      for refdes, conv in self._dictConv.items():
        if (conv.process):
          continue
        if (conv.model.type not in lstModelsDiscrete):
          self._updateStructure_PowerConverter(conv)
          convCnt += 1

      logger.debug("~~ loop {} {}".format(railCnt, convCnt))
      if ((railCnt == 0) and (convCnt == 0)):
        break

  def _check_rails(self):
    logger.info("-- check rails")
    for rail in self._dictRails.values():
      _chkPowerRail(rail)
      lstRailPorts = [self._dictPorts[t] for t in rail.setPorts]
      if (len(lstRailPorts) == 0):
        fail_PowerRail(rail, "NO ports")
      pp_types = _getStats_PortTypes(lstRailPorts)

      if (pp_types[None] != 0):
        fail_PowerRail(rail, "undefined ports")
      if (len(lstRailPorts) == pp_types[PwrPortOut]):
        logger.warning("{} has ONLY out ports, ADD dummy load".format(rail.name))
        self._createDummyLoad(rail.name, 0.01)
        #fail_PowerRail(rail, "has ONLY out ports")
      if (len(lstRailPorts) == pp_types[PwrPortSupply]):
        fail_PowerRail(rail, "has ONLY pwr ports")

      for port in lstRailPorts:
        if (port.railName != rail.name):
          fail_PowerPort(port, "")
      #rail.check()

  def _checkPortTypes_Load(self, load):
    """
    All PowerLoad ports should be defined as PwrPortSupply
    """
    _chkPowerLoad(load)
    if (len(load.setPorts) == 0):
      fail_PowerLoad(load, "NO ports")

    lstLoadPorts = [self._dictPorts[t] for t in load.setPorts]
    for port in lstLoadPorts:
      if (port.refdes != load.refdes):
        fail_PowerPort(port, "UNEXPECTED refdes")
      if (port.type != PwrPortSupply):
        fail_PowerPort(port, "INCORRECT type {}".format(port.type))

  def _checkPortCur_load(self, load):
    """
    All PowerLoad ports should have a current != 0.0
    """
    _chkPowerLoad(load)
    if (len(load.setPorts) == 0):
      fail_PowerLoad(load, "NO ports")

    lstLoadPorts = [self._dictPorts[t] for t in load.setPorts]
    for port in lstLoadPorts:
      if (port.refdes != load.refdes):
        fail_PowerPort(port, "UNEXPECTED refdes")
      if (port.i == 0.0):
        fail_PowerPort(port, "current MUST BE != 0")

  def _checkPortCnt_conv(self, conv):
    """
    All PowerConverters ports should match model:
      - port count and port numbers
      - if a supply port exists the type should be PwrPortSupply and have a current != 0.0
    """
    _chkPowerConverter(conv)
    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    if (len(lstConvPorts) == 0):
      fail_PowerConverter(conv, "NO ports")

    mdl = conv.model.get()
    if (conv.model.type in lstModelsDiscrete):
      if (len(lstConvPorts) != 2):
        fail_PowerConverter(conv, "ports DO NOT match discrete model")
      return

    nPorts_supply = 0
    nPorts_non_supply = 0
    i_supply = 0.0
    for port in lstConvPorts:
      if (port.type == PwrPortSupply):
        nPorts_supply += 1
        i_supply = port.i
      else:
        nPorts_non_supply += 1

    lstExpectedNo = []
    if ("supply" in mdl.keys()):
      if (nPorts_supply != 1):
        fail_PowerConverter(conv, "ports DO NOT match model (supply)")
      if (i_supply == 0.0):
        fail_PowerConverter(conv, "NO supply current")
      lstExpectedNo.append(0)
    if (nPorts_non_supply != (mdl["inputs"] + mdl["outputs"])):
      for port in lstConvPorts:
        logger.error(port)
      fail_PowerConverter(conv, "ports DO NOT match model (total)")
    lstExpectedNo += range(mdl["inputs"])
    lstExpectedNo += range(mdl["outputs"])

    actual_no = ",".join([str(t) for t in sorted([p.no for p in lstConvPorts])])
    expected_no = ",".join([str(t) for t in sorted(lstExpectedNo)])
    if (actual_no != expected_no):
      for port in lstConvPorts:
        logger.error(port)
      fail_PowerConverter(conv, "port numbers DO NOT match model")

  def _check_conv(self):
    """
    All PowerConverters ports should be
      - defined as PwrPortSupply and have a current != 0.0
      - defined as PwrPortIn or PwrPortOut and have a current != 0
      - ids should match model counts
    """
    logger.info("-- check converters")
    for conv in self._dictConv.values():
      _chkPowerConverter(conv)
      lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
      if (len(lstConvPorts) == 0):
        fail_PowerConverter(conv, "NO ports")

      pp_types = _getStats_PortTypes(lstConvPorts)
      mdl = conv.model.get()
      if (conv.model.type in lstModelsDiscrete):
        if ((pp_types[PwrPortIn] != 1) and (pp_types[PwrPortOut] != 1)):
          fail_PowerConverter(conv, "ports DO NOT match model")
      else:
        if (pp_types[PwrPortIn] > mdl["inputs"]):
          fail_PowerConverter(conv, "ports DO NOT match model (inputs)")
        if (pp_types[PwrPortOut] > mdl["outputs"]):
          fail_PowerConverter(conv, "ports DO NOT match model (outputs)")
        if ("supply" in mdl.keys()):
          if (pp_types[PwrPortSupply] != 1):
            fail_PowerConverter(conv, "ports DO NOT match model (supply)")

      set_portin = set()
      set_portout = set()
      for port in lstConvPorts:
        if (port.refdes != conv.refdes):
          fail_PowerPort(port, "")
        if (port.type is None):
          fail_PowerPort(port, "INCORRECT type {}".format(port.type))
        if (port.type == PwrPortIn):
          if (port.no in set_portin):
            fail_PowerPort(port, "duplicate IN id".format(port.no))
          set_portin.add(port.no)
          if (port.i != 0.0):
            fail_PowerPort(port, "current must be 0.0".format(port.type))
        if (port.type == PwrPortOut):
          if (port.no in set_portout):
            fail_PowerPort(port, "duplicate OUT id".format(port.no))
          set_portout.add(port.no)
          if (port.i != 0.0):
            fail_PowerPort(port, "current must be 0.0".format(port.type))
        if (port.type == PwrPortSupply):
          if (port.i == 0.0):
            fail_PowerPort(port, "current MUST BE != 0")
      if (conv.model.type not in lstModelsDiscrete):
        if (not set_portin <= set(list(range(0, mdl["inputs"])))):
          fail_PowerConverter(conv, "INCORRECT in ids")
        if (not set_portout <= set(list(range(0, mdl["outputs"])))):
          fail_PowerConverter(conv, "INCORRECT in ids")

  def build(self):
    self._check_models()
    self._processComponents()
    self._updateStructure()
    self._dumpPorts("ports_step3.txt")
    self._check_rails()
    self._check_conv()
    #logger.info(self)

  def _update_PowerLoad(self, load):
    load.power = 0.0
    lstLoadPorts = [self._dictPorts[t] for t in load.setPorts]

    for port in lstLoadPorts:
      tmp = port.v * port.i
      if (tmp < 0.0):
        fail_PowerPort(port, "INCORRECT power {} V, {} A".format(port.v, port.i))
      load.power += tmp

    if (load.power == 0.0):
      fail_PowerLoad(load, "NO power consumption")

  def _update_PowerRail(self, rail):
    lstRailPorts = [self._dictPorts[t] for t in rail.setPorts]
    logger.debug("_update_PowerRail {}".format(rail.name))
    #self._showDebug_PowerRail(rail, rail.name, bDie=False)
    lstInCurr = [pp.i for pp in lstRailPorts if (pp.type == PwrPortIn)]
    if (0.0 in lstInCurr):
      rail.processCnt += 1
      return

    i_tmp = 0.0
    n_out = 0
    for port in lstRailPorts:
      if (port.type == PwrPortOut):
        n_out += 1
        continue
      i_tmp += port.i

    rail.i = i_tmp
    if (rail.i == 0.0):
      fail_PowerRail(rail, "NO power consumption")

    if (n_out != 0):  # not a root power rail
      i_tmp = (i_tmp / n_out)
      for port in lstRailPorts:
        if (port.type == PwrPortOut):
          port.i = i_tmp
    rail.process = True
    #self._showDebug_PowerRail(rail, "P0V85_DLY", bDie=False)

  def _update_PowerConverter_Discrete(self, conv):
    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    #logger.debug("_update_PowerConverter_Discrete {},{}".format(conv.refdes, conv.value))
    lstOutCurr = [pp.i for pp in lstConvPorts if (pp.type == PwrPortOut)]
    if (0.0 in lstOutCurr):
      return

    pp_in, pp_out = self._getPortPairs_PowerConverter(conv)[0]
    pp_in.i = pp_out.i

    conv.power = 0.0
    mdl = conv.model.get()
    if (conv.model.type == PwrModelRes):
      pass  # TODO: values should come from schematic
    elif (conv.model.type == PwrModelFB):
      conv.power = pp_out.i * pp_out.i * mdl["dcr"]
    elif (conv.model.type == PwrModelInd):
      conv.power = pp_out.i * pp_out.i * mdl["dcr"]
    elif (conv.model.type == PwrModelTR):
      if ("r_dson" in mdl.keys()):
        conv.power = pp_out.i * pp_out.i * mdl["r_dson"]
      elif ("v_cesat" in mdl.keys()):
        conv.power = pp_out.i * mdl["v_cesat"]
      else:
        raise NotImplementedError
    else:
      msg = "UNSUPPORTED PowerConverter: {}".format(conv.model.type)
      logger.error(msg)
      raise RuntimeError(msg)
    conv.process = True
    #self._showDebug_PowerConverter(conv, "R1093", bDie=False)

  def _update_PowerConverter(self, conv):
    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    logger.debug("_update_PowerConverter {},{}".format(conv.refdes, conv.value))
    #self._showDebug_PowerConverter(conv, conv.refdes, bDie=False)
    lstOutCurr = [pp.i for pp in lstConvPorts if (pp.type == PwrPortOut)]
    if (0.0 in lstOutCurr):
      return

    conv.power = 0.0
    mdl = conv.model.get()
    for ppin, ppout in self._getPortPairs_PowerConverter(conv):
      #print("DBG", ppin.name, ppout.name)
      if (conv.model.type == PwrModelLDO):
        ppin.i += ppout.i
        conv.power += (ppin.v - ppout.v) * ppout.i
      elif (conv.model.type in (PwrModelDCDC, PwrModelSwitch)):
        p_out = ppout.v * ppout.i
        if (("efficiencies" in self._cfg.keys())
            and (conv.refdes in self._cfg["efficiencies"].keys())):
          eff = self._cfg["efficiencies"][conv.refdes]
          conv._efficiency = eff
        else:
          eff = conv.efficiency(ppin.v, ppout.i)
        p_in = p_out / eff
        ppin.i += p_in / ppin.v
        conv.power += (p_in - p_out)
      else:
        raise NotImplementedError
    if ("supply" in mdl.keys()):
      for pp in lstConvPorts:
        if (pp.type == PwrPortSupply):
          conv.power += pp.v * pp.i
    conv.process = True
    #self._showDebug_PowerConverter(conv, "SW4")

  def _derate_current(self, conv, port, derateFactor=0.8):
    mdl = conv.model.get(None)

    # yapf: disable
    if (port.i > mdl["i_max"]):
      msg = "{} port {} i_out ({} A) BIGGER than i_max ({} A)".format(conv, port.name, port.i, mdl["i_max"])
      logger.warning(msg)
    elif (port.i > (derateFactor * mdl["i_max"])):
      msg = "{} port {} i_out ({} A) TOO CLOSE to i_max ({} A)".format(conv, port.name, port.i, mdl["i_max"])
      logger.warning(msg)
    # yapf: enable

  def _derate_power(self, conv, derateFactor=0.8):
    mdl = conv.model.get(None)

    # yapf: disable
    if (conv.power > mdl["p_max"]):
      msg = "{}: power ({} W) BIGGER than p_max ({} W)".format(conv, conv.power, mdl["p_max"])
      logger.warning(msg)
    elif (conv.power > (derateFactor * mdl["p_max"])):
      msg = "{}: power ({} W) TOO CLOSE to p_max ({} W)".format(conv, conv.power, mdl["p_max"])
      logger.warning(msg)
    # yapf: enable

  def _deratePowerConverter_Switch(self, conv):
    if (conv.power <= 0.0):
      raise NotImplementedError

    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    lstPortsOut = [t for t in lstConvPorts if (t.type == PwrPortOut)]
    for port in lstPortsOut:
      self._derate_current(conv, port)

  def _deratePowerConverter_LDO(self, conv):
    if (conv.power <= 0.0):
      logger.warning("LDO {},{} dissipates NO power".format(conv.refdes, conv.value))
    #self._showDebug_PowerConverter(conv, conv.refdes)

    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    lstPortsOut = [t for t in lstConvPorts if (t.type == PwrPortOut)]
    for port in lstPortsOut:
      self._derate_current(conv, port)

  def _deratePowerConverter_TR(self, conv):
    if (conv.power <= 0.0):
      logger.warning("transistor {},{} dissipates NO power".format(conv.refdes, conv.value))
      raise NotImplementedError

    self._derate_power(conv)

  def _deratePowerConverter_IND(self, conv):
    if (conv.power <= 0.0):
      raise NotImplementedError

    lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
    lstPortsOut = [t for t in lstConvPorts if (t.type == PwrPortOut)]
    self._derate_current(conv, lstPortsOut[0])

  def _deratePowerConverter_RES(self, conv):
    if (conv.power <= 0.0):
      raise NotImplementedError

    self._derate_power(conv)

  def _deratePowerConverter(self, conv):
    if (conv.model.type in (PwrModelDCDC, PwrModelSwitch)):
      self._deratePowerConverter_Switch(conv)
    elif (conv.model.type == PwrModelLDO):
      self._deratePowerConverter_LDO(conv)
    elif (conv.model.type == PwrModelTR):
      self._deratePowerConverter_TR(conv)
    elif (conv.model.type in (PwrModelFB, PwrModelInd)):
      self._deratePowerConverter_IND(conv)
    elif (conv.model.type == PwrModelRes):
      pass  # TODO:
    else:
      msg = "UNSUPPORTED PowerConverter: {}".format(conv.model.type)
      logger.error(msg)
      raise RuntimeError(msg)

  def updatePower(self):
    for rail in self._dictRails.values():
      rail.process = False
      rail.processCnt = 0
    for conv in self._dictConv.values():
      conv.process = False
    self.totalPower = 0.0

    logger.info("-- update power for loads")
    for ic in self._dictLoads.values():
      self._update_PowerLoad(ic)
      self.totalPower += ic.power
    #logger.info("Total power: {:.3f}W".format(self.totalPower))

    logger.info("-- update power for rails")
    while (1):
      railCnt = 0
      for rail_name in self._tree.postorderNames():
        rail = self._dictRails[rail_name]
        if (rail.process):
          continue
        self._update_PowerRail(rail)
        if (rail.processCnt > _MAX_LOOP_CNT):
          self._dumpPorts("ports_fail.txt")
          msg = "CANNOT update power for rail {}".format(rail.name)
          logger.error(msg)
          raise RuntimeError(msg)
        railCnt += 1

      convCnt = 0
      for conv in self._dictConv.values():
        if (conv.process):
          continue

        if (conv.model.type in lstModelsDiscrete):
          self._update_PowerConverter_Discrete(conv)
        else:
          self._update_PowerConverter(conv)
        convCnt += 1

      logger.debug("~~ loop {} {}".format(railCnt, convCnt))
      if ((railCnt == 0) and (convCnt == 0)):
        break

    self._dumpPorts("ports_step4.txt")
    for conv in self._dictConv.values():
      self._deratePowerConverter(conv)
      self.totalPower += conv.power

    self.ts = pyauto_base.misc.getTimestamp()
    logger.info("Total power: {:.3f}W".format(self.totalPower))

  def updateStart(self):
    logger.info("--update start time")

    # check for incorrect startAfter attributes
    for rail_name in self._tree.preorderNames():
      rail = self._dictRails[rail_name]
      lstParents = self.getParents(rail)
      rail.t_0 = 0.0
      rail.t_on = rail.default_t_on
      if (lstParents == []):
        continue
      elif (len(lstParents) == 1):
        lstConv = self.getConverters(rail, lstParents[0])
        for conv in lstConv:
          if (conv.model.type in (PwrModelInd, PwrModelRes)
              and (rail.startAfter is not None)):
            msg = "PowerRail {} CANNOT startAfter with PowerConverter {}".format(
                rail.name, conv.model.type)
            logger.error(msg)
            raise RuntimeError(msg)
      else:
        raise NotImplementedError

    bUpdated = True
    cntUpdate = 0
    while (bUpdated):
      bUpdated = False
      for rail_name in self._tree.preorderNames():
        rail = self._dictRails[rail_name]
        lstParents = self.getParents(rail)
        if (lstParents == []):
          pass
        elif (len(lstParents) == 1):
          lstConv = self.getConverters(rail, lstParents[0])
          for conv in lstConv:
            if (conv.model.type in (PwrModelDCDC, PwrModelSwitch, PwrModelLDO, PwrModelTR)):
              if (rail.t_on <= lstParents[0].t_on):
                rail.t_0 = lstParents[0].t_on
                bUpdated = True
            else:
              if (rail.t_0 != lstParents[0].t_0):
                rail.t_0 = lstParents[0].t_0
                bUpdated = True
        else:
          raise NotImplementedError

        if (rail.startAfter is not None):
          # TODO: check child relationship
          enRail = self._dictRails[rail.startAfter]
          if (rail.t_on <= enRail.t_on):
            rail.t_0 = enRail.t_on
            bUpdated = True
      cntUpdate += 1
      if (cntUpdate > len(self._dictRails)):
        msg = "CANNOT update start times (convergence)"
        logger.error(msg)
        raise RuntimeError(msg)

    # check for inconsistent timing
    for rail_name in self._tree.preorderNames():
      rail = self._dictRails[rail_name]
      lstParents = self.getParents(rail)
      if (lstParents == []):
        if (rail.t_0 != 0.0):
          msg = "PowerRail {} is root and delayed".format(rail.name)
          logger.warning(msg)
      elif (len(lstParents) == 1):
        lstConv = self.getConverters(rail, lstParents[0])
        for conv in lstConv:
          if (conv.model.type in (PwrModelDCDC, PwrModelLDO, PwrModelTR)):
            if (rail.t_0 <= lstParents[0].t_0):
              msg = "PowerRail {} CANNOT start before parent {}".format(
                  rail.name, lstParents[0].name)
              logger.error(msg)
              raise RuntimeError(msg)
      else:
        raise NotImplementedError

  def _getReportData_IC(self, report):
    railNames = sorted(self._dictRails.keys())
    report.dataLoad.append(["Refdes", "Value", "Case", "power [W]"]
                           + ["I_{} [A]".format(t) for t in railNames])
    i_rail = len(railNames) * [0.0]
    pwr_t = 0.0
    for refdes in natsort.natsorted(self._dictLoads.keys()):
      load = self._dictLoads[refdes]
      lstLoadPorts = [self._dictPorts[t] for t in load.setPorts]
      row = [load.refdes, load.value, load.caseId, "-"] + (len(railNames) * ["-"])
      for i in range(0, len(railNames)):
        for pp in lstLoadPorts:
          if (pp.railName == railNames[i]):
            i_p = pp.i
            row[i + 4] = str(round(i_p, 4))
            i_rail[i] += i_p
            break
      #print("DBG", row)
      pwr_c = round(load.power, 3)
      pwr_t += pwr_c
      row[3] = str(pwr_c)
      report.dataLoad.append(row)

    #print("DBG", report.dataLoad)
    report.dataLoad.append(["TOTAL", "-", "-", str(round(pwr_t, 3))]
                           + [str(round(t, 4)) for t in i_rail])

  def _getReportData_Convertes(self, report):
    report.dataConv.append([
        "Converter", "Value", "Type", "power [W]", "Rail In", "I_IN [A]", "Rail Out",
        "I_OUT [A]"
    ])
    pwr_t = 0.0
    for refdes in natsort.natsorted(self._dictConv.keys()):
      conv = self._dictConv[refdes]
      for ppin, ppout in self._getPortPairs_PowerConverter(conv):
        i_i = round(ppin.i, 4)
        i_o = round(ppout.i, 4)
        # yapf: disable
        report.dataConv.append([conv.refdes, conv.value, conv.description, "-",
                                ppin.railName, str(i_i), ppout.railName, str(i_o)])
        # yapf: enable
      if (conv.power > 0.0):
        pwr_c = round(conv.power, 3)
        report.dataConv.append(
            [conv.refdes, conv.value, conv.description,
             str(pwr_c), "-", "-", "-", "-"])
        pwr_t += pwr_c
    report.dataConv.append(["TOTAL", "-", "-", str(round(pwr_t, 3)), "-", "-", "-", "-"])

  def _getReportData_Rails(self, report):
    for rail in self._dictRails.values():
      rail.process = False
    report.dataRail.append(["PowerRail", "current [A]"])

    for rail_name in self._tree.preorderNames():
      rail = self._dictRails[rail_name]
      if (rail.process):
        continue
      report.dataRail.append([rail.name, str(round(rail.i, 4))])
      rail.process = True

  def writeReport_CSV(self, fPath):
    report = PowerReport()
    self._getReportData_IC(report)
    self._getReportData_Convertes(report)
    self._getReportData_Rails(report)

    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")

      for row in report.dataLoad:
        csvOut.writerow(row)

      csvOut.writerow([])
      csvOut.writerow([])
      csvOut.writerow([])

      for row in report.dataConv:
        csvOut.writerow(row)

      csvOut.writerow([])
      csvOut.writerow([])
      csvOut.writerow([])

      for row in report.dataRail:
        csvOut.writerow(row)

    logger.info("{} written".format(fPath))

  def writeReport_HTML(self, fPath):
    report = PowerReport()
    self._getReportData_IC(report)
    self._getReportData_Convertes(report)
    self._getReportData_Rails(report)

    dictTpl = {
        "dictInfo": {
            "name": self.name,
            "timestamp": self.ts,
            "totalPower": str(round(self.totalPower, 3))
        },
        "lstPaths": [(os.path.basename(fPath)[:-5] + ".csv"),
                     (os.path.basename(fPath)[:-5] + "_graph.png")],
        "lstLoads":
        report.dataLoad,
        "lstConvs":
        report.dataConv,
        "lstRails":
        report.dataRail
    }

    fInPath = os.path.join(os.path.dirname(__file__), "templates", "power_report.tpl")
    pyauto_base.misc.templateWheezy(fInPath, fPath, dictTpl=dictTpl)

  def writeReport_sense_CSV(self, fPath):
    if ("sense" not in self._cfg.keys()):
      logger.warning("NO power sense information")
      return
    if ("NB" not in self._cfg["sense"].keys()):
      logger.warning("NO NB in power_sense")
      return
    if ("Vref" not in self._cfg["sense"].keys()):
      logger.warning("NO Vref in power_sense")
      return
    ADC_NB = int(self._cfg["sense"]["NB"])
    ADC_Vref = float(self._cfg["sense"]["Vref"])

    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      csvOut.writerow([
          "REFDES", "VALUE [ohm]", "RAIL_IN", "RAIL_OUT", "VOLTAGE [V]", "CURRENT [A]",
          "POWER [mW]", "DROP [mV]", "DROP [%]", "CORRECTION", "ADC VOLTAGE [mV]", "ADC NB",
          "ADC VREF [V]", "ADC STEP [uV]", "ADC MAX VALUE"
      ])

      for conv in self._dictConv.values():
        if (conv.model.type != PwrModelRes):
          continue
        if (re.match(r"RES_.*(SPECIAL|SENSE).*", conv.model.name) is None):
          continue
        try:
          r = float(conv.value)
          # if ((r == 0) or (r > 0.5)):
          #   continue
        except ValueError:
          continue

        lstConvPorts = [self._dictPorts[t] for t in conv.setPorts]
        corr = 1.0
        if (len(lstConvPorts) != 2):
          raise RuntimeError
        for pp in lstConvPorts:
          if (pp.type == PwrPortIn):
            rail_in = pp.railName
            v = pp.v
            i = round(pp.i, 2)
            p = round((r * i * i * 1e3), 2)
            drop = round((r * i * 1e3), 3)
          else:
            rail_out = pp.railName
            continue
          drop_rel = round((drop / (v * 1e3)) * 100, 2)
          adc_v = round((corr * drop), 3)
          adc_lsb = round(((ADC_Vref * 1e6) / ((2**ADC_NB) - 1)), 1)
          adc_val = int((adc_v / (ADC_Vref * 1e3)) * ((2**ADC_NB) - 1))

        csvOut.writerow([
            conv.refdes,
            conv.value,
            rail_in,
            rail_out,
            v,
            i,
            p,
            drop,
            drop_rel,
            corr,
            adc_v,
            str(ADC_NB),
            str(ADC_Vref),
            adc_lsb,
            hex(adc_val),
        ])

    logger.info("{} written".format(fPath))

  def writeDot(self, fPath):
    dict_edge_colors = {
        PwrModelRes: "black",
        PwrModelFB: "black",
        PwrModelInd: "black",
        PwrModelTR: "orange",
        PwrModelLDO: "blue",
        PwrModelDCDC: "red",
        PwrModelSwitch: "orange",
    }
    res = pyauto_base.cli.callGenericCmd("dot", ["-V"])
    if (res != 0):
      msg = "CANNOT FIND dot (graphviz)"
      logger.warning(msg)
      return

    with open(fPath, "w") as fOut:
      fOut.write("digraph {\n")
      fOut.write("  rankdir = \"LR\"\n")
      fOut.write("  label = \"{} ({})\"\n".format(self.name, self.ts))
      fOut.write("\n")

      for rail_name, rail in self._dictRails.items():
        fOut.write("  \"{}\" [label=\"{}\\n{:.3f} A\"]\n".format(
            rail_name, rail.name, rail.i))
      fOut.write("\n")

      for rail_name in self.getAllRailNames_preorder():
        child_rail_names = self._tree.getChildren(rail_name)
        for ch_name in child_rail_names:
          lst_refdes = self._getRailConversion(rail_name, ch_name)
          conversion_type = [self._dictConv[t].model.type for t in lst_refdes]
          conversion_type = list(set(conversion_type))  # remove duplicates
          if (len(conversion_type) != 1):
            msg = "UNEXPECTED conversion from {} -> {}: {}".format(
                rail_name, ch_name, conversion_type)
            logger.error(msg)
            raise RuntimeError(msg)
          fOut.write("  \"{}\" -> \"{}\" [color=\"{}\"]\n".format(
              rail_name, ch_name, dict_edge_colors[conversion_type[0]]))
      fOut.write("}\n")
    logger.info("{} written".format(fPath))
    fImgPath = fPath[:-3] + "png"
    res = pyauto_base.cli.callGenericCmd("dot", ["-Tpng", fPath, "-o", fImgPath])
    if (res == 0):
      logger.info("{} written".format(fImgPath))
    else:
      raise RuntimeError

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from .constants import *
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct

class PowerException(Exception):
  pass

def _parseName_0(strName):
  tmp = re.split(CADct.regexNetPwr[0], strName)
  #print("DBG", sys._getframe().f_code.co_name, strName, tmp)
  if (len(tmp) != 4):
    return [0.0, []]
  v = float(tmp[1].replace("V", "."))
  if (tmp[2] != ""):
    tags = [t for t in tmp[2].split("_") if (t != "")]
  else:
    tags = []
  #print("DBG", v, tags)
  return [v, tags]

def _parseName_1(strName):
  tmp = re.split(CADct.regexNetPwr[1], strName)
  #print("DBG", sys._getframe().f_code.co_name, strName, tmp)
  if (len(tmp) != 4):
    return [0.0, []]
  v = float(tmp[1].replace("P", "+").replace("N", "-").replace("V", "."))
  if (tmp[2] != ""):
    tags = [t for t in tmp[2].split("_") if (t != "")]
  else:
    tags = []
  #print("DBG", v, tags)
  return [v, tags]

def _parseName_2(strName):
  tmp = re.split(CADct.regexNetPwr[2], strName)
  #print("DBG", sys._getframe().f_code.co_name, strName, tmp)
  if (len(tmp) != 4):
    return [0.0, []]
  v = float(tmp[1][:-1])
  if (tmp[2] != ""):
    tags = [t for t in tmp[2].split("_") if (t != "")]
  else:
    tags = []
  #print("DBG", v, tags)
  return [v, tags]

_parseName = (_parseName_0, _parseName_1, _parseName_2)

def parseName(strName):
  for fct in _parseName:
    v, tags = fct(strName)
    if (v != 0.0):
      return v, tags
  raise RuntimeError

class _PowerInfo(object):

  def __init__(self):
    self.v = 0.0
    self.i = 0.0
    self.tags = []  # used to distinguish between objects with the same voltage

  @property
  def nTags(self):
    return 0 if (self.tags is None) else len(self.tags)

  def fromName(self, strName):
    if (pyauto_base.misc.isEmptyString(strName)):
      msg = "CANNOT parse empty PowerRail name"
      logger.error(msg)
      raise RuntimeError(msg)

    self.v = 0.0
    self.i = 0.0
    self.tags = []
    if ((strName == "POS") or (strName == "NEG")):
      self.v = strName
    else:
      v, tags = parseName(strName)
      self.v = v
      self.tags = tags

    if (self.v == 0.0):
      msg = "CANNOT parse: {}".format(strName)
      logger.error(msg)
      raise PowerException(msg)

  def fromList(self, lst):
    if (len(lst) != 3):
      msg = "INCORRECT list {}".format(lst)
      logger.error(msg)
      raise RuntimeError(msg)
    if (not isinstance(lst[1], list)):
      msg = "PowerRail '{}' INCORRECT tags".format(lst[1])
      logger.error(msg)
      raise RuntimeError(msg)
    if (not isinstance(lst[2], float)):
      msg = "PowerRail '{}' INCORRECT current".format(lst[2])
      logger.error(msg)
      raise RuntimeError(msg)

    self.v = lst[0]
    self.tags = lst[1]
    self.i = lst[2]

def fail_PowerPort(port, msg):
  tmp = "PowerPort {}: {}".format(port.name, msg)
  logger.error(tmp)
  raise RuntimeError(tmp)

class PowerPort(_PowerInfo):
  """
  Class that describes a power port.
  """

  def __init__(self, t=None, refdes=None, rail=None):
    super(PowerPort, self).__init__()
    self._id = 0  # unique id
    self._type = t
    self.refdes = refdes
    self._railName = rail
    self.no = 0  # port number for converters with multiple ports

  def __str__(self):
    # yapf: disable
    res = [
        self.__class__.__name__ + ":",
        "{}|{}|{}|{} {}".format(self._id, self.refdes, self.no, self._railName, self._type),
        "{:.2f}V".format(self.v) if (isinstance(self.v, float)) else str(self.v),
        str(self.tags),
        "{:.4f}A".format(self.i),
    ]
    # yapf: enable
    return " ".join(res)

  @property
  def name(self):
    return "{}|{}|{}|{}".format(self._id, self.refdes, self.no, self.railName)

  @property
  def id(self):
    return self._id

  @id.setter
  def id(self, val):
    if (not isinstance(val, int)):
      msg = "INCORRECT id: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    self._id = val

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (val not in lstPortAll):
      msg = "INCORRECT PowerPort type: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    self._type = val

  @property
  def railName(self):
    return self._railName

  def setRailName(self, strRailName):
    if (pyauto_base.misc.isEmptyString(strRailName)):
      msg = "CANNOT parse empty PowerRail name"
      logger.error(msg)
      raise RuntimeError(msg)

    self.v = 0.0
    self.tags = []
    if ((strRailName == "POS") or (strRailName == "NEG")):
      self.v = strRailName
    else:
      v, tags = parseName(strRailName)
      self.v = v
      self.tags = tags

    if (self.v == 0.0):
      msg = "CANNOT parse: {}".format(strRailName)
      logger.error(msg)
      raise PowerException(msg)

    self._railName = strRailName

  def fromName(self, strRefdes, strRailName):
    """
    Fill port info from a rail name
    :param strRefdes:
    :param strRailName:
    """
    super(PowerPort, self).fromName(strRailName)
    self.refdes = strRefdes
    self._railName = strRailName

  def fromList(self, strRefdes, lst):
    """
    Fill port info from a trio of rail values
    :param strRefdes:
    :param lst:
    """
    super(PowerPort, self).fromList(lst)
    self.refdes = strRefdes

def fail_PowerRail(rail, msg):
  tmp = "PowerRail {}: {}".format(rail.name, msg)
  logger.error(tmp)
  raise RuntimeError(tmp)

class PowerRail(_PowerInfo):
  """
  Class that describes a power rail
  """
  default_t_on = 5.0

  def __init__(self, strName):
    super(PowerRail, self).__init__()
    self.fromName(strName)
    self.name = strName
    self.process = False
    self.setPorts = set()
    self._start = {"t_0": 0.0, "t_on": self.default_t_on, "startAfter": None}
    self.decoupling = {}  # decupling info: dictionary of {"cap_pn": qty, ...}

  def __str__(self):
    # yapf: disable
    res = [
        (self.__class__.__name__ + ":"),
        "{: <24}".format(self.name),
        #str(self.tags),
        "{:.2f}V".format(self.v) if (isinstance(self.v, float)) else str(self.v),
        "{:.4f}A".format(self.i),
    ]
    # yapf: enable
    return " ".join(res)

  @property
  def t_0(self):
    return self._start["t_0"]

  @t_0.setter
  def t_0(self, t):
    if (t < 0.0):
      fail_PowerRail(self, "CANNOT start before 0.0")
    ramp = self._start["t_on"] - self._start["t_0"]
    self._start["t_0"] = t
    self._start["t_on"] = (t + ramp)

  @property
  def t_on(self):
    return self._start["t_on"]

  @t_on.setter
  def t_on(self, t):
    ramp = self._start["t_on"] - self._start["t_0"]
    self._start["t_0"] = (t - ramp)
    self._start["t_on"] = t
    if (self._start["t_0"] < 0.0):
      fail_PowerRail(self, "CANNOT start before t_0")

  @property
  def startAfter(self):
    return self._start["startAfter"]

  @startAfter.setter
  def startAfter(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      self._start["startAfter"] = None
    self._start["startAfter"] = val

  def fromName(self, strRailName):
    super(PowerRail, self).fromName(strRailName)
    self.name = strRailName

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")

PwrPortIn = "in"
PwrPortOut = "out"
PwrPortSupply = "pwr"
lstPortAll = (PwrPortIn, PwrPortOut, PwrPortSupply)

PwrModelRes = "RES"
PwrModelFB = "FB"
PwrModelInd = "IND"
PwrModelTR = "Q"
PwrModelIC = "IC"
PwrModelLDO = "PWR_LDO"
PwrModelDCDC = "PWR_DCDC"
PwrModelSwitch = "PWR_SWITCH"
lstModelsAll = (PwrModelRes, PwrModelFB, PwrModelInd, PwrModelTR, PwrModelIC, PwrModelLDO,
                PwrModelDCDC, PwrModelSwitch)
lstModelsLoads = (PwrModelRes, PwrModelIC)
lstModelsConverters = (PwrModelLDO, PwrModelDCDC, PwrModelSwitch)
lstModelsDiscrete = (PwrModelRes, PwrModelFB, PwrModelInd, PwrModelTR)

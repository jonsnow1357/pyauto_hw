@require(dictInfo, lstPaths, lstLoads, lstConvs, lstRails)
<!doctype html>
<html lang="en">
  <head>
    <title>@{dictInfo["name"]} Power Estimate</title>
    <!--
    <link rel="stylesheet" type="text/css" href="static/{{ modName }}.css">
    <script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
    -->
    <style>
body {
  font-family: sans-serif;
}

table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
}

thead {
  background-color: deepskyblue;
}

tfoot {
  background-color: limegreen;
}

tr:nth-child(even) {
  background-color: lightgrey;
}

th, td {
  padding: 0.2em;
}
    </style>
  </head>

  <body>
    <h1>@{dictInfo["name"]} Power Estimate</h1>
    <p>Total Power: <strong>@{dictInfo["totalPower"]} W</strong></p>
    <p>Generated on: @{dictInfo["timestamp"]}</p>
    <p><a href="./@{lstPaths[0]}">csv version</a></p>
    <p><a href="./@{lstPaths[1]}">png version (could be large)</a></p>

    <h2>ICs</h2>
    <table>
      <thead>
        <tr>
        @for v in lstLoads[0]:
          <th>@{v}</th>
        @end
        </tr>
      </thead>
      <tbody>
      @for idx in range(1, len(lstLoads) - 1):
        <tr>
        @for v in lstLoads[idx]:
          <td>@{v}</td>
        @end
        </tr>
      @end
      </tbody>
      <tfoot>
        <tr>
        @for v in lstLoads[-1]:
          <td>@{v}</td>
        @end
        </tr>
      </tfoot>
    </table>

    <h2>Converters</h2>
    <table>
      <thead>
        <tr>
        @for v in lstConvs[0]:
          <th>@{v}</th>
        @end
        </tr>
      </thead>
      <tbody>
      @for idx in range(1, len(lstConvs) - 1):
        <tr>
        @for v in lstConvs[idx]:
          <td>@{v}</td>
        @end
        </tr>
      @end
      </tbody>
      <tfoot>
        <tr>
        @for v in lstConvs[-1]:
          <td>@{v}</td>
        @end
        </tr>
      </tfoot>
    </table>

    <h2>Rails</h2>
    <table>
      <thead>
        <tr>
        @for v in lstRails[0]:
          <th>@{v}</th>
        @end
        </tr>
      </thead>
      <tbody>
      @for idx in range(1, len(lstRails)):
        <tr>
        @for v in lstRails[idx]:
          <td>@{v}</td>
        @end
        </tr>
      @end
      </tbody>
    </table>

  </body>
</html>

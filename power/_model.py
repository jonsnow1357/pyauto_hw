#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import copy

logger = logging.getLogger("lib")
from .constants import *
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.convert

def _failCheck(obj, msg):
  logger.error("model {}: {}".format(obj.name, msg))
  raise RuntimeError("model {}: {}".format(obj, msg))

def _checkDictKeys(obj, lstKeysReq, lstKeysOpt):
  mdl = obj.get(None)
  keys_act = set(mdl.keys())
  keys_req = set(lstKeysReq)
  keys_opt = set(lstKeysOpt)

  tmp = keys_req - keys_act
  if (len(tmp) != 0):
    _failCheck(obj, "missing keys: {}".format(tmp))
  tmp = keys_act - (keys_req | keys_opt)
  if (len(tmp) != 0):
    _failCheck(obj, "extra keys: {}".format(tmp))

def _checkVal_Int_NEQ(obj, valName, val, ref):
  if (not isinstance(val, int)):
    _failCheck(obj, "'{}' is not a int".format(valName))
  if (val == ref):
    _failCheck(obj, "'{}' should not be {}".format(valName, ref))

def _checkVal_Int_GT(obj, valName, val, ref):
  if (not isinstance(val, int)):
    _failCheck(obj, "'{}' is not a int".format(valName))
  if (val <= ref):
    _failCheck(obj, "'{}' be greater than {}".format(valName, ref))

def _checkVal_Int_LT(obj, valName, val, ref):
  if (not isinstance(val, int)):
    _failCheck(obj, "'{}' is not a float".format(valName))
  if (val >= ref):
    _failCheck(obj, "'{}' should be less than {}".format(valName, ref))

def _checkDictVal_Int_NEQ(obj, key, ref):
  val = obj.get(None)[key]
  _checkVal_Int_NEQ(obj, key, val, ref)

def _checkDictVal_Int_GT(obj, key, ref):
  val = obj.get(None)[key]
  _checkVal_Int_GT(obj, key, val, ref)

def _checkDictVal_Int_LT(obj, key, ref):
  val = obj.get(None)[key]
  _checkVal_Int_LT(obj, key, val, ref)

def _checkVal_Float_NEQ(obj, valName, val, ref):
  if (not isinstance(val, float)):
    _failCheck(obj, "'{}' is not a float".format(valName))
  if (val == ref):
    _failCheck(obj, "'{}' should not be {}".format(valName, ref))

def _checkVal_Float_GT(obj, valName, val, ref):
  if (not isinstance(val, float)):
    _failCheck(obj, "'{}' is not a float".format(valName))
  if (val <= ref):
    _failCheck(obj, "'{}' should be greater than {}".format(valName, ref))

def _checkVal_Float_LT(obj, valName, val, ref):
  if (not isinstance(val, float)):
    _failCheck(obj, "'{}' is not a float".format(valName))
  if (val >= ref):
    _failCheck(obj, "'{}' should be less than {}".format(valName, ref))

def _checkDictVal_Float_NEQ(obj, key, ref):
  val = obj.get(None)[key]
  _checkVal_Float_NEQ(obj, key, val, ref)

def _checkDictVal_Float_GT(obj, key, ref):
  val = obj.get(None)[key]
  _checkVal_Float_GT(obj, key, val, ref)

def _checkDictVal_Float_LT(obj, key, ref):
  val = obj.get(None)[key]
  _checkVal_Float_LT(obj, key, val, ref)

def _checkPowerPort(obj, lst):
  if (isinstance(lst[0], str)):
    if (lst[0] == "POS"):
      _checkVal_Float_GT(obj, "current", lst[2], 0.0)
    elif (lst[0] == "NEG"):
      _checkVal_Float_LT(obj, "current", lst[2], 0.0)
    else:
      _failCheck(obj, "INCORRECT list {}".format(lst))
  else:
    _checkVal_Float_NEQ(obj, "voltage", lst[0], 0.0)
    if (lst[0] > 0.0):
      _checkVal_Float_GT(obj, "current", lst[2], 0.0)
    else:
      _checkVal_Float_LT(obj, "current", lst[2], 0.0)

def _parse_PowerInfo(obj, lst):
  if (not isinstance(lst, list)):
    _failCheck(obj, "INCORRECT list: {}".format(lst))
  if (len(lst) != 3):
    _failCheck(obj, "INCORRECT list: {}".format(lst))

  lst[1] = lst[1].split("_")

  if (isinstance(lst[2], str)):
    if (lst[2].endswith("A")):
      lst[2] = pyauto_base.convert.value2float(lst[2][:-1])
    elif (lst[2].endswith("W")):
      tmp = pyauto_base.convert.value2float(lst[2][:-1])
      lst[2] = tmp / lst[0]
    else:
      _failCheck(obj, "INCORRECT list: {}".format(lst))
  elif (not isinstance(lst[2], float)):
    _failCheck(obj, "INCORRECT list: {}".format(lst))

class PowerModel(object):

  def __init__(self):
    self.name = None
    self._type = None
    self._path = None
    self._content = None

  def __str__(self):
    return "{}: {} {}".format(self.__class__.__name__, self._type, self.name)

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (val not in lstModelsAll):
      msg = "INCORRECT power model {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)
    self._type = val

  @property
  def path(self):
    return self._path

  @path.setter
  def path(self, val):
    pyauto_base.fs.chkPath_File(val)
    self._path = val

  @property
  def cases(self):
    if (self._type in lstModelsDiscrete):
      return []
    if (self._type == PwrModelIC):
      return self._content.keys()
    if (self._type in lstModelsConverters):
      return []

  def _load_IC(self):
    for case in self._content.keys():
      for entry in self._content[case]:
        _parse_PowerInfo(self, entry)

  def _load_LDO(self):
    if ("supply" in self._content.keys()):
      _parse_PowerInfo(self, self._content["supply"])

  def _load_Switch(self):
    if ("supply" in self._content.keys()):
      _parse_PowerInfo(self, self._content["supply"])

  def load(self):
    #logger.info("loading model '{}'".format(self._path))
    with open(self._path, "r") as fIn:
      try:
        tmp = json.load(fIn)
      except json.decoder.JSONDecodeError as ex:
        msg = "CANNOT load {}:\n{}".format(self._path, ex)
        logger.error(msg)
        raise RuntimeError(msg)
    if ("type" not in tmp.keys()):
      msg = "power model {} has no type".format(self._path)
      logger.error(msg)
      raise RuntimeError(msg)
    self.type = tmp["type"]

    self._content = tmp
    self._content.pop("type")
    try:
      self._content.pop("_comment")
    except KeyError:
      pass
    if (self._type == PwrModelIC):
      self._load_IC()
    elif (self._type == PwrModelLDO):
      self._load_LDO()
    elif (self._type in (PwrModelDCDC, PwrModelSwitch)):
      self._load_Switch()

  def get(self, caseId="max"):
    if (self._type == PwrModelIC):
      return self._content[caseId]
    else:
      return self._content

  def _check_Res(self):
    _checkDictKeys(self, ["p_max"], [])

    _checkDictVal_Float_GT(self, "p_max", 0.0)

  def _check_Ind(self):
    _checkDictKeys(self, ["i_max", "dcr"], [])

    _checkDictVal_Float_GT(self, "i_max", 0.0)
    _checkDictVal_Float_GT(self, "dcr", 0.0)

  def _check_TR(self):
    _checkDictKeys(self, ["p_max", "i_max"], ["r_dson", "v_cesat"])

    _checkDictVal_Float_GT(self, "p_max", 0.0)
    _checkDictVal_Float_NEQ(self, "i_max", 0.0)
    if ("r_dson" in self._content.keys()):
      _checkDictVal_Float_GT(self, "r_dson", 0.0)
    if ("v_cesat" in self._content.keys()):
      if (self._content["i_max"] < 0.0):
        _checkDictVal_Float_LT(self, "v_cesat", 0.0)
      else:
        _checkDictVal_Float_GT(self, "v_cesat", 0.0)

  def _check_IC(self):
    for case in self._content:
      for pp in self._content[case]:
        _checkPowerPort(self.name, pp)

  def _check_LDO(self):
    _checkDictKeys(self, ["inputs", "outputs", "map", "i_max"], ["supply"])

    _checkDictVal_Int_GT(self, "inputs", 0)
    _checkDictVal_Int_GT(self, "outputs", 0)
    _checkDictVal_Float_GT(self, "i_max", 0.0)

    nTrue = 0
    for i in range(0, self._content["inputs"]):
      for j in range(0, self._content["outputs"]):
        if (not isinstance(self._content["map"][i][j], bool)):
          _failCheck(self, "INCORRECT map {}".format(self._content["map"]))
        if (self._content["map"][i][j]):
          nTrue += 1
    if (nTrue != max(self._content["inputs"], self._content["outputs"])):
      _failCheck(self, "WRONG input to output map")

    if ("supply" in self._content.keys()):
      _checkPowerPort(self, self._content["supply"])

  def _check_Switch(self):
    _checkDictKeys(self, ["inputs", "outputs", "map", "i_max"], ["buck", "boost", "supply"])

    _checkDictVal_Int_GT(self, "inputs", 0)
    _checkDictVal_Int_GT(self, "outputs", 0)
    _checkDictVal_Float_GT(self, "i_max", 0.0)

    nTrue = 0
    for i in range(0, self._content["inputs"]):
      for j in range(0, self._content["outputs"]):
        if (not isinstance(self._content["map"][i][j], bool)):
          _failCheck(self, "INCORRECT map {}".format(self._content["map"]))
        if (self._content["map"][i][j]):
          nTrue += 1

    if (nTrue != max(self._content["inputs"], self._content["outputs"])):
      _failCheck(self, "WRONG input to output map")

    if ("supply" in self._content.keys()):
      _checkPowerPort(self, self._content["supply"])

  def check(self):
    if (self._type == PwrModelRes):
      self._check_Res()
    elif (self._type == PwrModelFB):
      self._check_Ind()
    elif (self._type == PwrModelInd):
      self._check_Ind()
    elif (self._type == PwrModelTR):
      self._check_TR()
    elif (self._type == PwrModelIC):
      self._check_IC()
    elif (self._type == PwrModelLDO):
      self._check_LDO()
    elif (self._type == PwrModelDCDC):
      self._check_Switch()
    elif (self._type == PwrModelSwitch):
      self._check_Switch()
    else:
      raise NotImplementedError

  def update_ResValue(self, val):
    if ((self._type == PwrModelRes) and isinstance(val, float)):
      if (val > 0.0):
        self._content["val"] = val

def mkModel_IC(strName, dictContent):
  mdl = PowerModel()
  mdl.name = strName
  mdl._type = PwrModelIC
  mdl._content = copy.deepcopy(dictContent)
  mdl.check()
  return mdl

class PowerModelDB(object):

  def __init__(self):
    self._path = None
    self._models = {}  # {<value>: PowerModel, ...}

  @property
  def path(self):
    return self._path

  @path.setter
  def path(self, val):
    if (pyauto_base.fs.chkPath_Dir(val, bDie=False)):
      self._path = val

  def load(self, projectName=None):
    self._models = {}
    for dirpath, dirs, files in os.walk(self._path):
      for f in files:
        if (not f.endswith(".json")):
          continue
        if ("projects" in dirpath):
          if (pyauto_base.misc.isEmptyString(projectName)):
            continue
          if (not dirpath.endswith(projectName)):
            continue
        mdl = PowerModel()
        mdl.name = f[:-5]
        mdl.path = os.path.join(dirpath, f)

        if (mdl.name in self._models.keys()):
          msg = "DUPLICATE model '{}' in power database".format(f)
          logger.error(msg)
          raise RuntimeError(msg)
        self._models[mdl.name] = mdl
    logger.info("found {:d} model(s) in '{}'".format(len(self._models), self._path))

  def get(self, modelId):
    try:
      return self._models[modelId]
    except KeyError:
      return

  def getAllValues(self):
    return sorted(self._models.keys())

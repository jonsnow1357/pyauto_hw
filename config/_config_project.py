#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import string

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.db
import pyauto_base.config
from ._config_CAD import *
from ._config_power import *

class BoardConfigException(Exception):
  pass

class BoardConfig(object):

  def __init__(self, path):
    self._path = path
    self._cfg = None
    self.sch = None
    self.pcb = None
    self.pwr = {}

    self._load()

  def __str__(self):
    # yapf: disable
    return "{} {}: sch={}, PCB={}, pwr={}".format(self.__class__.__name__, self.name,
                                                  "N" if (self.sch is None) else "Y",
                                                  "N" if (self.pcb is None) else "Y",
                                                  "N" if (len(self.pwr) == 0) else len(self.pwr))
    # yapf: enable

  @property
  def name(self):
    return self._cfg["name"]

  @property
  def vars(self):
    return self._cfg["vars"]

  @property
  def path(self):
    return self._cfg["path"]

  def _load(self):
    self._path = pyauto_base.fs.mkAbsolutePath(self._path)

    # path_cfg = os.path.join(self._path, "main.cfg")
    # pyauto_base.fs.chkPath_File(path_cfg)
    # self._cfg = pyauto_base.config.SimpleConfig()
    # self._cfg.loadCfgFile(path_cfg)
    #
    # print("DBG", self._cfg.default, self._cfg.path, self._cfg.vars)
    # tmp = self._cfg.default
    # if(len(self._cfg.path) > 0):
    #   tmp.update({"path": self._cfg.path})
    # if(len(self._cfg.vars) > 0):
    #   tmp.update({"vars": self._cfg.vars})
    # with open(os.path.join(self._path, "main.json"), "w") as f_out:
    #   json.dump(tmp, f_out, indent=2)

    path_cfg = os.path.join(self._path, "main.json")
    with open(path_cfg, "r") as f_in:
      self._cfg = json.load(f_in)

    if ("name" not in self._cfg.keys()):
      msg = "CANNOT find board name in '{}'".format(path_cfg)
      logger.error(msg)
      raise BoardConfigException(msg)
    if ("path" not in self._cfg.keys()):
      self._cfg["path"] = {}
    if ("vars" not in self._cfg.keys()):
      self._cfg["vars"] = {}

    path_sch = os.path.join(self._path, "{}.sch.json".format(self.name))
    if (pyauto_base.fs.chkPath_File(path_sch, msg=None, bDie=False)):
      self.sch = SchConfig(path_sch)
      self.sch.replaceVars(self._cfg["vars"])

    # path_pcb = os.path.join(self._path, "{}.PCB.xml".format(self.name))
    # if (pyauto_base.fs.chkPath_File(path_pcb, msg=None, bDie=False)):
    #   self.pcb = PCBConfig(path_pcb, None)
    #   self.pcb.replaceVars(self._cfg["vars"])
    # if (self.pcb is not None):
    #   if(self.pcb.stackup is not None):
    #     self.pcb.stackup.writeJSON(os.path.join(self._path, "{}.pcbstk.json".format(self.name)))
    #   if(self.pcb.artwork is not None):
    #     self.pcb.artwork.writeJSON(os.path.join(self._path, "{}.pcbart.json".format(self.name)))
    path_pcb1 = os.path.join(self._path, "{}.pcbstk.json".format(self.name))
    path_pcb2 = os.path.join(self._path, "{}.pcbart.json".format(self.name))
    self.pcb = PCBConfig(path_pcb1, path_pcb2)
    self.pcb.replaceVars(self._cfg["vars"])

    for f in os.listdir(self._path):
      if (not f.endswith("pwr.cfg")):
        continue
      path_pwr = os.path.join(self._path, f)
      pc = PwrConfig()
      pc.loadCfgFile(path_pwr)
      pc.replaceVars(self._cfg["vars"])
      self.pwr[f[:-8]] = pc

  def getPath_brd(self):
    lst_res = []
    for f in os.listdir(self._cfg.path["pcb"]):
      if (f.endswith(".brd")):
        lst_res.append(os.path.join(self._cfg.path["pcb"], f))

    if (len(lst_res) == 0):
      msg = "NO brd file found"
      logger.error(msg)
      raise BoardConfigException(msg)
    if (len(lst_res) > 1):
      msg = "MULTIPLE brd files found"
      logger.error(msg)
      raise BoardConfigException(msg)

    return lst_res[0]

  def showInfo(self):
    logger.info(self)
    if (len(self._cfg["path"]) > 0):
      logger.info("-- path")
      for k, v in self._cfg["path"].items():
        logger.info("  {} -> '{}'".format(k, v))
    if (len(self._cfg["vars"]) > 0):
      logger.info("-- vars")
      for k, v in self._cfg["vars"].items():
        logger.info("  {} -> '{}'".format(k, v))
    # if (self.sch is not None):
    #   self.sch.showInfo()
    # if (self.pcb is not None):
    #   self.pcb.showInfo()

class ProjectConfigException(Exception):
  pass

class ProjectConfig(object):

  def __init__(self, path=None):
    self._path = path
    self._cfg = None
    self.boards = []

    if (not pyauto_base.misc.isEmptyString(self._path)):
      self._load()

  def __str__(self):
    res = "{} {}: {} board(s)".format(self.__class__.__name__, self.name, len(self.boards))
    return res

  @property
  def name(self):
    return self._cfg["project_name"]

  def _load(self):
    self._path = pyauto_base.fs.mkAbsolutePath(self._path)
    path_cfg = os.path.join(self._path, "main.json")
    pyauto_base.fs.chkPath_File(path_cfg)

    with open(path_cfg, "r") as f_in:
      self._cfg = json.load(f_in)

    if ("project_name" not in self._cfg.keys()):
      msg = "CANNOT find project_name name in '{}'".format(path_cfg)
      logger.error(msg)
      raise ProjectConfigException(msg)

    for f in sorted(os.listdir(self._path)):
      tmp = os.path.join(self._path, f)
      if (os.path.isdir(tmp)):
        brd = BoardConfig(tmp)
        self.boards.append(brd)

  def showInfo(self):
    logger.info(self)
    logger.info("-- boards ({})".format(len(self.boards)))
    for brd in self.boards:
      logger.info("  {}".format(brd.name))
      #brd.showInfo()

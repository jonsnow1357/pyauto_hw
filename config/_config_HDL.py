#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import string

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.config

class PTFConfig(object):

  def __init__(self):
    self.partName = None
    self.dbId = None
    self.dbTable = None
    self.dbMap = None
    self.path = None
    self.propsKey = []
    self.propsInj = []

  def __str__(self):
    return "{}: '{}' ({}.{} -> {})".format(self.__class__.__name__, self.partName,
                                           self.dbId, self.dbTable, self.path)

class LibConfig(pyauto_base.config.DBConfig):

  def __init__(self):
    super(LibConfig, self).__init__()
    self._id = None
    self.lstPTFs = []

  @property
  def id(self):
    return self._id

  def __str__(self):
    return "{} ({:d} PTF(s))".format(self.__class__.__name__, len(self.lstPTFs))

  def _parseXML_ptf(self, elPTF):
    ptf = PTFConfig()

    if (not pyauto_base.misc.chkXMLElementHasTag(elPTF, "part", bDie=False)):
      return
    el = elPTF.find("part")
    if (not pyauto_base.misc.chkXMLElementHasText(el, bDie=False)):
      return
    ptf.partName = el.text

    if (not pyauto_base.misc.chkXMLElementHasTag(elPTF, "database", bDie=False)):
      return
    el = elPTF.find("database")
    if (not pyauto_base.misc.chkXMLElementHasAttr(el, "id", bDie=False)):
      return
    if (not pyauto_base.misc.chkXMLElementHasAttr(el, "table", bDie=False)):
      return
    if (not pyauto_base.misc.chkXMLElementHasText(el, bDie=False)):
      return
    ptf.dbId = el.attrib["id"]
    ptf.dbTable = el.attrib["table"]
    ptf.dbMap = json.loads(el.text)

    if (not pyauto_base.misc.chkXMLElementHasTag(elPTF, "path", bDie=False)):
      return
    el = elPTF.find("path")
    if (not pyauto_base.misc.chkXMLElementHasText(el, bDie=False)):
      return
    ptf.path = el.text

    if (not pyauto_base.misc.chkXMLElementHasTag(elPTF, "key", bDie=False)):
      return
    el = elPTF.find("key")
    if (not pyauto_base.misc.chkXMLElementHasText(el, bDie=False)):
      return
    ptf.propsKey = json.loads(el.text)

    if (not pyauto_base.misc.chkXMLElementHasTag(elPTF, "inject", bDie=False)):
      return
    el = elPTF.find("inject")
    if (not pyauto_base.misc.chkXMLElementHasText(el, bDie=False)):
      return
    ptf.propsInj = json.loads(el.text)

    return ptf

  def replaceVars(self, dictVars):
    for ptfCfg in self.lstPTFs:
      tpl = string.Template(ptfCfg.path)
      ptfCfg.path = tpl.substitute(**dictVars)
      ptfCfg.path = pyauto_base.fs.mkAbsolutePath(ptfCfg.path, bExists=False)

  def parseXml(self):
    super(LibConfig, self).parseXml()

    elLib = self._domObj.find("library")
    pyauto_base.misc.chkXMLElementHasAttr(elLib, "id")
    self._id = elLib.attrib["id"]

    for elPTF in elLib.findall("ptf"):
      ptf = self._parseXML_ptf(elPTF)
      if (ptf is not None):
        self.lstPTFs.append(ptf)

    self.replaceVars(self.vars)

  def showInfo(self):
    super(LibConfig, self).showInfo()
    if (len(self.lstPTFs) > 0):
      logger.info("-- PTFs ({})".format(len(self.lstPTFs)))
      for ptf in self.lstPTFs:
        logger.info(ptf)

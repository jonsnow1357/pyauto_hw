#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json

logger = logging.getLogger("lib")
from ._config_project import BoardConfig
from ._config_project import BoardConfigException

def getBoardConfig(path):
  logger.info("using path: '{}'".format(path))
  dict_boards = {}
  for dirpath, dirs, files in os.walk(path):
    #print("DBG", dirpath, dirs, files)
    if (len(dirs) == 0):
      if ("main.json" not in files):
        continue
      try:
        brd = BoardConfig(dirpath)
      except BoardConfigException:
        continue
      except json.decoder.JSONDecodeError as ex:
        logger.error(ex)
        logger.error(os.path.join(dirpath, d))
        raise RuntimeError
      dict_boards[brd.name] = brd
    else:
      for d in dirs:
        try:
          brd = BoardConfig(os.path.join(dirpath, d))
        except BoardConfigException:
          continue
        except json.decoder.JSONDecodeError as ex:
          logger.error(ex)
          logger.error(os.path.join(dirpath, d))
          raise RuntimeError
        dict_boards[brd.name] = brd
  logger.info("found {} boards(s)".format(len(dict_boards)))
  return dict_boards

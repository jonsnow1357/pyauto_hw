#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import string

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.base as CADbase
import pyauto_hw.CAD.constants as CADct

class SchConfig(object):

  def __init__(self, path):
    self._path = path

    self._load()

  def __str__(self):
    return "{}".format(self.__class__.__name__)

  def _load(self):
    self._path = pyauto_base.fs.mkAbsolutePath(self._path)

    raise NotImplementedError  # TODO:

  def replaceVars(self, dictVars: dict):
    raise NotImplementedError  # TODO:

  def showInfo(self):
    logger.info(self)

class PCBConfig(object):

  def __init__(self, pathStk, pathArt):
    self._pathStackup = pathStk
    self.stackup = None
    self._pathArtwork = pathArt
    self.artwork = None

    self._load()

  def __str__(self):
    # yapf: disable
    return "{}: stk={}, art={}".format(self.__class__.__name__,
                                       "N" if (self.stackup is None) else "Y",
                                       "N" if (self.artwork is None) else "Y")

    # yapf: enable

  def _parse_stackup(self):
    with open(self._pathStackup, "r") as f_in:
      data_in = json.load(f_in)

    pcb_stk = CADbase.PCBStackup(unit=data_in["unit"])
    for t in data_in["layers"]:
      pcb_ly = CADbase.PCBLayer(t["type"], t["thickness"], unit=str(pcb_stk.unit.units))
      pcb_ly.fromJSON(t)
      pcb_stk.addLayer(pcb_ly)
    for v in data_in["vias"]:
      pcb_via = CADbase.PCBVia(v["drill"], unit=str(pcb_stk.unit.units))
      pcb_via.fromJSON(v)
      pcb_stk.addVia(pcb_via)

    if (pcb_stk.validate()):
      self.stackup = pcb_stk
      #logger.info("stackup has {} layers(s)".format(self.stackup.nLayers))
    else:
      msg = "INCORRECT stackup"
      logger.error(msg)
      raise RuntimeError(msg)

  def _parse_artwork(self):
    with open(self._pathArtwork, "r") as f_in:
      data_in = json.load(f_in)

    pcb_art = CADbase.PCBArtwork()
    for ly in data_in["layers"]:
      art_ly = CADbase.PCBArtworkLayer(strType=ly["type"], strName=ly["name"])
      if ("options" in ly.keys()):
        art_ly.opts = ly["options"]
      if ("content" in ly.keys()):
        art_ly.content = ly["content"]
      pcb_art.addLayer(art_ly)

    self.artwork = pcb_art

  def _load(self):
    if (pyauto_base.fs.chkPath_File(self._pathStackup, msg=None, bDie=False)):
      self._pathStackup = pyauto_base.fs.mkAbsolutePath(self._pathStackup)
      self._parse_stackup()
    if (pyauto_base.fs.chkPath_File(self._pathArtwork, msg=None, bDie=False)):
      self._pathArtwork = pyauto_base.fs.mkAbsolutePath(self._pathArtwork)
      self._parse_artwork()

  def replaceVars(self, dictVars):
    if (self.artwork is not None):
      for ly in self.artwork.layers:
        tpl = string.Template(ly.name)
        ly.name = tpl.substitute(**dictVars)

  def showInfo(self):
    logger.info(self)
    logger.info(self.stackup)
    logger.info(self.artwork)

  def isConsistent(self):
    lst_names_stackup = []
    for ly in self.stackup.layers:
      if (ly.type == CADct.pcbArtCu):
        lst_names_stackup.append(ly.name)
    #print("DBG", lst_names_stackup)

    lst_names_artwork = []
    for ly in self.artwork.layers:
      if (ly.type == CADct.pcbArtCu):
        lst_names_artwork += ly.content
    #print("DBG", lst_names_artwork)

    set1 = set([t for t in lst_names_stackup if (t not in ["TOP", "BOT", "BOTTOM"])])
    set2 = set([t for t in lst_names_artwork if (t not in ["TOP", "BOT", "BOTTOM"])])
    if (set1 != set2):
      msg = "CU layer mismatch between stackup and artwork"
      logger.error(msg)
      return False

    return True

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json

logger = logging.getLogger("lib")
import pyauto_base.misc

class PwrConfig(object):
  """
  :var netRemove: list of ["net_name", ...] to remove from netlist
  :var netMerge: list of [["net_name_1", "net_name_2"], ... ] to merge in netlist
  :var netRename: list of [["net_name_old", "net_name_new"], ...]
    which defines nets that should be renamed

  :var compRemove: list of ["refdes", ...] to remove from netlist
  :var compAdd: list of [["refdes", "value", ["net_name", ...]], ...]
    which defines components that should be added to the netlist

  :var modelCases: dict of {"refdes": "case_name", ...} to override default PowerLoad case
  :var modelEfficiencies: dict of {"refdes": value} to override default PowerConverter efficiency

  :var powerPorts: dict of {"refdes": [["op", "net_name", port_cnt, "port_type], ...], ...}
    or dict of {["net_name", "tags"], ...} which adding/changing PowerPorts
  :var powerSense: dict with parameters for power sense resistors monitoring
  """

  def __init__(self):
    self._cfgObj = None
    self.netRemove = []
    self.netMerge = []
    self.netRename = []
    self.compRemove = []
    self.compAdd = []
    self.modelCases = {}
    self.modelEfficiencies = {}
    self.powerPorts = {}
    self.powerSense = {}

  def __str__(self):
    return "{}".format(self.__class__.__name__)

  def clear(self):
    self.netRemove = []
    self.netMerge = []
    self.netRename = []
    self.compRemove = []
    self.compAdd = []
    self.modelCases = {}
    self.modelEfficiencies = {}
    self.powerPorts = {}
    self.powerSense = {}

  def _parseCfg_netlist(self):
    sectionName = "netlist"
    sectionName_merge = "{}_merge".format(sectionName)
    sectionName_rename = "{}_rename".format(sectionName)
    sectionName_add = "{}_add".format(sectionName)

    for s in self._cfgObj.sections():
      if (s == sectionName):
        for k, v in self._cfgObj.items(s):
          if (k == "delNet"):
            self.netRemove = v.split(",")
            self.netRemove = [t.strip() for t in self.netRemove if t != ""]
          if (k == "delComp"):
            self.compRemove = v.split(",")
            self.compRemove = [t.strip() for t in self.compRemove if t != ""]

      if (s == sectionName_merge):
        for k, v in self._cfgObj.items(s):
          for t in v.split(","):
            if (t != ""):
              self.netMerge.append([k, t])

      if (s == sectionName_rename):
        for k, v in self._cfgObj.items(s):
          for t in v.split(","):
            if (t != ""):
              self.netRename.append([k, t])

      if (s == sectionName_add):
        for k, v in self._cfgObj.items(s):
          tmp = v.split("|")
          self.compAdd.append([k, tmp[0].strip(), tmp[1].strip().split(",")])

  def _parseCfg_model(self):
    sectionName = "model"
    sectionName_case = "{}_case".format(sectionName)
    sectionName_eff = "{}_efficiency".format(sectionName)

    for s in self._cfgObj.sections():
      if (s == sectionName_case):
        for k, v in self._cfgObj.items(s):
          self.modelCases[k] = v
      if (s == sectionName_eff):
        for k, v in self._cfgObj.items(s):
          self.modelEfficiencies[k] = float(v)

  def _parseCfg_power(self):
    sectionName = "power"
    sectionName_ports = "{}_ports".format(sectionName)
    sectionName_sense = "{}_sense".format(sectionName)

    for s in self._cfgObj.sections():
      if (s == sectionName_ports):
        for k, v in self._cfgObj.items(s):
          self.powerPorts[k] = json.loads(v)
      if (s == sectionName_sense):
        for k, v in self._cfgObj.items(s):
          self.powerSense[k] = json.loads(v)

  def loadCfgFile(self, fPath):
    self._cfgObj = pyauto_base.misc.readTXT_config(fPath)
    #logger.info(self._cfgObj.sections())
    self._parseCfg_netlist()
    self._parseCfg_model()
    self._parseCfg_power()

  def replaceVars(self, dictVars):
    pass  # TODO:

  def _showInfo_netlist(self):
    logger.info("-- netRemove ({:d})".format(len(self.netRemove)))
    for v in self.netRemove:
      logger.info("  {}".format(v))
    logger.info("-- netMerge ({:d})".format(len(self.netMerge)))
    for v in self.netMerge:
      logger.info("  {}".format(v))
    logger.info("-- netRename ({:d})".format(len(self.netRename)))
    for v in self.netRename:
      logger.info("  {}".format(v))

    logger.info("-- compRemove ({:d})".format(len(self.compRemove)))
    for v in self.compRemove:
      logger.info("  {}".format(v))
    logger.info("-- compAdd ({:d})".format(len(self.compAdd)))
    for v in self.compAdd:
      logger.info("  {}".format(v))

  def _showInfo_model(self):
    pass  # TODO:

  def _showInfo_power(self):
    logger.info("-- powerPorts ({:d})".format(len(self.powerPorts)))
    for k, v in self.powerPorts.items():
      logger.info("  {}: {}".format(k, v))
    logger.info("-- powerSense ({:d})".format(len(self.powerSense)))
    for k, v in self.powerSense.items():
      logger.info("  {}: {}".format(k, v))

  def showInfo(self):
    logger.info(self)
    self._showInfo_netlist()
    self._showInfo_model()
    self._showInfo_power()

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import string

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_base.db
import pyauto_base.config

class DBCheck(object):
  """
  holds information about how to check data in a database table
  :var table: table name
  :var checks: list of [{"id": ..., "type": ..., "expression": ...}, ...]
    defining checks to be done on the table column data
  """

  def __init__(self):
    self.table = None
    self.checks = []
    self.stats = pyauto_base.misc.StatCounter()

  def __str__(self):
    return "{} {:<20}: {:d} checks ({})".format(
        self.__class__.__name__, self.table, len(self.checks),
        ",".join(set([ck["type"] for ck in self.checks])))

class DBTransform(object):
  """
  holds information about how to move data between database tables.
  """

  def __init__(self):
    self.table = None
    self.src = None
    self.dst = None
    self.key = None
    self.queries = {"src": None, "dst": None, "ins": None, "upd": None, "del": None}

  def __str__(self):
    return "{} {:<16}: {} -> {} (key {})".format(self.__class__.__name__, self.table,
                                                 self.src, self.dst, self.key)

class PartsDBConfig(pyauto_base.config.DBConfig):

  def __init__(self):
    super(PartsDBConfig, self).__init__()
    self.lstDBChecks = []
    self.lstDBTransforms = []

  def _parseXml_DBcheck(self):
    self.lstDBChecks = []
    for elDBChk in self._domObj.getroot().findall("dbCheck"):
      dbchk = DBCheck()
      pyauto_base.misc.chkXMLElementHasAttr(elDBChk, "table", bDie=True)
      dbchk.table = elDBChk.attrib["table"]

      for elCol in elDBChk.findall("col"):
        pyauto_base.misc.chkXMLElementHasAttr(elCol, "id", bDie=True)
        pyauto_base.misc.chkXMLElementHasAttr(elCol, "type", bDie=True)
        dbchk.checks.append(elCol.attrib)
        if (pyauto_base.misc.chkXMLElementHasText(elCol, bWarn=False)):
          if (dbchk.checks[-1]["type"] == "regex"):
            dbchk.checks[-1]["expression"] = elCol.text
          else:
            tpl = string.Template(elCol.text)
            dbchk.checks[-1]["expression"] = tpl.substitute(**self.vars)
        else:
          dbchk.checks[-1]["expression"] = None

      for chk in dbchk.checks:
        if (chk["type"] == "list"):
          if (chk["expression"].startswith("[")):
            try:
              chk["expression"] = json.loads(chk["expression"])
            except ValueError:
              msg = "INCORRECT JSON string for check '{}'.'{}'".format(
                  dbchk.table, chk["col"])
              logger.error(msg)
              raise RuntimeError(msg)
          if ("null" in chk.keys()):
            chk["null"] = True if (chk["null"].lower() in ("yes", "true", "1", "on",
                                                           "ok")) else False
          else:
            chk["null"] = False
        elif (chk["type"] == "folder"):
          chk["expression"] = pyauto_base.fs.mkAbsolutePath(chk["expression"],
                                                            rootPath=self.fPath)
          if ("recursive" in chk.keys()):
            chk["recursive"] = True if (chk["recursive"].lower() in ("yes", "true", "1",
                                                                     "on", "ok")) else False
          else:
            chk["recursive"] = False
          if ("pattern" not in chk.keys()):
            chk["pattern"] = None
      self.lstDBChecks.append(dbchk)

  def _parseXml_DBtransform(self):
    self.lstDBTransforms = []
    for elDBXfrm in self._domObj.getroot().findall("dbTransform"):
      dbxfrm = DBTransform()
      pyauto_base.misc.chkXMLElementHasAttr(elDBXfrm, "table", bDie=True)
      pyauto_base.misc.chkXMLElementHasAttr(elDBXfrm, "src", bDie=True)
      pyauto_base.misc.chkXMLElementHasAttr(elDBXfrm, "dst", bDie=True)
      pyauto_base.misc.chkXMLElementHasAttr(elDBXfrm, "key", bDie=True)
      dbxfrm.table = elDBXfrm.attrib["table"]
      dbxfrm.src = elDBXfrm.attrib["src"]
      dbxfrm.dst = elDBXfrm.attrib["dst"]
      dbxfrm.key = elDBXfrm.attrib["key"]

      for tagName in ("selSrc", "selDst", "ins", "upd", "del"):
        tmp = elDBXfrm.findall(tagName)
        if (len(tmp) == 0):
          msg = "'{}' WITHOUT '{}' element".format(elDBXfrm.tag, tagName)
          logger.error(msg)
          raise RuntimeError(msg)
        elif (len(tmp) > 1):
          msg = "'{}' WITH MULTIPLE '{}' elements".format(elDBXfrm.tag, tagName)
          logger.error(msg)
          raise RuntimeError(msg)
        pyauto_base.misc.chkXMLElementHasText(tmp[0], bDie=True)
        if (tagName == "selSrc"):
          dbxfrm.queries["src"] = tmp[0].text
        elif (tagName == "selDst"):
          dbxfrm.queries["dst"] = tmp[0].text
        else:
          dbxfrm.queries[tagName] = tmp[0].text

      if (any([dbxfrm.queries[k] is None for k in dbxfrm.queries.keys()])):
        msg = "MISSING queries for dbTransform '{}'".format(dbxfrm.id)
        logger.error(msg)
        raise RuntimeError(msg)
      self.lstDBTransforms.append(dbxfrm)

  def parseXml(self):
    super(PartsDBConfig, self).parseXml()
    self._parseXml_DBcheck()
    self._parseXml_DBtransform()

  def _showInfo_DBCheck(self):
    logger.info("-- {:d} database check(s)".format(len(self.lstDBChecks)))
    for dbchk in self.lstDBChecks:
      logger.info("  {}".format(dbchk))

  def _showInfo_DBTransform(self):
    logger.info("-- {:d} database transform(s)".format(len(self.lstDBTransforms)))
    for dbxfrm in self.lstDBTransforms:
      logger.info("  {}".format(dbxfrm))

  def showInfo(self):
    super(PartsDBConfig, self).showInfo()
    self._showInfo_DBCheck()
    self._showInfo_DBTransform()

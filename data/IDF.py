#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.fs

ownerNone = "UNOWNED"
ownerMech = "MCAD"
ownerElec = "ECAD"
_lstOwners = (ownerNone, ownerMech, ownerElec)

class BoardData(object):

  def __init__(self):
    self.header: list = []
    self.sections: list = []

  def parseSection(self, lstLn: list):
    if (lstLn[0] == ".HEADER"):
      self.header = lstLn
    else:
      self.sections.append(lstLn)

  def changeOwner(self, owner: str):
    if (owner not in _lstOwners):
      logger.error("INCORRECT owner: {}".format(owner))
      return

    for section in self.sections:
      tmp = section[0].split()
      if (len(tmp) == 2):
        section[0] = "{: <16} {}".format(tmp[0], owner)

  def showInfo(self):
    tmp = self.header[1].split()
    logger.info("{} ver {}".format(tmp[0], tmp[1]))
    tmp = self.header[2].split()
    logger.info("name: {} ({})".format(tmp[0], tmp[1]))

    for s in self.sections:
      tmp = s[0].split()
      if (len(tmp) > 1):
        logger.info("{}, owner: {}".format(tmp[0], tmp[1]))
      else:
        logger.info(tmp[0])

class LibraryData(object):

  def __init__(self):
    self.header: list = []
    self.sections: list = []

  def parseSection(self, lstLn: list):
    pass

  def changeOwner(self, owner: str):
    if (owner not in _lstOwners):
      logger.error("INCORRECT owner: {}".format(owner))
      return

  def showInfo(self):
    pass

class PanelData(object):

  def __init__(self):
    self.header: list = []
    self.sections: list = []

  def parseSection(self, lstLn: list):
    pass

  def changeOwner(self, owner: str):
    if (owner not in _lstOwners):
      logger.error("INCORRECT owner: {}".format(owner))
      return

  def showInfo(self):
    pass

class IDFData(object):

  def __init__(self):
    self.brd = BoardData()
    self.lib = LibraryData()
    self.pnl = PanelData()

  def changeOwner(self, owner):
    self.brd.changeOwner(owner)
    self.lib.changeOwner(owner)
    self.pnl.changeOwner(owner)

  def showInfo(self):
    self.brd.showInfo()
    self.lib.showInfo()
    self.pnl.showInfo()

def read(fPath: str):
  if (fPath.lower().endswith(".emn")):
    brdPath = fPath
    tmp = os.path.split(fPath)
    libPath = os.path.join(tmp[0], tmp[1][:-4] + ".emp")
  else:
    logger.error("INCORRECT file path: {}".format(fPath))
    return

  pyauto_base.fs.chkPath_File(brdPath)
  if (not pyauto_base.fs.chkPath_File(libPath, bDie=False)):
    libPath = None

  data = IDFData()
  section = []
  with open(brdPath, "r") as fIn:
    for ln in fIn.readlines():
      ln = ln.strip("\r\n")
      if (ln.startswith("#")):
        continue

      if (ln.startswith(".")):
        if (ln.startswith(".END")):
          section.append(ln)
          data.brd.parseSection(section)
        else:
          section = [ln]
      else:
        section.append(ln)
  logger.info("read {} section(s) from {}".format(len(data.brd.sections),
                                                  os.path.basename(brdPath)))

  return data

def write(fPath: str, idf: IDFData):
  if (fPath.lower().endswith(".emn")):
    brdPath = fPath
    tmp = os.path.split(fPath)
    libPath = os.path.join(tmp[0], tmp[1][:-4] + ".emp")
  else:
    logger.error("INCORRECT file path: {}".format(fPath))
    return

  with open(brdPath, "w") as fOut:
    for ln in idf.brd.header:
      fOut.write(ln + "\n")
    for section in idf.brd.sections:
      for ln in section:
        fOut.write(ln + "\n")

  if (len(idf.lib.header) != 0):
    with open(libPath, "w") as fOut:
      for ln in idf.lib.header:
        fOut.write(ln + "\n")
      for section in idf.lib.sections:
        for ln in section:
          fOut.write(ln + "\n")

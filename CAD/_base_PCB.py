#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import json
import copy

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct

class PCBLayer(object):

  def __init__(self, strType, thickness, strName="", strMaterial="", unit="mm"):
    self._type = None
    self._material = None
    self._thickness = None
    self._flags = {
        "sig": False,
        "plane": False,
        "plated": False,
        "core": False,
        "prepreg": False
    }
    self._diel_dk = None  # dielectric permittivity
    self._diel_df = None  # loss tangent
    self.structure = ""
    self.unit = pyauto_base.misc.valueWithUnit(1, unit)

    self.type = strType
    self.name = strName
    self.material = strMaterial
    self.thickness = thickness

    if (self.type == CADct.pcbLayerCu):
      if (self.name in ("TOP", "BOT", "BOTTOM")):
        self.plated = True
    if (self.type == CADct.pcbLayerDiel):
      self.prepreg = True

  def __str__(self):
    res = "{}: {: >10}, {} ({})".format(self.__class__.__name__, self._type, self.name,
                                        self.thickness)
    return res

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (val not in CADct.PCBLayerTypes):
      msg = "UNKNOWN PCB layer type: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)
    self._type = val

  @property
  def material(self):
    return self._material

  @material.setter
  def material(self, val):
    if (pyauto_base.misc.isEmptyString(val)):
      return
    if (val not in CADct.PCBMaterials):
      msg = "UNKNOWN PCB material: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)
    self._material = val

  @property
  def thickness(self):
    return self._thickness

  @thickness.setter
  def thickness(self, val):
    self._thickness = pyauto_base.misc.valueWithUnit(val, unit=str(self.unit.units))

  @property
  def signal(self):
    return self._flags["sig"]

  @signal.setter
  def signal(self, bVal):
    if (self._type != CADct.pcbLayerCu):
      return
    if (bVal):
      self._flags["sig"] = True
      self._flags["plane"] = False
    else:
      self._flags["sig"] = False

  @property
  def plane(self):
    return self._flags["plane"]

  @plane.setter
  def plane(self, bVal):
    if (self._type != CADct.pcbLayerCu):
      return
    if (bVal):
      self._flags["plane"] = True
      self._flags["sig"] = False
    else:
      self._flags["plane"] = False

  @property
  def plated(self):
    return self._flags["plated"]

  @plated.setter
  def plated(self, bVal):
    if (self._type != CADct.pcbLayerCu):
      return
    if (bVal):
      self._flags["plated"] = True
    else:
      self._flags["plated"] = False

  @property
  def core(self):
    return self._flags["core"]

  @core.setter
  def core(self, bVal):
    if (self._type != CADct.pcbLayerDiel):
      return
    if (bVal):
      self._flags["core"] = True
      self._flags["prepreg"] = False
    else:
      self._flags["core"] = False

  @property
  def prepreg(self):
    return self._flags["prepreg"]

  @prepreg.setter
  def prepreg(self, bVal):
    if (self._type != CADct.pcbLayerDiel):
      return
    if (bVal):
      self._flags["prepreg"] = True
      self._flags["core"] = False
    else:
      self._flags["prepreg"] = False

  @property
  def dk(self):
    return self._diel_dk

  @dk.setter
  def dk(self, val):
    tmp = float(val)
    if (tmp < 0.0):
      return
    self._diel_dk = tmp

  @property
  def df(self):
    return self._diel_df

  @df.setter
  def df(self, val):
    tmp = float(val)
    if (tmp < 0.0):
      return
    self._diel_df = tmp

  def toJSON(self):
    res = {"type": self.type}
    if (self.name != ""):
      res.update({"name": self.name})
    res.update({"thickness": str(self.thickness.magnitude)})
    if (self.material is not None):
      res.update({"material": self.material})
    if (self.structure != ""):
      res.update({"structure": self.structure})
    # flags = [k for k,v in self._flags.items() if v is True]
    # if (len(flags) > 0):
    #   res.update({"flags": flags})
    return res

  def fromJSON(self, data):
    if ("type" in data.keys()):
      self.type = data["type"]
    if ("name" in data.keys()):
      self.name = data["name"]
    if ("thickness" in data.keys()):
      self.thickness = data["thickness"]
    if ("material" in data.keys()):
      self.material = data["material"]
    if ("structure" in data.keys()):
      self.structure = data["structure"]

class PCBVia(object):

  def __init__(self, drillSize, startLy="TOP", stopLy="BOT", unit="mm"):
    self._id = None
    self._drillSize = None
    self.unit = pyauto_base.misc.valueWithUnit(1, unit)
    self.startLy = startLy
    self.stopLy = stopLy
    self.fill = False
    self.laser = False

    self.drillSize = drillSize

  def __str__(self):
    return "{}: {} -> {}, {}{}{}".format(
        self.__class__.__name__,
        self.startLy,
        self.stopLy,
        self.drillSize,
        ", laser" if (self.laser) else "",
        ", fill" if (self.fill) else "",
    )

  @property
  def drillSize(self):
    return self._drillSize

  @drillSize.setter
  def drillSize(self, val):
    self._drillSize = pyauto_base.misc.valueWithUnit(val, unit=str(self.unit.units))

  def toJSON(self):
    res = {
        "drill": str(self._drillSize.magnitude),
        "start": self.startLy,
        "stop": self.stopLy
    }
    if (self.fill is True):
      res.update({"fill": "yes"})
    if (self.laser is True):
      res.update({"laser": "yes"})
    return res

  def fromJSON(self, data):
    if ("drill" in data.keys()):
      self.drillSize = data["drill"]
    if ("start" in data.keys()):
      self.startLy = data["start"]
    if ("stop" in data.keys()):
      self.stopLy = data["stop"]
    if ("fill" in data.keys()):
      self.fill = pyauto_base.misc.isTrueString(data["fill"])
    if ("laser" in data.keys()):
      self.laser = pyauto_base.misc.isTrueString(data["laser"])

class PCBStackup(object):

  def __init__(self, nCu=None, thickCu=None, thickDiel=None, unit="mm"):
    """
    :param nCu: number of Cu layers
    :param thickCu: thickness of Cu layer
    :param thickDiel: thickness of dielectric
    :param unit:
    """
    self._cnt_Cu = 0
    self._layers = []
    self._vias = []
    self.unit = pyauto_base.misc.valueWithUnit(1, unit)

    if ((nCu is None) or (nCu < 0)):
      return

    if (thickCu is None):
      tmp = pyauto_base.misc.valueWithUnit(0.03, "mm")
      thickCu = tmp.to(self.unit.units).magnitude
    if (thickDiel is None):
      tmp = pyauto_base.misc.valueWithUnit(0.1, "mm")
      thickDiel = tmp.to(self.unit.units).magnitude

    for i in range(0, nCu):
      ly = PCBLayer(strType=CADct.pcbLayerCu, thickness=thickCu, unit=str(self.unit.units))
      self._layers.append(ly)
      self._cnt_Cu += 1
      if (i != (nCu - 1)):
        ly = PCBLayer(strType=CADct.pcbLayerDiel,
                      thickness=thickDiel,
                      unit=str(self.unit.units))
        self._layers.append(ly)

  def __str__(self):
    return "{}: {:d} layer(s), {:d} via(s), {:.2f}".format(self.__class__.__name__,
                                                           self._cnt_Cu, len(self._vias),
                                                           self.thickness_total)

  @property
  def layers(self):
    return self._layers

  @property
  def vias(self):
    return self._vias

  @property
  def thickness_total(self):
    res = pyauto_base.misc.valueWithUnit(0.0, str(self.unit.units))
    for i in range(0, len(self._layers)):
      res += self._layers[i].thickness
    return res

  @property
  def thickness_noSMask(self):
    res = pyauto_base.misc.valueWithUnit(0.0, str(self.unit.units))
    for i in range(0, len(self._layers)):
      if (self._layers[i].type == CADct.pcbLayerSMask):
        continue
      res += self._layers[i].thickness
    return res

  @property
  def nLayers(self):
    return self._cnt_Cu

  @property
  def nVias(self):
    return len(self._vias)

  def validate(self):
    if (len(self._layers) == 0):
      logger.warning("NO layers in the stackup")
      return False

    if (self._cnt_Cu < 2):
      logger.warning("TOO FEW Cu layers in the stackup")
      return False

    if ((self._layers[0].type == CADct.pcbLayerSMask)
        and (self._layers[-1].type != CADct.pcbLayerSMask)):
      logger.warning("soldermask on TOP ONLY")
      return False
    elif ((self._layers[0].type != CADct.pcbLayerSMask)
          and (self._layers[-1].type == CADct.pcbLayerSMask)):
      logger.warning("soldermask on BOT ONLY")
      return False

    lstCuId = []
    for i in range(0, len(self._layers)):
      ly = self._layers[i]
      if (ly.type == CADct.pcbLayerCu):
        if (ly.name != ""):
          if (ly.name in lstCuId):
            logger.warning("DUPLICATE layer {}: {}".format(i, ly))
            return False
          else:
            lstCuId.append(ly.name)
    #if(lstCuId[0] != "TOP"):
    #  logger.warning("first Cu layer NOT MARKED as TOP")
    #  return False
    #if(lstCuId[-1] != "BOT"):
    #  logger.warning("last Cu layer NOT MARKED as BOT")
    #  return False
    #if (len(self._vias) == 0):
    #  logger.warning("NO vias in the stackup")

    lstViaId = []
    for i in range(0, len(self._vias)):
      via = self._vias[i]
      viaId = "{}_{}_{}".format(via.startLy, via.stopLy, via.drillSize.magnitude)
      if (viaId in lstViaId):
        logger.warning("DUPLICATE via {}: {}".format(i, via))
        return False
      else:
        lstViaId.append(viaId)

    return True

  def addLayer(self, ly):
    if (not isinstance(ly, PCBLayer)):
      msg = "CANNOT add type {} to {}".format(type(ly), self.__class__.__name__)
      logger.error(msg)
      raise RuntimeError(msg)
    if (ly.unit != self.unit):
      msg = "PCBLayer has different units: {} <> {}".format(ly.unit.units, self.unit.units)
      logger.error(msg)
      raise RuntimeError(msg)

    self._layers.append(ly)
    if (ly.type == CADct.pcbLayerCu):
      self._cnt_Cu += 1

  def addVia(self, via):
    if (not isinstance(via, PCBVia)):
      msg = "CANNOT add type {} to {}".format(type(via), self.__class__.__name__)
      logger.error(msg)
      raise RuntimeError(msg)
    if (via.unit != self.unit):
      msg = "PCBVia has different units: {} <> {}".format(via.unit.units, self.unit.units)
      logger.error(msg)
      raise RuntimeError(msg)

    self._vias.append(via)

  def getLayerIds(self):
    res = []
    for layer in self._layers:
      res.append(layer.name)
    return res

  def getLayerIds_Cu(self):
    res = []
    for layer in self._layers:
      if (layer.type == CADct.pcbLayerCu):
        res.append(layer.name)
    return res

  def addCore(self):
    raise NotImplementedError

  def addPrepreg(self):
    raise NotImplementedError

  def addSMask(self):
    raise NotImplementedError

  def showInfo(self):
    logger.info(str(self))
    for ly in self._layers:
      logger.info(ly)
    for via in self._vias:
      logger.info(via)

  def readJSON(self, fPath):
    raise NotImplementedError

  def writeJSON(self, fPath):
    content = {
        "unit": str(self.unit.units),
        "layers": [ly.toJSON() for ly in self._layers],
        "vias": [via.toJSON() for via in self._vias]
    }

    with open(fPath, "w") as f_out:
      json.dump(content, f_out, indent=2)
    logger.info("{} written".format(fPath))

  def writeSVG(self, fPath):
    import pyauto_base.svg
    #appParams.path["out_svg"] = os.path.join(appParams.path["out"], "stackup_{}.svg".format(stackup.id))
    dictColors = {
        CADct.pcbLayerCu: "rgb(184, 115, 51)",
        CADct.pcbLayerDiel: "rgb(0, 128, 0)",
        CADct.pcbLayerEmCap: "rgb(0, 0, 128)",
        CADct.pcbLayerEmRes: "rgb(128, 0, 0)",
        CADct.pcbLayerSMask: "rgb(0, 144, 192)",
        CADct.pcbLayerCoverlay: "rgb(223, 193, 42)",
        CADct.pcbLayerAdhesive: "rgb(255, 255, 224)",
    }

    widthVia = 50
    width = (200 + len(self._vias) * widthVia)
    svg = pyauto_base.svg.svgImage(fPath, width=width, height=200)

    svgScale = 10 // min([ly.thickness.magnitude for ly in self._layers])
    x0 = 10
    y0 = 10
    w = width - x0
    y = y0
    dictLayerCu = {}
    for ly in self._layers:
      h = (ly.thickness.magnitude * svgScale)
      sh = pyauto_base.svg.svgRectangle(x=x0, y=y, width=w, height=h)
      sh.fill = dictColors[ly.type]
      if (ly.type == CADct.pcbLayerCu):
        dictLayerCu[ly.name] = [float(sh.y), float(sh.height)]
      y += h
      svg.addElement(sh)
    svg.height = (y + y0)

    x = 200
    drl = 30
    thick = 4
    for via in self._vias:
      if (thick > min(dictLayerCu[via.startLy][1], dictLayerCu[via.stopLy][1])):
        logger.warning("UNEXPECTED Cu thickness - drawing may be affected")
        thick = 1

      sh = pyauto_base.svg.svgPolygon()
      y_startLy_top = dictLayerCu[via.startLy][0]
      y_startLy_bot = dictLayerCu[via.startLy][0] + dictLayerCu[via.startLy][1]
      y_stopLy_top = dictLayerCu[via.stopLy][0]
      y_stopLy_bot = dictLayerCu[via.stopLy][0] + dictLayerCu[via.stopLy][1]
      x_ul = x + ((widthVia - drl) / 2)
      x_ur = x_ul + drl
      x_bl = x_ul
      x_br = x_ur
      stopLy_idx = [ly.name for ly in self.layers].index(via.stopLy)
      if (via.laser):
        if (stopLy_idx < self.nLayers):
          x_bl += 5
          x_br -= 5
        else:
          x_ul += 5
          x_ur -= 5
      if (via.fill):
        y_u = (y_startLy_top + y_startLy_bot) / 2
        y_b = (y_stopLy_top + y_stopLy_bot) / 2
        if (via.laser):
          sh.fill = dictColors[CADct.pcbLayerCu]
        else:
          sh.fill = "rgb(192, 192, 192)"
        sh.stroke = dictColors[CADct.pcbLayerCu]
        sh.stroke_width = thick
      else:
        y_u = y_startLy_top
        y_b = y_stopLy_bot
        sh.fill = "rgb(255, 255, 255)"
        sh.stroke = "rgb(255, 255, 255)"
        sh.stroke_width = 0
      sh.addPoint(x_ul, y_u)
      sh.addPoint(x_ur, y_u)
      sh.addPoint(x_br, y_b)
      sh.addPoint(x_bl, y_b)
      svg.addElement(sh)

      if (not via.laser):
        wl = pyauto_base.svg.svgLine(x1=x_ul, y1=y_u, x2=x_bl, y2=y_b)
        wl.stroke = dictColors[CADct.pcbLayerCu]
        wl.stroke_width = thick
        svg.addElement(wl)
        wr = pyauto_base.svg.svgLine(x1=x_ur, y1=y_u, x2=x_br, y2=y_b)
        wr.stroke = dictColors[CADct.pcbLayerCu]
        wr.stroke_width = thick
        svg.addElement(wr)

      x += 50
    svg.write()

class PCBArtworkLayer(object):

  def __init__(self, strType, strName, opts=None):
    self._type = None
    self._content = []

    self.type = strType
    self.name = strName
    if (opts is None):
      self.opts = []
    else:
      self.opts = opts.split(",")

  def __str__(self):
    res = "{}: {: >4}, {}".format(self.__class__.__name__, self._type, self.name)
    return res

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (val not in CADct.PCBArtworkTypes):
      msg = "UNKNOWN PCB layer type: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)
    self._type = val

  @property
  def content(self):
    return self._content

  @content.setter
  def content(self, val):
    if (isinstance(val, list)):
      self._content = val
    elif (isinstance(val, str)):
      self._content = [val]

  def toJSON(self):
    res = {"type": self.type, "name": self.name}
    if (len(self.opts) > 0):
      res.update({"options": self.opts})
    res.update({"content": self.content})
    return res

  def fromJSON(self, content):
    raise NotImplementedError

class PCBArtwork(object):

  def __init__(self):
    self._layers = []

  def __str__(self):
    res = "{}: {} layer(s)".format(self.__class__.__name__, len(self._layers))
    return res

  @property
  def layers(self):
    return self._layers

  @property
  def nLayers(self):
    return len(self._layers)

  def addLayer(self, ly):
    if (not isinstance(ly, PCBArtworkLayer)):
      msg = "CANNOT add type {} to {}".format(type(ly), self.__class__.__name__)
      logger.error(msg)
      raise RuntimeError(msg)

    self._layers.append(ly)

  def showInfo(self):
    logger.info(str(self))
    for ly in self._layers:
      logger.info(ly)

  def readJSON(self, fPath):
    raise NotImplementedError

  def writeJSON(self, fPath):
    content = {"layers": [ly.toJSON() for ly in self._layers]}

    with open(fPath, "w") as f_out:
      json.dump(content, f_out, indent=2)
    logger.info("{} written".format(fPath))

  def _writeAllegroScript_v16(self, fPath):
    opts = {
        None: "(0 0 0 0 25400 1 0 0 0 0 0 1 1 0)",
        "line_1": "(0 0 0 100 25400 1 0 0 0 0 0 1 1 0)",
        "line_2": "(0 0 0 200 25400 1 0 0 0 0 0 1 1 0)",
        "line_5": "(0 0 0 500 25400 1 0 0 0 0 0 1 1 0)",
        "line_10": "(0 0 0 1000 25400 1 0 0 0 0 0 1 1 0)"
    }
    design_outline = "BOARD GEOMETRY/DESIGN_OUTLINE"
    route_path = "BOARD GEOMETRY/NCROUTE_PATH"

    res = []
    for ly in self._layers:
      #print("DBG", ly)
      #print("DBG", ly.opts)
      if (ly.type == CADct.pcbArtCu):
        artOpts = opts["line_1"]
        artContent = [design_outline, route_path]
        if (ly.content == []):
          artContent += ["{}/{}".format(t, ly.name) for t in ("ETCH", "PIN", "VIA CLASS")]
        else:
          artContent += [
              "{}/{}".format(t, ly.content[0]) for t in ("ETCH", "PIN", "VIA CLASS")
          ]

      elif (ly.type == CADct.pcbArtSMT):
        artOpts = opts[None]
        artContent = [design_outline]
        if ("negative" in ly.opts):
          artContent += [
              "{}/SOLDERMASK_TOP".format(t)
              for t in ("PIN", "VIA CLASS", "PACKAGE GEOMETRY", "BOARD GEOMETRY")
          ]
        elif ("positive" in ly.opts):
          artContent += [
              "PACKAGE GEOMETRY/SOLDERMASK_TOP_POS", "BOARD GEOMETRY/SOLDERMASK_TOP"
          ]
        else:
          raise NotImplementedError
      elif (ly.type == CADct.pcbArtSMB):
        artOpts = opts[None]
        artContent = [design_outline]
        if ("negative" in ly.opts):
          artContent += [
              "{}/SOLDERMASK_BOTTOM".format(t)
              for t in ("PIN", "VIA CLASS", "PACKAGE GEOMETRY", "BOARD GEOMETRY")
          ]
        elif ("positive" in ly.opts):
          artContent += [
              "PACKAGE GEOMETRY/SOLDERMASK_BOT_POS", "BOARD GEOMETRY/SOLDERMASK_BOTTOM"
          ]
        else:
          raise NotImplementedError

      elif (ly.type == CADct.pcbArtSPT):
        artContent = [design_outline]
        artContent += [
            "{}/PASTEMASK_TOP".format(t) for t in ("PIN", "VIA CLASS", "PACKAGE GEOMETRY")
        ]
        if (len(ly.opts) == 0):
          artOpts = opts[None]
        elif (len(ly.opts) == 1):
          artOpts = opts[ly.opts[0]]
        else:
          raise NotImplementedError
      elif (ly.type == CADct.pcbArtSPB):
        artContent = [design_outline]
        artContent += [
            "{}/PASTEMASK_BOTTOM".format(t)
            for t in ("PIN", "VIA CLASS", "PACKAGE GEOMETRY")
        ]
        if (len(ly.opts) == 0):
          artOpts = opts[None]
        elif (len(ly.opts) == 1):
          artOpts = opts[ly.opts[0]]
        else:
          raise NotImplementedError

      elif (ly.type == CADct.pcbArtSKT):
        artOpts = opts[None]
        artContent = [design_outline]
        if ("auto" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_TOP"]
        elif ("auto_ref" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_TOP", "REF DES/SILKSCREEN_TOP"]
        else:
          raise NotImplementedError
      elif (ly.type == CADct.pcbArtSKB):
        artOpts = opts[None]
        artContent = [design_outline]
        if ("auto" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_BOTTOM"]
        elif ("auto_ref" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_BOTTOM", "REF DES/SILKSCREEN_BOTTOM"]
        else:
          raise NotImplementedError

      elif (ly.type == CADct.pcbArtAST):
        artOpts = opts[None]
        artContent = [design_outline]
        artContent += ["REF DES/ASSEMBLY_TOP", "PACKAGE GEOMETRY/ASSEMBLY_TOP"]
      elif (ly.type == CADct.pcbArtASB):
        artOpts = opts[None]
        artContent = [design_outline]
        artContent += ["REF DES/ASSEMBLY_BOTTOM", "PACKAGE GEOMETRY/ASSEMBLY_BOTTOM"]

      elif (ly.type == CADct.pcbArtDOC):
        artContent = [design_outline, route_path]
        artContent += ly.content
        if (len(ly.opts) == 0):
          artOpts = opts[None]
        elif (len(ly.opts) == 1):
          artOpts = opts[ly.opts[0]]
        else:
          raise NotImplementedError
      else:
        raise NotImplementedError

      res.append("(axlfcreate \"{}\" '{}\n".format(ly.name, artOpts))
      res.append("\t'({} ))\n".format(" ".join(
          ["\"{}\"".format(t) for t in sorted(artContent)])))

    with open(fPath, "w") as fOut:
      for ln in res:
        fOut.write(ln)

    logger.info("{} written".format(fPath))

  def _writeAllegroScript_v17(self, fPath):
    opts = {
        "?negative": "nil",
        "?undefineLineWidth": "0.01",
        "?sequence": "0",
        "?rotation": "0",
        "?xOffset": "0.00",
        "?yOffset": "0.00",
        "?shapeBoundingBox": "254.00",
        "?mirrored": "nil",
        "?fullContact": "nil",
        "?suppressUnconnectPads": "nil",
        "?drawMissingPadApertures": "nil",
        "?useApertureRotation": "nil",
        "?suppressShapeFill": "t",
        "?vectorBasedPad": "t",
        "?drawHolesOnly": "nil",
        "?domains": "(ipc2581 pdf artwork visibility )",
        "?ipc2581": "()",
    }
    design_outline = "BOARD GEOMETRY/DESIGN_OUTLINE"
    design_cutout = "BOARD GEOMETRY/CUTOUT"
    route_path = "BOARD GEOMETRY/NCROUTE_PATH"

    res = []
    seqCnt = 0
    for ly in self._layers:
      #print("DBG", ly)
      #print("DBG", ly.opts)
      seqCnt += 1
      artOpts = copy.deepcopy(opts)
      artOpts["?sequence"] = str(seqCnt)
      if (ly.type == CADct.pcbArtCu):
        artContent = [design_outline, design_cutout]
        artOpts["?suppressUnconnectPads"] = "t"
        if (ly.content == []):
          artContent += ["{}/{}".format(t, ly.name) for t in ("ETCH", "PIN", "VIA CLASS")]
        else:
          artContent += [
              "{}/{}".format(t, ly.content[0]) for t in ("ETCH", "PIN", "VIA CLASS")
          ]

      elif (ly.type == CADct.pcbArtSMT):
        artContent = [design_outline]
        artContent += ["{}/SOLDERMASK_TOP".format(t) for t in ("PIN", "VIA CLASS")]
        if ("negative" in ly.opts):
          artContent += ["PACKAGE GEOMETRY/SOLDERMASK_TOP"]
          artContent += ["BOARD GEOMETRY/SOLDERMASK_TOP"]
        if ("positive" in ly.opts):
          artContent += ["PACKAGE GEOMETRY/SOLDERMASK_TOP_POS"]
          artContent += ["BOARD GEOMETRY/SOLDERMASK_TOP"]
        if ("socket" in ly.opts):
          artContent += ["BOARD GEOMETRY/SOLDERMASK_TOP_SOCKET"]
      elif (ly.type == CADct.pcbArtSMB):
        artContent = [design_outline]
        artContent += ["{}/SOLDERMASK_BOTTOM".format(t) for t in ("PIN", "VIA CLASS")]
        if ("negative" in ly.opts):
          artContent += ["PACKAGE GEOMETRY/SOLDERMASK_BOTTOM"]
          artContent += ["BOARD GEOMETRY/SOLDERMASK_BOTTOM"]
        if ("positive" in ly.opts):
          artContent += ["PACKAGE GEOMETRY/SOLDERMASK_BOT_POS"]
          artContent += ["BOARD GEOMETRY/SOLDERMASK_BOTTOM"]
        if ("socket" in ly.opts):
          artContent += ["BOARD GEOMETRY/SOLDERMASK_BOT_SOCKET"]

      elif (ly.type == CADct.pcbArtSPT):
        artContent = [design_outline]
        artContent += [
            "{}/PASTEMASK_TOP".format(t) for t in ("PIN", "VIA CLASS", "PACKAGE GEOMETRY")
        ]
      elif (ly.type == CADct.pcbArtSPB):
        artContent = [design_outline]
        artContent += [
            "{}/PASTEMASK_BOTTOM".format(t)
            for t in ("PIN", "VIA CLASS", "PACKAGE GEOMETRY")
        ]

      elif (ly.type == CADct.pcbArtSKT):
        artContent = [design_outline]
        if ("no_auto" in ly.opts):
          artContent += [
              "REF DES/SILKSCREEN_TOP", "PACKAGE GEOMETRY/SILKSCREEN_TOP",
              "BOARD GEOMETRY/SILKSCREEN_TOP"
          ]
        elif ("auto" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_TOP"]
        elif ("auto_ref" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_TOP", "REF DES/SILKSCREEN_TOP"]
        elif ("auto_brd" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_TOP", "BOARD GEOMETRY/SILKSCREEN_TOP"]
        elif ("auto_ref_brd" in ly.opts):
          artContent += [
              "MANUFACTURING/AUTOSILK_TOP", "REF DES/SILKSCREEN_TOP",
              "BOARD GEOMETRY/SILKSCREEN_TOP"
          ]
        else:
          raise NotImplementedError
      elif (ly.type == CADct.pcbArtSKB):
        artContent = [design_outline]
        if ("no_auto" in ly.opts):
          artContent += [
              "REF DES/SILKSCREEN_BOTTOM", "PACKAGE GEOMETRY/SILKSCREEN_BOTTOM",
              "BOARD GEOMETRY/SILKSCREEN_BOTTOM"
          ]
        elif ("auto" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_BOTTOM"]
        elif ("auto_ref" in ly.opts):
          artContent += ["MANUFACTURING/AUTOSILK_BOTTOM", "REF DES/SILKSCREEN_BOTTOM"]
        elif ("auto_brd" in ly.opts):
          artContent += [
              "MANUFACTURING/AUTOSILK_BOTTOM", "BOARD GEOMETRY/SILKSCREEN_BOTTOM"
          ]
        elif ("auto_ref_brd" in ly.opts):
          artContent += [
              "MANUFACTURING/AUTOSILK_BOTTOM", "REF DES/SILKSCREEN_BOTTOM",
              "BOARD GEOMETRY/SILKSCREEN_BOTTOM"
          ]
        else:
          raise NotImplementedError

      elif (ly.type == CADct.pcbArtAST):
        artContent = [design_outline]
        artContent += ["REF DES/ASSEMBLY_TOP", "PACKAGE GEOMETRY/ASSEMBLY_TOP"]
      elif (ly.type == CADct.pcbArtASB):
        artContent = [design_outline]
        artContent += ["REF DES/ASSEMBLY_BOTTOM", "PACKAGE GEOMETRY/ASSEMBLY_BOTTOM"]

      elif (ly.type == CADct.pcbArtDOC):
        artContent = [design_outline, design_cutout]
        ln_width = 0.0
        for t in ly.opts:
          if (t.startswith("line_")):
            ln_width = float(t[5:])
        artOpts["?undefineLineWidth"] = "{:.2f}".format(ln_width)
        if ("mirror" in ly.opts):
          artOpts["?mirrored"] = "1"
        artContent += ly.content
      else:
        raise NotImplementedError

      res.append("axlFilmCreate( \"{}\" ?negative {} ?undefineLineWidth {} "
                 "?sequence {} \n".format(ly.name, artOpts["?negative"],
                                          artOpts["?undefineLineWidth"],
                                          artOpts["?sequence"]))
      res.append("\t?rotation {} ?xOffset {} ?yOffset {} ?shapeBoundingBox {} "
                 "?mirrored {} ?fullContact {} \n".format(artOpts["?rotation"],
                                                          artOpts["?xOffset"],
                                                          artOpts["?yOffset"],
                                                          artOpts["?shapeBoundingBox"],
                                                          artOpts["?mirrored"],
                                                          artOpts["?fullContact"]))
      res.append("\t?suppressUnconnectPads {} ?drawMissingPadApertures {} "
                 "?useApertureRotation {} \n".format(artOpts["?suppressUnconnectPads"],
                                                     artOpts["?drawMissingPadApertures"],
                                                     artOpts["?useApertureRotation"]))
      res.append("\t?suppressShapeFill {} ?vectorBasedPad {} "
                 "?drawHolesOnly {} \n".format(artOpts["?suppressShapeFill"],
                                               artOpts["?vectorBasedPad"],
                                               artOpts["?drawHolesOnly"]))
      res.append("\t?domains '{} ?ipc2581 '{} \n".format(artOpts["?domains"],
                                                         artOpts["?ipc2581"]))
      res.append("\t?layers '({} ))\n".format(" ".join(
          ["\"{}\"".format(t) for t in sorted(artContent)])))

    with open(fPath, "w") as fOut:
      for ln in res:
        fOut.write(ln)

    logger.info("{} written".format(fPath))

  def writeAllegroScript(self, fPath):
    #self._writeAllegroScript_v16(fPath)
    self._writeAllegroScript_v17(fPath)

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")

pinTypeAnalog = "analog"
pinTypePwr = "pwr"
pinTypeGnd = "gnd"
pinTypeIN = "in"
pinTypeOUT = "out"
pinTypeIO = "inout"
pinTypeTri = "3state"
pinTypeOpenC = "openC"
pinTypeOpenE = "openE"
pinTypeOpenD = "openD"
pinTypeOpenS = "openS"
pinTypeNC = "NC"
lstPinTypes = (
    pinTypeAnalog,
    pinTypePwr,
    pinTypeGnd,
    pinTypeIN,
    pinTypeOUT,
    pinTypeIO,
    pinTypeTri,
    pinTypeOpenC,
    pinTypeOpenE,
    pinTypeOpenD,
    pinTypeOpenS,
    pinTypeNC,
)

class PinTypeConv(object):
  dictFromCIS = {
      "Power": pinTypePwr,
      "Passive": pinTypeAnalog,
      "Input": pinTypeIN,
      "Output": pinTypeOUT,
      "Bidirectional": pinTypeIO,
      "BiDirectional": pinTypeIO,
      "Hi-Z": pinTypeTri,
      "Open Collector": pinTypeOpenC,
      "Open Emitter": pinTypeOpenE,
  }
  dictToCIS = {
      pinTypePwr: "Power",
      pinTypeGnd: "Power",
      pinTypeAnalog: "Passive",
      pinTypeIN: "Input",
      pinTypeOUT: "Output",
      pinTypeIO: "Bidirectional",
      pinTypeTri: "Hi-Z",
      pinTypeOpenC: "Open Collector",
      pinTypeOpenE: "Open Emitter",
  }

  dictFromHDL = {
      "POWER": pinTypePwr,
      "GROUND": pinTypeGnd,
      "ANALOG": pinTypeAnalog,
      "UNSPEC": pinTypeAnalog,
      "INPUT": pinTypeIN,
      "OUTPUT": pinTypeOUT,
      "BIDIR": pinTypeIO,
      "NC": pinTypeNC,
  }
  dictToHDL = {
      pinTypePwr: "POWER",
      pinTypeGnd: "GROUND",
      pinTypeAnalog: "ANALOG",
      pinTypeIN: "INPUT",
      pinTypeOUT: "OUTPUT",
      pinTypeIO: "BIDIR",
      pinTypeNC: "NC",
  }

  dictFromDx = {
      "POWER": pinTypePwr,
      "GROUND": pinTypeGnd,
      "ANALOG": pinTypeAnalog,
      "IN": pinTypeIN,
      "OUT": pinTypeOUT,
      "BI": pinTypeIO,
      "OCL": pinTypeOpenC,
      "OEM": pinTypeOpenE,
  }
  dictToDx = {
      pinTypePwr: "POWER",
      pinTypeGnd: "GROUND",
      pinTypeAnalog: "ANALOG",
      pinTypeIN: "IN",
      pinTypeOUT: "OUT",
      pinTypeIO: "BI",
      pinTypeOpenC: "OCL",
      pinTypeOpenD: "OCL",
      pinTypeOpenE: "OEM",
      pinTypeOpenS: "OEM",
  }

  dictFromAltera = {
      "analog": pinTypeAnalog,
      "power": pinTypePwr,
      "gnd": pinTypeGnd,
      "input": pinTypeIN,
      "output": pinTypeOUT,
      "bidir": pinTypeIO,
  }
  dictToAltera = {
      pinTypeAnalog: "analog",
      pinTypePwr: "power",
      pinTypeGnd: "gnd",
      pinTypeIN: "input",
      pinTypeOUT: "output",
      pinTypeIO: "bidir",
  }

  dictFromKiCAD = {
      "analog": pinTypeAnalog,
      "passive": pinTypeAnalog,
      "input": pinTypeIN,
      "output": pinTypeOUT,
      "power_in": pinTypePwr,
      "power_out": pinTypePwr,
      "tri_state": pinTypeTri,
      "open_collector": pinTypeOpenC,
      "no_connect": pinTypeNC,
  }

pinSideTop = "top"
pinSideBot = "bot"
pinSideLeft = "left"
pinSideRight = "right"
lstPinSides = (pinSideTop, pinSideBot, pinSideLeft, pinSideRight)

pcbLayerCu = "Cu"
pcbLayerDiel = "dielectric"
pcbLayerEmCap = "embedded_cap"
pcbLayerEmRes = "embedded_res"
pcbLayerPolymide = "polymide"
pcbLayerAdhesive = "adhesive"
pcbLayerSMask = "soldermask"
pcbLayerCoverlay = "coverlay"
pcbLayerSilk = "silkscreen"
PCBLayerTypes = (pcbLayerCu, pcbLayerDiel, pcbLayerEmCap, pcbLayerEmRes, pcbLayerPolymide,
                 pcbLayerAdhesive, pcbLayerSMask, pcbLayerCoverlay, pcbLayerSilk)

pcbMaterial_Poly = "polymide"
pcbMaterial_Adhesive = "adhesive"
pcbMaterial_FR4 = "FR4"
pcbMaterial_FR408 = "FR408"
pcbMaterial_Meg6 = "Megtron6"
pcbMaterial_Meg7G = "Megtron7G"
pcbMaterial_Meg7N = "Megtron7N"
pcbMaterial_IT158 = "IT-158"
pcbMaterial_EM892BK = "EM-892BK"
pcbMaterial_EM892K = "EM-892K"
pcbMaterial_DS8502SQ = "DS-8502SQ"
pcbMaterial_DS8502SQN = "DS-8502SQN"
pcbMaterial_R5680N = "R-5680(N)"
pcbMaterial_R5785N = "R-5785(N)"
# yapf: disable
PCBMaterials = (pcbMaterial_Poly, pcbMaterial_Adhesive,
                pcbMaterial_FR4, pcbMaterial_FR408,
                pcbMaterial_Meg6, pcbMaterial_Meg7G, pcbMaterial_Meg7N,
                pcbMaterial_IT158, pcbMaterial_EM892BK, pcbMaterial_EM892K,
                pcbMaterial_DS8502SQ, pcbMaterial_DS8502SQN,
                pcbMaterial_R5680N, pcbMaterial_R5785N
                )
# yapf: enable

pcbArtCu = "Cu"
pcbArtSMT = "SMT"
pcbArtSMB = "SMB"
pcbArtSPT = "SPT"
pcbArtSPB = "SPB"
pcbArtSKT = "SKT"
pcbArtSKB = "SKB"
pcbArtAST = "AST"
pcbArtASB = "ASB"
pcbArtDOC = "DOC"
PCBArtworkTypes = (pcbArtCu, pcbArtSMT, pcbArtSMB, pcbArtSPT, pcbArtSPB, pcbArtSKT,
                   pcbArtSKB, pcbArtAST, pcbArtASB, pcbArtDOC)

regexPartRes = r"R[0-9]+"
regexPartCap = r"C[0-9]+"
regexPartInd = r"L[0-9]+"
regexPartFB = r"FB[0-9]+"
regexPartConn = r"[JP][0-9]+"
regexPartIC = r"[UXY][0-9]+"
regexPartICorConn = r"[UXYJP][0-9]+"
regexPartTP = r"TP[0-9]+"

# yapf: disable
regexNetPwr = (r"^([\+-][0-9]+V[0-9]*)([A-Za-z_]?.*)$",
               r"^([PN][0-9]+V[0-9]*)([A-Za-z_]?.*)$",
               r"^([\+-][0-9\.]+V)([A-Za-z_]?.*)$"
               )
# yapf: enable
regexNetGnd = r"[AS]?GND.*"
regexNetNC = r"NC"

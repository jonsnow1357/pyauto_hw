#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.base as CADbase

componentLibVer = ["2.3", "2.0"]
moduleLibVer = ["1", "1"]

ruleNameLocal = "local"
ruleNameCorp = "corporate"
ruleName = (ruleNameLocal, ruleNameCorp)

class libRules(object):

  def __init__(self, ruleId):
    self.name = ruleId
    if (ruleId == ruleName[0]):
      self.grid = 50
      self.pinTextOffset = [0, 20]
      self.pinLength = [0, 50, 75, 100, 125, 150, 175, 200]
      self.pinNameTextSize = 30
      self.pinNrTextSize = 30
      self.fieldTextSize = 40
      self.textSize = 60
      self.lineThickness = [5, 10]
    elif (ruleId == ruleName[1]):
      self.grid = 50
      self.pinTextOffset = [0, 20]
      self.pinLength = [0, 50, 100, 125, 150]
      self.pinNameTextSize = 40
      self.pinNrTextSize = 40
      self.fieldTextSize = 40
      self.textSize = 40
      self.lineThickness = [5, 10]

  def showInfo(self):
    logger.info("rule: {}".format(self.name))
    logger.info("{: <15}: {}".format("pinTextOffset", self.pinTextOffset))
    logger.info("{: <15}: {}".format("fieldTextSize", self.fieldTextSize))
    logger.info("{: <15}: {}".format("textSize", self.textSize))
    logger.info("{: <15}: {}".format("pinNrTextSize", self.pinNrTextSize))
    logger.info("{: <15}: {}".format("pinNameTextSize", self.pinNameTextSize))
    logger.info("{: <15}: {}".format("pinLength", self.pinLength))

class modRules(object):
  name = ""
  textSize = 0
  textWidth = 0

  def __init__(self, ruleId):
    self.name = ruleId
    if (ruleId == ruleName[0]):
      self.textSize = 50
      self.textWidth = 8
    elif (ruleId == ruleName[1]):
      self.textSize = 50
      self.textWidth = 8

  def showInfo(self):
    logger.info("rule: {}".format(self.name))
    logger.info("{: <9}: {}".format("textSize", self.textSize))
    logger.info("{: <9}: {}".format("textWidth", self.textWidth))

_work_libRules = libRules(ruleName[0])

def setLibRules(strRule):
  global _work_libRules

  if (strRule not in ruleName):
    logger.error("UNKNOWN rule: {}".format(strRule))
    return
  _work_libRules = libRules(strRule)

def getLibRules():
  return _work_libRules

_work_modRules = modRules(ruleName[0])

def setModRules(strRule):
  global _work_modRules

  if (strRule not in ruleName):
    logger.error("UNKNOWN rule: {}".format(strRule))
    return
  _work_modRules = modRules(strRule)

def getModRules():
  return _work_modRules

class libSymbol(object):

  def __init__(self):
    self.libDef = None
    self.libFields = []
    self.libFPList = []
    self.libAlias = []
    self.libDraw = []
    self.doc = []

    self._cntFix = 0
    self._cntErr = 0

  def getName(self):
    return self.libDef.split()[1]

  def getRef(self):
    return self.libDef.split()[2]

  def getDEF_textOffset(self):
    return int(self.libDef.split()[4])

  def getDEF_flag(self):
    return self.libDef.split()[9]

  def getField_posX(self, fId):
    return int(self.libFields[fId].split()[2])

  def getField_posY(self, fId):
    return int(self.libFields[fId].split()[3])

  def getField_textSize(self, fId):
    return int(self.libFields[fId].split()[4])

  def getField_textOrientation(self, fId):
    return self.libFields[fId].split()[5]

  def getField_hTextJustify(self, fId):
    return self.libFields[fId].split()[7]

  def getField_vTextJustify(self, fId):
    return self.libFields[fId].split()[8]

  def getLibText(self):
    res = [self.libDef]
    res += self.libFields
    if (len(self.libFPList) > 0):
      res += ["$FPLIST"]
      res += self.libFPList
      res += ["$ENDFPLIST"]
    res += ["DRAW"]
    res += self.libDraw
    res += ["ENDDRAW"]
    res += ["ENDDEF"]
    return res

  def getDocText(self):
    return self.doc

  def getCheckStats(self):
    return [self._cntErr, self._cntFix]

  def isField_visible(self, fId):
    tmp = self.libFields[fId].split()[6]
    if (tmp == "V"):
      return True
    elif (tmp == "I"):
      return False
    else:
      msg = "UNEXPECTED value for field: {}".format(tmp)
      logger.error(msg)
      raise RuntimeError(msg)

  def hasPowerFlag(self):
    if (self.getDEF_flag() == "P"):
      return True
    else:
      return False

  def setName(self, strName):
    tmp = self.libDef.split()
    tmp[1] = strName
    self.libDef = " ".join(tmp)

    tmp = self.libFields[1].split()
    tmp[1] = "\"{}\"".format(strName)
    self.libFields[1] = " ".join(tmp)

  def setDEF_textOffset(self, offset):
    tmp = self.libDef.split()
    tmp[4] = str(offset)
    self.libDef = " ".join(tmp)

  def setField_textSize(self, fId, textSize):
    tmp = self.libFields[fId].split()
    tmp[4] = str(textSize)
    self.libFields[fId] = " ".join(tmp)

  def setField_textOrientationH(self, fId):
    tmp = self.libFields[fId].split()
    tmp[5] = "H"
    self.libFields[fId] = " ".join(tmp)

  def setField_textOrientationV(self, fId):
    tmp = self.libFields[fId].split()
    tmp[5] = "V"
    self.libFields[fId] = " ".join(tmp)

  def setField_visible(self, fId):
    tmp = self.libFields[fId].split()
    tmp[6] = "V"
    self.libFields[fId] = " ".join(tmp)

  def setField_invisible(self, fId):
    tmp = self.libFields[fId].split()
    tmp[6] = "I"
    self.libFields[fId] = " ".join(tmp)

  def setField_hTextJustifyC(self, fId):
    tmp = self.libFields[fId].split()
    tmp[7] = "C"
    self.libFields[fId] = " ".join(tmp)

  def setField_hTextJustifyL(self, fId):
    tmp = self.libFields[fId].split()
    tmp[7] = "L"
    self.libFields[fId] = " ".join(tmp)

  def setField_hTextJustifyR(self, fId):
    tmp = self.libFields[fId].split()
    tmp[7] = "R"
    self.libFields[fId] = " ".join(tmp)

  def setField_vTextJustifyC(self, fId):
    tmp = self.libFields[fId].split()
    tmp[8] = "CNN"
    self.libFields[fId] = " ".join(tmp)

  def setField_vTextJustifyT(self, fId):
    tmp = self.libFields[fId].split()
    tmp[8] = "TNN"
    self.libFields[fId] = " ".join(tmp)

  def setField_vTextJustifyB(self, fId):
    tmp = self.libFields[fId].split()
    tmp[8] = "BNN"
    self.libFields[fId] = " ".join(tmp)

  def _chkDEF(self, bFix=True):
    dcmName = self.getName()
    fld = self.libDef.split()
    #logger.info(fld)

    if (fld[0] != "DEF"):
      logger.error("NOT a symbol definition: {}".format(" ".join(fld)))
      self._cntErr += 1
      return

    if (self.hasPowerFlag()):
      logger.info("{}: power symbol".format(dcmName))

    if (self.getDEF_textOffset() not in _work_libRules.pinTextOffset):
      if (bFix):
        logger.warning("{}: incorrect pinTextOffset {} - changed to {}".format(
            dcmName, self.getDEF_textOffset(), _work_libRules.pinTextOffset[0]))
        self.setDEF_textOffset(_work_libRules.pinTextOffset[0])
        self._cntFix += 1
      else:
        logger.error("{}: incorrect pinTextOffset {} - should be {}".format(
            dcmName, self.getDEF_textOffset(), _work_libRules.pinTextOffset))
        self._cntErr += 1

  def _chkField_position(self, fId, bFix):
    dcmName = self.getName()
    if ((self.getField_posX(fId) % _work_libRules.grid) != 0):
      logger.error("{} F{:d}: NOT on grid".format(dcmName, fId))
      self._cntErr += 1
    if ((self.getField_posY(fId) % _work_libRules.grid) != 0):
      logger.error("{} F{:d}: NOT on grid".format(dcmName, fId))
      self._cntErr += 1

  def _chkField_textSize(self, fId, bFix):
    dcmName = self.getName()
    ts = self.getField_textSize(fId)
    if (ts != _work_libRules.fieldTextSize):
      if (bFix):
        logger.warning("{} F{:d}: incorrect text size {} - changed to {}".format(
            dcmName, fId, ts, _work_libRules.fieldTextSize))
        self.setField_textSize(fId, _work_libRules.fieldTextSize)
        self._cntFix += 1
      else:
        logger.error("{} F{:d}: incorrect text size {} - should be {}".format(
            dcmName, fId, ts, _work_libRules.fieldTextSize))
        self._cntErr += 1

  def _chkField_textOrientation(self, fId, bFix):
    dcmName = self.getName()
    to = self.getField_textOrientation(fId)
    if (to != "H"):
      if (bFix):
        logger.warning("{} F{:d}: incorrect text orientation {} - changed to H".format(
            dcmName, fId, to))
        self.setField_textOrientationH(fId)
        self._cntFix += 1
      else:
        logger.error("{} F{:d}: incorrect text orientation {} - should be H".format(
            dcmName, fId, to))
        self._cntErr += 1

  def _chkField_visible(self, fId, bFix):
    dcmName = self.getName()
    if (not self.isField_visible(fId)):
      if (bFix):
        logger.warning("{} F{:d}: invisible - changed".format(dcmName, fId))
        self.setField_visible(fId)
        self._cntFix += 1
      else:
        logger.error("{} F{:d}: invisible".format(dcmName, fId))
        self._cntErr += 1

  def _chkField_invisible(self, fId, bFix):
    dcmName = self.getName()
    if (self.isField_visible(fId)):
      if (bFix):
        logger.warning("{} F{:d}: visible - changed".format(dcmName, fId))
        self.setField_invisible(fId)
        self._cntFix += 1
      else:
        logger.error("{} F{:d}: visible".format(dcmName, fId))
        self._cntErr += 1

  def _chkField_hTextJustify(self, fId, bFix):
    dcmName = self.getName()
    tj = self.getField_hTextJustify(fId)
    if (tj != "C"):
      if (bFix):
        logger.warning("{} F{:d}: not centered horiz. - changed".format(dcmName, fId))
        self.setField_hTextJustifyC(fId)
        self._cntFix += 1
      else:
        logger.error("{} F{:d}: not centered horiz.".format(dcmName, fId))
        self._cntErr += 1

  def _chkField_vTextJustify(self, fId, bFix):
    dcmName = self.getName()
    tj = self.getField_vTextJustify(fId)
    if (tj != "CNN"):
      if (bFix):
        logger.warning("{} F{:d}: not centered vertic. - changed".format(dcmName, fId))
        self.setField_vTextJustifyC(fId)
        self._cntFix += 1
      else:
        logger.error("{} F{:d}: not centered vertic.".format(dcmName, fId))
        self._cntErr += 1

  def _chkFieldRef(self, bFix=True):
    dcmName = self.getName()
    fld = self.libFields[0].split()
    #logger.info(fld)

    if (fld == []):
      logger.error("{} F0: missing (reference)".format(dcmName))
      self._cntErr += 1
      return
    if (fld[0] != "F0"):
      logger.error("{}: NOT a reference field: {}".format(dcmName, " ".join(fld)))
      self._cntErr += 1
      return

    self._chkField_position(0, bFix)
    self._chkField_textSize(0, bFix)
    self._chkField_textOrientation(0, bFix)
    if (self.hasPowerFlag()):
      self._chkField_invisible(0, bFix)
    else:
      self._chkField_visible(0, bFix)
    self._chkField_hTextJustify(0, bFix)
    self._chkField_vTextJustify(0, bFix)

  def _chkFieldVal(self, bFix=True):
    dcmName = self.getName()
    fld = self.libFields[1].split()
    #logger.info(fld)

    if (fld == []):
      logger.error("{} F1: missing (value)".format(dcmName))
      self._cntErr += 1
      return
    if (fld[0] != "F1"):
      logger.error("{}: NOT a value field: {}".format(dcmName, " ".join(fld)))
      self._cntErr += 1
      return

    self._chkField_position(1, bFix)
    self._chkField_textSize(1, bFix)
    self._chkField_textOrientation(1, bFix)
    self._chkField_visible(1, bFix)
    self._chkField_hTextJustify(1, bFix)
    self._chkField_vTextJustify(1, bFix)

  def _chkFieldFootprint(self, bFix=True):
    dcmName = self.getName()
    if (len(self.libFields) < 3):
      if (bFix):
        logger.warning("{} F2: missing (footprint) - added".format(dcmName))
        self.libFields.append("F2 \"~\" 0 0 {:d} H I C CNN".format(
            _work_libRules.fieldTextSize))
        self._cntFix += 1
        return
      else:
        logger.error("{} F2: missing (footprint)".format(dcmName))
        self._cntErr += 1
        return

    fld = self.libFields[2].split()
    #logger.info(fld)

    if (fld[0] != "F2"):
      logger.error("{}: NOT a value field: {}".format(dcmName, " ".join(fld)))
      self._cntErr += 1
      return

    self._chkField_position(2, bFix)
    self._chkField_textSize(2, bFix)
    self._chkField_textOrientation(2, bFix)
    self._chkField_invisible(2, bFix)
    self._chkField_hTextJustify(2, bFix)
    self._chkField_vTextJustify(2, bFix)

    if (fld[1] != "\"~\""):
      #if(fix):
      #  logger.warning("{}: incorrect footprint - removed".format(" ".join(fld[0:2])))
      #  fld[1] = "\"~\""
      #  res += 1
      #else:
      logger.error("{}: incorrect footprint name".format(dcmName))
      self._cntErr += 1

  def _chkFieldDatasheet(self, bFix=True):
    dcmName = self.getName()
    if (len(self.libFields) < 4):
      if (bFix):
        logger.warning("{} F3: missing (datasheet) - added".format(dcmName))
        self.libFields.append("F3 \"~\" 0 0 {:d} H I C CNN".format(
            _work_libRules.fieldTextSize))
        self._cntFix += 1
        return
      else:
        logger.error("{} F3: missing (datasheet)".format(dcmName))
        self._cntErr += 1
        return

    fld = self.libFields[3].split()
    #logger.info(fld)

    if (fld[0] != "F3"):
      logger.error("{}: NOT a value field: {}".format(dcmName, " ".join(fld)))
      self._cntErr += 1
      return

    self._chkField_position(3, bFix)
    self._chkField_textSize(3, bFix)
    self._chkField_textOrientation(3, bFix)
    self._chkField_invisible(3, bFix)
    self._chkField_hTextJustify(3, bFix)
    self._chkField_vTextJustify(3, bFix)

  def _chkFieldBOM(self, bFix=True):
    dcmName = self.getName()
    if (len(self.libFields) < 5):
      if (bFix):
        logger.warning("{} F4: missing (BOM) - added".format(dcmName))
        self.libFields.append("F4 \"~\" 0 0 {:d} H I C CNN \"BOM\"".format(
            _work_libRules.fieldTextSize))
        self._cntFix += 1
        return
      else:
        logger.error("{} F4: missing (BOM)".format(dcmName))
        self._cntErr += 1
        return

    fld = self.libFields[4].split()
    #logger.info(fld)

    if (fld[0] != "F4"):
      logger.error("{}: NOT a value field: {}".format(dcmName, " ".join(fld)))
      self._cntErr += 1
      return

    self._chkField_position(4, bFix)
    self._chkField_textSize(4, bFix)
    self._chkField_textOrientation(4, bFix)
    self._chkField_visible(4, bFix)
    self._chkField_hTextJustify(4, bFix)
    self._chkField_vTextJustify(4, bFix)

  def _chkFieldNone(self, fId, bFix=True):
    dcmName = self.getName()
    try:
      fld = self.libFields[fId].split()
      #logger.info(fld)
    except IndexError:
      return

    if (bFix):
      logger.warning("{} {}: extra field deleted".format(dcmName, fld[0]))
      del self.libFields[fId]
      self._cntFix += 1
    else:
      logger.error("{} {}: extra field present".format(dcmName, fld[0]))
      self._cntErr += 1

  def _chkFPList(self, bFix=True):
    dcmName = self.getName()
    if (self.libFPList != []):
      if (bFix):
        logger.warning("{}: footprint list deleted".format(dcmName))
        self.libFPList = []
        self._cntFix += 1
      else:
        logger.error("{}: footprint list present".format(dcmName))
        self._cntErr += 1

  def _chkAlias(self, bFix=True):
    dcmName = self.getName()
    if (self.libAlias != []):
      if (bFix):
        logger.error("{}: alias(es) deleted".format(dcmName))
        self.libAlias = []
        self._cntFix += 1
      else:
        logger.error("{}: alias(es) present".format(dcmName))
        self._cntErr += 1

  def _chkDraw_Pin(self, dId, bFix=True):
    dcmName = self.getName()
    fld = self.libDraw[dId].split()
    #logger.info(fld)

    if (fld[2] == "~"):
      logger.error("{}: no pin name".format(" ".join(fld[0:3])))
      self._cntErr += 1

    if (int(fld[5]) not in _work_libRules.pinLength):
      #if(fix):
      #  logger.warning("{}: incorrect pin length {} - changed to {}".format(" ".join(fld[0:3]), fld[5],
      #                                                                      cmpRules.pinLength[0]))
      #  fld[5] = str(cmpRules.pinLength[0])
      #else:
      logger.error("{} {}: incorrect pin length {} - should be {}".format(
          dcmName, " ".join(fld[0:3]), fld[5], _work_libRules.pinLength))
      self._cntErr += 1

    if (int(fld[7]) != _work_libRules.pinNameTextSize):
      if (bFix):
        logger.warning("{} {}: incorrect pin name text size {} - changed to {}".format(
            dcmName, " ".join(fld[0:3]), fld[7], _work_libRules.pinNameTextSize))
        fld[7] = str(_work_libRules.pinNameTextSize)
        self._cntFix += 1
      else:
        logger.error("{} {}: incorrect pin name text size {} - should be {}".format(
            dcmName, " ".join(fld[0:3]), fld[7], _work_libRules.pinNameTextSize))
        self._cntErr += 1

    if (int(fld[8]) != _work_libRules.pinNrTextSize):
      if (bFix):
        logger.warning("{} {}: incorrect pin nr text size {} - changed to {}".format(
            dcmName, " ".join(fld[0:3]), fld[8], _work_libRules.pinNrTextSize))
        fld[8] = str(_work_libRules.pinNrTextSize)
        self._cntFix += 1
      else:
        logger.error("{} {}: incorrect pin nr text size {} - should be {}".format(
            dcmName, " ".join(fld[0:3]), fld[8], _work_libRules.pinNrTextSize))
        self._cntErr += 1

    if (fld[9] == "0"):
      if (bFix):
        logger.warning("{} {}: pin on all package parts - cleared".format(
            dcmName, " ".join(fld[0:3])))
        fld[9] = "1"
        self._cntFix += 1
      else:
        logger.error("{} {}: pin on all package parts".format(dcmName, " ".join(fld[0:3])))
        self._cntErr += 1

    if (fld[10] == "0"):
      if (bFix):
        logger.warning("{} {}: pin on all body styles - cleared".format(
            dcmName, " ".join(fld[0:3])))
        fld[10] = "1"
        self._cntFix += 1
      else:
        logger.error("{} {}: pin on all body styles".format(dcmName, " ".join(fld[0:3])))
        self._cntErr += 1

    if ((len(fld) == 13) and (fld[12] == "N")):
      logger.warning("{} {}: invisible pin".format(dcmName, " ".join(fld[0:3])))

    if (bFix):
      self.libDraw[dId] = " ".join(fld)

  def _chkDraw_Circle(self, dId, bFix=True):
    dcmName = self.getName()
    fld = self.libDraw[dId].split()
    #logger.info(fld)

    if (int(fld[6]) not in _work_libRules.lineThickness):
      if (bFix):
        logger.warning("{} {}: incorrect line width {} - changed to {}".format(
            dcmName, " ".join(fld[0:3]), fld[6], _work_libRules.lineThickness[0]))
        fld[6] = str(_work_libRules.lineThickness[0])
        self._cntFix += 1
      else:
        logger.error("{} {}: incorrect line width {} - should be {}".format(
            dcmName, " ".join(fld[0:3]), fld[6], _work_libRules.lineThickness[0]))
        self._cntErr += 1

    if (bFix):
      self.libDraw[dId] = " ".join(fld)

  def _chkDraw_Rectangle(self, dId, bFix=True):
    dcmName = self.getName()
    fld = self.libDraw[dId].split()
    #logger.info(fld)

    if (int(fld[7]) not in _work_libRules.lineThickness):
      if (bFix):
        logger.warning("{} {}: incorrect line width {} - changed to {}".format(
            dcmName, " ".join(fld[0:3]), fld[7], _work_libRules.lineThickness[0]))
        fld[7] = str(_work_libRules.lineThickness[0])
        self._cntFix += 1
      else:
        logger.error("{} {}: incorrect line width {} - should be {}".format(
            dcmName, " ".join(fld[0:3]), fld[7], _work_libRules.lineThickness[0]))
        self._cntErr += 1

    if (bFix):
      self.libDraw[dId] = " ".join(fld)

  def _chkDraw_Polyline(self, dId, bFix=True):
    dcmName = self.getName()
    fld = self.libDraw[dId].split()
    #logger.info(fld)

    if (int(fld[4]) not in _work_libRules.lineThickness):
      if (bFix):
        logger.warning("{} {}: incorrect line width {} - changed to {}".format(
            dcmName, " ".join(fld[0:3]), fld[4], _work_libRules.lineThickness[0]))
        fld[4] = str(_work_libRules.lineThickness[0])
        self._cntFix += 1
      else:
        logger.error("{} {}: incorrect line width {} - should be {}".format(
            dcmName, " ".join(fld[0:3]), fld[4], _work_libRules.lineThickness[0]))
        self._cntErr += 1

    if (bFix):
      self.libDraw[dId] = " ".join(fld)

  def _chkDraw_Text(self, dId, bFix=True):
    dcmName = self.getName()
    fld = self.libDraw[dId].split()
    #logger.info(fld)

    if (int(fld[4]) != _work_libRules.textSize):
      if (bFix):
        logger.warning("{} {}: incorrect text size {} - changed to {}".format(
            dcmName, " ".join(fld[0:2]), fld[4], _work_libRules.textSize))
        fld[4] = str(_work_libRules.textSize)
        self._cntFix += 1
      else:
        logger.error("{} {}: incorrect text size {} - should be {}".format(
            dcmName, " ".join(fld[0:2]), fld[4], _work_libRules.textSize))
        self._cntErr += 1

    if (bFix):
      self.libDraw[dId] = " ".join(fld)

  def _chkDraw(self, bFix=True):
    for i in range(0, len(self.libDraw)):
      if (self.libDraw[i].startswith("X")):
        self._chkDraw_Pin(i, bFix)
      elif (self.libDraw[i].startswith("C")):
        self._chkDraw_Circle(i, bFix)
      elif (self.libDraw[i].startswith("S")):
        self._chkDraw_Rectangle(i, bFix)
      elif (self.libDraw[i].startswith("P")):
        self._chkDraw_Polyline(i, bFix)
      elif (self.libDraw[i].startswith("T")):
        self._chkDraw_Text(i, bFix)

  def check(self, bFix=False, bF3=False, bF4=False, bF5=False):
    """
    :param bFix: try to fix the components
    :param bF3: check field F3
    :param bF4: check field F4
    :param bF5: check field F5
    """
    logger.info("checking component {}".format(self.getName()))
    #for ln in self.getLibText():
    #  print(ln)
    self._chkDEF(bFix)

    self._chkFieldRef(bFix)
    self._chkFieldVal(bFix)
    if (bF3):
      self._chkFieldFootprint(bFix)
    if (bF4):
      self._chkFieldDatasheet(bFix)
    if (bF5):
      self._chkFieldBOM(bFix)
    self._chkFieldNone(6, bFix)
    self._chkFieldNone(7, bFix)
    self._chkFieldNone(8, bFix)

    self._chkFPList(bFix)

    self._chkAlias(bFix)

    self._chkDraw(bFix)

    #logger.info("check stats: {}".format(self.getCheckStats()))
    #for ln in self.getLibText():
    #  print(ln)

    if ((self._cntFix == 0) and (self._cntErr == 0)):
      logger.info("component OK")
      #return [True] + self.getCheckStats()
    elif ((self._cntFix > 0) and (self._cntErr == 0)):
      logger.info("component fixed")
      #return [False] + self.getCheckStats()
    else:
      logger.info("component has errors")
      #return [False] + self.getCheckStats()

class modFootprint_Pad(object):

  def __init__(self):
    self.libFields = []

  def getName(self):
    if (self.libFields[0].startswith("Sh ")):
      return self.libFields[0].split()[1].replace("\"", "")
    else:
      logger.error("incorrect pad line: {}".format(self.libFields[0]))

class modFootprint(object):

  def __init__(self):
    self.libFields = []
    self.libText = []
    self.libD = []
    self.libShape3D = []
    self.libPads = {}
    self.doc = []

  def getName(self):
    if (self.libFields[1].startswith("Li ")):
      return self.libFields[1].split()[1]
    else:
      logger.error("incorrect footprint line: {}".format(self.libFields[0]))

  def getLibText(self):
    name = self.getName()

    res = ["$MODULE {}".format(name)]
    res += self.libFields
    res += self.libText
    res += self.libD
    if (len(self.libPads) > 0):
      for k in sorted(self.libPads.keys()):
        res += ["$PAD"]
        res += self.libPads[k].libFields
        res += ["$EndPAD"]
    if (len(self.libShape3D) > 0):
      res += ["$SHAPE3D"]
      res += self.libShape3D
      res += ["$EndSHAPE3D"]
    res += ["$EndMODULE  {}".format(name)]
    return res

  def getDocText(self):
    name = self.getName()
    res = []

    if (len(self.doc) > 0):
      res = ["$MODULE {}".format(name)]
      res += self.doc
      res += ["$EndMODULE"]
    return res

  def setName(self, strName):
    if (self.libFields[1].startswith("Li ")):
      tmp = self.libFields[1].split()
      tmp[1] = strName
      self.libFields[1] = " ".join(tmp)
    else:
      logger.error("incorrect footprint line: {}".format(self.libFields[0]))
      return

    if (self.libText[0].startswith("T0 ")):
      tmp = self.libText[0].split()
      tmp[-1] = "N\"{}\"".format(strName)
      self.libText[0] = " ".join(tmp)
    else:
      logger.error("incorrect footprint line: {}".format(self.libFields[0]))
      return

    if (len(self.doc) > 0):
      if (self.doc[0].startswith("Li ")):
        tmp = self.doc[0].split()
        tmp[1] = strName
        self.doc[0] = " ".join(tmp)
      else:
        logger.error("incorrect footprint line: {}".format(self.libFields[0]))
        return

  def chkText(self, fix=True):
    res = 0

    for i in range(0, len(self.libText)):
      if (self.libText[i].startswith("T0")):
        fld = self.libText[i].split()
        #logger.info(" ".join(fld))
        if (int(fld[3]) != (10 * _work_modRules.textSize)):
          if (fix):
            logger.warning("{}: incorrect text size {} - changed to {}".format(
                fld[0], fld[3], 10 * _work_modRules.textSize))
            fld[3] = str(10 * _work_modRules.textSize)
            res += 1
          else:
            logger.error("{}: incorrect text size {} - should be {}".format(
                fld[0], fld[3], 10 * _work_modRules.textSize))
            res = -1
        if (int(fld[4]) != (10 * _work_modRules.textSize)):
          if (fix):
            logger.warning("{}: incorrect text size {} - changed to {}".format(
                fld[0], fld[4], 10 * _work_modRules.textSize))
            fld[4] = str(10 * _work_modRules.textSize)
            res += 1
          else:
            logger.error("{}: incorrect text size {} - should be {}".format(
                fld[0], fld[4], 10 * _work_modRules.textSize))
            res = -1
        if (int(fld[6]) != (10 * _work_modRules.textWidth)):
          if (fix):
            logger.warning("{}: incorrect text size {} - changed to {}".format(
                fld[0], fld[6], 10 * _work_modRules.textWidth))
            fld[6] = str(10 * _work_modRules.textWidth)
            res += 1
          else:
            logger.error("{}: incorrect text size {} - should be {}".format(
                fld[0], fld[6], 10 * _work_modRules.textWidth))
            res = -1
      if (self.libText[i].startswith("T1")):
        fld = self.libText[i].split()
        #logger.info(" ".join(fld))
        if (int(fld[1]) != 0):
          if (fix):
            logger.warning("{}: incorrect offset {} - changed to {}".format(
                fld[0], fld[1], 0))
            fld[1] = "0"
            res += 1
          else:
            logger.error("{}: incorrect offset {} - should be {}".format(fld[0], fld[1], 0))
            res = -1
        if (int(fld[2]) != 0):
          if (fix):
            logger.warning("{}: incorrect offset {} - changed to {}".format(
                fld[0], fld[2], 0))
            fld[2] = "0"
            res += 1
          else:
            logger.error("{}: incorrect offset {} - should be {}".format(fld[0], fld[2], 0))
            res = -1
        if (int(fld[3]) != (10 * _work_modRules.textSize)):
          if (fix):
            logger.warning("{}: incorrect text size {} - changed to {}".format(
                fld[0], fld[3], 10 * _work_modRules.textSize))
            fld[3] = str(10 * _work_modRules.textSize)
            res += 1
          else:
            logger.error("{}: incorrect text size {} - should be {}".format(
                fld[0], fld[3], 10 * _work_modRules.textSize))
            res = -1
        if (int(fld[4]) != (10 * _work_modRules.textSize)):
          if (fix):
            logger.warning("{}: incorrect text size {} - changed to {}".format(
                fld[0], fld[4], 10 * _work_modRules.textSize))
            fld[4] = str(10 * _work_modRules.textSize)
            res += 1
          else:
            logger.error("{}: incorrect text size {} - should be {}".format(
                fld[0], fld[4], 10 * _work_modRules.textSize))
            res = -1
        if (int(fld[6]) != (10 * _work_modRules.textWidth)):
          if (fix):
            logger.warning("{}: incorrect text size {} - changed to {}".format(
                fld[0], fld[6], 10 * _work_modRules.textWidth))
            fld[6] = str(10 * _work_modRules.textWidth)
            res += 1
          else:
            logger.error("{}: incorrect text size {} - should be {}".format(
                fld[0], fld[6], 10 * _work_modRules.textWidth))
            res = -1
      #if(fix):
      #  self.libText[i] = " ".join(fld)

    return res

  def check(self, fix=False):
    logger.info("checking component {}".format(self.getName()))

    nFix = 0
    nErr = 0

    tmp = self.chkText(fix)
    if (tmp < 0):
      nErr += tmp
    else:
      nFix += tmp

    if ((nFix == 0) and (nErr == 0)):
      logger.info("component OK")
      return [True, nFix, nErr]
    elif ((nFix > 0) and (nErr == 0)):
      logger.info("component fixed")
      return [False, nFix, nErr]
    else:
      logger.info("component has errors")
      return [False, nFix, nErr]

def readSymbols_libFile(fPath):
  """
  :param fPath:
  :return: dictionary of {name: dcmSymbol}
  """
  if (not fPath.endswith(".lib")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  libPath = fPath
  docPath = "{}dcm".format(fPath[:-3])
  pyauto_base.fs.chkPath_File(libPath)

  logger.info("-- reading KiCAD symbol library")
  dictSym = {}

  logger.info("-- reading file: {}".format(libPath))
  with open(libPath, "r") as fIn:
    lnCnt = 0
    processComponent = False
    inFPLIST = False
    inDRAW = False
    for ln in [t.strip("\n\r") for t in fIn.readlines()]:
      lnCnt += 1
      #logger.info("[{:>5d}] {}".format(lnCnt, ln))
      if (lnCnt == 1):
        if (not ln.startswith("EESchema-LIBRARY")):
          msg = "INCORRECT file format"
          logger.error(msg)
          raise RuntimeError(msg)
        logger.info(ln)
        libVer = ln.split()[2]
        if (libVer != componentLibVer[0]):
          logger.warning("library version change: got {} - expected {}".format(
              libVer, componentLibVer[0]))
        continue
      if (ln.startswith("#")):
        continue
      elif (ln.startswith("DEF ")):
        sym = libSymbol()
        sym.libDef = ln
        processComponent = True
        continue
      elif (ln == "ENDDEF"):
        processComponent = False
        dictSym[sym.getName()] = sym
        continue

      if (processComponent):
        if (re.match(r"^F[0-9]+ .*$", ln)):
          sym.libFields.append(ln)
        elif (ln == "$FPLIST"):
          inFPLIST = True
        elif (ln == "$ENDFPLIST"):
          inFPLIST = False
        elif (ln == "DRAW"):
          inDRAW = True
        elif (ln == "ENDDRAW"):
          inDRAW = False
        else:
          if (inFPLIST and not inDRAW):
            sym.libFPList.append(ln)
          elif (inDRAW and not inFPLIST):
            sym.libDraw.append(ln)
          else:
            logger.warning("unprocessed line: {:d} - {}".format(lnCnt, ln))
      else:
        logger.warning("unprocessed line: {:d} - {}".format(lnCnt, ln))
  logger.info("read {:d} line(s); {:d} component(s)".format(lnCnt, len(dictSym)))
  #logger.info(sorted(dictSym.keys()))
  logger.info("-- done reading file")

  if (pyauto_base.fs.chkPath_File(docPath, bDie=False)):
    logger.info("-- reading file: {}".format(docPath))
    with open(docPath, "r") as fIn:
      lnCnt = 0
      lstLn = []
      cpName = None
      for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
        lnCnt += 1
        #logger.info("[{:>5d}] {}".format(lnCnt, ln))
        if (lnCnt == 1):
          if (not ln.startswith("EESchema-DOCLIB")):
            msg = "INCORRECT file format"
            logger.error(msg)
            raise RuntimeError(msg)
          logger.info(ln)
          libVer = ln.split()[2]
          if (libVer != componentLibVer[1]):
            logger.warning("library version change: got {} - expected {}".format(
                libVer, componentLibVer[1]))
          continue
        if (ln.startswith("#")):
          continue
        elif (ln.startswith("$CMP")):
          cpName = ln.split()[1]
        elif (ln == "$ENDCMP"):
          lstLn.append(ln)
          dictSym[cpName].doc = lstLn
          cpName = None
          lstLn = []
        if (cpName is not None):
          lstLn.append(ln)
  logger.info("read {:d} line(s)".format(lnCnt))
  logger.info("-- done reading file")

  logger.info("-- done reading library")
  return dictSym

def writeSymbols_libFile(fPath, dictSym):
  if ((fPath is None) or (len(fPath) == 0)):
    msg = "CANNOT write file with no path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not fPath.endswith(".lib")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  libPath = fPath
  docPath = "{}dcm".format(fPath[:-3])

  logger.info("-- writing KiCAD symbol library")

  with open(libPath, "w") as fOut:
    fOut.write("EESchema-LIBRARY Version {}  Date: {}\n".format(
        componentLibVer[0],
        datetime.datetime.now().strftime("%a %d %b %Y %H:%M:%S %p")))
    fOut.write("#encoding utf-8\n")
    for k in sorted(dictSym.keys()):
      fOut.write("#\n# {}\n#\n".format(dictSym[k].getName()))
      for ln in dictSym[k].getLibText():
        fOut.write(ln + "\n")
    fOut.write("#\n")
    fOut.write("#End Library\n")
  logger.info("{} written".format(libPath))

  with open(docPath, "w") as fOut:
    fOut.write("EESchema-DOCLIB  Version {}  Date: {}\n".format(
        componentLibVer[1],
        datetime.datetime.now().strftime("%a %d %b %Y %H:%M:%S %p")))
    for k in sorted(dictSym.keys()):
      if (dictSym[k].getDocText() != []):
        fOut.write("#\n")
        for ln in dictSym[k].getDocText():
          fOut.write(ln + "\n")
    fOut.write("#\n")
    fOut.write("#End Doc Library\n")
  logger.info("{} written".format(docPath))

  logger.info("-- done writing library")

def readFootprints_modFile(fPath):
  """
  :param fPath:
  :return: dictionary of {name: modFootprint}
  """
  libPath = fPath
  docPath = "{}mdc".format(fPath[:-3])
  if (not fPath.endswith(".mod")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  pyauto_base.fs.chkPath_File(libPath)

  logger.info("-- reading KiCAD footprint library")
  dictFpt = {}

  logger.info("reading file: {}".format(libPath))
  with open(libPath, "r") as fIn:
    lnCnt = 0
    processIndex = False
    processFootprint = False
    inPAD = False
    inSHAPE3D = False
    for ln in [t.strip("\n\r") for t in fIn.readlines()]:
      lnCnt += 1
      #logger.info("[{:>5d}] {}".format(lnCnt, ln))
      if (lnCnt == 1):
        if (not ln.startswith("PCBNEW-LibModule-V")):
          msg = "INCORRECT file format"
          logger.error(msg)
          raise RuntimeError(msg)
        logger.info(ln)
        libVer = ln.split()[0][18:]
        if (libVer != moduleLibVer[0]):
          logger.warning("library version change: got {} - expected {}".format(
              libVer, moduleLibVer[0]))
        continue
      #if(ln.startswith("#")):
      #  continue
      if (ln == "$INDEX"):
        processIndex = True
      elif (ln == "$EndINDEX"):
        processIndex = False
        continue
      elif (ln.startswith("$MODULE ")):
        fpt = modFootprint()
        processIndex = False
        processFootprint = True
        continue
      elif (ln.startswith("$EndMODULE ")):
        processFootprint = False
        dictFpt[fpt.getName()] = fpt
        continue
      elif (ln == "$EndLIBRARY"):
        continue

      if (processIndex):
        continue

      if (processFootprint):
        if ((not inPAD) and (not inSHAPE3D)
            and re.match(r"^(Po|Li|Cd|Sc|AR|Op|At|Kw).*$", ln)):
          fpt.libFields.append(ln)
        elif ((not inPAD) and (not inSHAPE3D) and re.match(r"^T[0-9].*$", ln)):
          fpt.libText.append(ln)
        elif ((not inPAD) and (not inSHAPE3D) and re.match(r"^D[ACS].*$", ln)):
          fpt.libD.append(ln)
        elif (ln == "$PAD"):
          p = modFootprint_Pad()
          inPAD = True
        elif (ln == "$EndPAD"):
          fpt.libPads[p.getName()] = p
          inPAD = False
        elif (ln == "$SHAPE3D"):
          inSHAPE3D = True
        elif (ln == "$EndSHAPE3D"):
          inSHAPE3D = False
        else:
          if (inPAD):
            p.libFields.append(ln)
          elif (inSHAPE3D):
            fpt.libShape3D.append(ln)
          else:
            logger.warning("unprocessed line: {:d} - {}".format(lnCnt, ln))
      else:
        logger.warning("unprocessed line: {:d} - {}".format(lnCnt, ln))
  logger.info("read {:d} line(s); {:d} footprint(s)".format(lnCnt, len(dictFpt)))
  #logger.info(sorted(dictFpt.keys()))
  logger.info("-- done reading file")

  if (pyauto_base.fs.chkPath_File(docPath, bDie=False)):
    logger.info("-- reading file: {}".format(docPath))
    with open(docPath, "r") as fIn:
      lnCnt = 0
      processDoc = False
      fptName = None

      for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
        lnCnt += 1
        #logger.info("[{:>5d}] {}".format(lnCnt, ln))
        if (lnCnt == 1):
          if (not ln.startswith("PCBNEW-LibDoc----V")):
            msg = "INCORRECT file format"
            logger.error(msg)
            raise RuntimeError(msg)
          logger.info(ln)
          libVer = ln.split()[0][18:]
          if (libVer != moduleLibVer[1]):
            logger.warning("library version change: got {} - expected {}".format(
                libVer, moduleLibVer[1]))
        #if(ln.startswith("#")):
        #  None
        if (ln.startswith("$MODULE")):
          processDoc = True
          fptName = ln.split()[1]
        elif (ln == "$EndMODULE"):
          processDoc = False
          fptName = None
        if (processDoc):
          if (ln.startswith("$MODULE")):
            continue
          dictFpt[fptName].doc.append(ln)
  logger.info("read {:d} line(s)".format(lnCnt))
  logger.info("-- done reading file")

  logger.info("-- done reading library")
  return dictFpt

def writeFootprints_modFile(fPath, dictFpt):
  if ((fPath is None) or (len(fPath) == 0)):
    msg = "CANNOT write file with no path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not fPath.endswith(".mod")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  libPath = fPath
  docPath = "{}mdc".format(fPath[:-3])

  logger.info("-- writing KiCAD footprint library")
  with open(libPath, "w") as fOut:
    fOut.write("PCBNEW-LibModule-V{} {}\n".format(
        moduleLibVer[0],
        datetime.datetime.now().strftime("%a %d %b %Y %H:%M:%S %p")))
    fOut.write("$INDEX\n")
    for k in sorted(dictFpt.keys()):
      fOut.write("{}\n".format(k))
    fOut.write("$EndINDEX\n")
    for k in sorted(dictFpt.keys()):
      for ln in dictFpt[k].getLibText():
        fOut.write(ln + "\n")
    fOut.write("$EndLIBRARY\n")
  logger.info("{} written".format(libPath))

  with open(docPath, "w") as fOut:
    fOut.write("PCBNEW-LibDoc----V{} {}\n".format(
        moduleLibVer[1],
        datetime.datetime.now().strftime("%a %d %b %Y %H:%M:%S %p")))
    for k in sorted(dictFpt.keys()):
      if (dictFpt[k].getDocText() != []):
        fOut.write("#\n")
        for ln in dictFpt[k].getDocText():
          fOut.write(ln + "\n")
    fOut.write("#\n")
    fOut.write("$EndLIBDOC\n")
  logger.info("{} written".format(docPath))

  logger.info("-- done writing library")

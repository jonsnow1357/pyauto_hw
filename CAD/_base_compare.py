#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import re
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.misc

class PinCompare(object):

  def __init__(self):
    self.cp1 = None
    self.cp2 = None
    self.compareName = "full"
    self.bCompareType = True
    self.bCompareBank = True
    self.bCompareSide = True
    self.bCompareOrder = True
    self.lstPinNo_del = []
    self.lstPinNo_add = []
    self.lstPinNo_chg = []

  def clear(self):
    self.lstPinNo_del = []
    self.lstPinNo_add = []
    self.lstPinNo_chg = []

  def compare(self):
    """self.compareName will affect the way the comparison is done.
    * no_underscore - will remove '_' from pin names
    * prefix_only - assumes one of the pin names is the prefix of the other one
    * replace=[12]=S1=S2 - replace string S1 with S2 in the first/second pin name
    * fuzzy=LVL - fuzzy match using difflib.SequenceMatcher
    """
    setPN1 = set(self.cp1.pinNumbers)
    setPN2 = set(self.cp2.pinNumbers)
    if ((len(setPN1) == 0) and (len(setPN2) == 0)):
      return

    self.clear()

    #logger.info(setPN1)
    #logger.info(setPN2)
    #setPN_cmn = sorted(setPN1 & setPN2)
    setPN_diff12 = sorted(setPN1 - setPN2)
    setPN_diff21 = sorted(setPN2 - setPN1)

    if (len(setPN_diff12) > 0):
      #logger.info("deleted pin(s): {}".format(setPN_diff12))
      self.lstPinNo_del = list(setPN_diff12)

    if (len(setPN_diff21) > 0):
      #logger.info("added pin(s): {}".format(setPN_diff21))
      self.lstPinNo_add = list(setPN_diff21)

    setPN_cmn = sorted(setPN1 & setPN2)
    if (len(setPN_cmn) > 0):
      for pn in setPN_cmn:
        pin1 = self.cp1.getPinByNumber(pn)
        pin2 = self.cp2.getPinByNumber(pn)
        # compare type
        if (self.bCompareType and (pin1.type != pin2.type)):
          self.lstPinNo_chg.append(pn)
          continue  # avoid adding the refdes multiple times

        #compare name
        if (self.compareName == "no_underscore"):
          if (pin1.name.replace("_", "") != pin2.name.replace("_", "")):
            self.lstPinNo_chg.append(pn)
        elif (self.compareName == "prefix_only"):
          if (pin1.name == pin2.name):
            pass
          elif ((len(pin1.name) < 4) or (len(pin2.name) < 4)):
            if (pin1.name != pin2.name):
              self.lstPinNo_chg.append(pn)
          else:
            len_prefix = 0
            for i in range(0, (min([len(pin1.name), len(pin2.name)]))):
              if (pin1.name[i] == pin2.name[i]):
                len_prefix = (i + 1)
              else:
                break
            len_suffix1 = len(pin1.name) - len_prefix
            len_suffix2 = len(pin2.name) - len_prefix
            if ((len_prefix < 4) or (abs(len_suffix1 - len_suffix2) > 2)):
              self.lstPinNo_chg.append(pn)
        elif (self.compareName.startswith("replace")):
          tmp = self.compareName.split("=")
          if (len(tmp) != 4):
            raise RuntimeError
          if (tmp[1] == "1"):
            if (pin1.name.replace(tmp[2], tmp[3]) != pin2.name):
              self.lstPinNo_chg.append(pn)
          elif (tmp[1] == "2"):
            if (pin1.name != pin2.name.replace(tmp[2], tmp[3])):
              self.lstPinNo_chg.append(pn)
          else:
            raise RuntimeError
        elif (self.compareName.startswith("fuzzy")):
          th = float(self.compareName.split("=")[1])
          if (pyauto_base.misc.compareStrings(pin1.name, pin2.name, bCase=True,
                                              fuzzyTh=th) == "no"):
            self.lstPinNo_chg.append(pn)
        else:  # default is "full" even if mis-typed
          if ((pin1.name != pin2.name) and (pin2.name != (pin1.name + "_" + pin1.number))):
            self.lstPinNo_chg.append(pn)

        #compare bank
        if ((self.bCompareBank) and (pin1.bank != "") and (pin2.bank != "")):
          if (pin1.bank != pin2.bank):
            self.lstPinNo_chg.append(pn)

        #compare side
        if ((self.bCompareSide) and (pin1.side != "") and (pin2.side != "")):
          if (pin1.side != pin2.side):
            self.lstPinNo_chg.append(pn)

  def showInfo(self):
    logger.info("== Pin compare info:")
    logger.info("pins: {} COMP1, {} COMP2".format(self.cp1.nPins, self.cp2.nPins))
    logger.info("pins: {} deleted / {} added / {} changed".format(
        len(self.lstPinNo_del), len(self.lstPinNo_add), len(self.lstPinNo_chg)))

  def showReport(self):
    if ((len(self.lstPinNo_del) == 0) and (len(self.lstPinNo_add) == 0)
        and (len(self.lstPinNo_chg) == 0)):
      return

    logger.info("== Pin compare:")
    for pn in self.lstPinNo_del:
      pin1 = self.cp1.getPinByNumber(pn)
      logger.info("  {: <16} deleted ({})".format(pn, pin1.name))
    for pn in self.lstPinNo_add:
      pin2 = self.cp2.getPinByNumber(pn)
      logger.info("  {: <16} added ({})".format(pn, pin2.name))
    for pn in self.lstPinNo_chg:
      pin1 = self.cp1.getPinByNumber(pn)
      pin2 = self.cp2.getPinByNumber(pn)
      info = []
      if (self.bCompareType and (pin1.type != pin2.type)):
        info.append("type: {} -> {}".format(pin1.type, pin2.type))
      if (pin1.name != pin2.name):
        info.append("name: {} -> {}".format(pin1.name, pin2.name))
      if ((self.bCompareBank) and (pin1.bank != "") and (pin2.bank != "")
          and (pin1.bank != pin2.bank)):
        info.append("bank: {} -> {}".format(pin1.bank, pin2.bank))
      if ((self.bCompareSide) and (pin1.side != "") and (pin2.side != "")
          and (pin1.side != pin2.side)):
        info.append("side: {} -> {}".format(pin1.side, pin2.side))
      info = ", ".join(info)
      logger.info("  {: <16} ({})".format(pn, info))

class NetCompare(object):

  def __init__(self):
    self.cp1 = None
    self.cp2 = None
    self.lstPinNo_del = []
    self.lstPinNo_add = []
    self.lstPinNo_otherNet = []
    self.lstPinNo_fuzzyNet = []
    self.lstPinNo_sameNet = []

  def clear(self):
    self.lstPinNo_del = []
    self.lstPinNo_add = []
    self.lstPinNo_otherNet = []
    self.lstPinNo_fuzzyNet = []
    self.lstPinNo_sameNet = []

  def compare(self):
    setPN1 = set(self.cp1.pinNumbers)
    setPN2 = set(self.cp2.pinNumbers)
    if ((len(setPN1) == 0) and (len(setPN2) == 0)):
      return

    self.clear()

    #dictPins1 = self.cp1.getAllPinsByNumber()
    #dictPins2 = self.cp2.getAllPinsByNumber()

    #logger.info(setPN1)
    #logger.info(setPN2)
    setPN_cmn = sorted(setPN1 & setPN2)
    setPN_diff12 = (setPN1 - setPN2)
    setPN_diff21 = (setPN2 - setPN1)
    if (len(setPN_diff12) > 0):
      self.lstPinNo_del = list(setPN_diff12)
    if (len(setPN_diff21) > 0):
      self.lstPinNo_add = list(setPN_diff21)

    for pn in setPN_cmn:
      net1 = self.cp1.getPinByNumber(pn).netName
      net2 = self.cp2.getPinByNumber(pn).netName
      #logger.info("_compareNetNames: '{}' '{}'".format(net1, net2))
      res = pyauto_base.misc.compareStrings(net1, net2)
      if (res == "yes"):
        self.lstPinNo_sameNet.append(pn)
      elif (res == "fuzzy"):
        self.lstPinNo_fuzzyNet.append(pn)
      else:
        self.lstPinNo_otherNet.append(pn)

  def showInfo(self):
    logger.info("== Net compare info:")
    logger.info("pins: {} COMP1, {} COMP2".format(self.cp1.nPins, self.cp2.nPins))
    nOther = len(self.lstPinNo_del) + len(self.lstPinNo_add) + len(self.lstPinNo_otherNet)
    logger.info("pins: {} same / {} fuzzy / {} other".format(len(self.lstPinNo_sameNet),
                                                             len(self.lstPinNo_fuzzyNet),
                                                             nOther))

  def showReport(self):
    logger.info("== Net compare:")
    for pn in self.lstPinNo_fuzzyNet:
      net1 = self.cp1.getPinByNumber(pn).netName
      net2 = self.cp2.getPinByNumber(pn).netName
      logger.info("  {: <6} ({} -> {})".format(pn, net1, net2))
    for pn in self.lstPinNo_otherNet:
      net1 = self.cp1.getPinByNumber(pn).netName
      net2 = self.cp2.getPinByNumber(pn).netName
      logger.info("  {: <6} ({} -> {})".format(pn, net1, net2))
    for pn in self.lstPinNo_del:
      net1 = self.cp1.getPinByNumber(pn).netName
      logger.info("  {: <6} ({} -> x)".format(pn, net1))
    for pn in self.lstPinNo_add:
      net2 = self.cp2.getPinByNumber(pn).netName
      logger.info("  {: <6} (x -> {})".format(pn, net2))

  def writeReport(self, fPath):
    dictReport = {}

    for pn in self.lstPinNo_sameNet:
      net1 = self.cp1.getPinByNumber(pn).netName
      net2 = self.cp2.getPinByNumber(pn).netName
      dictReport[pn] = [net1, pn, net2, "same"]
    for pn in self.lstPinNo_fuzzyNet:
      net1 = self.cp1.getPinByNumber(pn).netName
      net2 = self.cp2.getPinByNumber(pn).netName
      dictReport[pn] = [net1, pn, net2, "fuzzy"]
    for pn in self.lstPinNo_otherNet:
      net1 = self.cp1.getPinByNumber(pn).netName
      net2 = self.cp2.getPinByNumber(pn).netName
      dictReport[pn] = [net1, pn, net2, "other"]
    for pn in self.lstPinNo_del:
      net1 = self.cp1.getPinByNumber(pn).netName
      dictReport[pn] = [net1, pn, "", "other"]
    for pn in self.lstPinNo_add:
      net2 = self.cp2.getPinByNumber(pn).netName
      dictReport[pn] = ["", pn, net2, "other"]

    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      if ((self.cp1.value != "") and (self.cp2.value != "")):
        csvOut.writerow([
            "{} Net Name".format(self.cp1.value), "Pin Number",
            "{} Net Name".format(self.cp2.value), "compare"
        ])
      else:
        csvOut.writerow(["COMP1 Net Name", "Pin Number", "COMP2 Net Name", "compare"])

      for k in sorted(dictReport.keys()):
        csvOut.writerow(dictReport[k])
    logger.info("{} written".format(fPath))

class BOMCompare(object):
  """
  Class for comparing 2 BOMs.

  :var lstPN_del: list of deleted PNs
  :var lstPN_add: list of added PNs
  :var lstRefDes_del: deleted refdes [[refdes, pn], ...]
  :var lstRefDes_add: list of added refdes [[refdes, pn], ...]
  :var lstRefDes_chg: list of deleted refdes [[refdes, pn1, pn2], ...] or [[refdes, dnp1, dnp2], ...]

  All above lists represent changes from BOM1 to BOM2.
  """

  def __init__(self):
    self.bom1 = None
    self.bom2 = None
    self.lstVal_del = []
    self.lstVal_add = []
    self.lstPN_del = []
    self.lstPN_add = []
    self.lstRefDes_del = []
    self.lstRefDes_add = []
    self.lstRefDes_chg = []

  def clear(self):
    self.lstPN_del = []
    self.lstPN_add = []
    self.lstRefDes_del = []
    self.lstRefDes_add = []
    self.lstRefDes_chg = []

  def _compare_values(self):
    setVal1 = set(self.bom1.values)
    setVal2 = set(self.bom2.values)
    if ((len(setVal1) == 0) and (len(setVal2) == 0)):
      return

    #logger.info(setVal1)
    #logger.info(setVal2)
    #setVal_cmn = sorted(setVal1 & setVal2)
    setVal_diff12 = sorted(setVal1 - setVal2)
    setVal_diff21 = sorted(setVal2 - setVal1)

    if (len(setVal_diff12) > 0):
      #logger.info("deleted value(s): {}".format(setVal_diff12))
      self.lstVal_del = list(setVal_diff12)

    if (len(setVal_diff21) > 0):
      #logger.info("added value(s): {}".format(setVal_diff21))
      self.lstVal_add = list(setVal_diff21)

  def _compare_PNs(self):
    setPN1 = set(self.bom1.PNs)
    setPN2 = set(self.bom2.PNs)
    if ((len(setPN1) == 0) and (len(setPN2) == 0)):
      return

    #logger.info(setPN1)
    #logger.info(setPN2)
    #setPN_cmn = sorted(setPN1 & setPN2)
    setPN_diff12 = sorted(setPN1 - setPN2)
    setPN_diff21 = sorted(setPN2 - setPN1)

    if (len(setPN_diff12) > 0):
      #logger.info("deleted PN(s): {}".format(setPN_diff12))
      self.lstPN_del = list(setPN_diff12)

    if (len(setPN_diff21) > 0):
      #logger.info("added PN(s): {}".format(setPN_diff21))
      self.lstPN_add = list(setPN_diff21)

  def _compare_refdes(self):
    setRefDes1 = set(self.bom1.refdes)
    setRefDes2 = set(self.bom2.refdes)
    if ((len(setRefDes1) == 0) and (len(setRefDes2) == 0)):
      return

    #logger.info(setRefDes1)
    #logger.info(setRefDes2)
    setRefDes_cmn = sorted(setRefDes1 & setRefDes2)
    setRefDes_diff12 = sorted(setRefDes1 - setRefDes2)
    setRefDes_diff21 = sorted(setRefDes2 - setRefDes1)

    if (len(setRefDes_diff12) > 0):
      #logger.info("deleted refdes: {}".format(setRefDes_diff12))
      self.lstRefDes_del = list(setRefDes_diff12)

    if (len(setRefDes_diff21) > 0):
      #logger.info("added refdes: {}".format(setRefDes_diff21))
      self.lstRefDes_add = list(setRefDes_diff21)

    if (len(setRefDes_cmn) > 0):
      for refdes in setRefDes_cmn:
        part1 = self.bom1.getPartByRefdes(refdes)
        part2 = self.bom2.getPartByRefdes(refdes)
        if (part1.dnp != part2.dnp):
          self.lstRefDes_chg.append(refdes)
          continue  # avoid adding the refdes multiple times
        if (part1.value != part2.value):
          self.lstRefDes_chg.append(refdes)
          continue  # avoid adding the refdes multiple times
        if (part1.pn != part2.pn):
          self.lstRefDes_chg.append(refdes)

  def compare(self):
    self.clear()

    self._compare_values()
    self._compare_PNs()
    self._compare_refdes()

  def showInfo(self):
    logger.info("== BOM compare info:")
    logger.info("values: {} BOM1, {} BOM2".format(self.bom1.nValues, self.bom2.nValues))
    logger.info("values: {} deleted / {} added".format(len(self.lstVal_del),
                                                       len(self.lstVal_add)))

    logger.info("PNs   : {} BOM1, {} BOM2".format(self.bom1.nPN, self.bom2.nPN))
    logger.info("PNs   : {} deleted / {} added".format(len(self.lstPN_del),
                                                       len(self.lstPN_add)))

    logger.info("refdes: {} BOM1, {} BOM2".format(self.bom1.nRefdes, self.bom2.nRefdes))
    logger.info("refdes: {} deleted / {} added / {} changed".format(
        len(self.lstRefDes_del), len(self.lstRefDes_add), len(self.lstRefDes_chg)))

  def _showReport_values(self):
    if ((len(self.lstVal_del) == 0) and (len(self.lstVal_add) == 0)):
      return

    logger.info("== BOM compare values:")
    for val in self.lstVal_del:
      logger.info("  {: <24} deleted".format(val))
    for val in self.lstVal_add:
      logger.info("  {: <24} added".format(val))

  def _showReport_PNs(self):
    if ((len(self.lstPN_del) == 0) and (len(self.lstPN_add) == 0)):
      return

    logger.info("== BOM compare PNs:")
    for pn in self.lstPN_del:
      logger.info("  {: <24} deleted".format(pn))
    for pn in self.lstPN_add:
      logger.info("  {: <24} added".format(pn))

  def _showReport_refdes(self):
    if ((len(self.lstRefDes_del) == 0) and (len(self.lstRefDes_add) == 0)
        and (len(self.lstRefDes_chg) == 0)):
      return

    logger.info("== BOM compare refdes:")
    for refdes in self.lstRefDes_del:
      part1 = self.bom1.getPartByRefdes(refdes)
      info1 = ", ".join([(part1.value if
                          (part1.value == "") else "val: {}".format(part1.value)),
                         (part1.pn if (part1.pn == "") else "PN: {}".format(part1.pn))])
      logger.info("  {: <6} deleted ({})".format(refdes, info1))
    for refdes in self.lstRefDes_add:
      part2 = self.bom2.getPartByRefdes(refdes)
      info2 = ", ".join([(part2.value if
                          (part2.value == "") else "val: {}".format(part2.value)),
                         (part2.pn if (part2.pn == "") else "PN: {}".format(part2.pn))])
      logger.info("  {: <6} added ({})".format(refdes, info2))
    for refdes in self.lstRefDes_chg:
      part1 = self.bom1.getPartByRefdes(refdes)
      part2 = self.bom2.getPartByRefdes(refdes)
      info = []
      if ((part1.dnp is True) and (part2.dnp is False)):
        info.append("placed")
      elif ((part1.dnp is False) and (part2.dnp is True)):
        info.append("removed")
      if (part1.value != part2.value):
        info.append("val: {} -> {}".format(part1.value, part2.value))
      if (part1.pn != part2.pn):
        info.append("PN: {} -> {}".format(part1.pn, part2.pn))
      info = ", ".join(info)
      logger.info("  {: <6} ({})".format(refdes, info))

  def showReport(self):
    #self._showReport_values()
    #self._showReport_PNs()
    self._showReport_refdes()

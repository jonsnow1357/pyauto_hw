#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc

def mkBOMPart(strValue, lstRefdes, bDNP=None, strPN=""):
  bomp = BOMPart(strValue, strPN)
  bomp.refdes = lstRefdes
  if (bDNP is not None):
    bomp.dnp = bDNP
  return bomp

class BOMPart(object):
  """
  Class for BOM parts. A BOM part has:
  - value and/or PN
  - multiple refdes
  - multiple (manufacturer + PN)
  - can be present or not (in a extended BOM)
  - database parameters

  :var value:
  :var pn:
  :var refdes: list of [refdes, ...]
  :var paramsDB: dictionary {"id": ..., "type": ..., "description": ...}
  :var _dnp: Do Not Place
  :var _mfr: list of [[MfrName1, MfrPN1, DistrName1, DistrPN1], [...], ...]
  """

  def __init__(self, strValue="", strPN=""):
    """
    :param strValue:
    :param strPN:
    """
    self.value = strValue
    self.pn = strPN
    self.refdes = []
    self.paramsDB = {}
    self._dnp = False
    self._mfr = []

  def __str__(self):
    res = "{}: '{}' / '{}', qty: {}".format(self.__class__.__name__, self.pn, self.value,
                                            len(self.refdes))
    if (self._dnp):
      res += ", DNP"
    return res

  @property
  def qty(self):
    return len(self.refdes)

  @property
  def id(self):
    """
    :return: pn if no value, value if no pn, or both if present
    """
    if (pyauto_base.misc.isEmptyString(self.pn)):
      return self.value
    elif (pyauto_base.misc.isEmptyString(self.value)):
      return self.pn
    else:
      return "{}/{}".format(self.value, self.pn)

  def addMfrInfo(self, mfr=None, mfrPN=None, distr=None, distrPN=None):
    lstTemp = [None, None, None, None]
    if ((mfr is not None) and isinstance(mfr, str) and (len(mfr) > 0)):
      if ((mfrPN is not None) and isinstance(mfrPN, str) and (len(mfrPN) > 0)):
        lstTemp[0] = mfr
        lstTemp[1] = mfrPN
      else:
        msg = "CANNOT add mfr without mfrPN"
        logger.error(msg)
        raise RuntimeError(msg)

    if ((distr is not None) and isinstance(distr, str) and (len(distr) > 0)):
      if ((distrPN is not None) and isinstance(distrPN, str) and (len(distrPN) > 0)):
        lstTemp[2] = distr
        lstTemp[3] = distrPN
      else:
        msg = "CANNOT add distr without distrPN"
        logger.error(msg)
        raise RuntimeError(msg)

    self._mfr.append(lstTemp)

  def getMfrInfo(self):
    return self._mfr

  def clrMfrInfo(self):
    self._mfr = []

  @property
  def mfrCnt(self):
    return len(self._mfr)

  @property
  def dnp(self):
    return self._dnp

  @dnp.setter
  def dnp(self, bVal):
    if (bVal not in (True, False)):
      msg = "INCORRECT DNP value: {}".format(bVal)
      logger.error(msg)
      raise RuntimeError(msg)

    self._dnp = bVal

class BOM(object):
  """
  Class for BOM use. A BOM is essentially a collection of BOMPart().

  :var _dictParts: dictionary of {[value, pn, dnp]: BOMPart}
  :var _lstValue: list of values
  :var _lstPN: list of PNs
  :var _lstRefdes: list of refdes
  :var nRefdes: total number of parts
  :var nRefdes_DNP: number of DNP parts
  """

  def __init__(self):
    self._dictParts = {}
    self._lstValue = []
    self._lstPN = []
    self._lstRefdes = []
    self.nRefdes = 0
    self.nRefdes_DNP = 0

  def __str__(self):
    return "{}: {} refdes / {} DNP".format(self.__class__.__name__, self.nRefdes,
                                           self.nRefdes_DNP)

  @property
  def nPN(self):
    return len(self._lstPN)

  @property
  def PNs(self):
    return sorted(self._lstPN)

  @property
  def refdes(self):
    return sorted(self._lstRefdes)

  @property
  def nValues(self):
    return len(self._lstValue)

  @property
  def values(self):
    return sorted(self._lstValue)

  def clear(self):
    self._dictParts = {}
    self._lstValue = []
    self._lstPN = []
    self._lstRefdes = []
    self.nRefdes = 0
    self.nRefdes_DNP = 0

  def addBOMPart(self, part):
    if (not isinstance(part, BOMPart)):
      msg = "INCORRECT type: {}".format(type(part))
      logger.error(msg)
      raise RuntimeError(msg)

    try:
      dPart = self._dictParts[part.value, part.pn, part.dnp]
      dPart.refdes += part.refdes
    except KeyError:
      self._dictParts[part.value, part.pn, part.dnp] = part

  """
  def rmBOMPart(self, part):
    if(type(part) != type(BOMPart())):
      msg = "INCORRECT type: {}".format(type(part))
      logger.error(msg)
      raise RuntimeError(msg)

    try:
      del self._dictParts[part.value, part.pn, part.dnp]
    except KeyError:
      msg = "part not in BOM: {}".format(part)
      logger.error(msg)
      raise RuntimeError(msg)
  """

  def refresh(self):
    """
    refresh BOM after change(s).
    """
    if (len(self._dictParts) == 0):
      msg = "BOM HAS NO parts"
      logger.error(msg)
      raise RuntimeError(msg)

    #logger.info("{} refresh".format(self.__class__.__name__))
    keys = self._dictParts.keys()
    self._lstValue = [t[0] for t in keys if (t[0] != "")]
    self._lstValue = list(set(self._lstValue))  # remove duplicates

    self._lstPN = [t[1] for t in keys if (t[1] != "")]
    self._lstPN = list(set(self._lstPN))  # remove duplicates

    self._lstRefdes = []
    self.nRefdes = 0
    self.nRefdes_DNP = 0
    for p in self._dictParts.values():
      self._lstRefdes += p.refdes
      self.nRefdes += p.qty
      if (p.dnp):
        self.nRefdes_DNP += p.qty

    setRefdes = set(self._lstRefdes)
    if (len(self._lstRefdes) != len(setRefdes)):
      msg = "BOM HAS DUPLICATE refdes"
      logger.error(msg)
      raise RuntimeError(msg)

  def getPartsByValue(self, value):
    """
    :param value:
    :return: list of BOMParts with the specified value
    """
    if (value not in self._lstValue):
      return

    res = []
    for k, v in self._dictParts.items():
      if (value == k[0]):
        res.append(v)
    return res

  def getPartsByPN(self, pn):
    """
    :param pn:
    :return: list of BOMParts with the specified PN
    """
    if (pn not in self._lstPN):
      return

    res = []
    for k, v in self._dictParts.items():
      if (pn == k[1]):
        res.append(v)
    return res

  def getPartByRefdes(self, refdes):
    """
    :param refdes:
    :return: the BOMPart with the specified refdes
    """
    if (refdes not in self._lstRefdes):
      return

    for v in self._dictParts.values():
      if (refdes in v.refdes):
        return v

  def getAllPartsByPN(self):
    """
    Returns a dictionary with pn as keys.
    Each key has a list of BOMParts as value, since we can have BOMParts
    which are placed and BOMParts which are not placed.
    """
    dictRes = {}
    for k, v in self._dictParts.items():
      if (k[1] is None):
        msg = "MISSING PN: {}".format(v)
        logger.error(msg)
        raise RuntimeError(msg)

      if (k[1] in dictRes.keys()):
        #if part already in dictionary add the refdes
        dictRes[k[1]].append(v)
      else:
        #if part not in dictionary add it
        dictRes[k[1]] = [v]

    return dictRes

  def getAllPartsByValue(self):
    """
    Returns a dictionary with part value as keys.
    Each key has a list of BOMParts as value, since we can have BOMParts
    which are placed and BOMParts which are not placed.
    """
    dictRes = {}
    for k, v in self._dictParts.items():
      if (k[0] is None):
        msg = "MISSING value: {}".format(v)
        logger.error(msg)
        raise RuntimeError(msg)

      if (k[0] in dictRes.keys()):
        #if part already in dictionary add the refdes
        dictRes[k[0]].append(v)
      else:
        #if part not in dictionary add it
        dictRes[k[0]] = [v]

    return dictRes

  def getAllPartsByRefdes(self):
    """
    Returns a dictionary with refdes as keys.
    Each key has ONE BOMPart as value.
    """
    dictRes = {}
    for v in self._dictParts.values():
      for ref in v.refdes:
        dictRes[ref] = v

    if (dictRes == {}):
      msg = "BOM without refdes"
      logger.error(msg)
      raise RuntimeError(msg)
    return dictRes

  def readCSV(self, fPath, bomCfg=None):
    """
    :param fPath: path to BOM file
    :param bomCfg: BOMFileCfg object
    :return: BOM() object
    """
    if (bomCfg is None):
      bomCfg = BOMFileCfg()

    #if(not fPath.lower().endswith(".bom")):
    #  logger.error("incorrect file name: {}".format(fPath))
    #  return
    pyauto_base.fs.chkPath_File(fPath)
    logger.info("-- reading {}: {}".format(bomCfg.description, fPath))

    hdrReqRow = [bomCfg.value, bomCfg.refdes]
    hdrOptRow = []
    bQty = False
    bDNP = False
    bMPN = False
    if (not pyauto_base.misc.isEmptyString(bomCfg.qty)):
      hdrOptRow.append(bomCfg.qty)
      bQty = True
    if (not pyauto_base.misc.isEmptyString(bomCfg.DNP)):
      hdrOptRow.append(bomCfg.DNP)
      bDNP = True
    if (not pyauto_base.misc.isEmptyString(bomCfg.MPN)):
      hdrOptRow.append(bomCfg.MPN)
      bMPN = True
    #print("DBG", hdrReqRow)
    #print("DBG", hdrOptRow)

    nCols = len(hdrReqRow) + len(hdrOptRow)
    rowCnt = 0
    self.clear()
    with open(fPath, "r") as fIn:
      csvIn = csv.reader(fIn, delimiter=bomCfg.delimiter)

      for row in csvIn:
        if (len(row) == 0):  # ignore empty lines
          continue
        if (len(row) < nCols):  # ignore lines with less than expected values
          logger.warning("IGNORE(1) line {}: {}".format(csvIn.line_num, row))
          continue
        if (pyauto_base.misc.isEmptyString(row[0])):
          logger.warning("IGNORE(2) line {}: {}".format(csvIn.line_num, row))
          continue

        #print("DBG", rowCnt, row)
        if (rowCnt < bomCfg.nRows_skip):
          pass
        elif (rowCnt == bomCfg.nRows_skip):
          colIdx = pyauto_base.misc.getListIndexByName(row, hdrReqRow, hdrOptRow)
          #print("DBG", colIdx)
        else:
          value = row[colIdx[bomCfg.value]].strip()
          refdes = row[colIdx[bomCfg.refdes]].strip()
          if ((value == "") or (refdes == "")):
            logger.warning("IGNORE(3) line {}: {}".format(csvIn.line_num, row))
            continue
          if (bQty):
            qty = int(float(row[colIdx[bomCfg.qty]]))
            if (qty == 0):
              logger.warning("IGNORE(4) line {}: {}".format(csvIn.line_num, row))
              continue
          if (bDNP):
            dnp = False if (row[colIdx[bomCfg.DNP]] == bomCfg.DNP_placed) else True
          else:
            dnp = False
          if (bMPN):
            pn = row[colIdx[bomCfg.MPN]].strip()
          else:
            pn = ""

          refdes = pyauto_base.misc.parseRangeString(refdes)
          p = mkBOMPart(value, refdes, dnp, pn)

          if (bQty):
            if (bomCfg.qty_check):
              if (qty != p.qty):
                msg = "BOM quantity {} does not match refdes count: {}".format(qty, p)
                logger.error(msg)
                raise RuntimeError(msg)
            else:
              logger.warning("adding dummy refdes to match required qty")
              p.refdes = ["{}_{}".format(p.refdes[0], i) for i in range(0, qty)]
          #print("DBG", p)
          self.addBOMPart(p)
        rowCnt += 1

    self.refresh()
    #logger.info("read {:d} line(s); {:d} part(s)".format(csvIn.line_num, self.nRefdes))
    logger.info("-- done reading file")
    logger.info(self)

  def writeCSV_byRefdes(self, fPath):
    if ((fPath is None) or (len(fPath) == 0)):
      msg = "CANNOT write file with no path"
      logger.error(msg)
      raise RuntimeError(msg)
    if (self.nRefdes == 0):
      msg = "CANNOT write file with no parts"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("-- writing generic csv BOM (by refdes)")
    bomCfg = BOMFileCfg()
    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      csvOut.writerow([bomCfg.refdes, bomCfg.value, bomCfg.MPN, bomCfg.qty, bomCfg.DNP])
      for refdes, bomp in sorted(self.getAllPartsByRefdes().items()):
        csvOut.writerow([refdes, bomp.value, bomp.pn, 1, "DNP" if (bomp.dnp) else ""])
    logger.info("{} written".format(fPath))

  def writeCSV_byVal(self, fPath):
    if ((fPath is None) or (len(fPath) == 0)):
      msg = "CANNOT write file with no path"
      logger.error(msg)
      raise RuntimeError(msg)
    if (self.nRefdes == 0):
      msg = "CANNOT write file with no parts"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("-- writing generic csv BOM (by value)")
    bomCfg = BOMFileCfg()
    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      csvOut.writerow([bomCfg.refdes, bomCfg.value, bomCfg.MPN, bomCfg.qty, bomCfg.DNP])
      for val, lstBomp in self.getAllPartsByValue().items():
        for bomp in lstBomp:
          csvOut.writerow([
              ",".join(bomp.refdes), bomp.value, bomp.pn, bomp.qty, "DNP" if
              (bomp.dnp) else ""
          ])
    logger.info("{} written".format(fPath))

  def writeCSV_byPN(self, fPath):
    if ((fPath is None) or (len(fPath) == 0)):
      msg = "CANNOT write file with no path"
      logger.error(msg)
      raise RuntimeError(msg)
    if (self.nRefdes == 0):
      msg = "CANNOT write file with no parts"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("-- writing generic csv BOM (by PN)")
    bomCfg = BOMFileCfg()
    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      csvOut.writerow([bomCfg.refdes, bomCfg.value, bomCfg.MPN, bomCfg.qty, bomCfg.DNP])
      for val, lstBomp in self.getAllPartsByPN().items():
        for bomp in lstBomp:
          csvOut.writerow([
              ",".join(bomp.refdes), bomp.value, bomp.pn, bomp.qty, "DNP" if
              (bomp.dnp) else ""
          ])
    logger.info("{} written".format(fPath))

class BOMFileCfg(object):

  def __init__(self):
    self.description = "csv file"
    self.delimiter = ","
    self.nRows_skip = 0

    self.value = "value"
    self.refdes = "refdes"
    self.qty = "qty"
    self.DNP = "DNP"
    self.MPN = "MFR_PN"

    self.qty_check = True
    self.DNP_placed = ""

  def setReqNames(self, strValue, strRefdes):
    self.value = strValue
    self.refdes = strRefdes

  def setOptNames(self, strQty=None, strDNP=None, strMPN=None):
    self.qty = strQty
    self.DNP = strDNP
    self.MPN = strMPN

bomFileCIS = BOMFileCfg()
bomFileCIS.description = "CIS BOM file"
bomFileCIS.delimiter = "\t"
bomFileCIS.setReqNames("Value", "Part Reference")
bomFileCIS.setOptNames("Quantity", "DNS", "Part Number")

bomFilePRP = BOMFileCfg()
bomFilePRP.description = "CIS PRP file"
bomFilePRP.delimiter = "\t"
bomFilePRP.setReqNames("Value", "Part Reference")
bomFilePRP.setOptNames(None, "Variant", "Part Number")
bomFilePRP.DNP_placed = "N"

bomFileDX = BOMFileCfg()
bomFileDX.description = "DX BOM file"
bomFileDX.setReqNames("VALUE", "REFDES")
bomFileDX.setOptNames("QTY", "DNS", "DEVICE")

bomFileEpicor = BOMFileCfg()
bomFileEpicor.nRows_skip = 1
bomFileEpicor.setReqNames("Part No.", "Reference Designator")
bomFileEpicor.setOptNames("Qty", None)

bomFileHDL = BOMFileCfg()
bomFileHDL.description = "HDL BOM file"
bomFileHDL.setReqNames("VALUE", "Ref Des")
bomFileHDL.setOptNames("Qty", "STUFF", "PART_NUMBER")
bomFileHDL.DNP_placed = "?"

bomFileAgile = BOMFileCfg()
bomFileAgile.description = "Agile BOM file"
bomFileAgile.setReqNames("COMPONENT_NUMBER", " REFERENCE_DESIGNATOR")
bomFileAgile.setOptNames("QUANTITY", None)

bomFileKiCad = BOMFileCfg()
bomFileKiCad.description = "KiCad BOM file"
bomFileKiCad.setReqNames("value", "ref")
bomFileKiCad.setOptNames(None, None)

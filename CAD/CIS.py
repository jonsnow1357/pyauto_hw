#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase

def readCSV(fPath):
  """
  Reads a *.csv Component dump file from CIS.
  :param fPath:
  :return: Component object
  """
  pyauto_base.fs.chkPath_File(fPath)

  hdrRow = ["Name", "Number", "Type"]
  hdrOpt = ["Net Name"]
  cp = CADbase.Component("U?")
  # cp.value = os.path.basename(fPath)

  logger.info("-- reading CIS component dump file: {}".format(fPath))
  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn)

    for row in csvIn:
      #row = [t for t in row if t != ""]  # strip empty list enties
      row = [t.strip(" \n\r") for t in row]  # cleanup list entries
      if (len(row) == 0):  # ignore empty lines
        continue
      #logger.info(row)
      if (csvIn.line_num == 1):
        dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow, hdrOpt)
        #logger.info(dictIdx)
        continue

      p = CADbase.Pin("U?.{}".format(row[dictIdx[hdrRow[1]]]))
      p.name = row[dictIdx[hdrRow[0]]]
      p.setType_CIS(row[dictIdx[hdrRow[2]]])
      if (hdrOpt[0] in dictIdx.keys()):
        p.netName = row[dictIdx[hdrOpt[0]]]
      #logger.info(p)
      cp.addPin(p)

  cp.refresh()
  logger.info("read {:d} line(s); {:d} pin(s)".format(csvIn.line_num, cp.nPins))
  logger.info("-- done reading file")
  return cp

def writeCSV(fPath, cp):
  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow([
        "Number", "Name", "Type", "Pin Visibility", "Shape", "Pin Group", "Position",
        "Section"
    ])

    for pn in cp.pinNumbers:
      pin = cp.getPinByNumber(pn)
      csvOut.writerow([
          pn, pin.name, CADct.PinTypeConv.dictToCIS[pin.type], "1", "Short", "", "Left", ""
      ])

  logger.info("{} written".format(fPath))

def _readNetlist_Component(ln):
  tmp = [t for t in ln.strip(" \n\r").split("  ") if t != ""]
  #logger.info(tmp)
  if (len(tmp) < 2):
    return

  cp = CADbase.Component(tmp[1].strip(" "))
  cp.value = tmp[0].strip(" ")
  cp.paramsCAD["footprint"] = tmp[2].strip(" ")
  #logger.info(cp)

  if (cp.refdes == ""):
    logger.error("netlist has component without refdes")
    return
  return cp

def _readNetlist_Net(lstLn):
  #logger.info(lstLn)
  if (len(lstLn) < 2):
    return
  if (re.match(r"^\[[0-9]+\].*$", lstLn[0]) is None):
    return

  net = CADbase.Net()
  #logger.info(lstLn[0])
  tmp = lstLn[0].strip(" \n\r").split("]")
  #logger.info(tmp)
  net.id = tmp[0].strip(" []")
  net.name = tmp[1].strip(" ")
  #logger.info(net)
  if (net.name == ""):
    logger.warning("netlist has nets without name")
    return

  for i in range(1, len(lstLn)):
    ln = lstLn[i].split(" ")
    ln = [t for t in ln if (t != "")]
    if (len(ln) != 5):
      if (ln[3] == "Open"):
        ln = [ln[0], ln[1], ln[2], "{} {}".format(ln[3], ln[4]), ln[5]]
      elif (ln[-2] in CADct.PinTypeConv.dictFromCIS.keys()):
        ln = [ln[0], ln[1], " ".join(ln[2:-2]), ln[-2], ln[-1]]
      elif (ln[3] in CADct.PinTypeConv.dictFromCIS.keys()):
        ln = [ln[0], ln[1], ln[2], ln[3], " ".join(ln[4:])]
      else:
        logger.warning("unparseable line: {}".format(ln))

    p = CADbase.Pin("{}.{}".format(ln[0], ln[1]))
    p.name = ln[2]
    p.setType_CIS(ln[3])
    #logger.info(p)
    net.addPin(p)
  net.refresh()
  #logger.info(net)
  return net

def readNetlist(fPath):
  """
  Reads a .NET netlist file.
  :param fPath:
  :return: Netlist object
  """
  if (not fPath.lower().endswith(".net")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  pyauto_base.fs.chkPath_File(fPath)

  ntl = CADbase.Netlist()

  logger.info("-- reading CIS netlist file: {}".format(fPath))
  with open(fPath) as fIn:
    lnNr = 1
    bComponentsParse = False
    bNetsParse = False
    netLines = []

    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      if ((lnNr == 1) and (ln != "Wire List")):
        msg = "UNKNOWN net format"
        logger.error(msg)
        raise CADbase.CADException(msg)

      if (ln == "<<< Component List >>>"):
        logger.info("start component parsing")
        bComponentsParse = True
        bNetsParse = False
      elif (ln == "<<< Wire List >>>"):
        logger.info("start net parsing")
        bComponentsParse = False
        bNetsParse = True
      elif (bComponentsParse):
        if (ln != ""):
          cp = _readNetlist_Component(ln)
          if (cp is not None):
            ntl.addComponent(cp)
      elif (bNetsParse):
        if (ln != ""):
          netLines.append(ln)
        else:
          n = _readNetlist_Net(netLines)
          if (n is not None):
            ntl.addNet(n)
          netLines = []
      #else:
      #  logger.warning("IGNORE line {}: {}".format(lnNr, ln))
      lnNr += 1

  ntl.refresh()
  if ((ntl.nComponents == 0) or (ntl.nNets == 0)):
    msg = "INCORRECT netlist ({:d} component(s); {:d} net(s)".format(
        ntl.nComponents, ntl.nNets)
    logger.error(msg)
    raise RuntimeError(msg)
  logger.info("-- done reading netlist")
  return ntl

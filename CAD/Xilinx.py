#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.base as CADbase

def readPin(fPath):
  """
  Reads a Xilinx .rpt file from Vivado.
  :param fPath:
  :return: Component()
  """
  pyauto_base.fs.chkPath_File(fPath)

  hdrRow = ["Pin Number", "Pin Name", "IO Standard", "Voltage", "IO Bank"]
  hdrOpt = ["Signal Name"]
  cp = CADbase.Component("U?")

  logger.info("-- reading Xilinx pin file: {}".format(fPath))
  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn, delimiter="|", quotechar="\"")
    bHdrFound = False

    for row in csvIn:
      #row = [t for t in row if t != ""]  # strip empty list enties
      row = [t.strip(" \n\r") for t in row]  # cleanup list entries
      if (len(row) == 0):  # ignore empty lines
        continue
      #logger.info(row)
      if (len(row) < 2):  # ignore very short lines
        continue
      if (len(row) == 2):  # ignore short lines but print CAD info
        logger.info(row[1])
        if (row[1].startswith("Device")):
          cp.value = row[1].split(":")[-1].strip()
        continue
      if (len(row) < len(hdrRow)):  # ignore lines shorter than needed
        continue
      if (row[1] == hdrRow[0]):
        dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow, hdrOpt)
        #logger.info(dictIdx)
        bHdrFound = True
        continue

      if (bHdrFound):
        #logger.info(row)
        pinNo = row[dictIdx[hdrRow[0]]]
        pinName = row[dictIdx[hdrRow[1]]]
        pinSig = row[dictIdx[hdrRow[2]]]
        pinVolt = row[dictIdx[hdrRow[3]]]
        pinBank = row[dictIdx[hdrRow[4]]]
        p = CADbase.Pin("U?.{}".format(pinNo))
        p.name = pinName
        p.signaling = pinSig
        p.voltage = pinVolt
        p.bank = pinBank
        if (hdrOpt[0] in dictIdx.keys()):
          p.netName = row[dictIdx[hdrOpt[0]]]
        #logger.info(p)
        cp.addPin(p)

  cp.refresh()
  logger.info("read {:d} line(s); {:d} pin(s)".format(csvIn.line_num, cp.nPins))
  logger.info("-- done reading file")
  return cp

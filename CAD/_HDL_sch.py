#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.base as CADbase

def readNetlist_BodyOrdered(fPath):
  """
  Reads HDL Body-Ordered Netlist.
  :param fPath: path to folder (must contain dialbonl.dat)
  :return: Netlist()
  """
  pyauto_base.fs.chkPath_File(fPath)
  if (not fPath.endswith(".dat")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)

  ntl = CADbase.Netlist()

  logger.info("-- reading HDL Body-Ordered Netlist file: {}".format(fPath))
  with open(fPath) as fIn:
    lnNr = 1

    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      if (lnNr == 1):
        if (not ln.startswith("BODY ORDERED NET LIST")):
          msg = "UNKNOWN net format"
          logger.error(msg)
          raise CADbase.CADException(msg)
      elif (ln == "END BODY ORDERED NET LIST"):
        pass
      else:
        tmp = [t for t in ln.strip(" \n\r").split(" ") if t != ""]
        #logger.info(tmp)
        if (len(tmp) == 4):
          refdes = tmp[0].strip(" ")
          value = tmp[1].strip(" ").split(",")[0]
          pinno = tmp[2].strip(" ")
          netname = tmp[3].strip(" ")
        elif (len(tmp) == 2):
          pinno = tmp[0].strip(" ")
          netname = tmp[1].strip(" ")
        else:
          logger.warning("IGNORE line {}: {}".format(lnNr, ln))
          continue
        try:
          ntl.getComponentByRefdes(refdes)
        except KeyError:
          cp = CADbase.Component(refdes)
          cp.value = value
          ntl.addComponent(cp)
        try:
          net = ntl.getNetByName(netname)
          net.addPinInfo("{}.{}".format(refdes, pinno))
          net.refresh()
        except KeyError:
          net = CADbase.Net()
          net.name = netname
          net.addPinInfo("{}.{}".format(refdes, pinno))
          net.refresh()
          ntl.addNet(net)
      lnNr += 1

  ntl.refresh()
  if ((ntl.nComponents == 0) or (ntl.nNets == 0)):
    msg = "INCORRECT netlist ({:d} component(s); {:d} net(s)".format(
        ntl.nComponents, ntl.nNets)
    logger.error(msg)
    raise RuntimeError(msg)
  logger.info("read {:d} line(s); {:d} component(s); {:d} net(s)".format(
      lnNr, ntl.nComponents, ntl.nNets))
  logger.info("-- done reading file")
  return ntl

def readNetlist_Concise(fPath):
  """
  Reads HDL Concise Netlist.
  :param fPath: path to folder (must contain dialcnet.dat)
  :return: Netlist()
  """
  pyauto_base.fs.chkPath_File(fPath)
  if (not fPath.endswith(".dat")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)

  ntl = CADbase.Netlist()

  logger.info("-- reading HDL Concise Netlist file: {}".format(fPath))
  with open(fPath) as fIn:
    lnNr = 1

    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      # logger.info(ln)
      if (lnNr == 1):
        if (not ln.startswith("CONCISE NET LIST")):
          msg = "UNKNOWN net format"
          logger.error(msg)
          raise CADbase.CADException(msg)
      elif (ln == "END CONCISE NET LIST"):
        pass
      else:
        tmp = [t for t in ln.strip(" \n\r").split(" ") if t != ""]
        #logger.info(tmp)
        if (len(tmp) >= 4):
          netname = tmp[0].strip(" ")
          refdes = tmp[1].strip(" ")
          pinno = tmp[2].strip(" ")
          value = tmp[3].strip(" ").split(",")[0]
        else:
          logger.warning("IGNORE line {}: {}".format(lnNr, ln))
          continue
        try:
          ntl.getComponentByRefdes(refdes)
        except KeyError:
          cp = CADbase.Component(refdes)
          cp.value = value
          ntl.addComponent(cp)
        try:
          net = ntl.getNetByName(netname)
          net.addPinInfo("{}.{}".format(refdes, pinno))
          net.refresh()
        except KeyError:
          net = CADbase.Net()
          net.name = netname
          net.addPinInfo("{}.{}".format(refdes, pinno))
          net.refresh()
          ntl.addNet(net)
      lnNr += 1

  ntl.refresh()
  if ((ntl.nComponents == 0) or (ntl.nNets == 0)):
    msg = "INCORRECT netlist ({:d} component(s); {:d} net(s)".format(
        ntl.nComponents, ntl.nNets)
    logger.error(msg)
    raise RuntimeError(msg)
  logger.info("read {:d} line(s); {:d} component(s); {:d} net(s)".format(
      lnNr, ntl.nComponents, ntl.nNets))
  logger.info("-- done reading file")
  return ntl

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import copy

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.base as CADbase

def _readNetlist_Primitive(lstLn):
  #print("DBG", lstLn)
  if (len(lstLn) < 10):
    raise RuntimeError

  primitive = lstLn[0][11:-2]
  dictProps = {}
  bBody = False
  for ln in lstLn:
    if (ln == "body"):
      bBody = True
      continue
    if (ln == "end_body;"):
      bBody = False
      continue

    if (bBody):
      tmp = ln.strip().split("=")
      if (len(tmp) == 2):
        k = tmp[0]
        v = tmp[1][:-1].strip("'")
        dictProps[k] = v
  #print("DBG", primitive, dictProps)
  return [primitive, dictProps]

def _readNetlist_Part(lstLn, dictPrimitiveInfo):
  #print("DBG", lstLn)
  if (lstLn[0] != "PART_NAME"):
    raise RuntimeError

  tmp = re.split(r"([^ ]+) (.*)", lstLn[1])
  if (len(tmp) != 4):
    msg = "UN-PARSEABLE PART_NAME: {}".format(lstLn[1])
    logger.error(msg)
    raise RuntimeError(msg)

  refdes = tmp[1]
  primitive = tmp[2].strip("':;")
  dictProps = copy.deepcopy(dictPrimitiveInfo[primitive])
  bSection = False
  for ln in lstLn[2:]:
    if (ln.startswith("SECTION_NUMBER")):
      bSection = True
      continue
    if (ln == ""):
      bSection = False
      continue

    if (bSection):
      tmp = re.split(r"([A-Z0-9_]+)='([^']+)',", ln)
      if (len(tmp) == 4):
        dictProps[tmp[1]] = tmp[2]

  val = None
  mfr_pn = None
  lcl_pn = None
  fpt = None
  dnp = False
  room = None
  page = None
  opt_type = None
  opt_tol = None
  opt_fpt = None
  if ("VALUE" in dictProps.keys()):
    val = dictProps["VALUE"]
  if ("PART_NUMBER" in dictProps.keys()):
    lcl_pn = dictProps["PART_NUMBER"]
  if ("MFG_PN" in dictProps.keys()):
    mfr_pn = dictProps["MFG_PN"]
  if ("JEDEC_TYPE" in dictProps.keys()):
    fpt = dictProps["JEDEC_TYPE"]
  if ("ALT_SYMBOLS" in dictProps.keys()):
    opt_fpt = dictProps["ALT_SYMBOLS"]
  # handle DNP/DNA/DNI properties, everybody does it differently
  if ("STUFF" in dictProps.keys()):
    dnp = True if dictProps["STUFF"] in ("DNP", "DNA", "DNI") else False
  if ("ROOM" in dictProps.keys()):
    room = dictProps["ROOM"]
  if ("PHYS_PAGE" in dictProps.keys()):
    page = dictProps["PHYS_PAGE"]
  if ("TYPE" in dictProps.keys()):
    opt_type = dictProps["TYPE"]
  if ("TOLERANCE" in dictProps.keys()):
    opt_tol = dictProps["TOLERANCE"]
  #print("DBG", refdes, primitive, dictProps)
  #print("DBG", refdes, val, lcl_pn, mfr_pn, fpt, dnp, room, page)
  #print("DBG", refdes, opt_fpt, opt_type, opt_tol)

  if (val is not None):
    cp = CADbase.Component(refdes, strValue=val)
  elif (mfr_pn is not None):
    cp = CADbase.Component(refdes, strValue=mfr_pn)
  elif (lcl_pn is not None):
    cp = CADbase.Component(refdes, strValue=lcl_pn)
  else:
    cp = CADbase.Component(refdes, strValue="NONE")
  if (fpt is not None):
    cp.paramsCAD["footprint"] = fpt
    if (opt_fpt is not None):
      cp.paramsCAD["footprint"] += (";" + opt_fpt)
  cp.dnp = dnp
  cp.paramsCAD["mfr_pn"] = mfr_pn
  cp.paramsCAD["local_pn"] = lcl_pn
  cp.paramsCAD["room"] = room
  cp.paramsCAD["page"] = page
  try:
    cp.paramsCAD["type"] = opt_type
  except KeyError:
    cp.paramsCAD["type"] = None
  try:
    cp.paramsCAD["tolerance"] = opt_tol
  except KeyError:
    cp.paramsCAD["tolerance"] = None
  #print("DBG", cp)
  #print("DBG", cp.refdes, cp.value, cp.paramsCAD)
  return cp

def _readNetlist_Net(lstLn):
  #logger.info(lstLn)
  if (lstLn[0] != "NET_NAME"):
    raise RuntimeError
  if (len(lstLn) < 4):
    raise RuntimeError

  net = CADbase.Net(lstLn[1].replace("'", ""))
  for ln in lstLn:
    if (ln.startswith("NODE_NAME")):
      tmp = ln.split()
      net.addPinInfo("{}.{}".format(tmp[-2], tmp[-1]))
  net.refresh()
  #logger.info(net)
  return net

def readNetlist(fPath):
  """
  Reads Allegro netlist.
  :param fPath: path to folder (it must contain pstchip.dat, pstxnet.dat, pstxprt.dat files)
  :return: Netlist object
  """
  chipPath = os.path.join(fPath, "pstchip.dat")
  netsPath = os.path.join(fPath, "pstxnet.dat")
  propsPath = os.path.join(fPath, "pstxprt.dat")
  pyauto_base.fs.chkPath_File(chipPath)
  pyauto_base.fs.chkPath_File(netsPath)
  pyauto_base.fs.chkPath_File(propsPath)

  ntl = CADbase.Netlist()
  dictPrimitiveInfo = {}

  logger.info("-- reading Allegro netlist file: {}".format(chipPath))
  with open(chipPath) as fIn:
    lnNr = 1
    bPrimitive = False
    lstLn = []

    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      #print("DBG", ln)
      if ((lnNr == 1) and (re.match(r"FILE_TYPE ?= ?LIBRARY_PARTS;", ln) is None)):
        msg = "UNKNOWN net format"
        logger.error(msg)
        raise CADbase.CADException(msg)

      if (ln.startswith("primitive ")):
        bPrimitive = True
        lstLn = [ln]
      elif (ln == "end_primitive;"):
        bPrimitive = False
        p, d = _readNetlist_Primitive(lstLn)
        dictPrimitiveInfo[p] = d
      elif (bPrimitive):
        lstLn.append(ln)
      # else:
      #  logger.warning("IGNORE line {}: {}".format(lnNr, ln))
      lnNr += 1
  #logger.info(dictPrimitiveInfo)

  logger.info("-- reading Allegro netlist file: {}".format(propsPath))
  with open(propsPath) as fIn:
    lnNr = 1
    lstLn = []

    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      #print("DBG", ln)
      if ((lnNr == 1) and (re.match(r"FILE_TYPE ?= ?EXPANDEDPARTLIST;", ln) is None)):
        msg = "UNKNOWN net format"
        logger.error(msg)
        raise CADbase.CADException(msg)

      if (ln == "PART_NAME"):
        if (len(lstLn) > 0) and (lstLn[0] == "PART_NAME"):
          cp = _readNetlist_Part(lstLn, dictPrimitiveInfo)
          ntl.addComponent(cp)
        lstLn = [ln]
      elif (ln == "END."):
        cp = _readNetlist_Part(lstLn, dictPrimitiveInfo)
        ntl.addComponent(cp)
      else:
        lstLn.append(ln)
      lnNr += 1

  logger.info("-- reading Allegro netlist file: {}".format(netsPath))
  with open(netsPath) as fIn:
    lnNr = 1
    lstLn = []

    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      if ((lnNr == 1) and (ln != "FILE_TYPE = EXPANDEDNETLIST;")):
        msg = "UNKNOWN net format"
        logger.error(msg)
        raise CADbase.CADException(msg)

      if (ln == "NET_NAME"):
        if (len(lstLn) > 0) and (lstLn[0] == "NET_NAME"):
          net = _readNetlist_Net(lstLn)
          ntl.addNet(net)
        lstLn = [ln]
      elif (ln == "END."):
        net = _readNetlist_Net(lstLn)
        ntl.addNet(net)
      else:
        lstLn.append(ln)
      lnNr += 1

  ntl.refresh()
  if ((ntl.nComponents == 0) or (ntl.nNets == 0)):
    msg = "INCORRECT netlist ({:d} component(s); {:d} net(s)".format(
        ntl.nComponents, ntl.nNets)
    logger.error(msg)
    raise RuntimeError(msg)
  logger.info("-- done reading netlist")
  return ntl

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase

def readCSV(fPath):
  """
  Reads a .csv file component description from IO designer.
  :param fPath:
  :return: Component()
  """
  pyauto_base.fs.chkPath_File(fPath)

  hdrRow = ["Pin Order", "Pin Label", "Pin Number", "Pin Type", "Side", "Inverted"]
  cp = CADbase.Component("U?")
  # cp.value = os.path.basename(fPath)[:-4]

  logger.info("-- reading DX component file: {}".format(fPath))
  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn)

    for row in csvIn:
      #row = [t for t in row if t != ""]  # strip empty list enties
      row = [t.strip(" \n\r") for t in row]  # cleanup list entries
      if (len(row) == 0):  # ignore empty lines
        continue
      #logger.info(row)
      if (csvIn.line_num == 1):
        dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow)
        #logger.info(dictIdx)
        continue

      p = CADbase.Pin("U?.{}".format(row[dictIdx[hdrRow[2]]]))
      p.name = row[dictIdx[hdrRow[1]]]
      p.type = CADct.PinTypeConv.dictFromDx[row[dictIdx[hdrRow[3]]]]
      cp.addPin(p)

  cp.refresh()
  logger.info("read {:d} line(s); {:d} pin(s)".format(csvIn.line_num, cp.nPins))
  logger.info("-- done reading file")
  return cp

def _readNetlist_Component(ln):
  tmp = [t for t in ln.strip(" \n\r").split(" ") if t != ""]
  #logger.info(tmp)

  if (len(tmp) != 2):
    return

  cp = CADbase.Component(tmp[0].strip(" "))
  cp.value = tmp[1].strip(" ")
  #logger.info(cp)

  if (cp.refdes == ""):
    logger.warning("netlist has component without refdes")
    return
  return cp

def _readNetlist_Net(lstLn):
  #logger.info(lstLn)
  if (len(lstLn) < 2):
    return
  if (re.match(r"^\*SIGNAL\* .*$", lstLn[0]) is None):
    return

  net = CADbase.Net()
  lstLn = [t.strip(" \n\r\t") for t in lstLn]
  #logger.info(lstLn)
  net.name = lstLn[0][9:]
  #logger.info(n)
  if (net.name == ""):
    logger.warning("netlist has nets without name")
    return

  pinInfo = " ".join(lstLn[1:]).split(" ")
  pinInfo = [t.strip(" ") for t in pinInfo if (t != "")]
  pinInfo = list(set(pinInfo))
  #logger.info(pinInfo)

  for val in pinInfo:
    p = CADbase.Pin(val)
    #p.netName = n.name
    net.addPin(p)
  net.refresh()
  #logger.info(net)
  return net

def readNetlist(fPath):
  """
  Reads a DxDesigner netlist file.
  :param fPath:
  :return: Netlist()
  """
  if (not fPath.lower().endswith(".asc")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  pyauto_base.fs.chkPath_File(fPath)

  ntl = CADbase.Netlist()

  logger.info("-- reading DX netlist file: {}".format(fPath))
  with open(fPath) as fIn:
    lnNr = 1
    bComponentsParse = False
    bNetsParse = False
    netLines = []

    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      if (lnNr == 1):
        if (not ln.startswith("!PADS-POWERPCB")):
          msg = "UNKNOWN net format"
          logger.error(msg)
          raise CADbase.CADException(msg)
      elif (ln == "*PART*"):
        logger.info("start component parsing")
        bComponentsParse = True
        bNetsParse = False
      elif (ln == "*CONNECTION*"):
        logger.info("start net parsing")
        bComponentsParse = False
        bNetsParse = True
      elif (ln == "*MISC*"):
        logger.info("start properties parsing")
        bComponentsParse = False
        bNetsParse = False
      elif (bComponentsParse):
        if (ln != ""):
          cp = _readNetlist_Component(ln)
          if (cp is not None):
            ntl.addComponent(cp)
      elif (bNetsParse):
        if (ln.startswith("*SIGNAL*")):
          n = _readNetlist_Net(netLines)
          if (n is not None):
            ntl.addNet(n)
          netLines = [ln]
        else:
          netLines.append(ln)
      #else:
      #  logger.warning("IGNORE line {}: {}".format(lnNr, ln))
      lnNr += 1

  ntl.refresh()
  if ((ntl.nComponents == 0) or (ntl.nNets == 0)):
    msg = "INCORRECT netlist (no nets or components)"
    logger.error(msg)
    raise RuntimeError(msg)
  logger.info("-- done reading netlist")
  return ntl

def writeCSV(fPath, cp):
  """
  Writes a csv Pin-out file that can be imported in DX Designer.
  :param fPath:
  :param cp: Component()
  """
  if ((fPath is None) or (len(fPath) == 0)):
    msg = "CANNOT write file with no path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not fPath.lower().endswith(".csv")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  cp.refresh()

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(
        ["Pin Order", "Pin Label", "Pin Number", "Pin Type", "Side", "Inverted", "Pin Net"])
    for pn in cp.pinNumbers:
      #logger.info(comp.getPinByNumber(pn))
      csvOut.writerow([
          "-",
          cp.getPinByNumber(pn).name,
          cp.getPinByNumber(pn).number, "-", "-", "-",
          cp.getPinByNumber(pn).netName
      ])
  logger.info("{} written".format(fPath))

def writeCSV_Short(fPath, cp):
  """
  Writes a csv Pin-out file that can be imported in DX Designer.
  :param fPath:
  :param cp: Component()
  """
  if ((fPath is None) or (len(fPath) == 0)):
    msg = "CANNOT write file with no path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not fPath.lower().endswith(".csv")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  cp.refresh()

  with open(fPath, "w") as fOut:
    csvOut = csv.writer(fOut, lineterminator="\n")
    csvOut.writerow(["Pin Number", "Pin Label", "Pin Net"])
    for pn in cp.pinNumbers:
      #logger.info(comp.getPinByNumber(pn))
      csvOut.writerow([
          cp.getPinByNumber(pn).number,
          cp.getPinByNumber(pn).name,
          cp.getPinByNumber(pn).netName
      ])
  logger.info("{} written".format(fPath))

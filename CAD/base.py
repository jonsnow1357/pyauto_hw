#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv
import json
import natsort

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct
from ._base_BOM import *
from ._base_PCB import *
from ._base_compare import *

class CADException(Exception):
  pass

def mkPin(strRefdes,
          strNo,
          strName="",
          strType=CADct.pinTypeAnalog,
          strBank="",
          strNetName=""):
  pin = Pin("{}.{}".format(strRefdes, strNo))
  pin.type = strType
  pin.name = strName
  pin.bank = strBank
  pin.netName = strNetName
  return pin

class Pin(object):
  """
  Class for pin schematic information. A pin has:
  - type (one of lstPinTypes)
  - refdes
  - number

  :var refdes: reference designator of the part which has the pin
  :var number: pin number
  :var name: pin name
  :var nameAlt: pin alternate names, dictionary where key can be:
    - opt, cfg
    - alteraTXRXCh, alteraLVDS, alteraDQS8, alteraDQS16, alteraDQS32, alteraHDDR, alteraHLPDDR, alteraRLDRAM, alteraQDR
    - other[0-9]+
  :var params: dictionary of parameters
  :var paramsCAD: dictionary of parameters (from CAD)
  :var signaling: any of lstPinTypes
  :var voltage:
  """

  def __init__(self, strVal="U?.1"):
    """
    :param strVal: <refdes>.<pin_no> (ex: U1.A1)
    """
    self._type = CADct.pinTypeAnalog
    self.refdes = None
    self.number = None
    self.setInfo(strVal=strVal)
    self.name = ""
    self.nameAlt = {}
    self.params = {"bank": "", "side": "", "order": None, "signaling": "", "voltage": ""}
    self.paramsCAD = {"net": ""}

  def __str__(self):
    res = "{}: {}.{: <4} [{: <6}]".format(self.__class__.__name__, self.refdes, self.number,
                                          self._type)
    if (self.name != ""):
      res += ", name: {}".format(self.name)
    if (self.paramsCAD["net"] != ""):
      res += ", net: {}".format(self.paramsCAD["net"])
    return res

  @property
  def type(self):
    return self._type

  @type.setter
  def type(self, val):
    if (val in CADct.lstPinTypes):
      self._type = val
    else:
      logger.warning("UNRECOGNIZED pin type: {}".format(val))
      self._type = CADct.pinTypeAnalog

  @property
  def bank(self):
    return self.params["bank"]

  @bank.setter
  def bank(self, val):
    self.params["bank"] = val

  @property
  def side(self):
    return self.params["side"]

  @side.setter
  def side(self, val):
    if (val not in CADct.lstPinSides):
      msg = "INCORRECT pin side: {}".format(val)
      logger.error(msg)
      raise RuntimeError(msg)

    self.params["side"] = val

  @property
  def order(self):
    return self.params["order"]

  @order.setter
  def order(self, val):
    if (isinstance(val, int)):
      self.params["order"] = val
    else:
      self.params["order"] = int(val)

  @property
  def signaling(self):
    return self.params["signaling"]

  @signaling.setter
  def signaling(self, val):
    self.params["signaling"] = val

  @property
  def voltage(self):
    return self.params["voltage"]

  @voltage.setter
  def voltage(self, val):
    self.params["voltage"] = val

  @property
  def netName(self):
    return self.paramsCAD["net"]

  @netName.setter
  def netName(self, val):
    self.paramsCAD["net"] = val

  @property
  def info(self):
    return "{}.{}".format(self.refdes, self.number)

  @property
  def json(self):
    return {"name": self.name, "number": self.number, "type": self.type}

  def setType_CIS(self, CISType):
    if (CISType in CADct.PinTypeConv.dictFromCIS.keys()):
      self._type = CADct.PinTypeConv.dictFromCIS[CISType]
    else:
      logger.warning("UNRECOGNIZED CIS pin type: {}".format(CISType))
      self._type = CADct.PinTypeConv.dictFromCIS[CADct.pinTypeAnalog]

  def setType_Altera(self, AltType):
    if (AltType in CADct.PinTypeConv.dictFromAltera.keys()):
      self._type = CADct.PinTypeConv.dictFromAltera[AltType]
    else:
      logger.warning("UNRECOGNIZED Altera pin type: {}".format(AltType))
      self._type = CADct.PinTypeConv.dictFromAltera[CADct.pinTypeAnalog]

  def setType_KiCAD(self, KiType):
    if (KiType in CADct.PinTypeConv.dictFromKiCAD.keys()):
      self._type = CADct.PinTypeConv.dictFromKiCAD[KiType]
    else:
      logger.warning("UNRECOGNIZED KiCAD pin type: {}".format(KiType))
      self._type = CADct.PinTypeConv.dictFromKiCAD[CADct.pinTypeAnalog]

  def setInfo(self, strVal):
    """
    :param strVal: <refdes>.<pin_no> (ex: U1.A1)
    """
    if (pyauto_base.misc.isEmptyString(strVal)):
      msg = "CANNOT set empty pin info"
      logger.error(msg)
      raise RuntimeError(msg)

    tmp = strVal.split(".")
    if ((len(tmp) != 2) or (tmp[0] == "") or (tmp[1] == "")):
      msg = "INCORRECT pin info: {}".format(strVal)
      logger.error(msg)
      raise RuntimeError(msg)

    self.refdes = tmp[0]
    self.number = tmp[1]

  def getFullName(self):
    res = [self.name]
    for k in sorted(self.nameAlt.keys()):
      res.append(self.nameAlt[k])
    return "/".join(res)

  def getNetName_CIS(self):
    """returns a cleaned CIS schematic net name

    If the pin name has a bar over the name (represented in CIS as "\" after each character)
    it cleans the name and adds "b" as prefix.
    If the name end is a number it adds square brackets around the number
    assuming it's part of a bus.
    """
    res = self.paramsCAD["net"]
    #logger.info(res)
    if (re.match(r"^.*([a-zA-Z0-9]\\)+.*$", self.paramsCAD["net"])):
      res = res.replace("\\", "") + "b"
    res = re.sub(r"^(.*[a-zA-Z_-])([0-9]+)$", r"\1[\2]", res)
    return res

  def getName_AlteraPin(self):
    """returns a pin name based on Altera specific information

    The pin name is based on the following information:
      number
      name
      bank
      nameAlt["opt"] - optional pin function
      nameAlt["cfg"] - configuration pin function
      nameAlt["alteraTXRXCh"] - dedicated TX/RX channel for pin
      nameAlt["alteraLVDS"] - LVDS channel for pin
      nameAlt["alteraDQSx"] - DQS info for pin
      nameAlt["alteraHx"] - hard IP info for pin
    """
    res = []
    #power pins
    if (self.name.startswith("VCC") or self.name.startswith("GND")
        or self.name.startswith("VREF") or self.name.startswith("DNU")
        or (self.name == "NC")):
      return (self.name + "_" + self.number).upper()
    #IO pins
    if (self.name == "IO"):
      res += ["_".join([self.name, self.bank, self.number])]
      if ("opt" in self.nameAlt.keys()):
        #put RUP, RDN in front of the name
        if (self.nameAlt["opt"].startswith("RUP") or self.nameAlt["opt"].startswith("RDN")):
          res = [self.nameAlt["opt"]] + res
        #remove multiple FPLL descriptions
        elif (self.nameAlt["opt"].startswith("FPLL")):
          res += [self.nameAlt["opt"].split(",")[0]]
        else:
          res += [self.nameAlt["opt"]]

      if ("cfg" in self.nameAlt.keys()):
        res += [self.nameAlt["cfg"]]

      if ("alteraTXRXCh" in self.nameAlt.keys()):
        if (self.nameAlt["alteraTXRXCh"].startswith("DIFFIO_")):
          res += [self.nameAlt["alteraTXRXCh"][7:]]
      #usually similar with alteraTXRXCh
      #if("alteraLVDS" in self.nameAlt.keys()):
      #  if(self.nameAlt["alteraLVDS"].startswith("DIFFOUT_")):
      #    res += [self.nameAlt["alteraLVDS"][8:]]

      lstDQS = []
      if ("alteraDQS8" in self.nameAlt.keys()):
        if (self.nameAlt["alteraDQS8"] not in lstDQS):
          lstDQS += [self.nameAlt["alteraDQS8"]]
      if ("alteraDQS16" in self.nameAlt.keys()):
        if (self.nameAlt["alteraDQS16"] not in lstDQS):
          lstDQS += [self.nameAlt["alteraDQS16"]]
      if ("alteraDQS32" in self.nameAlt.keys()):
        if (self.nameAlt["alteraDQS32"] not in lstDQS):
          lstDQS += [self.nameAlt["alteraDQS32"]]
      res += sorted(lstDQS)

      lstHardIp = []
      if ("alteraHDDR" in self.nameAlt.keys()):
        if (self.nameAlt["alteraHDDR"] not in lstHardIp):
          lstHardIp += [self.nameAlt["alteraHDDR"]]
      if ("alteraHLPDDR" in self.nameAlt.keys()):
        if (self.nameAlt["alteraHLPDDR"] not in lstHardIp):
          lstHardIp += [self.nameAlt["alteraHLPDDR"]]
      if ("alteraHRLDRAM" in self.nameAlt.keys()):
        if (self.nameAlt["alteraHRLDRAM"] not in lstHardIp):
          lstHardIp += [self.nameAlt["alteraHRLDRAM"]]
      if ("alteraQDR" in self.nameAlt.keys()):
        if (self.nameAlt["alteraQDR"] not in lstHardIp):
          lstHardIp += [self.nameAlt["alteraQDR"]]
      res += sorted(lstHardIp)

      #logger.info(res)
      return "/".join(res).replace(",", "/").replace(" ", "").upper()
    # rest of pins
    res += [self.name]
    if ("opt" in self.nameAlt.keys()):
      res += [self.nameAlt["opt"]]

    return "/".join(res).replace(",", "/").replace(" ", "").upper()

  def getInfo_AlteraTcl(self, nameOpt=0):
    """
    Returns a Altera tcl pin location constraint
    :param nameOpt: [0|1|2]
      - nameOpt = 0 uses Pin.name
      - nameOpt = 1 uses Pin.netName
      = nameOpt = 2 uses getNetName_CIS()
    """
    res = None
    if (nameOpt == 0):
      if ((self.name.find(" ") != -1) or (self.name.endswith(")"))):
        res = "set_location_assignment PIN_{} -to \"{}\"".format(self.number, self.name)
      else:
        res = "set_location_assignment PIN_{} -to {}".format(self.number, self.name)
    elif (nameOpt == 1):
      if ((self.paramsCAD["net"].find(" ") != -1) or (self.paramsCAD["net"].endswith(")"))):
        res = "set_location_assignment PIN_{} -to \"{}\"".format(
            self.number, self.paramsCAD["net"])
      else:
        res = "set_location_assignment PIN_{} -to {}".format(self.number,
                                                             self.paramsCAD["net"])
    elif (nameOpt == 2):
      if ((self.getNetName_CIS().find(" ") != -1) or (self.getNetName_CIS().endswith(")"))):
        res = "set_location_assignment PIN_{} -to \"{}\"".format(
            self.number, self.getNetName_CIS())
      else:
        res = "set_location_assignment PIN_{} -to {}".format(self.number,
                                                             self.getNetName_CIS())
    return res

  def getInfo_AlteraPinFile(self):
    """Returns a text line similar with those in Altera .pin files"""
    res = []
    if (len(self.name) == 0):
      logger.warning("name MISSING from Pin")
      return
    res.append(self.name)
    if (len(self.number) == 0):
      logger.warning("number MISSING from Pin")
      return
    res.append(self.number)
    #if(ctPinType[self.__type][2] is None):
    if (CADct.PinTypeConv.dictToAltera[self._type] is None):
      logger.warning("type MISSING from Pin")
      return
    res.append(CADct.PinTypeConv.dictToAltera[self._type])
    if (len(self.signaling) == 0):
      logger.warning("signaling MISSING from Pin")
      return
    res.append(self.signaling)
    if (len(self.voltage) == 0):
      logger.warning("voltage MISSING from Pin")
      return
    res.append(self.voltage)
    if (len(self.bank) == 0):
      logger.warning("bank MISSING from Pin")
      return
    res.append(self.bank)
    #if("alteraUser" in self.nameAlt.keys()):
    #  res.append(self.nameAlt["alteraUser"])
    return " : ".join(res)

  def guessType(self):
    if (re.match(r".*(GND|VSS).*", self.name) is not None):
      self._type = CADct.pinTypeGnd
    elif (re.match(r".*(VCC|VDD|VIN|VOUT).*", self.name) is not None):
      self._type = CADct.pinTypePwr
    elif (re.match(r".*(CLK|RST).*", self.name) is not None):
      self._type = CADct.pinTypeIN
    else:
      self._type = CADct.pinTypeAnalog

class BaseComponent(object):
  """
   Class for a simple component. A component has:
  - 1 refdes
  - 1 value
  - can be present or not (in a schematic or netlist)

  :var refdes:
  :var value:
  :var _dnp: Do Not Place
  """

  def __init__(self, strRefdes="U?", strValue=""):
    self.refdes = None
    self.value = None
    self._dnp = False
    self.setInfo(strRefdes=strRefdes, strValue=strValue)

  @property
  def dnp(self):
    return self._dnp

  @dnp.setter
  def dnp(self, bVal):
    if (bVal not in (True, False)):
      msg = "INCORRECT DNP value: {}".format(bVal)
      logger.error(msg)
      raise RuntimeError(msg)

    self._dnp = bVal

  def _str_list(self):
    res = ["{}:".format(self.__class__.__name__)]
    if (pyauto_base.misc.isEmptyString(self.refdes)):
      res.append("??")
    else:
      res.append(self.refdes)

    if (pyauto_base.misc.isEmptyString(self.value)):
      res.append("????")
    else:
      res.append("{}".format(self.value))

    return res

  def __str__(self):
    return " ".join(self._str_list())

  def setInfo(self, strRefdes, strValue=""):
    if (pyauto_base.misc.isEmptyString(strRefdes)):
      msg = "CANNOT set empty refdes"
      logger.error(msg)
      raise RuntimeError(msg)

    self.refdes = strRefdes
    self.value = strValue

class Component(BaseComponent):
  """
   Class for component/part schematic related information. A component has:
  - 1 refdes
  - 1 value
  - multiple pins (can have same name)
  - multiple nets (one net possible connected to multiple pins)

  :var _dictPins: dictionary of {Pin.number: Pin}
  :var _lstNetNames: list of net names
  :var _lstPinNames: list of pin names
  :var refdes: reference designator
  :var value: usually manufacturer PN
  :var paramsDB: dictionary of parameters (from database or config files)
  :var paramsCAD: dictionary of parameters (from CAD)
  """

  def __init__(self, strRefdes="U?", strValue=""):
    super(Component, self).__init__(strRefdes=strRefdes, strValue=strValue)
    self._dictPins = {}
    self._lstNetNames = []
    self._lstPinNames = []
    self._lstBanks = []
    self.paramsCAD = {"footprint": "", "symbol": "", "mfr_pn": "", "local_pn": ""}
    self.paramsDB = {}

  def _str_list(self):
    res = super(Component, self)._str_list()
    res[0] = self.__class__.__name__
    res.append("{:d} pins(s)/{:d} bank(s)/{:d} net(s)".format(len(self._dictPins),
                                                              len(self._lstBanks),
                                                              len(self._lstNetNames)))
    #if(len(self.params) > 0):
    #  res.append("[{:d} param(s)]".format(len(self.params)))
    #if(self.CAD is not None):
    #  res.append("[CAD: {}]".format(type(self.CAD)))

    return res

  @property
  def nPins(self):
    return len(self._dictPins)

  @property
  def nNets(self):
    return len(self._lstNetNames)

  @property
  def nBanks(self):
    return len(self._lstBanks)

  @property
  def pinNumbers(self):
    return sorted(self._dictPins.keys())

  @property
  def pinNames(self):
    return sorted(self._lstPinNames)

  @property
  def netNames(self):
    return sorted(self._lstNetNames)

  @property
  def banks(self):
    return natsort.natsorted(self._lstBanks)

  @property
  def footprint(self):
    return self.paramsCAD["footprint"]

  @footprint.setter
  def footprint(self, val):
    self.paramsCAD["footprint"] = val

  @property
  def json(self):
    return {
        "refdes": self.refdes,
        "value": self.value,
        "footprint": self.footprint,
        "pins": [pin.json for pin in self._dictPins.values()]
    }

  def dumps(self, indent=None):
    return json.dumps(self.json, indent=indent)

  def clearPins(self):
    self._dictPins = {}
    self._lstPinNames = []
    self._lstBanks = []

  def clearNets(self):
    """removes net information from Component"""
    self._lstNetNames = []
    for p in self._dictPins.values():
      p.netName = ""

  def addPin(self, pin):
    """
    :param Pin pin:
    """
    if (not isinstance(pin, Pin)):
      msg = "INCORRECT type: {}".format(type(pin))
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.refdes != self.refdes):
      msg = "Component refdes {} DOES NOT match Pin refdes {}".format(
          self.refdes, pin.refdes)
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.number == ""):
      msg = "CANNOT add Pin without number to Component {}".format(self.refdes)
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.number in self._dictPins.keys()):
      return
      #msg = "Component {} already has Pin {}.{}".format(self.refdes, pin.refdes, pin.number)
      #logger.warning(msg)
      #raise RuntimeError(msg)

    self._dictPins[pin.number] = pin

  def delPin(self, pin):
    if (not isinstance(pin, Pin)):
      msg = "INCORRECT type: {}".format(type(pin))
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.refdes != self.refdes):
      msg = "Component refdes {} DOES NOT match Pin refdes {}".format(
          self.refdes, pin.refdes)
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.number == ""):
      msg = "CANNOT del Pin with no number from Component {}".format(self.refdes)
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.number not in self._dictPins.keys()):
      return
      #msg = "Component {} has NO Pin {}.{}".format(self.refdes, pin.refdes, pin.number)
      #logger.warning(msg)
      #raise RuntimeError(msg)

    del self._dictPins[pin.number]

  def addPinInfo(self, strPin):
    """
    :param strPin: <refdes_no>.<pin_no> (ex: U1.A1)
    """
    pin = Pin(strPin)
    self.addPin(pin)

  def delPinInfo(self, strPin):
    """
    :param strPin: <refdes_no>.<pin_no> (ex: U1.A1)
    """
    pin = Pin(strPin)
    self.delPin(pin)

  def delNet(self, netName):
    if (netName not in self._lstNetNames):
      return
      #msg = "Component {} has NO Net {}".format(self.refdes, netName)
      #logger.warning(msg)
      #raise RuntimeError(msg)

    for pin in self._dictPins.values():
      if (pin.netName == netName):
        pin.netName = ""

  def refresh(self):
    """
    refresh Component after change(s).
    """
    if (len(self._dictPins) == 0):
      msg = "Component '{}' HAS NO pins".format(self.refdes)
      logger.warning(msg)
      #raise RuntimeError(msg)

    #logger.info("{} refresh".format(self.__class__.__name__))
    self._lstPinNames = []
    self._lstBanks = []
    self._lstNetNames = []
    for pin in self._dictPins.values():
      if (pin.refdes != self.refdes):
        msg = "Component {} has Pin with refdes {}".format(self.refdes, pin.refdes)
        logger.error(msg)
        raise RuntimeError(msg)
      if (pin.name != ""):
        self._lstPinNames.append(pin.name)
      if (pin.bank != ""):
        self._lstBanks.append(pin.bank)
      if (pin.netName != ""):
        self._lstNetNames.append(pin.netName)

    self._lstPinNames = list(set(self._lstPinNames))  # remove duplicates
    self._lstBanks = list(set(self._lstBanks))  # remove duplicates
    self._lstNetNames = list(set(self._lstNetNames))  # remove duplicates

    setPinno = set(self._dictPins.keys())
    if (len(self._dictPins) != len(setPinno)):
      msg = "Component HAS DUPLICATE Pin numbers"
      logger.error(msg)
      raise RuntimeError(msg)

  def getPinByNumber(self, pn):
    """
    :param pn: pin number
    :return: Pin with the above pin number
    """
    return self._dictPins[pn]

  def getPinsByName(self, pName):
    """
    :param pName: pin name
    :return: list of Pin with the above name
    """
    lstPins = []
    for p in self._dictPins.values():
      if (p.name == pName):
        lstPins.append(p)
    return lstPins

  def getAllPinsByNumber(self):
    """
    :return: dictionary of {pin_number: Pin}
    """
    return self._dictPins

  #def getAllPinsByName(self):
  #  #this method should not be needed since pins can have the same name
  #  return NotImplementedError

  def getDuplicatePinNames(self):
    """
    This method searches only in the main pin name, does not look at the alternate names.

    :return: dictionary of {pin_name: [Pin, Pin, ...]
    """
    dupNames = []
    allPinNames = [p.name for p in self._dictPins.values()]
    while (len(allPinNames) > 0):
      pn = allPinNames.pop()
      if (pn in allPinNames):
        dupNames.append(pn)
    dupNames = list(set(dupNames))  # remove multi-plicates

    res = {}
    for pn in dupNames:
      res[pn] = self.getPinsByName(pn)
    return res

  def readCSV(self, fPath):
    if (not os.path.basename(fPath).lower().endswith(".pins.csv")):
      msg = "INCORRECT file name (*.pins.csv): {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)
    pyauto_base.fs.chkPath_File(fPath)

    self.clearPins()
    self.clearNets()
    self.value = os.path.basename(fPath)[:-9]

    #logger.info("-- reading *.pins.csv file: {}".format(fPath))
    lstReq = ["number", "type", "name"]
    with open(fPath, "r") as fIn:
      tmp = fIn.readline().strip(" \n\r").split(",")
    pyauto_base.misc.getListIndexByName(tmp, lstReq)
    if ("" in tmp):
      msg = "empty column name: {}".format(tmp)
      logger.error(msg)
      raise RuntimeError(msg)

    res = pyauto_base.misc.readCSV_asRows(fPath)
    for dict_row in res:
      p = Pin("{}.{}".format(self.refdes, dict_row["number"]))
      p.type = CADct.pinTypeAnalog if (dict_row["type"] == "") else dict_row["type"]
      p.name = dict_row["name"]
      for k, v in dict_row.items():
        if ((k.startswith("name_")) and (v != "")):
          p.nameAlt[k[5:]] = v
        if ((k == "bank") and (v != "")):
          p.bank = v
        if ((k == "side") and (v != "")):
          p.side = v
        if ((k == "order") and (v != "")):
          p.order = v
      self.addPin(p)

    self.refresh()
    #logger.info("-- done reading file")
    logger.info("read {:d} pin(s)".format(self.nPins))

  def writeCSV(self, fPath):
    if (not os.path.basename(fPath).lower().endswith(".pins.csv")):
      msg = "INCORRECT file name (*.pins.csv): {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)

    lstNameAlt = []
    bSide = False
    bOrder = False
    for pin in self._dictPins.values():
      if (len(pin.nameAlt) > len(lstNameAlt)):
        lstNameAlt = pin.nameAlt.keys()
      if (pin.side != ""):
        bSide = True
      if (pin.order is not None):
        bOrder = True
    lstNameAlt = sorted(lstNameAlt)

    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")
      lstColHdr = ["number", "type", "name"]
      if (len(self._lstBanks) > 0):
        lstColHdr += ["bank"]
      if (bSide):
        lstColHdr += ["side"]
      if (bOrder):
        lstColHdr += ["order"]
      if (len(lstNameAlt) > 0):
        lstColHdr += ["name_{}".format(t) for t in lstNameAlt]
      csvOut.writerow(lstColHdr)

      for pn in self.pinNumbers:
        pin = self._dictPins[pn]
        lstColVal = [pn, pin.type, pin.name]
        if (len(self._lstBanks) > 0):
          lstColVal.append(pin.bank)
        if (bSide):
          lstColVal.append(pin.side)
        if (bOrder):
          lstColVal.append(pin.order)
        if (len(lstNameAlt) > 0):
          for k in lstNameAlt:
            try:
              lstColVal.append(pin.nameAlt[k])
            except KeyError:
              lstColVal.append("")
        csvOut.writerow(lstColVal)
    logger.info("{} written".format(fPath))

  def readJSON(self, fPath):
    if (not os.path.basename(fPath).lower().endswith(".pins.json")):
      msg = "INCORRECT file name (*.pins.json): {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)
    pyauto_base.fs.chkPath_File(fPath)

    self.clearPins()
    self.clearNets()

    #logger.info("-- reading *.pins.json file: {}".format(fPath))
    with open(fPath, "r") as fIn:
      data = json.load(fIn)
    self.refdes = data["refdes"]
    self.value = data["value"]
    for tmp in data["pins"]:
      p = Pin("{}.{}".format(self.refdes, tmp["number"]))
      p.type = tmp["type"]
      p.name = tmp["name"]
      self.addPin(p)

    self.refresh()
    #logger.info("-- done reading file")
    logger.info("read {:d} pin(s)".format(self.nPins))

  def writeJSON(self, fPath):
    if (not os.path.basename(fPath).lower().endswith(".pins.json")):
      msg = "INCORRECT file name (*.pins.json): {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)

    res = {"refdes": self.refdes, "value": self.value}
    lstPins = []
    for pn in self.pinNumbers:
      pin = self._dictPins[pn]
      dictPin = {"number": pn, "name": pin.name, "type": pin.type}
      if (pin.bank != ""):
        dictPin["bank"] = pin.bank
      lstPins.append(dictPin)
    res["pins"] = lstPins
    with open(fPath, "w") as fOut:
      json.dump(res, fOut, indent="  ")
    logger.info("{} written".format(fPath))

  def readCSV_cadence_hdl(self, fPath):
    if (not os.path.basename(fPath).lower().endswith(".hdl.csv")):
      msg = "INCORRECT file name (*.hdl.csv): {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)
    pyauto_base.fs.chkPath_File(fPath)

    self.clearPins()
    self.clearNets()
    self.value = os.path.basename(fPath)[:-9]

    #logger.info("-- reading *.pins.csv file: {}".format(fPath))
    lstReq = ["PIN_NUMBER", "PIN_TYPE", "PIN_NAME"]
    row_skip = 0
    with open(fPath, "r") as fIn:
      tmp = []
      while (len(tmp) < 3):
        tmp = fIn.readline().strip(" \n\r").split(",")
        row_skip += 1
    pyauto_base.misc.getListIndexByName(tmp, lstReq)
    if ("" in tmp):
      msg = "empty column name: {}".format(tmp)
      logger.error(msg)
      raise RuntimeError(msg)

    conv_pin_type = CADct.PinTypeConv.dictFromHDL
    res = pyauto_base.misc.readCSV_asRows(fPath, rowSkip=row_skip)
    for dict_row in res:
      p = Pin("{}.{}".format(self.refdes, dict_row["PIN_NUMBER"]))
      p.type = conv_pin_type[dict_row["PIN_TYPE"]]
      p.name = dict_row["PIN_NAME"]
      if ("SYMBOL" in dict_row.keys()):
        p.bank = dict_row["SYMBOL"]
      if ("PIN_LOCATION" in dict_row.keys()):
        p.side = dict_row["PIN_LOCATION"].lower()
      # if ("PIN_POSITION" in dict_row.keys()):
      #   p.order = dict_row["PIN_POSITION"]
      self.addPin(p)

    self.refresh()
    #logger.info("-- done reading file")
    logger.info("read {:d} pin(s)".format(self.nPins))

  def writeCSV_cadence_hdl(self, fPath):
    if (not os.path.basename(fPath).lower().endswith(".hdl.csv")):
      msg = "INCORRECT file name (*.hdl.csv): {}".format(fPath)
      logger.error(msg)
      raise RuntimeError(msg)

    conv_pin_type = CADct.PinTypeConv.dictToHDL
    bSide = False
    bOrder = False
    for pin in self._dictPins.values():
      if (pin.side != ""):
        bSide = True
      if (pin.order != ""):
        bOrder = True

    with open(fPath, "w") as fOut:
      csvOut = csv.writer(fOut, lineterminator="\n")

      csvOut.writerow(["CLASS", "IC"])
      csvOut.writerow(["PHYS_DES_PREFIX", "U"])
      csvOut.writerow(["BODY_NAME", self.value.upper()])
      csvOut.writerow(["PACKAGE_NAME", self.value.upper()])

      lstColHdr = ["PIN_NUMBER", "PIN_TYPE", "PIN_NAME"]
      if (len(self._lstBanks) > 0):
        lstColHdr += ["SYMBOL"]
      if (bSide):
        lstColHdr += ["PIN_LOCATION"]
      if (bOrder):
        lstColHdr += ["PIN_POSITION", "PIN_SHAPE"]
      csvOut.writerow(lstColHdr)

      for pn in self.pinNumbers:
        pin = self._dictPins[pn]
        lstColVal = [pn, conv_pin_type[pin.type], pin.name]
        if (len(self._lstBanks) > 0):
          lstColVal.append(pin.bank)
        if (bSide):
          lstColVal.append(pin.side)
        if (bOrder):
          lstColVal.append(pin.order)
          if (pin.name.endswith("_N")):
            lstColVal.append("Dot")
          else:
            lstColVal.append("Line")
        csvOut.writerow(lstColVal)
    logger.info("{} written".format(fPath))

class Net(object):
  """
  Class for net schematic related information. A net has:
  - 1 name
  - multiple pins (on multiple components)

  :var _id:
  :var _dictPins: dictionary of {Pin.info: Pin, ...}
  :var _lstRefdes: list of refdes
  :var name:
  :var params: dictionary of parameters
  :var CAD: CAD dependent data structure
  """

  def __init__(self, name=""):
    self._id = ""
    self._dictPins = {}
    self._lstRefdes = []
    self.name = name
    self.paramsCAD = {}

  def __str__(self):
    if (self._id == ""):
      return "{}: '{}' {} pins(s)/{} refdes".format(self.__class__.__name__, self.name,
                                                    len(self._dictPins),
                                                    len(self._lstRefdes))
    else:
      return "{}: '{}' / '{}' {} pins(s)]/{} refdes".format(self.__class__.__name__,
                                                            self.name, self._id,
                                                            len(self._dictPins),
                                                            len(self._lstRefdes))

  @property
  def id(self):
    return self._id

  @id.setter
  def id(self, val):
    self._id = val

  @property
  def nPins(self):
    return len(self._dictPins)

  @property
  def nRefdes(self):
    return len(self._lstRefdes)

  @property
  def pinInfo(self):
    return sorted([t for t in self._dictPins.keys()])

  @property
  def refdes(self):
    return sorted(self._lstRefdes)

  @property
  def json(self):
    return {"name": self.name, "pins": list(self._dictPins.keys())}

  def dumps(self, indent=None):
    return json.dumps(self.json, indent=indent)

  def addPin(self, pin):
    if (not isinstance(pin, Pin)):
      msg = "INCORRECT type: {}".format(type(pin))
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.info in self._dictPins.keys()):
      return
      #msg = "Net {} already has Pin {}".format(self.name, pin.info)
      #logger.warning(msg)
      #raise RuntimeError(msg)

    self._dictPins[pin.info] = pin

  def addPinInfo(self, strPin):
    """
    :param strPin: <refdes_no>.<pin_no> (ex: U1.A1)
    """
    pin = Pin(strPin)
    self.addPin(pin)

  def addPinInfoList(self, lstPin):
    """
    :param lstPin: list of [<refdes_no>.<pin_no>, ...] (like U5.H10)
    """
    for val in lstPin:
      pin = Pin(val)
      self.addPin(pin)

  def delPin(self, pin):
    if (not isinstance(pin, Pin)):
      msg = "INCORRECT type: {}".format(type(pin))
      logger.error(msg)
      raise RuntimeError(msg)
    if (pin.info not in self._dictPins.keys()):
      return
      #msg = "Net {} has NO Pin {}".format(self.name, pin.info)
      #logger.warning(msg)
      #raise RuntimeError(msg)

    del self._dictPins[pin.info]

  def delPinInfo(self, strPin):
    """
    :param strPin: <refdes_no>.<pin_no> (ex: U1.A1)
    """
    pin = Pin(strPin)
    self.delPin(pin)

  def delPinInfoList(self, lstPin):
    """
    :param lstPin: list of [<refdes_no>.<pin_no>, ...] (like U5.H10)
    """
    for val in lstPin:
      pin = Pin(val)
      self.delPin(pin)

  def refresh(self):
    """
    refresh Net after change(s).
    """
    if (len(self._dictPins) == 0):
      msg = "Net HAS NO pins"
      logger.error(msg)
      raise RuntimeError(msg)

    #logger.info("{} refresh".format(self.__class__.__name__))
    for pin in self._dictPins.values():
      pin.netName = self.name

    tmp = [t.split(".")[0] for t in self._dictPins.keys()]
    self._lstRefdes = list(set(tmp))  # remove duplicates

  def getPinByInfo(self, strPin):
    """
    :param strPin: <refdes_no>.<pin_no> (ex: U1.A1)
    :return: Pin
    """
    return self._dictPins[strPin]

  def getAllPins(self):
    return self._dictPins

  def getRefdesMatch(self, regex=".*"):
    """
    :param regex: regular expression for matching
    :return: list of [refdes, ...]
    """
    crgx = re.compile(regex)
    return [t for t in self._lstRefdes if (crgx.match(t) is not None)]

'''
class DiffNet(object):

  def __init__(self):
    self._id = ""
    self.name = ""
    self.netp = None
    self.netn = None

  def __str__(self):
    return "{}: '{}' [p:'{}', n:'{}']".format(self.__class__.__name__, self.name, self.netp.name, self.netn.name)
'''

def isPower(netName):
  for regex in CADct.regexNetPwr:
    if (re.match(regex, netName) is not None):
      return True
  return False

def isGround(netName):
  if (re.match(CADct.regexNetGnd, netName) is not None):
    return True
  return False

class Netlist(object):
  """
  Class for netlist. A Netlist is a collection of Component and Net objects describing a schematic.

  :var _dictComponents: dictionary of {Component.refdes: Component}
  :var _dictNets: dictionary of {Net.name: Net}
  :var name:
  """

  def __init__(self):
    self._dictComponents = {}
    self._dictNets = {}
    self.name = ""

  def __str__(self):
    if (self.name != ""):
      return "{} {}: {} component(s)/{} net(s)".format(self.__class__.__name__, self.name,
                                                       len(self._dictComponents),
                                                       len(self._dictNets))
    else:
      return "{}: {} component(s)/{} net(s)".format(self.__class__.__name__,
                                                    len(self._dictComponents),
                                                    len(self._dictNets))

  @property
  def nComponents(self):
    return len(self._dictComponents)

  @property
  def nNets(self):
    return len(self._dictNets)

  @property
  def refdes(self):
    return sorted(self._dictComponents.keys())

  @property
  def netNames(self):
    return sorted(self._dictNets.keys())

  @property
  def json(self):
    return {
        "name": self.name,
        "components": [cp.json for cp in self._dictComponents.values()],
        "nets": [net.json for net in self._dictNets.values()],
    }

  def showInfo(self):
    logger.info(self)
    for k in sorted(self._dictComponents.keys()):
      logger.info("  {}".format(self._dictComponents[k]))
    for k in sorted(self._dictNets.keys()):
      logger.info("  {}".format(self._dictNets[k]))

  def dumps(self, indent=None):
    return json.dumps(self.json, indent=indent)

  def addComponent(self, cp):
    if (not isinstance(cp, Component)):
      msg = "incorrect type: {}".format(type(cp))
      logger.error(msg)
      raise RuntimeError(msg)
    if (cp.refdes == ""):
      msg = "CANNOT add Component without refdes to netlist"
      logger.error(msg)
      raise RuntimeError(msg)
    if (cp.netNames != []):
      # TODO:
      msg = "CANNOT add Component with nets to netlist"
      logger.error(msg)
      raise RuntimeError(msg)

    self._dictComponents[cp.refdes] = cp

  def delComponent(self, cRefdes):
    if (cRefdes not in self._dictComponents.keys()):
      #return
      msg = "Netlist {} has NO component {}".format(self.name, cRefdes)
      logger.warning(msg)
      raise RuntimeError(msg)

    cp = self._dictComponents[cRefdes]
    del self._dictComponents[cRefdes]
    for netName in cp.netNames:
      net = self._dictNets[netName]
      lstPinInfo = list(
          set(net.pinInfo)
          & set(["{}.{}".format(cp.refdes, t) for t in cp.pinNumbers]))
      net.delPinInfoList(lstPinInfo)
      if (len(net.pinInfo) > 0):
        net.refresh()

  def addNet(self, net):
    if (not isinstance(net, Net)):
      msg = "incorrect type: {}".format(type(net))
      logger.error(msg)
      raise RuntimeError(msg)
    if (net.name == ""):
      msg = "CANNOT add Net without name to Netlist"
      logger.error(msg)
      raise RuntimeError(msg)

    self._dictNets[net.name] = net

  def delNet(self, netName):
    if (netName not in self._dictNets.keys()):
      return
      #msg = "Netlist {} has NO net {}".format(self.name, netName)
      #logger.warning(msg)
      #raise RuntimeError(msg)

    net = self._dictNets[netName]
    del self._dictNets[netName]
    for refdes in net.refdes:
      cp = self._dictComponents[refdes]
      cp.delNet(netName)
      if (len(cp.netNames) > 0):
        cp.refresh()

  def mergeNet(self, lstNetNames, bFirstName=False):
    if (bFirstName):
      finalNetName = lstNetNames[0]
    else:
      finalNetName = min(lstNetNames, key=len)
    #logger.info("merge net(s) {} into {}".format(lstNetNames, finalNetName))
    finalNet = self._dictNets[finalNetName]
    for netname in lstNetNames:
      if (netname != finalNetName):
        net = self._dictNets[netname]
        finalNet.addPinInfoList(net.pinInfo)
        self.delNet(netname)
    finalNet.refresh()

  def renameNet(self, netName, newNetName):
    if (netName not in self._dictNets.keys()):
      msg = "Netlist {} has NO net {}".format(self.name, netName)
      logger.error(msg)
      raise RuntimeError(msg)
    if (newNetName in self._dictNets.keys()):
      msg = "Netlist {} ALREADY has net {}, USE merge NOT rename".format(
          self.name, newNetName)
      logger.error(msg)
      raise RuntimeError(msg)
    #logger.info("rename net {} to {}".format(netName, newNetName))
    net = self._dictNets[netName]
    net.name = newNetName
    net.refresh()
    self._dictNets.pop(netName)
    self._dictNets[net.name] = net

  def _refreshComponentPins(self):
    """
    updates all Components with Pins found in Nets
    """
    #for cp in self._dictComponents.values():
    #  cp.clearPins()

    for net in self._dictNets.values():
      #net.refresh()
      for pininfo in net.pinInfo:
        refdes, pinno = pininfo.split(".")
        if (refdes not in self._dictComponents.keys()):
          msg = "Netlist: refdes {} found in nets BUT NOT in components".format(refdes)
          logger.error(msg)
          raise RuntimeError(msg)

        cp = self._dictComponents[refdes]
        if (pinno not in self._dictComponents[refdes].pinNumbers):
          pin = Pin()
          pin.refdes = refdes
          pin.number = pinno
          pin.netName = net.name
          cp.addPin(pin)
          #logger.info("Netlist: add pin {}".format(pininfo))
        else:
          pin = cp.getPinByNumber(pinno)
          if (pin.netName != net.name):
            pin.netName = net.name
            #logger.info("Netlist: update net for pin {}".format(pininfo))

    for cp in self._dictComponents.values():
      cp.refresh()

  def refresh(self):
    """
    refresh netlist after a change has been done to it.
    """
    if (len(self._dictComponents) == 0):
      msg = "Netlist HAS NO components"
      logger.error(msg)
      raise RuntimeError(msg)
    if (len(self._dictNets) == 0):
      msg = "Netlist HAS NO nets"
      logger.error(msg)
      raise RuntimeError(msg)

    logger.info("{} refresh ... ".format(self.__class__.__name__))
    self._refreshComponentPins()

    # check number of pins (there should be more on components than in nets since some can be not connected)
    lstPinInfo_Comps = []
    lstPinInfo_Nets = []
    for cp in self._dictComponents.values():
      lstPinInfo_Comps += ["{}.{}".format(cp.refdes, t) for t in cp.pinNumbers]
    for net in self._dictNets.values():
      lstPinInfo_Nets += net.pinInfo
    if (len(lstPinInfo_Comps) < len(lstPinInfo_Nets)):
      msg = "Netlist HAS MISMATCHED pins: {} in components, {} in nets".format(
          len(lstPinInfo_Comps), len(lstPinInfo_Nets))
      logger.error(msg)
      raise RuntimeError(msg)

    # TODO: check nets
    #lstNetName_Comps = []
    #lstNetName_Nets = list(self._dictNets.keys())
    #for cp in self._dictComponents.values():
    #  lstNetName_Comps += cp.netNames
    #if(len(lstNetName_Comps) != lstNetName_Nets):
    #  msg = "Netlist HAS MISMATCHED nets: {} in components, {} in nets".format(len(lstNetName_Comps),
    #                                                                           len(lstNetName_Nets))
    #  logger.error(msg)
    #  raise RuntimeError(msg)
    logger.info("{} has {:d} component(s), {:d} net(s)".format(self.__class__.__name__,
                                                               self.nComponents,
                                                               self.nNets))

  def getComponentByRefdes(self, refdes):
    return self._dictComponents[refdes]

  def getComponentsByRefdes(self, regex=".*"):
    """
    :param regex:
    :return: {"refdes": Conponent, ...}
    """
    dictRes = {}
    crgx = re.compile(regex)

    for k in self._dictComponents.keys():
      if (crgx.match(k) is not None):
        dictRes[k] = self._dictComponents[k]

    return dictRes

  def getComponentsByValue(self, value):
    """
    :return: {"refdes": Conponent, ...}
    """
    dictRes = {}

    for refdes, comp in self._dictComponents.items():
      if (comp.value == value):
        dictRes[refdes] = comp

    return dictRes

  def getNetByName(self, name):
    return self._dictNets[name]

  def getAllComponentsByRefdes(self):
    return self._dictComponents

  def getAllNetsByName(self):
    return self._dictNets

  def getXNetName(self, net, regexPwr=None, regexGnd=None):
    """
    :param net:
    :param regexPwr: regular expression for the power net(s)
    :param regexGnd: regular expression for the ground net(s)
    :return: the name of the net (if any) after a series R/C/L/F on the current net.
    """
    if (not isinstance(net, Net)):
      msg = "INCORRECT type: {}".format(type(net))
      logger.error(msg)
      raise RuntimeError(msg)
    if (net.name not in self._dictNets.keys()):
      msg = "Net {} not in Netlist".format(net.name)
      logger.error(msg)
      raise RuntimeError(msg)
    if (regexPwr is not None):
      if (re.match(regexPwr, net.name) is not None):
        return
    if (regexGnd is not None):
      if (re.match(regexGnd, net.name) is not None):
        return

    logger.info("XNet check - {}".format(net))
    #logger.info("pwr: {}; gnd: {}".format(regexPwr, regexGnd))
    if ((net.nPins != 2) and (net.nPins != 3)):
      logger.warning("net TOO COMPLICATED")
      return

    lstRefdesSeries = [t for t in net.refdes if re.match(r"[RLCF][0-9]*", t)]
    #logger.info("lstRefdesSeries: {}".format(lstRefdesSeries))

    if (len(lstRefdesSeries) == 0):
      logger.warning("NO series components found")
      return

    dictRes = {}
    for refdes in lstRefdesSeries:
      cp = self.getComponentByRefdes(refdes)
      if (cp.nPins != 2):
        logger.warning("series component has MORE THAN 2 pins")
        continue
      for pin in cp.getAllPinsByNumber().values():
        if (pin.netName == net.name):
          continue
        if (
            len(lstRefdesSeries) > 1
        ):  # ignore pull-ups, pull-downs only if there are more than 1 series components
          if (regexPwr is not None):
            if (re.match(regexPwr, pin.netName) is not None):
              logger.info("found pull-up: {}".format(refdes))
              continue
          if (regexGnd is not None):
            if (re.match(regexGnd, pin.netName) is not None):
              logger.info("found pull-down: {}".format(refdes))
              continue
        dictRes[refdes] = pin.netName
    #logger.info("xnet name: {}".format(dictRes))

    if (len(dictRes) == 0):
      return
    elif (len(dictRes) == 1):
      tmp = list(dictRes.values())[0]
      logger.info("Xnet name - {}".format(tmp))
      return tmp
    else:
      logger.warning("too many xnets: {}".format(dictRes))

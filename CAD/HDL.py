#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._HDL_base import *
from ._HDL_sch import *

def _parse_lib_file(fPath):
  res = {}

  dirPath = os.path.dirname(fPath)
  logger.info("-- reading '{}'".format(fPath))
  with open(os.path.join(fPath, fPath), "r") as fIn:
    for ln in [t.strip(" \n\r") for t in fIn.readlines()]:
      if ((ln == "") or (ln.startswith("#"))):
        continue

      if (ln[:6].upper() == "DEFINE"):
        tmp = ln.split()
        if (len(tmp) != 3):
          msg = "CANNOT PARSE line: '{}'".format(ln)
          logger.error(msg)
          raise RuntimeError(msg)
        libName = tmp[1]
        libPath = tmp[2].replace("\\", "/")
        if (libPath.startswith(".")):
          libPath = os.path.abspath(os.path.join(dirPath, libPath[2:]))
        res[libName] = Library(libName, os.path.normpath(libPath))
      else:
        msg = "CANNOT PARSE line: '{}'".format(ln)
        logger.error(msg)
        raise RuntimeError(msg)
  logger.info("-- done reading file")
  return res

def getLibraries(fPath):
  """
  returns HDL library folders found in fPath
  :param fPath:
  :return: dictionary of {<lib_name>: Library, ... }
  """
  res = {}

  for f in os.listdir(fPath):
    if ("Copy" in f):
      continue
    if (f.endswith(".lib")):
      tmp = _parse_lib_file(os.path.join(fPath, f))
      res.update(tmp)
      if (len(res) > 0):
        break

  if (len(res) == 1):
    logger.info("found {} library".format(len(res)))
  else:
    logger.info("found {} libraries".format(len(res)))
  return res

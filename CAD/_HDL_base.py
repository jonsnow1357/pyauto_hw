#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.geometry as geo
from ._HDL_sch import *

gridDefault = 50
offsetPinNo = 0
offsetPinName = 50

def _mkLline(x0, y0, x1, y1):
  return "L {} {} {} {} -1 0".format(x0, y0, x1, y1)

def _parse_format_props(lst):
  names = []
  opts = []
  for val in lst:
    if (val.endswith(")")):
      tmp = re.split(r"(.*)\(OPT(.*)\)", val)
      if (len(tmp) < 2):
        #print("DBG", tmp)
        msg = "CANNOT parse format"
        logger.error(msg)
        raise RuntimeError(msg)
      elif (len(tmp) < 4):
        msg = "CANNOT parse OPT"
        logger.warning(msg)
        names.append(tmp[1].strip("'").strip())
        opts.append(None)
      else:
        names.append(tmp[1].replace("'", ""))
        opt = tmp[2][1:].strip().strip("'").strip()
        opt = None if (opt == "") else opt
        opts.append(opt)
    else:
      names.append(val.strip("'").strip())
      opts.append(None)
  return names, opts

class Library(object):

  def __init__(self, libName, libPath):
    #print("DBG", libName, libPath)
    pyauto_base.fs.chkPath_Dir(libPath)
    self.name = libName
    self.path = libPath
    self.nCells = None

  def __str__(self):
    return "{}: {} ({} cells)".format(self.__class__.__name__, self.name,
                                      "-" if self.nCells is None else self.nCells)

  def getCellNames(self):
    """
    :return: list of cell names
    """
    res = []
    self.nCells = 0
    for f in os.listdir(self.path):
      if (f in ("origin", "dummy_root_design")):
        continue
      full_path = os.path.join(self.path, f)
      if (not os.path.isdir(full_path)):
        continue

      res.append(f)
      self.nCells += 1
    return res

  def getCell(self, cellName):
    cell = Cell(cellName, os.path.abspath(os.path.join(self.path, cellName)))
    return cell

class CellPrimitive_Pin(object):

  def __init__(self):
    self.number = ""
    self.name = ""
    self.type = None

  def __str__(self):
    return "{}: {}, {}, {}".format(self.__class__.__name__, self.name, self.number,
                                   self.type)

class CellPrimitive(object):

  def __init__(self, strName):
    self.name = strName
    self._body = []
    self._pins = []
    self._dictPins = {}

  def __str__(self):
    return "{}: '{}'".format(self.__class__.__name__, self.name)

  @property
  def pins(self):
    return self._dictPins

  @property
  def content(self):
    return ["primitive '{}';".format(self.name)
            ] + self._pins + self._body + ["end_primitive;"]

  def addBody(self, strVal):
    self._body.append(strVal)

  def addPin(self, strVal):
    self._pins.append(strVal)

  def getProperty(self, propName):
    for ln in [t.strip() for t in self._body]:
      if (ln.startswith(propName)):
        tmp = re.split(r".*=(.*);", ln)
        if (len(tmp) == 3):
          return tmp[1].strip("'")
        else:
          msg = "CANNOT get primitive '{}' property '{}'".format(self.name, propName)
          logger.error(msg)
          raise RuntimeError(msg)

  def _createPin(self, lstLn):
    if (len(lstLn) == 0):
      return

    pin = CellPrimitive_Pin()
    pin.name = lstLn[0][:-1].replace("'", "")
    _dict_prop = {}
    for ln in lstLn[1:]:
      tmp = re.split("([^=]+)='([^']+)';", ln)
      _dict_prop[tmp[1]] = tmp[2]
    #print("DBG", _dict_prop)
    if ("PIN_NUMBER" in _dict_prop.keys()):
      pin.number = _dict_prop["PIN_NUMBER"].strip("()")
    if ("PIN_TYPE" in _dict_prop.keys()):
      pin.type = CADct.PinTypeConv.dictFromHDL[_dict_prop["PIN_TYPE"]]
    elif ("PINUSE" in _dict_prop.keys()):
      pin.type = CADct.PinTypeConv.dictFromHDL[_dict_prop["PINUSE"]]
    elif ("BIDIRECTIONAL" in _dict_prop.keys()):
      pin.type = CADct.pinTypeIO
    elif ("INPUT_LOAD" in _dict_prop.keys()):
      pin.type = CADct.pinTypeIN
    elif ("OUTPUT_LOAD" in _dict_prop.keys()):
      pin.type = CADct.pinTypeOUT
    self._dictPins[pin.name] = pin
    #print("DBG", pin)

  def parse(self):
    lst = []
    for ln in self._pins[1:-1]:
      if (re.match(r"^ +'[^`]+'(<[0-9]+>)?: *$", ln) is not None):
        self._createPin(lst)
        lst = []
      lst.append(ln.strip())
    self._createPin(lst)

  def updatePins(self, cp):
    logger.info("update primitive '{}': pins".format(self.name))
    self._pins = ["  pin"]
    for pin in cp.getAllPinsByNumber().values():
      ln = "    '{}':".format(pin.number)
      self._pins.append(ln)
      if (cp.nBanks > 1):
        ln = "      PIN_NUMBER='({})';".format(",".join(
            ["0" if (pin.bank != b) else pin.number for b in cp.banks]))
      else:
        ln = "      PIN_NUMBER='({})';".format(pin.number)
      self._pins.append(ln)
      if (pin.type == CADct.pinTypeIN):
        self._pins.append("      INPUT_LOAD='(-0.01,0.01)';")
      elif (pin.type == CADct.pinTypeOUT):
        self._pins.append("      OUTPUT_LOAD='(1.0,-1.0)';")
      elif (pin.type == CADct.pinTypeIO):
        self._pins.append("      BIDIRECTIONAL='TRUE';")
        self._pins.append("      INPUT_LOAD='(-0.01,0.01)';")
        self._pins.append("      OUTPUT_LOAD='(1.0,-1.0)';")
      elif ((pin.type == CADct.pinTypeOpenC) or (pin.type == CADct.pinTypeOpenD)):
        self._pins.append("      OUTPUT_TYPE='(OC,AND)';")
        self._pins.append("      OUTPUT_LOAD='(1.0,*)';")
      elif (pin.type == CADct.pinTypeAnalog):
        self._pins.append("      PIN_TYPE='ANALOG';")
        self._pins.append("      NO_LOAD_CHECK='Both';")
        self._pins.append("      NO_IO_CHECK='Both';")
        self._pins.append("      NO_ASSERT_CHECK='TRUE';")
        self._pins.append("      NO_DIR_CHECK='TRUE';")
        self._pins.append("      ALLOW_CONNECT='TRUE';")
      elif (pin.type == CADct.pinTypePwr):
        self._pins.append("      PINUSE='POWER';")
        self._pins.append("      NO_LOAD_CHECK='Both';")
        self._pins.append("      NO_IO_CHECK='Both';")
        self._pins.append("      NO_ASSERT_CHECK='TRUE';")
        self._pins.append("      NO_DIR_CHECK='TRUE';")
        self._pins.append("      ALLOW_CONNECT='TRUE';")
      elif (pin.type == CADct.pinTypeGnd):
        self._pins.append("      PINUSE='GROUND';")
        self._pins.append("      NO_LOAD_CHECK='Both';")
        self._pins.append("      NO_IO_CHECK='Both';")
        self._pins.append("      NO_ASSERT_CHECK='TRUE';")
        self._pins.append("      NO_DIR_CHECK='TRUE';")
        self._pins.append("      ALLOW_CONNECT='TRUE';")
      else:
        msg = "UNSUPPORTED pin type: '{}'".format(pin.type)
        logger.error(msg)
        raise RuntimeError(msg)
    self._pins.append("  end_pin;")

class CellSymbol_Pin(object):

  def __init__(self):
    self._pin = None
    self._properties = []
    self._geometry = None
    self.name = None
    self.pos = []
    self.length = None
    self.side = None
    self.order = None

  def __str__(self):
    return "{}: {: <16} {: >5} {: >3} ({}); {:d} prop(s)".format(
        self.__class__.__name__, self.name, "-" if (self.side is None) else self.side,
        "-" if (self.order is None) else self.order, self.length, len(self._properties))

  @property
  def content(self):
    return ([self._geometry, self._pin] + self._properties)

  @property
  def Cline(self):
    return self._pin

  @property
  def Lline(self):
    return self._geometry

  def init(self, ln):
    tmp = ln.split()
    if (tmp[0] != "C"):
      msg = "UNEXPECTED pin data: '{}'".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)
    if (self._pin is not None):
      msg = "EXTRA pin data"
      logger.error(msg)
      raise RuntimeError(msg)

    #logger.info(ln)
    self.pos = [int(tmp[1]), int(tmp[2])]
    self.name = tmp[3].replace("\"", "")
    self._pin = ln

  def addProperty(self, ln):
    tmp = ln.split()
    if (tmp[0] != "X"):
      msg = "UNEXPECTED property data: '{}'".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)

    self._properties.append(ln)

  def getAllProperties(self):
    """
    :return: dictionary of {<property_name>: [<property_value>, <rest_of_attributes>], ...}
    """
    res = {}
    for ln in self._properties:
      #print("DBG", ln)
      tmp = ln.split()
      #print("DBG", tmp)
      propname = tmp[1].replace("\"", "")
      propval = tmp[2].replace("\"", "")
      propattr = " ".join(tmp[3:])
      res[propname] = [propval, propattr]
      #print("DBG '{}' '{}' '{}'".format(propname, propval, propattr))
    return res

  def getProperty(self, propName):
    for ln in self._properties:
      ln = ln.split()
      if (ln[1].replace("\"", "") == propName):
        return ln[2:]

  def setProperty(self, propName, lstVal):
    if (len(lstVal) != 14):
      msg = "CANNOT set property '{}', INCORRECT values".format(propName)
      logger.error(msg)
      raise RuntimeError(msg)

    for i in range(0, len(self._properties)):
      tmp = self._properties[i].split()
      if (tmp[1].replace("\"", "") == propName):
        self._properties[i] = " ".join(tmp[:2] + lstVal)

  def hasGeometry(self):
    if (self._geometry is not None):
      return True
    else:
      return False

  def hasPos(self, x, y):
    if ((x == self.pos[0]) and (y == self.pos[1])):
      return True
    return False

  def isGeometryEqual(self, x0, y0, x1, y1):
    xp0, yp0, xp1, yp1 = [int(t) for t in self._geometry.split()[1:5]]
    #print("DBG", xp0, yp0, xp1, yp1)
    #print("DBG", x0, y0, x1, y1)
    if ((x0 == xp0) and (y0 == yp0) and (x1 == xp1) and (y1 == yp1)):
      return True
    elif ((x1 == xp0) and (y1 == yp0) and (x0 == xp1) and (y0 == yp1)):
      return True
    else:
      return False

  def setName(self, pinName):
    lstVal = self.getProperty("PIN_TEXT")
    pin_name = lstVal[0].replace("\"", "")
    if (pin_name == pinName):
      return
    #logger.info("change '{}' name: '{}' -> '{}'".format(pinNo, pin_name, pinName))
    lstVal[0] = "\"{}\"".format(pinName)
    self.setProperty("PIN_TEXT", lstVal)

  def _setPosition(self, x, y):
    tmp = self._pin.split()
    tmp[1] = str(x)
    tmp[2] = str(y)
    self._pin = " ".join(tmp)

  def _setPosition_pinNo(self, x, y):
    tmp = self._pin.split()
    if (self.side == CADct.pinSideTop):
      tmp[4] = str(x)
      tmp[5] = str(y + offsetPinNo)
    elif (self.side == CADct.pinSideBot):
      tmp[4] = str(x)
      tmp[5] = str(y - offsetPinNo)
    elif (self.side == CADct.pinSideLeft):
      tmp[4] = str(x - offsetPinNo)
      tmp[5] = str(y)
    elif (self.side == CADct.pinSideRight):
      tmp[4] = str(x + offsetPinNo)
      tmp[5] = str(y)
    self._pin = " ".join(tmp)

  def _setStyle_pinNo(self):
    tmp = self._pin.split()
    if (self.side == CADct.pinSideTop):
      tmp[9] = "1"
      tmp[10] = "L"
    elif (self.side == CADct.pinSideBot):
      tmp[9] = "1"
      tmp[10] = "R"
    elif (self.side == CADct.pinSideLeft):
      tmp[9] = "0"
      tmp[10] = "R"
    elif (self.side == CADct.pinSideRight):
      tmp[9] = "0"
      tmp[10] = "L"
    self._pin = " ".join(tmp)

  def _setPosition_pinName(self, x, y):
    lstVal = self.getProperty("PIN_TEXT")
    if (self.side == CADct.pinSideTop):
      lstVal[1] = str(x)
      lstVal[2] = str(y - offsetPinName)
    elif (self.side == CADct.pinSideBot):
      lstVal[1] = str(x)
      lstVal[2] = str(y + offsetPinName)
    elif (self.side == CADct.pinSideLeft):
      lstVal[1] = str(x + offsetPinName)
      lstVal[2] = str(y)
    elif (self.side == CADct.pinSideRight):
      lstVal[1] = str(x - offsetPinName)
      lstVal[2] = str(y)
    self.setProperty("PIN_TEXT", lstVal)

  def _setStyle_pinName(self):
    lstVal = self.getProperty("PIN_TEXT")
    if (self.side == CADct.pinSideTop):
      lstVal[3] = "90"
      lstVal[8] = "2"
    elif (self.side == CADct.pinSideBot):
      lstVal[3] = "90"
      lstVal[8] = "0"
    elif (self.side == CADct.pinSideLeft):
      lstVal[3] = "0"
      lstVal[8] = "0"
    elif (self.side == CADct.pinSideRight):
      lstVal[3] = "0"
      lstVal[8] = "2"
    self.setProperty("PIN_TEXT", lstVal)

  def _parse_geometry_L(self):
    tmp = self._geometry.split()
    #print("DBG", tmp)
    x0 = int(tmp[1])
    y0 = int(tmp[2])
    x1 = int(tmp[3])
    y1 = int(tmp[4])

    if ((self.pos[0] == x0) and (self.pos[1] == y0)):
      dx = x0 - x1
      dy = y0 - y1
    elif ((self.pos[0] == x1) and (self.pos[1] == y1)):
      dx = x1 - x0
      dy = y1 - y0
    else:
      msg = "INCORRECT geometry:\n  {}\n  {}".format(self._pin, self._geometry)
      logger.error(msg)
      raise RuntimeError(msg)

    if ((dx > 0) and (dy == 0)):
      self.length = dx
      self.side = CADct.pinSideRight
    elif ((dx < 0) and (dy == 0)):
      self.length = -1 * dx
      self.side = CADct.pinSideLeft
    elif ((dx == 0) and (dy > 0)):
      self.length = dy
      self.side = CADct.pinSideTop
    elif ((dx == 0) and (dy < 0)):
      self.length = -1 * dy
      self.side = CADct.pinSideBot
    else:
      msg = "INCORRECT geometry - pin {} not straight".format(self.name)
      logger.warning(msg)
      #raise RuntimeError(msg)

  def _parse_geometry_A(self):
    tmp = self._geometry.split()
    #print("DBG", tmp)
    xc = int(tmp[1])
    yc = int(tmp[2])
    r = int(tmp[3])

    self.length = 2 * r
    if ((self.pos[0] == (xc - r)) and (self.pos[1] == yc)):
      self.side = CADct.pinSideLeft
    elif ((self.pos[0] == (xc + r)) and (self.pos[1] == yc)):
      self.side = CADct.pinSideRight
    elif ((self.pos[0] == xc) and (self.pos[1] == (yc - r))):
      self.side = CADct.pinSideBot
    elif ((self.pos[0] == xc) and (self.pos[1] == (yc + r))):
      self.side = CADct.pinSideTop
    else:
      msg = "INCORRECT geometry:\n  {}\n  {}".format(self._pin, self._geometry)
      logger.error(msg)
      raise RuntimeError(msg)

  def addGeometry(self, ln):
    #logger.info(ln)
    if (ln.startswith("L ")):
      self._geometry = ln
      self._parse_geometry_L()
    elif (ln.startswith("A ")):
      self._geometry = ln
      self._parse_geometry_A()
    else:
      msg = "UNEXPECTED geometry data: '{}'".format(ln)
      logger.error(msg)
      raise RuntimeError(msg)

  def setGeometry(self, x0, y0, x1, y1):
    #print("DBG", self._geometry)
    self._geometry = _mkLline(x0, y0, x1, y1)
    #print("DBG", self._geometry)

    #print("DBG", self._pin)
    self._setPosition(x0, y0)
    #print("DBG", self._pin)
    self._setPosition_pinNo(x0, y0)
    #print("DBG", self._pin)

    #print("DBG", self._properties)
    self._setPosition_pinName(x0, y0)
    #print("DBG", self._properties)

  def setStyle(self):
    self._setStyle_pinNo()
    self._setStyle_pinName()

class CellSymbol(object):

  def __init__(self, strName):
    self.name = strName
    self._properties = []
    self._geometry = []
    self._dictPins = {}  # {<pin_number>: CellSymbol_Pin, ...}
    self._outline = []  # outline limits [x_min, y_max, x_max, y_min]
    self.grid = gridDefault

  def __str__(self):
    return "{}: '{}' {:d} pin(s), {:d} prop(s)".format(self.__class__.__name__, self.name,
                                                       len(self._dictPins),
                                                       len(self._properties))

  # @property
  # def content(self):
  #   res = self._properties + self._geometry
  #   for pin in self._dictPins.values():
  #     res += pin.content
  #   return res

  @property
  def properties(self):
    return self._properties

  @property
  def outline(self):
    return self._outline

  @property
  def pinNumbers(self):
    return sorted(self._dictPins.keys())

  def getPinByNumber(self, pn):
    """
    :param pn: pin number
    :return: Pin with the above pin number
    """
    return self._dictPins[pn]

  def getAllPinsByNumber(self):
    """
    :return: dictionary of {pin_number: Pin}
    """
    return self._dictPins

  def _parse_properties(self):
    #print("DBG", self._properties)
    pass

  def _parse_pins(self, lstLnPins, lstLnGeo):
    for ln in lstLnPins:
      #logger.info(ln)
      pin_1 = CellSymbol_Pin()
      ln_type = ln[0:2]
      if (ln_type == "C "):
        pin_1.init(ln)
        self._dictPins[pin_1.name] = pin_1
      elif (ln_type == "X "):
        pin_1.addProperty(ln)
    #print("DBG: {} pins".format(len(self._dictPins)))

    for pin in self._dictPins.values():
      #logger.info("search geometry for {} {} {}".format(pin, pin.pos, pin.hasGeometry()))
      for ln in lstLnGeo:
        #logger.info(ln)
        ln_type = ln[0:2]
        if (ln_type == "L "):
          tmp = ln.split()
          x1 = int(tmp[1])
          y1 = int(tmp[2])
          x2 = int(tmp[3])
          y2 = int(tmp[4])
          if (pin.hasPos(x1, y1) or pin.hasPos(x2, y2)):
            #logger.info("add L geometry for {} {} '{}'".format(pin.name, pin.pos, ln))
            pin.addGeometry(ln)
            lstLnGeo.remove(ln)
            continue
        elif (ln_type == "A "):
          tmp = ln.split()
          x = int(tmp[1])
          y = int(tmp[2])
          dx = int(abs(pin.pos[0] - x))
          dy = int(abs(pin.pos[1] - y))
          delta = abs(int(tmp[3]))
          if (((dx == 0) and (dy == delta)) or ((dx == delta) and (dy == 0))):
            #logger.info("add A geometry for {} {} '{}'".format(pin.name, pin.pos, ln))
            pin.addGeometry(ln)
            lstLnGeo.remove(ln)
            continue
      if (not pin.hasGeometry()):
        msg = "NO geometry for pin: {}".format(pin)
        logger.warning(msg)

    #print("DBG", lstLnGeo)
    self._geometry = lstLnGeo
    #logger.info("added {} pin(s)".format(len(self._dictPins)))

  def _parse_geometry(self):
    lst_pts = []
    for ln in self._geometry:
      #print("DBG", ln)
      ln = ln.split(" ")
      ln = [t for t in ln if (t != "")]
      if ((ln[0] in ("L", "M")) and (len(ln) == 7)):
        lst_pts.append(geo.Point2D(float(ln[1]), float(ln[2])))
        lst_pts.append(geo.Point2D(float(ln[3]), float(ln[4])))
      elif ((ln[0] == "A") and (len(ln) == 7)):
        pt = geo.Point2D(float(ln[1]), float(ln[2]))
        r = float(float(ln[3]))
        lst_pts.append(pt + geo.Point2D(r, 0.0))
        lst_pts.append(pt + geo.Point2D(-r, 0.0))
        lst_pts.append(pt + geo.Point2D(0.0, r))
        lst_pts.append(pt + geo.Point2D(0.0, -r))
    [ll, ur] = geo.boundingBox(lst_pts)
    self._outline = [ll.x, ll.y, ur.x, ur.y]

    [dx, dy] = geo.guessGrid(lst_pts)
    if ((dx is None) and (dy is not None)):
      self.grid = int(dy)
    elif ((dx is not None) and (dy is None)):
      self.grid = int(dx)
    elif ((dx is not None) and (dy is not None)):
      self.grid = int(min([dx, dy]))

    for pin in self._dictPins.values():
      if ((pin.side == CADct.pinSideTop) or (pin.side == CADct.pinSideBot)):
        pin.order = (pin.pos[0] - self._outline[0]) / self.grid
      elif ((pin.side == CADct.pinSideLeft) or (pin.side == CADct.pinSideRight)):
        pin.order = (self._outline[1] - pin.pos[1]) / self.grid

  def read(self, fPath):
    prev = None
    lstLnGeo = []
    lstLnPins = []

    logger.info("-- reading symbol '{}'".format(self.name))
    with open(fPath, "r") as fIn:
      for ln in [t.strip("\n\r") for t in fIn.readlines()]:
        if (ln == ""):
          continue
        #logger.info(ln)

        ln_type = ln[0:2]
        prev_type = "" if prev is None else prev[0:2]
        #print("DBG '{}' -> '{}'".format(prev_type, ln_type))
        if (ln_type == "P "):
          self._properties.append(ln)
        elif ((ln_type == "L ") or (ln_type == "M ") or (ln_type == "A ")):
          lstLnGeo.append(ln)
        elif (ln_type == "T "):
          lstLnGeo.append(ln)
        elif ((ln_type == "C ") or (ln_type == "X ")):
          lstLnPins.append(ln)
        elif (prev_type == "T "):
          lstLnGeo.append(ln)
        else:
          msg = "UNKNOWN line: '{}' in file:\n  '{}'".format(ln, fPath)
          logger.error(msg)
          raise RuntimeError(msg)
        prev = ln
    #print("DBG", lstLnGeo)
    #print("DBG", lstLnPins)

    self._parse_properties()
    self._parse_pins(lstLnPins, lstLnGeo)
    self._parse_geometry()

    for pin in self._dictPins.values():
      if (pin.Cline is None):
        logger.warning("pin '{}' has no geometry".format(pin.number))

  def getAllProperties(self):
    """
    :return: dictionary of {<property_name>: [<property_value>, <rest_of_attributes>], ...}
    """
    res = {}
    for ln in self._properties:
      #print("DBG", ln)
      tmp = ln.split("\"")
      #print("DBG", tmp)
      propname = tmp[1]
      propval = tmp[3]
      propattr = tmp[-1].strip()
      res[propname] = [propval, propattr]
      #print("DBG '{}' '{}' '{}'".format(propname, propval, propattr))
    return res

  def getProperty(self, propName):
    for ln in self._properties:
      ln = ln.split()
      if (ln[1].replace("\"", "") == propName):
        return ln[2:]

  def setProperty(self, propName, lstVal):
    if (len(lstVal) != 14):
      msg = "CANNOT set property '{}', INCORRECT values".format(propName)
      logger.error(msg)
      raise RuntimeError(msg)

    for i in range(0, len(self._properties)):
      tmp = self._properties[i].split()
      if (tmp[1].replace("\"", "") == propName):
        self._properties[i] = " ".join(tmp[:2] + lstVal)

  def _mkPinGeometry(self, pinLength, pinSide, pinOrder):
    if (pinSide == CADct.pinSideTop):
      return [(self._outline[0] + pinOrder * self.grid), (self._outline[1] + pinLength),
              (self._outline[0] + pinOrder * self.grid), self._outline[1]]
    elif (pinSide == CADct.pinSideBot):
      return [(self._outline[0] + pinOrder * self.grid), (self._outline[3] - pinLength),
              (self._outline[0] + pinOrder * self.grid), self._outline[3]]
    elif (pinSide == CADct.pinSideLeft):
      return [(self._outline[0] - pinLength), (self._outline[1] - pinOrder * self.grid),
              self._outline[0], (self._outline[1] - pinOrder * self.grid)]
    elif (pinSide == CADct.pinSideRight):
      return [(self._outline[2] + pinLength), (self._outline[1] - pinOrder * self.grid),
              self._outline[2], (self._outline[1] - pinOrder * self.grid)]

  def updatePinNames(self, cp):
    logger.info("update symbol '{}': pin names".format(self.name))
    for pin in self._dictPins.values():
      try:
        new_pin = cp.getPinByNumber(pin.name)
        pin.setName(new_pin.name)
      except KeyError:
        logger.warning("UNRECOGNIZED symbol pin name: '{}'".format(pin.name))

  def _updateOutline(self, xmin, ymax, xmax, ymin):
    if ((self._outline[0] == xmin) and (self._outline[1] == ymax)
        and (self._outline[2] == xmax) and (self._outline[3] == ymin)):
      return

    logger.info("update symbol '{}': outline".format(self.name))
    #print("DBG", self._outline, [xmin, ymax, xmax, ymin])
    new_geometry = [
        _mkLline(xmin, ymax, xmin, ymin),
        _mkLline(xmin, ymin, xmax, ymin),
        _mkLline(xmax, ymin, xmax, ymax),
        _mkLline(xmax, ymax, xmin, ymax)
    ]

    setX_outline = {self._outline[0], self._outline[2]}
    setY_outline = {self._outline[1], self._outline[3]}
    for ln in self._geometry:
      ln_type = ln[0:2]
      if (ln_type != "L "):
        new_geometry.append(ln)
        continue

      tmp = ln.split()
      setX = {int(tmp[1]), int(tmp[3])}
      setY = {int(tmp[2]), int(tmp[4])}
      if ((setX <= setX_outline) and (setY <= setY_outline)):
        continue
      else:
        new_geometry.append(ln)
    self._geometry = new_geometry

    lstVal = self.getProperty("CDS_LMAN_SYM_OUTLINE")
    lstVal[0] = "\"{},{},{},{}\"".format(xmin, ymax, xmax, ymin)
    self.setProperty("CDS_LMAN_SYM_OUTLINE", lstVal)
    self._outline = [xmin, ymax, xmax, ymin]

  def _updateText(self, x, y):
    logger.info("update symbol '{}': text".format(self.name))
    cnt = 0
    idx = -1
    for i in range(0, len(self._geometry)):
      ln_type = self._geometry[i][0:2]
      if (ln_type == "T "):
        cnt += 1
        idx = i

    if (cnt == 0):
      logger.warning("NO text")
      return
    elif (cnt > 1):
      logger.warning("MULTIPLE text instances")
      return

    tmp = self._geometry[idx].split(" ")
    tmp[1] = str(x)
    tmp[2] = str(y)
    self._geometry[idx] = " ".join(tmp)
    self._geometry[idx + 1] = self._geometry[idx + 1].upper()

  def _updatePins_Geometry(self, pin, new_pin):

    if ((new_pin.side == "") or (new_pin.order is None)):
      return
    #if((new_pin.side == pin.side) and (new_pin.order == pin.order)):
    #  return

    new_geometry = self._mkPinGeometry(pin.length, new_pin.side, new_pin.order)
    if (not pin.isGeometryEqual(*new_geometry)):
      logger.info("  {} {} -> {} {}".format(pin.side, pin.order, new_pin.side,
                                            new_pin.order))
      pin.side = new_pin.side
      pin.order = new_pin.order
      pin.setGeometry(*new_geometry)

  def _updatePins_Style(self, pin):
    pin.setStyle()

  def updateGeometry(self, cp):
    new_outline = [None, None, None, None]
    for i in range(0, len(self._outline)):
      tmp = math.modf(self._outline[i] / self.grid)
      if (tmp[0] < 0.0):
        new_outline[i] = int((tmp[1] - 1) * self.grid)
      elif (tmp[0] > 0.0):
        new_outline[i] = int((tmp[1] + 1) * self.grid)
      else:
        new_outline[i] = self._outline[i]
    self._updateOutline(*new_outline)
    self._updateText(0, 0)

    logger.info("update symbol '{}': pin geometry".format(self.name))
    for pin in self._dictPins.values():
      try:
        new_pin = cp.getPinByNumber(pin.name)
        self._updatePins_Geometry(pin, new_pin)
      except KeyError:
        # same as updatePinNames()
        pass

    logger.info("update symbol '{}': pin style".format(self.name))
    for pin in self._dictPins.values():
      self._updatePins_Style(pin)

class CellPTF_Format(object):

  def __init__(self):
    self._propsInstance = []
    self._propsInstanceOpt = []
    self._propsInjected = []
    self._propsInjectedOpt = []

  def __str__(self):
    raise NotImplementedError

  @property
  def propsInstance(self):
    return self._propsInstance

  @property
  def propsInjected(self):
    return self._propsInjected

  @property
  def props(self):
    return (self._propsInstance + self._propsInjected)

  @property
  def nProps(self):
    return len(self._propsInstance) + len(self._propsInjected)

  def parse(self, strVal):
    #print("DBG", strVal)
    tmp = strVal[1:-1].strip()
    tmp = tmp.replace("'=", "' =")
    tmp = tmp.replace(")=", ") =")
    tmp = tmp.replace("='", "= '")
    tmp = tmp.replace("\t", " ")
    #print("DBG", tmp)

    strPropsInst, strPropsInj = tmp.split(" = ")
    tmp = [t.strip() for t in strPropsInst.strip().split("|")]
    self._propsInstance, self._propsInstanceOpt = _parse_format_props(tmp)
    tmp = [t.strip() for t in strPropsInj.strip().split("|")]
    self._propsInjected, self._propsInjectedOpt = _parse_format_props(tmp)
    #print("DBG", self._propsInstance)
    #print("DBG", self._propsInstanceOpt)
    #print("DBG", self._propsInjected)
    #print("DBG", self._propsInjectedOpt)

class CellPTF_Entry(object):

  def __init__(self):
    self._propsInstance = []
    self._propsInjected = []
    self._nameSpec = None

  def __str__(self):
    raise NotImplementedError

  @property
  def propsInstance(self):
    return self._propsInstance

  @property
  def propsInjected(self):
    return self._propsInjected

  @property
  def props(self):
    return (self._propsInstance + self._propsInjected)

  @property
  def nProps(self):
    return len(self._propsInstance) + len(self._propsInjected)

  def parse(self, strVal):
    #print("DBG", strVal)
    tmp = strVal.replace("'=", "' =")
    tmp = tmp.replace(")=", ") =")
    tmp = tmp.replace("='", "= '")
    tmp = tmp.replace("\t", " ")
    #print("DBG", tmp)

    if (" : " in tmp):
      tmp, strPropsAdded = tmp.split(" : ")
      if (len(strPropsAdded) > 0):
        logger.warning("PTF has added properties")
    strPropsInst, strPropsInj = tmp.split(" = ")
    self._propsInstance = [t.strip() for t in strPropsInst.strip().split("|")]
    self._propsInjected = [t.strip() for t in strPropsInj.strip().split("|")]
    if (self._propsInstance[-1].endswith(")")):
      tmp = re.split(r"(.*)\((.*)\)", self._propsInstance[-1])
      self._propsInstance[-1] = tmp[1]
      self._nameSpec = tmp[2]
    self._propsInstance = [t.strip(" '") for t in self._propsInstance]
    self._propsInjected = [t.strip(" '") for t in self._propsInjected]
    #print("DBG", self._propsInstance)
    #print("DBG", self._propsInjected)
    #print("DBG", self._nameSpec)

class CellPTF(object):

  def __init__(self, strName):
    self.name = strName
    self._content_fmt = None
    self._content_ent = []
    self.format = CellPTF_Format()
    self.entries = []

  def __str__(self):
    return "{}: {} {:d} entry(es)".format(self.__class__.__name__, self.name,
                                          len(self.entries))

  @property
  def partEntries(self):
    res = []
    for ent in self.entries:
      res.append(dict(zip(self.format.props, ent.props)))
    #print("DBG", res)
    return res

  def addFormat(self, strVal):
    if (self._content_fmt is not None):
      raise RuntimeError
    self._content_fmt = strVal.replace("\t", " ")

  def addEntry(self, strVal):
    self._content_ent.append(strVal.replace("\t", " "))

  def parse(self):
    self.format.parse(self._content_fmt)

    self.entries = []
    for ln in self._content_ent:
      ent = CellPTF_Entry()
      ent.parse(ln)
      if (ent.nProps < self.format.nProps):
        ent._propsInjected += (self.format.nProps - ent.nProps) * [""]
        msg = "ADDING empty properties for PTF '{}'".format(self.name)
        logger.warning(msg)
      elif (ent.nProps > self.format.nProps):
        #logger.error(ent.propsInstance + ent.propsInjected)
        if (len(ent.propsInstance) > self.format.nProps):
          msg = "TOO MANY properties for PTF '{}'".format(self.name)
          logger.error(msg)
          raise RuntimeError(msg)
        ent._propsInjected = ent._propsInjected[0:(self.format.nProps
                                                   - len(ent.propsInstance))]
        msg = "REMOVING properties for PTF '{}'".format(self.name)
        logger.warning(msg)

      self.entries.append(ent)

class Cell(object):

  def __init__(self, cellName, cellPath):
    #print("DBG", cellName, cellPath)
    pyauto_base.fs.chkPath_Dir(cellPath)
    self.name = cellName
    self._path = {"root": cellPath}
    self._primitives = []
    self._symbols = {}
    self._ptfs = []

  def __str__(self):
    return "{}: {} (pri: {}, sym: {}, ptf: {})".format(self.__class__.__name__, self.name,
                                                       self.nPrimitives, self.nSymbols,
                                                       self.nPTFs)

  @property
  def path(self):
    return self._path["root"]

  @property
  def nPrimitives(self):
    return len(self._primitives)

  @property
  def primitives(self):
    return self._primitives

  @property
  def nSymbols(self):
    return len(self._symbols)

  @property
  def symbols(self):
    return self._symbols

  @property
  def nPTFs(self):
    return len(self._ptfs)

  def getPrimitive(self, primitiveId=0):
    return self._primitives[primitiveId]

  def getPTF(self, partId=0):
    return self._ptfs[partId]

  def _read_chips(self):
    self._path["chips"] = os.path.join(self._path["root"], "chips", "chips.prt")
    bExists = pyauto_base.fs.chkPath_File(self._path["chips"], bDie=False)
    if (not bExists):
      return

    logger.info("-- reading prt")
    with open(self._path["chips"], "r") as fIn:
      prm = None
      bPin = False
      bBody = False
      for ln in [t.strip("\n\r") for t in fIn.readlines()]:
        if (ln == ""):
          continue
        #logger.info(ln)

        if (ln.startswith("primitive")):
          tmp = ln.split()
          prm = CellPrimitive(tmp[1][:-1].replace("'", ""))
        elif (ln == "end_primitive;"):
          self._primitives.append(prm)
          bPin = False
          bBody = False
          prm = None
        elif (ln == "  pin"):
          bPin = True
        elif (ln == "  body"):
          bPin = False
          bBody = True

        if (prm is not None) and bPin:
          prm.addPin(ln)
          continue
        if (prm is not None) and bBody:
          prm.addBody(ln)
          continue

    for prm in self.primitives:
      prm.parse()

  def _read_sym(self):
    for f in os.listdir(self._path["root"]):
      if (f.startswith("sym_")):
        self._path[f] = os.path.join(self._path["root"], f, "symbol.css")

    for k in [t for t in self._path.keys() if t.startswith("sym_")]:
      bExists = pyauto_base.fs.chkPath_File(self._path[k], bDie=False)
      if (not bExists):
        continue
      sym = CellSymbol(k)
      sym.read(self._path[k])
      self._symbols[k] = sym

  def _read_ptf(self):
    dirPath = os.path.join(self._path["root"], "part_table")
    bExists = pyauto_base.fs.chkPath_Dir(dirPath, bDie=False)
    if (not bExists):
      return
    self._path["ptf"] = os.path.join(dirPath, "part.ptf")
    bExists = pyauto_base.fs.chkPath_File(self._path["ptf"], bDie=False)
    if (not bExists):
      for f in os.listdir(dirPath):
        if (f.endswith("ptf")):
          self._path["ptf"] = os.path.join(dirPath, f)
          logger.warning("FOUND other file: {}".format(self._path["ptf"]))
          break
      bExists = pyauto_base.fs.chkPath_File(self._path["ptf"], bDie=False)
      if (not bExists):
        return

    logger.info("-- reading ptf")
    # add encoding to read library files, this breaks py2 compatibility
    with open(self._path["ptf"], "r", encoding="iso-8859-1") as fIn:
      ptf = None
      cnt_sep = 0
      for ln in [t.strip("\n\r ") for t in fIn.readlines()]:
        if (ln == ""):
          continue
        #logger.info(ln)

        if (ln.startswith("PART")):
          tmp = ln.split()
          ptf = CellPTF(tmp[-1].replace("'", ""))
          continue
        if (ln.startswith("{====")):
          cnt_sep += 1
          continue
        if (ln == "END_PART"):
          ptf.parse()
          self._ptfs.append(ptf)
          ptf = None
          cnt_sep = 0
          continue
        tmp = ln.strip()
        if (tmp.startswith(":") and (cnt_sep == 1)):
          ptf.addFormat(tmp)
        if (tmp.startswith("'") and (len(tmp) > 2) and (cnt_sep == 2)):
          ptf.addEntry(tmp)

  def read(self):
    logger.info("-- reading cell '{}'".format(self.name))
    self._read_chips()
    self._read_sym()
    self._read_ptf()
    logger.info("-- done reading cell")

  def _write_chips(self):
    pyauto_base.fs.chkPath_File(self._path["chips"])

    with open(self._path["chips"], "w") as fOut:
      fOut.write("FILE_TYPE=LIBRARY_PARTS;\n")
      for prm in self._primitives:
        for ln in prm.content:
          fOut.write(ln + "\n")
      fOut.write("\n")
      fOut.write("END.\n")
    logger.info("{} written".format(self._path["chips"]))

  def _write_sym(self):
    for k, sym in self._symbols.items():
      with open(self._path[k], "w") as fOut:
        for ln in sym.content:
          fOut.write(ln + "\n")
      logger.info("{} written".format(self._path[k]))

  def _write_ptf(self):
    raise NotImplementedError

  def write(self):
    self._write_chips()
    self._write_sym()
    #self._write_ptf()

  def updatePins(self, cp):
    self._primitives[0].updatePins(cp)

    for k, sym in self._symbols.items():
      sym.updatePinNames(cp)
      sym.updateGeometry(cp)

  def _getComponent_PrimitivePin(self, lstLn):
    """
    :param lstLn:
    :return: [pinNo, pinName, pinType, pinBank]
    """
    if (len(lstLn) < 3):
      return []

    #print("DBG", lstLn)
    pinInfo = [None, None, None, None]
    tmp = lstLn[0].strip(":'")
    if (tmp.startswith("-")):
      # looks like Cadence does this stupid thing where * is coded as - at the beginning of the name
      pinInfo[1] = tmp[1:] + "*"
    else:
      pinInfo[1] = tmp

    if (not lstLn[1].startswith("PIN_NUMBER=")):
      msg = "CANNOT parse: chips.prt '{}'".format(lstLn[0])
      logger.error(msg)
      raise RuntimeError(msg)
    tmp = lstLn[1][13:-3].split(",")
    if (len(tmp) == 1):
      pinInfo[0] = tmp[0]
      pinInfo[3] = ""
    else:
      bank = [(t != "0") for t in tmp]
      if (bank.count(True) != 1):
        msg = "CANNOT parse: chips.prt '{}'".format(lstLn[0])
        logger.error(msg)
        raise RuntimeError(msg)
      idx = bank.index(True)
      pinInfo[0] = tmp[idx]
      pinInfo[3] = str(idx + 1)

    for ln in lstLn[2:]:
      if (ln == "PIN_TYPE='ANALOG';"):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypeAnalog
      elif (ln == "PINUSE='POWER';"):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypePwr
      elif (ln == "PINUSE='GROUND';"):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypeGnd
      elif (ln == "PINUSE='NC';"):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypeAnalog
      elif (ln == "PINUSE='UNSPEC';"):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypeAnalog
      elif (ln == "BIDIRECTIONAL='TRUE';"):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypeIO
      elif (ln.startswith("INPUT_LOAD=")):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypeIN
      elif (ln.startswith("OUTPUT_LOAD=")):
        if (pinInfo[2] is None):
          pinInfo[2] = CADct.pinTypeOUT

    #logger.info("_getComponent_PrimitivePin: {}".format(pinInfo))
    if (None in pinInfo):
      msg = "CANNOT parse: chips.prt '{}'".format(lstLn[0])
      logger.error(msg)
      raise RuntimeError(msg)
    return pinInfo

  def getComponent(self, primitiveId=0):
    cp = CADbase.Component()

    prm = self._primitives[primitiveId]
    cp.value = prm.name
    cp.refdes = prm.getProperty("PHYS_DES_PREFIX") + "?"

    lstLn = []
    for ln in [t.strip() for t in prm._pins]:
      if (re.match(r"\'.*\':", ln) is not None):
        pinInfo = self._getComponent_PrimitivePin(lstLn)
        if (len(pinInfo) != 0):
          cp.addPin(
              CADbase.mkPin(strRefdes=cp.refdes,
                            strNo=pinInfo[0],
                            strName=pinInfo[1],
                            strType=pinInfo[2],
                            strBank=pinInfo[3]))
        lstLn = [ln]
      else:
        lstLn.append(ln)
    if (len(lstLn) > 3):
      pinInfo = self._getComponent_PrimitivePin(lstLn)
      cp.addPin(
          CADbase.mkPin(strRefdes=cp.refdes,
                        strNo=pinInfo[0],
                        strName=pinInfo[1],
                        strType=pinInfo[2],
                        strBank=pinInfo[3]))

    # update side from symbols
    for k, sym in self._symbols.items():
      for pn in sym.pinNumbers:
        #print("DBG", sym.getPinByNumber(pn))
        lstPin = cp.getPinsByName(pn)
        if (len(lstPin) != 1):
          msg = "FOUND INCORRECT pins '{}': {}".format(pn, len(lstPin))
          logger.error(msg)
          raise RuntimeError(msg)
        #print("DBG", lstPin[0])
        lstPin[0].side = sym.getPinByNumber(pn).side

    cp.refresh()
    return cp

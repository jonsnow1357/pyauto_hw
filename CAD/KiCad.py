#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import sexpdata
import packaging.version

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase

def _get_val(sym):
  if (packaging.version.parse(sexpdata.__version__) < packaging.version.parse("0.0.4")):
    return sym.value()
  else:
    return str(sym)

def _readNetlist_Component(lst):
  if (_get_val(lst[0]) != "comp"):
    return

  refdes = None
  value = None
  libsrc = [None, None]
  for comp_info in lst[1:]:
    #print("DBG", comp_info)
    if (_get_val(comp_info[0]) == "ref"):
      refdes = comp_info[1]
    if (_get_val(comp_info[0]) == "value"):
      value = comp_info[1]
    if (_get_val(comp_info[0]) == "libsource"):
      for t in comp_info[1:]:
        if (_get_val(t[0]) == "lib"):
          libsrc[0] = t[1]
        if (_get_val(t[0]) == "part"):
          libsrc[1] = t[1]

  libsrc = "/".join(libsrc)
  #print("DBG", refdes, value, libsrc)
  cp = CADbase.Component(refdes)
  cp.value = value
  cp.paramsCAD["libsrc"] = libsrc
  #logger.info(cp)

  if (cp.refdes == ""):
    logger.warning("netlist has component without refdes")
    return
  return cp

def _updNetlist_Component_Pins(ntl, lst):
  libsrc = [None, None]
  footprint = None
  lst_pins = []
  for part_info in lst[1:]:
    #print("DBG", part_info)
    if (_get_val(part_info[0]) == "lib"):
      libsrc[0] = part_info[1]
    if (_get_val(part_info[0]) == "part"):
      libsrc[1] = part_info[1]
    if (_get_val(part_info[0]) == "footprints"):
      footprint = part_info[1][1]
    if (_get_val(part_info[0]) == "pins"):
      pin_no = None
      pin_name = None
      pin_type = None
      for pin_info in part_info[1:]:
        if (_get_val(pin_info[0]) != "pin"):
          continue
        for t in pin_info[1:]:
          if (_get_val(t[0]) == "num"):
            pin_no = t[1]
          if (_get_val(t[0]) == "name"):
            pin_name = t[1]
          if (_get_val(t[0]) == "type"):
            pin_type = t[1]
        lst_pins.append([pin_no, pin_name, pin_type])

  libsrc = "/".join(libsrc)
  #print("DBG", libsrc, footprint)
  for cp in ntl.getAllComponentsByRefdes().values():
    if (cp.paramsCAD["libsrc"] == libsrc):
      cp.footprint = footprint
      for pin_info in lst_pins:
        pin = CADbase.Pin("{}.{}".format(cp.refdes, pin_info[0]))
        pin.name = pin_info[1]
        pin.setType_KiCAD(pin_info[2])
        cp.addPin(pin)
      #logger.info(cp)

def _readNetlist_Net(lst):
  net_id = None
  net_name = None
  net_pins = []
  for net_info in lst[1:]:
    #print("DBG", net_info)
    if (_get_val(net_info[0]) == "code"):
      net_id = net_info[1]
    if (_get_val(net_info[0]) == "name"):
      net_name = net_info[1]
    if (_get_val(net_info[0]) == "node"):
      pin_info = [None, None]
      for t in net_info[1:]:
        if (_get_val(t[0]) == "ref"):
          pin_info[0] = t[1]
        if (_get_val(t[0]) == "pin"):
          pin_info[1] = t[1]
      net_pins.append(".".join(pin_info))

  #print("DBG", net_id, net_name, net_pins)
  if (len(net_pins) == 1):
    return None
  net = CADbase.Net(net_name)
  net.id = net_id
  for pi in net_pins:
    net.addPinInfo(pi)
  return net

def readNetlist(fPath):
  if (not fPath.lower().endswith(".net")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  pyauto_base.fs.chkPath_File(fPath)

  logger.info("-- reading KiCad netlist file: {}".format(fPath))
  ntl = CADbase.Netlist()
  with open(fPath, "r") as fIn:
    data = sexpdata.load(fIn)
  for item in data:
    if (isinstance(item, list)):
      if (_get_val(item[0]) == "components"):
        for comp in item[1:]:
          cp = _readNetlist_Component(comp)
          if (cp is not None):
            ntl.addComponent(cp)
      if (_get_val(item[0]) == "libparts"):
        for comp in item[1:]:
          _updNetlist_Component_Pins(ntl, comp)
      if (_get_val(item[0]) == "nets"):
        for net in item[1:]:
          net = _readNetlist_Net(net)
          if (net is not None):
            ntl.addNet(net)

  ntl.refresh()
  if ((ntl.nComponents == 0) or (ntl.nNets == 0)):
    msg = "INCORRECT netlist (no nets or components)"
    logger.error(msg)
    raise RuntimeError(msg)
  logger.info("-- done reading netlist")
  return ntl

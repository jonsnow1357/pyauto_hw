#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")
import pyauto_base.fs
import pyauto_base.misc
import pyauto_hw.CAD.constants as CADct
import pyauto_hw.CAD.base as CADbase

def readPin(fPath):
  """
  Reads a Altera .pin file from Quartus.
  :param fPath:
  :return: Component()
  """
  pyauto_base.fs.chkPath_File(fPath)

  hdrRow = [
      "Pin Name/Usage", "Location", "Dir.", "I/O Standard", "Voltage", "I/O Bank",
      "User Assignment"
  ]
  hdrOpt = ["PCB Net"]
  cp = CADbase.Component("U?")

  logger.info("-- reading Altera pin file: {}".format(fPath))
  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn, delimiter=":", quotechar="\"")
    bHdrFound = False

    for row in csvIn:
      #row = [t for t in row if t != ""]  # strip empty list enties
      row = [t.strip(" \n\r") for t in row]  # cleanup list entries
      if (len(row) == 0):  # ignore empty lines
        continue
      #logger.info(row)
      if (len(row) == 1):  # ignore short lines and print CAD version
        if (row[0].startswith("--")):
          continue
        if (row[0].startswith("Quartus")):
          logger.info(row[0])
          continue
      if (len(row) < 6) or (len(row) > 9):  # ignore short lines and print chip assignment
        if (row[0].startswith("--")):
          continue
        if (row[0].startswith("CHIP")):
          cp.value = row[-1]
          logger.info(": ".join(row))
          continue
      if (row[0] == hdrRow[0]):
        dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow, hdrOpt)
        #logger.info(dictIdx)
        bHdrFound = True
        continue

      if (bHdrFound):
        #logger.info(row)
        pinName = row[dictIdx[hdrRow[0]]]
        pinNo = row[dictIdx[hdrRow[1]]]
        pinType = row[dictIdx[hdrRow[2]]]
        pinSig = row[dictIdx[hdrRow[3]]]
        pinVolt = row[dictIdx[hdrRow[4]]]
        pinBank = row[dictIdx[hdrRow[5]]]
        p = CADbase.Pin("U?.{}".format(pinNo))
        p.name = pinName
        if (pinType != ""):
          p.setType_Altera(pinType)
        else:
          p.setType_Altera(CADct.pinTypeAnalog)
        p.signaling = pinSig
        p.voltage = pinVolt
        p.bank = pinBank
        #if(hdrRow[6] in dictIdx.keys()):
        #  p.nameAlt["alteraUser"] = row[dictIdx[hdrRow[6]]]
        if (hdrOpt[0] in dictIdx.keys()):
          p.netName = row[dictIdx[hdrOpt[0]]]
        #logger.info(p)
        cp.addPin(p)

  cp.refresh()
  logger.info("read {:d} line(s); {:d} pin(s)".format(csvIn.line_num, cp.nPins))
  logger.info("-- done reading file")
  return cp

def _readPinOut_PinType(p):
  regexPwr = "(^VCC.*$|^VREF.*$)"
  regexGnd = "^GND.*$"
  regexIn = "(^CLK.*$|^REFCLK.*$|^GXB_RX.*$|^MSEL.*$|nCONFIG|nCE|TCK|TMS|TDI)"
  regexOut = "(^GXB_TX.*$|nSTATUS|nCSO|CONF_DONE|CRC_ERROR|TDO)"
  regexIO = "(^IO.*$|^.*DATA[0-9].*$|DCLK)"
  regexAnalog = "(^NC.*$|^DNU.*$|^RREF.*$)"

  if (re.match(regexPwr, p.name) is not None):
    p.type = CADct.pinTypePwr
  elif (re.match(regexGnd, p.name) is not None):
    p.type = CADct.pinTypeGnd
  elif (re.match(regexIn, p.name) is not None):
    p.type = CADct.pinTypeIN
  elif (re.match(regexOut, p.name) is not None):
    p.type = CADct.pinTypeOUT
  elif (re.match(regexIO, p.name) is not None):
    p.type = CADct.pinTypeIO
  elif (re.match(regexAnalog, p.name) is not None):
    p.type = CADct.pinTypeAnalog
  else:
    logger.warning("UNSUPPORTED pin name: {}".format(p.name))
    p.type = CADct.pinTypeAnalog
  #logger.info(p)

def readPinOut(fPath, pkgId=None):
  """
  Reads a Altera .txt file with pin-out information.
  :param fPath:
  :param pkgId: package ID
  :return: Component()
  """
  pyauto_base.fs.chkPath_File(fPath)

  hdrRow = [
      "Bank Number",
      "Pin Name/Function",
      "Optional Function(s)",
      "Configuration Function",
  ]
  hdrOpt = [
      pkgId,
      "Dedicated Tx/Rx Channel",
      "Emulated LVDS Output Channel",
      "DQS for X8/X9",
      "DQS for X16/X18",
      "DQS for X32/X36",
      "DDR3/DDR2 hard memory PHY",
      "LPDDR2 hard memory PHY",
      "RLDRAMII hard memory PHY",
      "QDRII hard memory PHY",
  ]
  cp = CADbase.Component("U?")

  logger.info("-- reading Altera pin-out file: {}".format(fPath))
  with open(fPath, "r") as fIn:
    csvIn = csv.reader(fIn, delimiter="\t", quotechar="\"")
    bHdrFound = False

    for row in csvIn:
      #row = [t for t in row if t != ""]  # strip empty list enties
      row = [t.strip(" \n\r") for t in row]  # cleanup list entries
      if (len(row) == 0):  # ignore empty lines
        bHdrFound = False
        continue
      #logger.info(row)
      if (row[0].startswith("Pin Information")):
        logger.info(row[0])
        tmp = row[0].split(" ")
        cp.value = tmp[tmp.index("Device") - 1]
        bHdrFound = False
        continue
      if (row[0].startswith("Notes")):
        bHdrFound = False
        continue
      if (row[0] == hdrRow[0]):
        logger.warning("\nSince Altera has no consistent naming convention "
                       "you might need to change this script and/or the pin-out file\n")

        # try to determine available packages
        if (hdrOpt[0] is not None):
          logger.info("using package: {}".format(hdrOpt[0]))
          dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow, hdrOpt)
        else:
          logger.warning("no package specified")
          dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow, hdrOpt)
          validIdx = [t for t in dictIdx.values() if t != -1]
          pkgLst = []
          for i in range(0, len(row)):
            if (i not in validIdx):
              if (row[i].startswith("VREF") or row[i].startswith("DQS")
                  or row[i].startswith("Pad")):
                continue
              if (row[i] != ""):
                pkgLst.append(row[i])
          if (len(pkgLst) == 1):
            hdrOpt[0] = pkgLst[0]
            logger.info("using single package: {}".format(hdrOpt[0]))
            dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow, hdrOpt)
          else:
            msg = "MUST specify one of the available packages: {}".format(pkgLst)
            logger.error(msg)
            raise RuntimeError(msg)
        #logger.info(dictIdx)

        cp.value = (cp.value + "-" + hdrOpt[0])
        #try to allow for Altera naming their column headers in various ways
        if (hdrOpt[3] not in dictIdx.keys()):
          logger.warning("DQS info not found (1)")
          hdrOpt[3] += (" in {}".format(hdrOpt[0]))
          hdrOpt[4] += (" in {}".format(hdrOpt[0]))
          hdrOpt[5] += (" in {}".format(hdrOpt[0]))
          dictIdx = pyauto_base.misc.getListIndexByName(row, hdrRow, hdrOpt)
        if (hdrOpt[3] not in dictIdx.keys()):
          logger.warning("DQS info not found (2)")
        #logger.info(dictIdx)
        if (hdrOpt[0] in dictIdx.keys()):
          bHdrFound = True
        continue

      if (bHdrFound):
        #logger.info(row)
        if (len(row) <= dictIdx[hdrOpt[0]]):
          continue
        pin_no = row[dictIdx[hdrOpt[0]]]
        if (pin_no == ""):
          continue
        p = CADbase.Pin("U?.{}".format(pin_no))
        p.name = row[dictIdx[hdrRow[1]]]
        p.bank = row[dictIdx[hdrRow[0]]]
        if (row[dictIdx[hdrRow[3]]] != ""):
          p.nameAlt["cfg"] = row[dictIdx[hdrRow[3]]]
        if (row[dictIdx[hdrRow[2]]] != ""):
          tmp = row[dictIdx[hdrRow[2]]]
          if (("cfg" not in p.nameAlt.keys()) or (p.nameAlt["cfg"] != tmp)):
            p.nameAlt["opt"] = tmp

        if ((hdrOpt[1] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[1]])):
          if (row[dictIdx[hdrOpt[1]]] != ""):
            p.nameAlt["alteraTXRXCh"] = row[dictIdx[hdrOpt[1]]]
        if ((hdrOpt[2] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[3]])):
          if (row[dictIdx[hdrOpt[2]]] != ""):
            p.nameAlt["alteraLVDS"] = row[dictIdx[hdrOpt[2]]]
        if ((hdrOpt[3] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[3]])):
          if (row[dictIdx[hdrOpt[3]]] != ""):
            p.nameAlt["alteraDQS8"] = row[dictIdx[hdrOpt[3]]]
        if ((hdrOpt[4] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[4]])):
          if (row[dictIdx[hdrOpt[4]]] != ""):
            p.nameAlt["alteraDQS16"] = row[dictIdx[hdrOpt[4]]]
        if ((hdrOpt[5] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[5]])):
          if (row[dictIdx[hdrOpt[5]]] != ""):
            p.nameAlt["alteraDQS32"] = row[dictIdx[hdrOpt[5]]]
        if ((hdrOpt[6] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[6]])):
          if (row[dictIdx[hdrOpt[6]]] != ""):
            p.nameAlt["alteraHDDR"] = row[dictIdx[hdrOpt[6]]]
        if ((hdrOpt[7] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[7]])):
          if (row[dictIdx[hdrOpt[7]]] != ""):
            p.nameAlt["alteraHLPDDR"] = row[dictIdx[hdrOpt[7]]]
        if ((hdrOpt[8] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[8]])):
          if (row[dictIdx[hdrOpt[8]]] != ""):
            p.nameAlt["alteraHRLDRAM"] = row[dictIdx[hdrOpt[8]]]
        if ((hdrOpt[9] in dictIdx.keys()) and (len(row) > dictIdx[hdrOpt[9]])):
          if (row[dictIdx[hdrOpt[9]]] != ""):
            p.nameAlt["alteraHQDR"] = row[dictIdx[hdrOpt[9]]]

        _readPinOut_PinType(p)
        #logger.info(p)
        #logger.info([p.name, p.nameAlt.items()])
        #logger.info(",".join([p.getName_AlteraPin(), p.number, p.type, p.signaling]))
        cp.addPin(p)

  cp.refresh()
  logger.info("read {:d} line(s); {:d} pin(s)".format(csvIn.line_num, cp.nPins))
  logger.info("-- done reading file")
  return cp

def writeQsf(fPath, cp):
  """
  Writes a .qsf constraints text file that can be used by Quartus.
  :param cp:
  :param fPath:
  """
  if ((fPath is None) or (len(fPath) == 0)):
    msg = "CANNOT write file with no path"
    logger.error(msg)
    raise RuntimeError(msg)
  if (not fPath.endswith(".qsf")):
    msg = "INCORRECT file name: {}".format(fPath)
    logger.error(msg)
    raise RuntimeError(msg)
  cp.refresh()

  with open(fPath, "w") as fOut:
    for pn in cp.pinNumbers:
      p = cp.getPinByNumber(pn)
      #logger.info(p)
      fOut.write(p.getInfo_AlteraTcl())
      fOut.write("\n")
  logger.info("{} written".format(fPath))

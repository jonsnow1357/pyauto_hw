#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.misc
import pyauto_base.bin

class MDIOFrame(object):

  def __init__(self, clause):
    if (clause == 45):
      self.start = "00"
    elif (clause == 22):
      self.start = "01"
    else:
      logger.warning("MDIO frame assumes clause 22")
      self.start = "01"

    self.opcode = "00"
    self.phyaddr = "00000"
    self.dev_reg = "00000"
    self.data = "0x0000"
    self.ta = "00"

  def setValue(self, opcode, phyaddr, dev, data):
    if (not pyauto_base.bin.isHexString(phyaddr, 1)):
      logger.error("phyaddr is not a hex number: {}".format(phyaddr))
      return
    if (not pyauto_base.bin.isHexString(dev, 1)):
      logger.error("dev/reg is not a hex number: {}".format(dev))
      return
    if (not pyauto_base.bin.isHexString(data, 2)):
      logger.error("data is not a hex number: {}".format(data))
      return

    if (self.start == "00"):
      if (opcode == "a"):
        self.opcode = "00"
        self.ta = "10"
      elif (opcode == "w"):
        self.opcode = "01"
        self.ta = "10"
      elif (opcode == "r"):
        self.opcode = "11"
        self.ta = "11"
      elif (opcode == "ra"):
        self.opcode = "10"
        self.ta = "11"
      else:
        msg = "INCORRECT opcode '{}' for clause 45".format(opcode)
        logger.error(msg)
        raise RuntimeError(msg)
    else:
      if (opcode == "w"):
        self.opcode = "01"
        self.ta = "10"
      elif (opcode == "r"):
        self.opcode = "10"
        self.ta = "11"
      else:
        msg = "INCORRECT opcode '{}' for clause 22".format(opcode)
        logger.error(msg)
        raise RuntimeError(msg)

    self.phyaddr = "b{:0>5b}".format(phyaddr)
    self.dev_reg = "b{:0>5b}".format(dev)
    self.data = data

  def getValue(self):
    """
    returns the hex value of the frame
    """
    res = (self.start + self.opcode + self.phyaddr + self.dev_reg + self.ta)
    logger.debug("frame (MSb): {}".format(res))
    res = hex(int(res, 2))
    logger.debug("frame (MSb): {}".format(res))
    res = "0x{:0>4}{:0>4}".format(res[2:], self.data[2:].upper())
    logger.debug("frame: {}".format(res))

    return res

class PHYAddr(object):
  # Basic Register Set (802.3-2008 22.2.4)
  BCR = "0x00"  # Basic Mode Control Register (MII)
  BSR = "0x01"  # Basic Mode Status Register (MII)
  ESR = "0x0F"  # Extended Status Register (GMII)

  # Extended Register Set (802.3-2008 22.2.4)
  PHYIDR1 = "0x02"  # PHY Identifier Register 1
  PHYIDR2 = "0x03"  # PHY Identifier Register 2
  ANAR = "0x04"  # Auto-Negotiation Advertisement Register
  ANLPBPR = "0x05"  # Auto-Negotiation Link Partner Base Page Register
  ANER = "0x06"  # Auto-Negotiation Expansion Register
  ANNPTR = "0x07"  # Auto-Negotiation Next Page TX Register
  ANLPNPR = "0x08"  # Auto-Negotiation Link Partner Next Page Register

class PHY(object):

  def __init__(self):
    self.page0_data = 32 * ["0x0000"]
    self.infoPage0 = {}

  def clear(self):
    self.page0_data = 32 * ["0x0000"]
    self.infoPage0 = {}

  def _parse_Page0(self):
    self.infoPage0["BCR"] = self.page0_data[0]
    self.infoPage0["BSR"] = self.page0_data[1]
    self.infoPage0["ESR"] = self.page0_data[15]

    self.infoPage0["PHYID"] = self.page0_data[2:4]
    self.infoPage0["ANAR"] = self.page0_data[4]
    self.infoPage0["ANLPBPR"] = self.page0_data[5]
    self.infoPage0["ANER"] = self.page0_data[6]
    self.infoPage0["ANNPTR"] = self.page0_data[7]
    self.infoPage0["ANLPNPR"] = self.page0_data[8]

  def _parse_Page0_base(self):
    lstParse = {"Operation": []}
    if (pyauto_base.bin.getBit(15, self.infoPage0["BCR"]) == 1):
      lstParse["Operation"].append("RESET")
    if (pyauto_base.bin.getBit(14, self.infoPage0["BCR"]) == 1):
      lstParse["Operation"].append("LOOPBACK")
    if (pyauto_base.bin.getBit(12, self.infoPage0["BCR"]) == 1):
      lstParse["AutoNeg"] = "Enabled"
    else:
      lstParse["AutoNeg"] = "Disabled"
    if (pyauto_base.bin.getBit(11, self.infoPage0["BCR"]) == 1):
      lstParse["Operation"].append("POWER DOWN")
    if (pyauto_base.bin.getBit(10, self.infoPage0["BCR"]) == 1):
      lstParse["Operation"].append("ISOLATE")
    if (pyauto_base.bin.getBit(8, self.infoPage0["BCR"]) == 1):
      lstParse["Duplex"] = "Full"
    else:
      lstParse["Duplex"] = "Half"
    if (pyauto_base.bin.getBit(7, self.infoPage0["BCR"]) == 1):
      lstParse["COL Test"] = True
    if (pyauto_base.bin.getBit(5, self.infoPage0["BCR"]) == 1):
      lstParse["Operation"].append("UniDir")
    tmp = "0x{:X}".format(int(self.infoPage0["BCR"]) & 0x2040)
    if (tmp == "0x2040"):
      lstParse["Speed"] = "Reserved"
    elif (tmp == "0x40"):
      lstParse["Speed"] = "1000Mbps"
    elif (tmp == "0x2000"):
      lstParse["Speed"] = "100Mbps"
    elif (tmp == "0x00"):
      lstParse["Speed"] = "10Mbps"
    if (len(lstParse) > 0):
      self.infoPage0["BCR"] = lstParse

    lstParse = {"Capability": []}
    if (pyauto_base.bin.getBit(15, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("100BASE-T4")
    if (pyauto_base.bin.getBit(14, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("100BASE-X_FD")
    if (pyauto_base.bin.getBit(13, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("100BASE-X_HD")
    if (pyauto_base.bin.getBit(12, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("10BASE-T_FD")
    if (pyauto_base.bin.getBit(11, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("10BASE-T_HD")
    if (pyauto_base.bin.getBit(10, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("100BASE-T2_HD")
    if (pyauto_base.bin.getBit(9, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("100BASE-T2_FD")
    if (pyauto_base.bin.getBit(8, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("Extended Status")
    if (pyauto_base.bin.getBit(7, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("UniDir")
    if (pyauto_base.bin.getBit(6, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("MF Preamble Suppresion")
    if (pyauto_base.bin.getBit(3, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("AutoNeg")
    if (pyauto_base.bin.getBit(0, self.infoPage0["BSR"]) == 1):
      lstParse["Capability"].append("Extended Register Set")

    if (pyauto_base.bin.getBit(5, self.infoPage0["BSR"]) == 1):
      lstParse["AutoNeg"] = "Complete"
    else:
      lstParse["AutoNeg"] = "NOT Complete"
    if (pyauto_base.bin.getBit(4, self.infoPage0["BSR"]) == 1):
      lstParse["Remote Fault"] = True
    else:
      lstParse["Remote Fault"] = False
    if (pyauto_base.bin.getBit(2, self.infoPage0["BSR"]) == 1):
      lstParse["Link"] = True
    else:
      lstParse["Link"] = False
    if (pyauto_base.bin.getBit(1, self.infoPage0["BSR"]) == 1):
      lstParse["Jabber"] = True
    if (len(lstParse) > 0):
      self.infoPage0["BSR"] = lstParse

    lstParse = {"Capability": []}
    if (pyauto_base.bin.getBit(15, self.infoPage0["ESR"]) == 1):
      lstParse["Capability"].append("1000BASE-X_FD")
    if (pyauto_base.bin.getBit(14, self.infoPage0["ESR"]) == 1):
      lstParse["Capability"].append("1000BASE-X_HD")
    if (pyauto_base.bin.getBit(13, self.infoPage0["ESR"]) == 1):
      lstParse["Capability"].append("1000BASE-T_FD")
    if (pyauto_base.bin.getBit(12, self.infoPage0["ESR"]) == 1):
      lstParse["Capability"].append("1000BASE-T_HD")
    if (len(lstParse) > 0):
      self.infoPage0["ESR"] = lstParse

    self.infoPage0["PHYID"] = "0x{}{}".format(self.infoPage0["PHYID"][0][2:],
                                              self.infoPage0["PHYID"][1][2:])

  def _parse_Page0_AutoNeg(self):
    lstParse = {"Ability": []}
    if (pyauto_base.bin.getBit(15, self.infoPage0["ANAR"]) == 1):
      lstParse["Next Page"] = True
    else:
      lstParse["Next Page"] = False
    if (pyauto_base.bin.getBit(13, self.infoPage0["ANAR"]) == 1):
      lstParse["Remote Fault"] = True
    else:
      lstParse["Remote Fault"] = False
    if (pyauto_base.bin.getBit(12, self.infoPage0["ANAR"]) == 1):
      lstParse["Extended Next Page"] = True
    else:
      lstParse["Extended Next Page"] = False
    if (pyauto_base.bin.getBit(11, self.infoPage0["ANAR"]) == 1):
      lstParse["Ability"].append("Asymmetric PAUSE")
    if (pyauto_base.bin.getBit(10, self.infoPage0["ANAR"]) == 1):
      lstParse["Ability"].append("PAUSE")
    if (pyauto_base.bin.getBit(9, self.infoPage0["ANAR"]) == 1):
      lstParse["Ability"].append("100BASE-T4")
    if (pyauto_base.bin.getBit(8, self.infoPage0["ANAR"]) == 1):
      lstParse["Ability"].append("100BASE-X_FD")
    if (pyauto_base.bin.getBit(7, self.infoPage0["ANAR"]) == 1):
      lstParse["Ability"].append("100BASE-X_HD")
    if (pyauto_base.bin.getBit(6, self.infoPage0["ANAR"]) == 1):
      lstParse["Ability"].append("10BASE-T_FD")
    if (pyauto_base.bin.getBit(5, self.infoPage0["ANAR"]) == 1):
      lstParse["Ability"].append("10BASE-T_HD")
    tmp = "0x{:X}".format(int(self.infoPage0["ANAR"]) & 0x1F)
    lstParse["Selector"] = tmp
    if (len(lstParse) > 0):
      self.infoPage0["ANAR"] = lstParse

    lstParse = {"Ability": []}
    if (pyauto_base.bin.getBit(15, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Next Page"] = True
    else:
      lstParse["Next Page"] = False
    if (pyauto_base.bin.getBit(14, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["ACK"] = True
    else:
      lstParse["ACK"] = False
    if (pyauto_base.bin.getBit(13, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Remote Fault"] = True
    else:
      lstParse["Remote Fault"] = False
    if (pyauto_base.bin.getBit(12, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Extended Next Page"] = True
    else:
      lstParse["Extended Next Page"] = False
    if (pyauto_base.bin.getBit(11, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Ability"].append("Asymmetric PAUSE")
    if (pyauto_base.bin.getBit(10, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Ability"].append("PAUSE")
    if (pyauto_base.bin.getBit(9, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Ability"].append("100BASE-T4")
    if (pyauto_base.bin.getBit(8, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Ability"].append("100BASE-X_FD")
    if (pyauto_base.bin.getBit(7, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Ability"].append("100BASE-X_HD")
    if (pyauto_base.bin.getBit(6, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Ability"].append("10BASE-T_FD")
    if (pyauto_base.bin.getBit(5, self.infoPage0["ANLPBPR"]) == 1):
      lstParse["Ability"].append("10BASE-T_HD")
    tmp = "0x{:X}".format(int(self.infoPage0["ANLPBPR"]) & 0x1F)
    lstParse["Selector"] = tmp
    if (len(lstParse) > 0):
      self.infoPage0["ANLPBPR"] = lstParse

    lstParse = []
    if (pyauto_base.bin.getBit(4, self.infoPage0["ANER"]) == 1):
      lstParse.append("Parallel Detection Fault")
    if (pyauto_base.bin.getBit(3, self.infoPage0["ANER"]) == 1):
      lstParse.append("Link Partner NP Able")
    if (pyauto_base.bin.getBit(2, self.infoPage0["ANER"]) == 1):
      lstParse.append("Local NP Able")
    if (pyauto_base.bin.getBit(1, self.infoPage0["ANER"]) == 1):
      lstParse.append("Link Code Word Page Rcvd")
    if (pyauto_base.bin.getBit(0, self.infoPage0["ANER"]) == 1):
      lstParse.append("Link Partner AutoNeg Able")
    if (len(lstParse) > 0):
      self.infoPage0["ANER"] = lstParse

    lstParse = []
    if (pyauto_base.bin.getBit(15, self.infoPage0["ANNPTR"]) == 1):
      lstParse.append("NP follows")
    else:
      lstParse.append("last NP")
    if (pyauto_base.bin.getBit(13, self.infoPage0["ANNPTR"]) == 1):
      lstParse.append("Formatted Page")
    else:
      lstParse.append("Unformatted Page")
    if (pyauto_base.bin.getBit(12, self.infoPage0["ANNPTR"]) == 1):
      lstParse.append("ACK2")
    if (pyauto_base.bin.getBit(11, self.infoPage0["ANNPTR"]) == 1):
      lstParse.append("TOGGLE")
    tmp = "0x{:X}".format(int(self.infoPage0["ANNPTR"]) & 0x7FF)
    lstParse.append("message: {}".format(tmp))
    if (len(lstParse) > 0):
      self.infoPage0["ANNPTR"] = lstParse

    lstParse = []
    if (pyauto_base.bin.getBit(15, self.infoPage0["ANLPNPR"]) == 1):
      lstParse.append("NP follows")
    else:
      lstParse.append("last NP")
    if (pyauto_base.bin.getBit(13, self.infoPage0["ANLPNPR"]) == 1):
      lstParse.append("Formatted Page")
    else:
      lstParse.append("Unformatted Page")
    if (pyauto_base.bin.getBit(12, self.infoPage0["ANLPNPR"]) == 1):
      lstParse.append("ACK2")
    if (pyauto_base.bin.getBit(11, self.infoPage0["ANLPNPR"]) == 1):
      lstParse.append("TOGGLE")
    tmp = "0x{:X}".format(int(self.infoPage0["ANLPNPR"]) & 0x7FF)
    lstParse.append("message: {}".format(tmp))
    if (len(lstParse) > 0):
      self.infoPage0["ANLPNPR"] = lstParse

  def _parse_Page0_Vendor(self):
    pass

  def parse(self, raw=False):
    self._parse_Page0()
    if (not raw):
      self._parse_Page0_base()
      self._parse_Page0_AutoNeg()
      self._parse_Page0_Vendor()

  def mkCtrlReg(self,
                speed,
                duplex,
                reset=False,
                loopback=False,
                autoneg=True,
                pwrdown=False,
                isolate=False,
                coltest=True,
                unidir=False):
    val = "0x0000"
    if (reset):
      val = pyauto_base.bin.setBit(15, val)
    if (loopback):
      val = pyauto_base.bin.setBit(14, val)
    if (speed == "100M"):
      val = pyauto_base.bin.setBit(13, val)
    elif (speed == "1000M"):
      val = pyauto_base.bin.setBit(6, val)
    elif (speed == "10M"):
      pass
    else:
      logger.warning("PHY speed set to 10M")
    if (autoneg):
      val = pyauto_base.bin.setBit(12, val)
    if (pwrdown):
      val = pyauto_base.bin.setBit(11, val)
    if (isolate):
      val = pyauto_base.bin.setBit(10, val)
    if (duplex == "full"):
      val = pyauto_base.bin.setBit(8, val)
    elif (duplex == "half"):
      pass
    else:
      logger.warning("PHY duplex set to half")
    if (coltest):
      val = pyauto_base.bin.setBit(7, val)
    if (unidir):
      val = pyauto_base.bin.setBit(5, val)

    return val

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")

class CFI(object):

  def __init__(self):
    self.id_data = 11 * ["0x00"]
    self.if_data = 12 * ["0x00"]
    self.geom_data = 10 * ["0x00"]
    self.infoID = {}
    self.infoIf = {}
    self.infoGeom = {}

  def clear(self):
    self.id_data = 11 * ["0x00"]
    self.if_data = 12 * ["0x00"]
    self.geom_data = 10 * ["0x00"]
    self.infoID = {}
    self.infoIf = {}
    self.infoGeom = {}

  def _parse_id(self):
    # JEDEC 68.01 Table 6
    self.infoID["ascii"] = self.id_data[0:3]
    self.infoID["PriACSID"] = self.id_data[3:5]
    self.infoID["PriAddrQry"] = self.id_data[5:7]
    self.infoID["AltACSID"] = self.id_data[7:9]
    self.infoID["AltAddrQry"] = self.id_data[9:11]

  def _parse_if(self):
    # JEDEC 68.01 Table 7
    self.infoIf["VCCmin"] = self.if_data[0]
    self.infoIf["VCCmax"] = self.if_data[1]
    self.infoIf["VPPmin"] = self.if_data[2]
    self.infoIf["VPPmax"] = self.if_data[3]
    self.infoIf["TOtyp"] = self.if_data[4:8]
    self.infoIf["TOmax"] = self.if_data[8:12]

  def _parse_geom(self):
    # JEDEC 68.01 Table 8
    self.infoGeom["DevSize"] = self.geom_data[0]
    self.infoGeom["DevIf"] = self.geom_data[1:3]
    self.infoGeom["MaxB_MP"] = self.geom_data[3:5]
    self.infoGeom["nEBR"] = self.geom_data[5]
    self.infoGeom["EBRInfo"] = self.geom_data[6:10]

  def _parse_id_full(self):
    qryStr = ""
    try:
      for v in self.infoID["ascii"]:
        qryStr += chr(int(v, 16))
    except ValueError:
      pass
    if (qryStr != "QRY"):
      logger.error("INCORRECT CFI data")
      return
    else:
      self.infoID["ascii"] = qryStr

    tmp = "0x" + "".join([b[-2:] for b in self.infoID["PriACSID"][::-1]]).upper()
    self.infoID["PriACSID"] = tmp

    tmp = "0x" + "".join([b[-2:] for b in self.infoID["PriAddrQry"][::-1]]).upper()
    self.infoID["PriAddrQry"] = tmp

    tmp = "0x" + "".join([b[-2:] for b in self.infoID["AltACSID"][::-1]]).upper()
    self.infoID["AltACSID"] = tmp

    tmp = "0x" + "".join([b[-2:] for b in self.infoID["AltAddrQry"][::-1]]).upper()
    self.infoID["AltAddrQry"] = tmp

  def _parse_if_full(self):
    raise NotImplementedError  # TODO:

  def _parse_geom_full(self):
    tmp = 2**(int(self.infoGeom["DevSize"], 16) - 20)
    self.infoGeom["DevSize"] = "{:d}MB".format(tmp)

    tmp = "0x" + "".join([b[-2:] for b in self.infoGeom["DevIf"][::-1]]).upper()
    if (tmp == "0x0000"):
      self.infoGeom["DevIf"] = "x8-only async"
    elif (tmp == "0x0001"):
      self.infoGeom["DevIf"] = "x16-only async"
    elif (tmp == "0x0002"):
      self.infoGeom["DevIf"] = "x8/x16 async (BYTE#)"
    elif (tmp == "0x0003"):
      self.infoGeom["DevIf"] = "x32-only async"
    elif (tmp == "0x0004"):
      self.infoGeom["DevIf"] = "x16/x32 async (WORD#)"
    else:
      self.infoGeom["DevIf"] = tmp

    tmp = "0x" + "".join([b[-2:] for b in self.infoGeom["MaxB_MP"][::-1]]).upper()
    self.infoGeom["MaxB_MP"] = "{:d} bytes".format(2**int(tmp, 16))

    self.infoGeom["nEBR"] = int(self.infoGeom["nEBR"], 16)

    tmp = "0x" + "".join([b[-2:] for b in self.infoGeom["EBRInfo"][::-1]]).upper()
    self.infoGeom["EBRInfo"] = tmp

  def parse(self, raw=False):
    if (len(self.id_data) != 11):
      logger.error("wrong size for ID data ({:d} bytes)".format(len(self.id_data)))
      return
    if (len(self.if_data) != 12):
      logger.error("wrong size for interface data ({:d} bytes)".format(len(self.if_data)))
      return
    if (len(self.geom_data) != 10):
      logger.error("wrong size for geometry data ({:d} bytes)".format(len(self.geom_data)))
      return

    self._parse_id()
    self._parse_if()
    self._parse_geom()
    if (not raw):
      self._parse_id_full()
      #self._parse_if_full()
      self._parse_geom_full()

  def showInfo(self):
    for k, v in sorted(self.infoID.items()):
      logger.info("{:<12}: {}".format(k, v))
    for k, v in sorted(self.infoIf.items()):
      logger.info("{:<12}: {}".format(k, v))
    for k, v in sorted(self.infoGeom.items()):
      logger.info("{:<12}: {}".format(k, v))

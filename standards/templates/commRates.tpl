@require(dictRates)
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>rates</title>
  <!--<link type="text/css" rel="stylesheet" href="???.css"/>-->
  <!--<script src="/js/jquery-2.2.1.min.js"></script>-->
  <style>
table {
    border-collapse: collapse;
    font-family: sans-serif;
}
table td {
    padding: 0.5em;
}
table thead th {
    background-color: #54585d;
    color: #ffffff;
    font-weight: bold;
    padding: 0.5em;
    border: 2px solid #54585d;
}
table tbody td {
    color: #636363;
    border: 2px solid #dddfe1;
}
table tbody tr {
    background-color: #f9fafb;
}
  </style>
</head>

<body>
  <table>
    <thead>
      <tr>
        <th>Id</th>
        <th>Baud Rate [Mbdps]</th>
        <th>Bit Rate [Mbps]</th>
        <th>Lines</th>
        <th>Coding</th>
        <th>Modulation</th>
      </tr>
    </thead>

    <tbody>
      @for rid in sorted(dictRates.keys()):
      <tr>
        <td>@{dictRates[rid].id}</td>
        <td>@{str(dictRates[rid].baudRate)}</td>
        <td>@{str(dictRates[rid].bitRate)}</td>
        <td>@{str(dictRates[rid].lines)}</td>
        <td>@{dictRates[rid].coding}</td>
        <td>@{dictRates[rid].modulation}</td>
      </tr>
      @end
    </tbody>
  </table>

</body>
</html>

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
import pyauto_base.bin

class PMBusCmd(object):
  PAGE = "0x00"
  OPERATION = "0x01"
  ON_OFF_CONFIG = "0x02"
  CLEAR_FAULTS = "0x03"
  PHASE = "0x04"
  PAGE_PLUS_WRITE = "0x05"
  PAGE_PLUS_READ = "0x06"
  WRITE_PROTECT = "0x10"
  STORE_DEFAULT_ALL = "0x11"
  RESTORE_DEFAULT_ALL = "0x12"
  STORE_DEFAULT_CODE = "0x13"
  RESTORE_DEFAULT_CODE = "0x14"
  STORE_USER_ALL = "0x15"
  RESTORE_USER_ALL = "0x16"
  STORE_USER_CODE = "0x17"
  RESTORE_USER_CODE = "0x18"
  CAPABILITY = "0x19"
  QUERY = "0x1A"
  SMBALERT_MASK = "0x1B"
  VOUT_MODE = "0x20"
  VOUT_COMMAND = "0x21"
  VOUT_TRIM = "0x22"
  VOUT_CAL_OFFSET = "0x23"
  VOUT_MAX = "0x24"
  VOUT_MARGIN_HIGH = "0x25"
  VOUT_MARGIN_LOW = "0x26"
  VOUT_TRANSITION_RATE = "0x27"
  VOUT_DROOP = "0x28"
  VOUT_SCALE_LOOP = "0x29"
  VOUT_SCALE_MONITOR = "0x2A"
  COEFFICIENTS = "0x30"
  POUT_MAX = "0x31"
  MAX_DUTY = "0x32"
  FREQUENCY_SWITCH = "0x33"
  VIN_ON = "0x35"
  VIN_OFF = "0x36"
  INTERLEAVE = "0x37"
  IOUT_CAL_GAIN = "0x38"
  IOUT_CAL_OFFSET = "0x39"
  FAN_CONFIG_1_2 = "0x3A"
  FAN_COMMAND_1 = "0x3B"
  FAN_COMMAND_2 = "0x3C"
  FAN_CONFIG_3_4 = "0x3D"
  FAN_COMMAND_3 = "0x3E"
  FAN_COMMAND_4 = "0x3F"
  VOUT_OV_FAULT_LIMIT = "0x40"
  VOUT_OV_FAULT_RESPONSE = "0x41"
  VOUT_OV_WARN_LIMIT = "0x42"
  VOUT_UV_WARN_LIMIT = "0x43"
  VOUT_UV_FAULT_LIMIT = "0x44"
  VOUT_UV_FAULT_RESPONSE = "0x45"
  IOUT_OC_FAULT_LIMIT = "0x46"
  IOUT_OC_FAULT_RESPONSE = "0x47"
  IOUT_OC_LV_FAULT_LIMIT = "0x48"
  IOUT_OC_LV_FAULT_RESPONSE = "0x49"
  IOUT_OC_WARN_LIMIT = "0x4A"
  IOUT_UC_FAULT_LIMIT = "0x4B"
  IOUT_UC_FAULT_RESPONSE = "0x4C"
  OT_FAULT_LIMIT = "0x4F"
  OT_FAULT_RESPONSE = "0x50"
  OT_WARN_LIMIT = "0x51"
  UT_WARN_LIMIT = "0x52"
  UT_FAULT_LIMIT = "0x53"
  UT_FAULT_RESPONSE = "0x54"
  VIN_OV_FAULT_LIMIT = "0x55"
  VIN_OV_FAULT_RESPONSE = "0x56"
  VIN_OV_WARN_LIMIT = "0x57"
  VIN_UV_WARN_LIMIT = "0x58"
  VIN_UV_FAULT_LIMIT = "0x59"
  VIN_UV_FAULT_RESPONSE = "0x5A"
  IIN_OC_FAULT_LIMIT = "0x5B"
  IIN_OC_FAULT_RESPONSE = "0x5C"
  IIN_OC_WARN_LIMIT = "0x5D"
  POWER_GOOD_ON = "0x5E"
  POWER_GOOD_OFF = "0x5F"
  TON_DELAY = "0x60"
  TON_RISE = "0x61"
  TON_MAX_FAULT_LIMIT = "0x62"
  TON_MAX_FAULT_RESPONSE = "0x63"
  TOFF_DELAY = "0x64"
  TOFF_FALL = "0x65"
  TOFF_MAX_WARN_LIMIT = "0x66"
  POUT_OP_FAULT_LIMIT = "0x68"
  POUT_OP_FAULT_RESPONSE = "0x69"
  POUT_OP_WARN_LIMIT = "0x6A"
  PIN_OP_WARN_LIMIT = "0x6B"
  STATUS_BYTE = "0x78"
  STATUS_WORD = "0x79"
  STATUS_VOUT = "0x7A"
  STATUS_IOUT = "0x7B"
  STATUS_INPUT = "0x7C"
  STATUS_TEMPERATURE = "0x7D"
  STATUS_CML = "0x7E"
  STATUS_OTHER = "0x7F"
  STATUS_MFR_SPECIFIC = "0x80"
  STATUS_FANS_1_2 = "0x81"
  STATUS_FANS_3_4 = "0x82"
  READ_EIN = "0x86"
  READ_EOUT = "0x87"
  READ_VIN = "0x88"
  READ_IIN = "0x89"
  READ_VCAP = "0x8A"
  READ_VOUT = "0x8B"
  READ_IOUT = "0x8C"
  READ_TEMPERATURE_1 = "0x8D"
  READ_TEMPERATURE_2 = "0x8E"
  READ_TEMPERATURE_3 = "0x8F"
  READ_FAN_SPEED_1 = "0x90"
  READ_FAN_SPEED_2 = "0x91"
  READ_FAN_SPEED_3 = "0x92"
  READ_FAN_SPEED_4 = "0x93"
  READ_DUTY_CYCLE = "0x94"
  READ_FREQUENCY = "0x95"
  READ_POUT = "0x96"
  READ_PIN = "0x97"
  PMBUS_REVISION = "0x98"
  MFR_ID = "0x99"
  MFR_MODEL = "0x9A"
  MFR_REVISION = "0x9B"
  MFR_LOCATION = "0x9C"
  MFR_DATE = "0x9D"
  MFR_SERIAL = "0x9E"
  APP_PROFILE_SUPPORT = "0x9F"
  MFR_VIN_MIN = "0xA0"
  MFR_VIN_MAX = "0xA1"
  MFR_IIN_MAX = "0xA2"
  MFR_PIN_MAX = "0xA3"
  MFR_VOUT_MIN = "0xA4"
  MFR_VOUT_MAX = "0xA5"
  MFR_IOUT_MAX = "0xA6"
  MFR_POUT_MAX = "0xA7"
  MFR_TAMBIENT_MAX = "0xA8"
  MFR_TAMBIENT_MIN = "0xA9"
  MFR_EFFICIENCY_LL = "0xAA"
  MFR_EFFICIENCY_HL = "0xAB"
  MFR_PIN_ACCURACY = "0xAC"
  IC_DEVICE_ID = "0xAD"
  IC_DEVICE_REV = "0xAE"
  USER_DATA_00 = "0xB0"
  USER_DATA_01 = "0xB1"
  USER_DATA_02 = "0xB2"
  USER_DATA_03 = "0xB3"
  USER_DATA_04 = "0xB4"
  USER_DATA_05 = "0xB5"
  USER_DATA_06 = "0xB6"
  USER_DATA_07 = "0xB7"
  USER_DATA_08 = "0xB8"
  USER_DATA_09 = "0xB9"
  USER_DATA_10 = "0xBA"
  USER_DATA_11 = "0xBB"
  USER_DATA_12 = "0xBC"
  USER_DATA_13 = "0xBD"
  USER_DATA_14 = "0xBE"
  USER_DATA_15 = "0xBF"
  MFR_MAX_TEMP_1 = "0xC0"
  MFR_MAX_TEMP_2 = "0xC1"
  MFR_MAX_TEMP_3 = "0xC2"
  MFR_SPECIFIC_00 = "0xD0"
  MFR_SPECIFIC_01 = "0xD1"
  MFR_SPECIFIC_02 = "0xD2"
  MFR_SPECIFIC_03 = "0xD3"
  MFR_SPECIFIC_04 = "0xD4"
  MFR_SPECIFIC_05 = "0xD5"
  MFR_SPECIFIC_06 = "0xD6"
  MFR_SPECIFIC_07 = "0xD7"
  MFR_SPECIFIC_08 = "0xD8"
  MFR_SPECIFIC_09 = "0xD9"
  MFR_SPECIFIC_10 = "0xDA"
  MFR_SPECIFIC_11 = "0xDB"
  MFR_SPECIFIC_12 = "0xDC"
  MFR_SPECIFIC_13 = "0xDD"
  MFR_SPECIFIC_14 = "0xDE"
  MFR_SPECIFIC_15 = "0xDF"
  MFR_SPECIFIC_16 = "0xE0"
  MFR_SPECIFIC_17 = "0xE1"
  MFR_SPECIFIC_18 = "0xE2"
  MFR_SPECIFIC_19 = "0xE3"
  MFR_SPECIFIC_20 = "0xE4"
  MFR_SPECIFIC_21 = "0xE5"
  MFR_SPECIFIC_22 = "0xE6"
  MFR_SPECIFIC_23 = "0xE7"
  MFR_SPECIFIC_24 = "0xE8"
  MFR_SPECIFIC_25 = "0xE9"
  MFR_SPECIFIC_26 = "0xEA"
  MFR_SPECIFIC_27 = "0xEB"
  MFR_SPECIFIC_28 = "0xEC"
  MFR_SPECIFIC_29 = "0xED"
  MFR_SPECIFIC_30 = "0xEE"
  MFR_SPECIFIC_31 = "0xEF"
  MFR_SPECIFIC_32 = "0xF0"
  MFR_SPECIFIC_33 = "0xF1"
  MFR_SPECIFIC_34 = "0xF2"
  MFR_SPECIFIC_35 = "0xF3"
  MFR_SPECIFIC_36 = "0xF4"
  MFR_SPECIFIC_37 = "0xF5"
  MFR_SPECIFIC_38 = "0xF6"
  MFR_SPECIFIC_39 = "0xF7"
  MFR_SPECIFIC_40 = "0xF8"
  MFR_SPECIFIC_41 = "0xF9"
  MFR_SPECIFIC_42 = "0xFA"
  MFR_SPECIFIC_43 = "0xFB"
  MFR_SPECIFIC_44 = "0xFC"
  MFR_SPECIFIC_45 = "0xFD"
  MFR_SPECIFIC_COMMAND_EXT = "0xFE"
  PMBUS_COMMAND_EXT = "0xFF"

  def byte2Name(self, val):
    if (isinstance(val, str)):
      pass
    elif (isinstance(val, int)):
      val = "0x{:02x}".format(val)
    for k, v in self.__class__.__dict__.items():
      if (v == val):
        return k

def convert_from_L16u(val, exponent=-12):
  tmp = int(val, 16) * (2**exponent)
  return tmp

def convert_to_L16u(val, exponent=-12):
  tmp = int(round((val / (2**exponent)), 0))
  return "0x{:0>4X}".format(tmp)

def convert_from_L11(hexVal):
  #logger.info(hexVal)
  N = "0x{:X}".format(int(hexVal, 16) >> 11)
  Y = "0x{:X}".format(int(hexVal, 16) & 0x07FF)
  #logger.info([Y, N])
  N = pyauto_base.bin.hex2int_2c(N, 5)
  Y = pyauto_base.bin.hex2int_2c(Y, 11)
  #logger.info([Y, N])
  return Y * (2**N)

def show_PMBUS_REVISION(hexVal):
  logger.info("PMBUS_REVISION: Part  I rev. 1.{:d}".format(int(hexVal, 16) // 32))
  logger.info("PMBUS_REVISION: Part II rev. 1.{:d}".format(int(hexVal, 16) % 16))

def show_CAPABILITY(hexVal):
  lstCap = []
  if (pyauto_base.bin.getBit(7, hexVal) == 1):
    lstCap.append("PEC")
  if ((int(hexVal, 16) & 0x60) == 0):
    lstCap.append("100KHz")
  elif ((int(hexVal, 16) & 0x60) == 0x20):
    lstCap.append("400KHz")
  if (pyauto_base.bin.getBit(4, hexVal) == 1):
    lstCap.append("SMBus Alert Response")
  logger.info("CAPABILITY: {}".format(lstCap))

def show_VOUT_MODE(hexVal):
  if ((int(hexVal, 16) & 0xE0) == 0):
    tmp = int(hexVal, 16) & 0x1F
    logger.info("VOUT_MODE: linear [exponent: {}, {}]".format(
        tmp, pyauto_base.bin.hex2int_2c(tmp, 5)))
  elif ((int(hexVal, 16) & 0xE0) == 0x20):
    tmp = int(hexVal, 16) & 0x1F
    logger.info("VOUT_MODE: VID 0x{:X}".format(tmp))
  elif ((int(hexVal, 16) & 0xE0) == 0x40):
    logger.info("VOUT_MODE: direct")

def show_OPERATION(hexVal):
  if ((int(hexVal, 16) & 0xC0) == 0):
    logger.info("OPERATION: Immediate OFF (no sequencing)")
  elif ((int(hexVal, 16) & 0xC0) == 0x40):
    logger.info("OPERATION: Soft OFF (with sequencing)")
  elif ((int(hexVal, 16) & 0xC0) == 0x80):
    if ((int(hexVal, 16) & 0x30) == 0):
      logger.info("OPERATION: ON (margin off)")
    elif ((int(hexVal, 16) & 0x3C) == 0x14):
      logger.info("OPERATION: ON (margin low, ignore fault)")
    elif ((int(hexVal, 16) & 0x3C) == 0x18):
      logger.info("OPERATION: ON (margin low, act on fault)")
    elif ((int(hexVal, 16) & 0x3C) == 0x24):
      logger.info("OPERATION: ON (margin high, ignore fault)")
    elif ((int(hexVal, 16) & 0x3C) == 0x28):
      logger.info("OPERATION: ON (margin high, act on fault)")
    else:
      logger.warning("OPERATION: INVALID ({})".format(hexVal))
  else:
    logger.warning("OPERATION: INVALID ({})".format(hexVal))

def show_ON_OFF_CONFIG(hexVal):
  if (pyauto_base.bin.getBit(4, hexVal) == 0):
    logger.info("ON_OFF_CONFIG: power-up if power present")
  else:
    logger.info("ON_OFF_CONFIG: power-up on CONTROL pin and OPERATION")

  if (pyauto_base.bin.getBit(3, hexVal) == 0):
    logger.info("ON_OFF_CONFIG: ignore OPERATION")
  else:
    logger.info("ON_OFF_CONFIG: follow OPERATION")

  if (pyauto_base.bin.getBit(2, hexVal) == 0):
    logger.info("ON_OFF_CONFIG: ignore CONTROL pin")
  else:
    logger.info("ON_OFF_CONFIG: follow CONTROL pin")

  if (pyauto_base.bin.getBit(1, hexVal) == 0):
    logger.info("ON_OFF_CONFIG: CONTROL pin active low")
  else:
    logger.info("ON_OFF_CONFIG: CONTROL pin active high")

  if (pyauto_base.bin.getBit(0, hexVal) == 0):
    logger.info("ON_OFF_CONFIG: CONTROL pin soft turn off")
  else:
    logger.info("ON_OFF_CONFIG: CONTROL pin fast turn off")

def show_STATUS(hexLst):
  if (pyauto_base.bin.getBit(7, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: unit busy")
  if (pyauto_base.bin.getBit(6, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: unit OFF")
  if (pyauto_base.bin.getBit(5, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: VOUT_OV fault")
  if (pyauto_base.bin.getBit(4, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: IOUT_OC fault")
  if (pyauto_base.bin.getBit(3, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: VIN_UV fault")
  if (pyauto_base.bin.getBit(2, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: TEMPERATURE fault or warn")
  if (pyauto_base.bin.getBit(1, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: CML fault")
  if (pyauto_base.bin.getBit(0, hexLst[0]) == 1):
    logger.info("STATUS_BYTE: none of the above")

  if (len(hexLst) == 1):
    if (int(hexLst[0], 16) == 0):
      logger.info("STATUS_BYTE: OK")
    return

  if (pyauto_base.bin.getBit(7, hexLst[1]) == 1):
    logger.info("STATUS_WORD: VOUT fault or warn")
  if (pyauto_base.bin.getBit(6, hexLst[1]) == 1):
    logger.info("STATUS_WORD: IOUT/POUT fault or warn")
  if (pyauto_base.bin.getBit(5, hexLst[1]) == 1):
    logger.info("STATUS_WORD: INPUT fault or warn")
  if (pyauto_base.bin.getBit(4, hexLst[1]) == 1):
    logger.info("STATUS_WORD: MFR fault")
  if (pyauto_base.bin.getBit(3, hexLst[1]) == 1):
    logger.info("STATUS_WORD: NOT POWER_GOOD")
  if (pyauto_base.bin.getBit(2, hexLst[1]) == 1):
    logger.info("STATUS_WORD: FAN fault or warn")
  if (pyauto_base.bin.getBit(1, hexLst[1]) == 1):
    logger.info("STATUS_WORD: OTHER fault")
  if (pyauto_base.bin.getBit(0, hexLst[1]) == 1):
    logger.info("STATUS_WORD: UNKNOWN fault or warn")

  if ((int(hexLst[0], 16) == 0) and (int(hexLst[1], 16) == 0)):
    logger.info("STATUS_WORD: OK")

def show_STATUS_VOUT(hexVal):
  if (pyauto_base.bin.getBit(7, hexVal) == 1):
    logger.info("STATUS_VOUT: VOUT_OV fault")
  if (pyauto_base.bin.getBit(6, hexVal) == 1):
    logger.info("STATUS_VOUT: VOUT_OV warn")
  if (pyauto_base.bin.getBit(5, hexVal) == 1):
    logger.info("STATUS_VOUT: VOUT_UV warn")
  if (pyauto_base.bin.getBit(4, hexVal) == 1):
    logger.info("STATUS_VOUT: VOUT_UV fault")
  if (pyauto_base.bin.getBit(3, hexVal) == 1):
    logger.info("STATUS_VOUT: VOUT_MAX warn")
  if (pyauto_base.bin.getBit(2, hexVal) == 1):
    logger.info("STATUS_VOUT: TON_MAX fault")
  if (pyauto_base.bin.getBit(1, hexVal) == 1):
    logger.info("STATUS_VOUT: TOFF_MAX warn")
  if (pyauto_base.bin.getBit(0, hexVal) == 1):
    logger.info("STATUS_VOUT: power-on track error")

def show_STATUS_IOUT(hexVal):
  if (pyauto_base.bin.getBit(7, hexVal) == 1):
    logger.info("STATUS_IOUT: IOUT_OC fault")
  if (pyauto_base.bin.getBit(6, hexVal) == 1):
    logger.info("STATUS_IOUT: IOUT_OC fault w LV shutdown")
  if (pyauto_base.bin.getBit(5, hexVal) == 1):
    logger.info("STATUS_IOUT: IOUT_OC warn")
  if (pyauto_base.bin.getBit(4, hexVal) == 1):
    logger.info("STATUS_IOUT: IOUT_UC fault")
  if (pyauto_base.bin.getBit(3, hexVal) == 1):
    logger.info("STATUS_IOUT: current share fault")
  if (pyauto_base.bin.getBit(2, hexVal) == 1):
    logger.info("STATUS_IOUT: IN power limiting")
  if (pyauto_base.bin.getBit(1, hexVal) == 1):
    logger.info("STATUS_IOUT: POUT_OP fault")
  if (pyauto_base.bin.getBit(0, hexVal) == 1):
    logger.info("STATUS_IOUT: POUT_OP warn")

def show_STATUS_INPUT(hexVal):
  if (pyauto_base.bin.getBit(7, hexVal) == 1):
    logger.info("STATUS_INPUT: VIN_OV fault")
  if (pyauto_base.bin.getBit(6, hexVal) == 1):
    logger.info("STATUS_INPUT: VIN_OV warn")
  if (pyauto_base.bin.getBit(5, hexVal) == 1):
    logger.info("STATUS_INPUT: VIN_UV warn")
  if (pyauto_base.bin.getBit(4, hexVal) == 1):
    logger.info("STATUS_INPUT: VIN_UV fault")
  if (pyauto_base.bin.getBit(3, hexVal) == 1):
    logger.info("STATUS_INPUT: unit OFF for low input voltage")
  if (pyauto_base.bin.getBit(2, hexVal) == 1):
    logger.info("STATUS_INPUT: IIN_OC fault")
  if (pyauto_base.bin.getBit(1, hexVal) == 1):
    logger.info("STATUS_INPUT: IIN_OC warn")
  if (pyauto_base.bin.getBit(0, hexVal) == 1):
    logger.info("STATUS_INPUT: PIN_OP warn")

def show_STATUS_TEMPERATURE(hexVal):
  if (pyauto_base.bin.getBit(7, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: OT fault")
  if (pyauto_base.bin.getBit(6, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: OT warn")
  if (pyauto_base.bin.getBit(5, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: UT warn")
  if (pyauto_base.bin.getBit(4, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: UT fault")
  if (pyauto_base.bin.getBit(3, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: reserved 3")
  if (pyauto_base.bin.getBit(2, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: reserved 2")
  if (pyauto_base.bin.getBit(1, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: reserved 1")
  if (pyauto_base.bin.getBit(0, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: reserved 0")

def show_STATUS_CML(hexVal):
  if (pyauto_base.bin.getBit(7, hexVal) == 1):
    logger.info("STATUS_CML: invalid/unsupported command")
  if (pyauto_base.bin.getBit(6, hexVal) == 1):
    logger.info("STATUS_CML: invalid/unsupported data")
  if (pyauto_base.bin.getBit(5, hexVal) == 1):
    logger.info("STATUS_CML: PEC failed")
  if (pyauto_base.bin.getBit(4, hexVal) == 1):
    logger.info("STATUS_CML: memory fault")
  if (pyauto_base.bin.getBit(3, hexVal) == 1):
    logger.info("STATUS_CML: processor fault")
  if (pyauto_base.bin.getBit(2, hexVal) == 1):
    logger.info("STATUS_CML: reserved")
  if (pyauto_base.bin.getBit(1, hexVal) == 1):
    logger.info("STATUS_CML: other communication fault")
  if (pyauto_base.bin.getBit(0, hexVal) == 1):
    logger.info("STATUS_CML: other memory or logic fault")

def show_STATUS_OTHER(hexVal):
  if (pyauto_base.bin.getBit(7, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: reserved 7")
  if (pyauto_base.bin.getBit(6, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: reserved 6")
  if (pyauto_base.bin.getBit(5, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: IN_A fuse/breaker fault")
  if (pyauto_base.bin.getBit(4, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: IN_B fuse/breaker fault")
  if (pyauto_base.bin.getBit(3, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: IN_A OR-ing fault")
  if (pyauto_base.bin.getBit(2, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: IN_B OR-in fault")
  if (pyauto_base.bin.getBit(1, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: OUT OR-ing fault")
  if (pyauto_base.bin.getBit(0, hexVal) == 1):
    logger.info("STATUS_TEMPERATURE: reserved 0")

def show_FaultResponse(hexVal):
  if ((int(hexVal, 16) & 0xC0) == 0):
    logger.info("FAULT_RESPONSE: device continues operation")
  elif ((int(hexVal, 16) & 0xC0) == 0x40):
    logger.info(
        "FAULT_RESPONSE: device continues operation for delay[2:0] and then retries[5:3] on fault"
    )
  elif ((int(hexVal, 16) & 0xC0) == 0x80):
    logger.info("FAULT_RESPONSE: device shuts-down and then retries[5:3] on fault")
  elif ((int(hexVal, 16) & 0xC0) == 0xC0):
    logger.info("FAULT_RESPONSE: device shuts-down for fault duration")

  if ((int(hexVal, 16) & 0x38) == 0):
    logger.info("FAULT_RESPONSE: no retry for fault duration")
  elif ((int(hexVal, 16) & 0x38) == 0x38):
    logger.info("FAULT_RESPONSE: always retry")
  else:
    tmp = (int(hexVal, 16) & 0xC0) // 8
    logger.info("FAULT_RESPONSE: {:d} retries".format(tmp))

  if ((int(hexVal, 16) & 0x07) != 0):
    tmp = int(hexVal, 16) & 0x07
    logger.info("FAULT_RESPONSE: {:d} delay units".format(tmp))

def show_CurrentFaultResponse(hexVal):
  if ((int(hexVal, 16) & 0xC0) == 0):
    logger.info("FAULT_RESPONSE: device continues operation with IOUT_OC_FAULT_LIMIT")
  elif ((int(hexVal, 16) & 0xC0) == 0x40):
    logger.info("FAULT_RESPONSE: device continues operation with "
                "IOUT_OC_FAULT_LIMIT if(VOUT > IOUT_OC_UV_FAULT_LIMIT) "
                "else shuts-down and then retries[5:3] on fault")
  elif ((int(hexVal, 16) & 0xC0) == 0x80):
    logger.info("FAULT_RESPONSE: device continues operation with "
                "IOUT_OC_FAULT_LIMIT for delay[2:0] and then retries[5:3] on fault")
  elif ((int(hexVal, 16) & 0xC0) == 0xC0):
    logger.info("FAULT_RESPONSE: device shuts-down and then retries[5:3] on fault")

  if ((int(hexVal, 16) & 0x38) == 0):
    logger.info("FAULT_RESPONSE: no retry for fault duration")
  elif ((int(hexVal, 16) & 0x38) == 0x38):
    logger.info("FAULT_RESPONSE: always retry")
  else:
    tmp = (int(hexVal, 16) & 0x38) // 8
    logger.info("FAULT_RESPONSE: {:d} retries".format(tmp))

  if ((int(hexVal, 16) & 0x07) != 0):
    tmp = int(hexVal, 16) & 0x07
    logger.info("FAULT_RESPONSE: {:d} delay units".format(tmp))

def show_READ_VIN(hexLst, gain=1.0, offset=0.0):
  hexVal = hex(int(hexLst[1], 16) * (2**8) + int(hexLst[0], 16))
  vin = convert_from_L11(hexVal)
  vin = (vin * gain) + offset
  logger.info("READ_VIN: {:.4f}V".format(vin))

def show_READ_TEMPERATURE(hexLst):
  hexVal = hex(int(hexLst[1], 16) * (2**8) + int(hexLst[0], 16))
  temp = convert_from_L11(hexVal)
  logger.info("READ_TEMPERATURE: {:.4f}C".format(temp))

def show_READ_VOUT(hexLst, scale=1.0, gain=1.0, offset=0.0):
  hexVal = hex(int(hexLst[1], 16) * (2**8) + int(hexLst[0], 16))
  #vout = pyauto_base.binUtils.hex2int_2c(hexVal, 16)
  vout = int(hexVal, 16)
  vout = (vout * scale * gain) + offset
  logger.info("READ_VOUT: {:.4f}V".format(vout))

def show_READ_IOUT(hexLst, gain=1.0, offset=0.0):
  hexVal = hex(int(hexLst[1], 16) * (2**8) + int(hexLst[0], 16))
  iout = convert_from_L11(hexVal)
  iout = (iout * gain) + offset
  logger.info("READ_IOUT: {:.4f}A".format(iout))

def show_READ_FREQUENCY(hexLst):
  hexVal = hex(int(hexLst[1], 16) * (2**8) + int(hexLst[0], 16))
  freq = convert_from_L11(hexVal)
  logger.info("READ_FREQUENCY: {:.4f}kHz".format(freq))

def show_READ_DUTY_CYCLE(hexLst):
  hexVal = hex(int(hexLst[1], 16) * (2**8) + int(hexLst[0], 16))
  duty = convert_from_L11(hexVal)
  logger.info("READ_DUTY_CYCLE: {:.4f}%".format(duty))

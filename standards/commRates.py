#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
import csv

logger = logging.getLogger("lib")

class BaseCommRate(object):

  def __init__(self, rateId):
    self._id = rateId
    self.desc = ""
    self._baudRate = 0.0
    self._bitRate = 0.0
    self._lines = 1
    self._coding = "NA"  # line coding
    self._modulation = "NRZ"
    self._params = {}

  @property
  def id(self):
    return self._id

  @property
  def baudRate(self):
    return self._baudRate

  @baudRate.setter
  def baudRate(self, val):
    if (isinstance(val, float)):
      self._baudRate = val
    else:
      self._baudRate = float(val)

    if (self._modulation == "PAM-4"):
      self._bitRate = 2.0 * self._baudRate
    elif (self._modulation in ("PAM-16", "4D-PAM-5")):
      self._bitRate = 4.0 * self._baudRate
    elif (self._modulation == "NRZ"):
      self._bitRate = self._baudRate
    else:
      raise NotImplementedError("UNSUPPORTED modulation {}".format(self._modulation))

  @property
  def bitRate(self):
    return self._bitRate

  @property
  def lines(self):
    return self._lines

  @lines.setter
  def lines(self, val):
    if (isinstance(val, int)):
      self._lines = val
    else:
      self._lines = int(val)

  @property
  def coding(self):
    return self._coding

  @coding.setter
  def coding(self, val):
    self._coding = str(val)

  @property
  def modulation(self):
    return self._modulation

  @modulation.setter
  def modulation(self, val):
    self._modulation = str(val)

  @property
  def params(self):
    return self._params

  def updateParams(self, dictParams):
    self._params.update(dictParams)

  def __str__(self):
    return "{}: {: <18} | {: >22.12f} Mbdps | {: >22.12f} Mbps | x{: <2d} | {}".format(
        self.__class__.__name__, self._id, self.baudRate, self.bitRate, self.lines,
        ", ".join([self.coding, self.modulation]))

bitRate_10M = 10.0
bitRate_100M = 100.0
bitRate_1G = 1000.0
bitRate_10G = 10000.0
bitRate_25G = 25000.0
bitRate_40G = 40000.0
bitRate_100G = 100000.0

_dictRates = {}

def addRate(strId, strDesc="", coding=None, modulation=None, dictParams=None):
  rate = BaseCommRate(strId)
  rate.desc = strDesc
  if (coding is not None):
    rate.coding = coding
  if (modulation is not None):
    rate.modulation = modulation
  if (isinstance(dictParams, dict)):
    rate.updateParams(dictParams)
  _dictRates[rate.id] = rate
  return rate

def getRate(strId):
  try:
    return _dictRates[strId]
  except KeyError:
    logger.error("rate '{}' DOES NOT exist".format(strId))
    return None

def getNames():
  return _dictRates.keys()

def showNames():
  for rid in sorted(_dictRates.keys()):
    print(rid)

def show():
  for rid in sorted(_dictRates.keys()):
    print(_dictRates[rid])

def writeHtml():
  import pyauto_base.fs
  import pyauto_base.misc

  fOutPath = os.path.join(pyauto_base.fs.mkOutFolder(), "commRates.html")
  tplPath = os.path.join(os.path.dirname(__file__), "templates", "commRates.tpl")
  dictTpl = {"dictRates": _dictRates}
  pyauto_base.misc.templateWheezy(tplPath, fOutPath, dictTpl=dictTpl)
  logger.info("{} written".format(fOutPath))

def writeCsv():
  import pyauto_base.fs

  fOutPath = os.path.join(pyauto_base.fs.mkOutFolder(), "commRates.csv")
  with open(fOutPath, "w") as fOut:
    csvOut = csv.writer(fOut)
    csvOut.writerow(
        ["Id", "Baud Rate [Mbpds]", "Bit Rate [Mbps]", "Lines", "Coding", "Modulation"])
    for rid in sorted(_dictRates.keys()):
      rt = _dictRates[rid]
      csvOut.writerow([rt.id, rt.baudRate, rt.bitRate, rt.lines, rt.coding, rt.modulation])
  logger.info("{} written".format(fOutPath))

_rate = addRate("10BASE-T", "Ethernet 10M over twisted-pair", coding="Manchester")
_rate.baudRate = bitRate_10M

_rate = addRate("100BASE-TX", "Ethernet 100M over twisted-pair", coding="4B/5B")
_rate.baudRate = bitRate_100M * (5.0 / 4.0)

_rate = addRate("1000BASE-T", "Ethernet 1G over twisted-pair", modulation="4D-PAM-5")
_rate.lines = 4
_rate.baudRate = bitRate_1G * (10.0 / 8.0) / _rate.lines

_rate = addRate("1000BASE-SX", "Ethernet 1G over MMF (850nm)", coding="8B/10B")
_rate.baudRate = bitRate_1G * (10.0 / 8.0)

_rate = addRate("10GBASE-T", "Ethernet 10G over twisted-pair", modulation="PAM-16")
_rate.lines = 4
_rate.baudRate = (bitRate_10G * 2) / _rate.lines  # TX and RX are full duplex on all lines
_rate.baudRate = _rate.baudRate / (2 * 3.125)

_rate = addRate("10GBASE-SR",
                "Ethernet 10G over MMF (850nm)",
                coding="64B/66B",
                dictParams={"xfpRate": "103"})
_rate.baudRate = bitRate_10G * (66.0 / 64.0)

_rate = addRate("25GBASE-SR", "Ethernet 25G over MMF (850nm)", coding="64B/66B")
_rate.baudRate = bitRate_25G * (66.0 / 64.0)

_rate = addRate("100GBASE-SR10", "Ethernet 100G over MMF (850nm)", coding="64B/66B")
_rate.lines = 10
_rate.baudRate = bitRate_100G * (66.0 / 64.0) / _rate.lines

_rate = addRate("100GBASE-SR4", "Ethernet 100G over MMF (850nm)", coding="64B/66B")
_rate.lines = 4
_rate.baudRate = bitRate_100G * (66.0 / 64.0) / _rate.lines

_rate = addRate("100GBASE-KR4",
                "Ethernet 100G over electrical backplane",
                dictParams={
                    "transcoding": "256B/257B",
                    "FEC": "RS(528,514,t=7,m=10)"
                })
_rate.lines = 4
_rate.baudRate = bitRate_100G * (257.0 / 256.0) * (528.0 / 514.0) / _rate.lines
_rate.baudRate = round(_rate.baudRate, 2)  # all of the above = 66/64

_rate = addRate("100GBASE-KP4",
                "Ethernet 100G over electrical backplane",
                modulation="PAM-4",
                dictParams={
                    "transcoding": "256B/257B",
                    "FEC": "RS(544,514,t=15,m=10)"
                })
_rate.lines = 4
_rate.baudRate = bitRate_100G * (257.0 / 256.0) * (544.0 / 514.0) / (_rate.lines * 2)

_rate = addRate("MII")
_rate.lines = 4
_rate.baudRate = bitRate_100M / _rate.lines

_rate = addRate("RMII")
_rate.lines = 2
_rate.baudRate = bitRate_100M / _rate.lines

_rate = addRate("GMII")
_rate.lines = 8
_rate.baudRate = bitRate_1G / _rate.lines

_rate = addRate("RGMII", "", dictParams={"DDR": True})
_rate.lines = 4
_rate.baudRate = bitRate_1G / (_rate.lines * 2)

_rate = addRate("SGMII", "", dictParams={"DDR": True})
_rate.baudRate = bitRate_1G * (10.0 / 8.0) / 2

_rate = addRate("XGMII", "", dictParams={"DDR": True})
_rate.lines = 32
_rate.baudRate = bitRate_10G / (_rate.lines * 2)

_rate = addRate("XAUI", "", coding="8B/10B")
_rate.lines = 4
_rate.baudRate = bitRate_10G * (10.0 / 8.0) / _rate.lines

_rate = addRate("XLAUI", "", coding="64B/66B")
_rate.lines = 4
_rate.baudRate = bitRate_40G * (66.0 / 64.0) / _rate.lines

_rate = addRate("CAUI-10", "", coding="64B/66B")
_rate.lines = 10
_rate.baudRate = bitRate_100G * (66.0 / 64.0) / _rate.lines

_rate = addRate("CAUI-4", "", coding="64B/66B")
_rate.lines = 4
_rate.baudRate = bitRate_100G * (66.0 / 64.0) / _rate.lines

_rate = addRate("25GAUI-1", "", coding="64B/66B")
_rate.lines = 1
_rate.baudRate = (bitRate_100G / 4) * (66.0 / 64.0)

_rate = addRate("50GAUI-1",
                "",
                modulation="PAM-4",
                dictParams={
                    "transcoding": "256B/257B",
                    "FEC": "RS(544,514,t=15,m=10)",
                })
_rate.lines = 1
_rate.baudRate = (bitRate_100G / 2) * (257.0 / 256.0) * (544.0 / 514.0)
_rate.baudRate = _rate.baudRate / 2  # PAM-4

_rate = addRate("100GAUI-2",
                "",
                modulation="PAM-4",
                dictParams={
                    "transcoding": "256B/257B",
                    "FEC": "RS(544,514,t=15,m=10)",
                })
_rate.lines = 2
_rate.baudRate = bitRate_100G * (257.0 / 256.0) * (544.0 / 514.0) / _rate.lines
_rate.baudRate = _rate.baudRate / 2  # PAM-4

_rate = addRate("200GAUI-4",
                "",
                modulation="PAM-4",
                dictParams={
                    "transcoding": "256B/257B",
                    "FEC": "RS(544,514,t=15,m=10)",
                })
_rate.lines = 4
_rate.baudRate = (2 * bitRate_100G) * (257.0 / 256.0) * (544.0 / 514.0) / _rate.lines
_rate.baudRate = _rate.baudRate / 2  # PAM-4

_rate = addRate("400GAUI-8",
                "",
                modulation="PAM-4",
                dictParams={
                    "transcoding": "256B/257B",
                    "FEC": "RS(544,514,t=15,m=10)",
                })
_rate.lines = 8
_rate.baudRate = (4 * bitRate_100G) * (257.0 / 256.0) * (544.0 / 514.0) / _rate.lines
_rate.baudRate = _rate.baudRate / 2  # PAM-4

_rate = addRate("OC-1")
_rate.baudRate = 51.84

_rate = addRate("OC-3")
_rate.baudRate = _dictRates["OC-1"].baudRate * 3

_rate = addRate("OC-12")
_rate.baudRate = _dictRates["OC-1"].baudRate * 12

_rate = addRate("OC-24")
_rate.baudRate = _dictRates["OC-1"].baudRate * 24

_rate = addRate("OC-48")
_rate.baudRate = _dictRates["OC-1"].baudRate * 48

_rate = addRate("OC-192")
_rate.baudRate = _dictRates["OC-1"].baudRate * 192

_rate = addRate("OC-768")
_rate.baudRate = _dictRates["OC-1"].baudRate * 768

_rate = addRate("OC-1920")
_rate.baudRate = _dictRates["OC-1"].baudRate * 1920

_rate = addRate("1GFC")
_rate.baudRate = 1062.5

_rate = addRate("2GFC")
_rate.baudRate = _dictRates["1GFC"].baudRate * 2

_rate = addRate("4GFC")
_rate.baudRate = _dictRates["1GFC"].baudRate * 4

_rate = addRate("8GFC")
_rate.baudRate = _dictRates["1GFC"].baudRate * 8

_rate = addRate("10GFC")
_rate.baudRate = 10518.75

_rate = addRate("16GFC")
_rate.baudRate = 14025.0

_rate = addRate("32GFC")
_rate.baudRate = _dictRates["16GFC"].baudRate * 2

_rate = addRate("64GFC")
_rate.baudRate = 28900.0

_rate = addRate("128GFC")
_rate.baudRate = _dictRates["16GFC"].baudRate * 8

_rate = addRate("256GFC")
_rate.baudRate = _dictRates["64GFC"].baudRate * 4

_rate = addRate("OTU1")
_rate.baudRate = _dictRates["OC-48"].baudRate * (255.0 / 238.0)

_rate = addRate("OTU2", "", dictParams={"xfpRate": "107"})
_rate.baudRate = _dictRates["OC-192"].baudRate * (255.0 / 237.0)

_rate = addRate("OTL3.4")
_rate.lines = 4
_rate.baudRate = _dictRates["OC-192"].baudRate * (255.0 / 236.0)

_rate = addRate("OTL4.10")
_rate.lines = 10
_rate.baudRate = _dictRates["OC-192"].baudRate * (255.0 / 227.0)

_rate = addRate("OTU3")
_rate.baudRate = _dictRates["OC-768"].baudRate * (255.0 / 236.0)

_rate = addRate("OTU4")
_rate.baudRate = _dictRates["OC-1920"].baudRate * (255.0 / 227.0)

_rate = addRate("OTUC1")
_rate.baudRate = _dictRates["OC-1920"].baudRate * (239.0 / 226.0)

_rate = addRate("OTUC2")
_rate.baudRate = _dictRates["OC-1920"].baudRate * 2 * (239.0 / 226.0)

_rate = addRate("OTUC4")
_rate.baudRate = _dictRates["OC-1920"].baudRate * 4 * (239.0 / 226.0)

_rate = addRate("OTL4.4")
_rate.lines = 4
_rate.baudRate = (_dictRates["OC-1920"].baudRate / 4) * (255.0 / 227.0)

_rate = addRate("OTL4.2", modulation="PAM-4")
_rate.lines = 2
_rate.baudRate = _dictRates["OTL4.4"].baudRate

_rate = addRate("FOIC1.4-RS")
_rate.lines = 4
_rate.baudRate = (_dictRates["OC-1920"].baudRate / 4) * (256.0 / 241.0) * (239.0 / 226.0)

_rate = addRate("FOIC2.8-RS")
_rate.lines = 8
_rate.baudRate = (_dictRates["OC-1920"].baudRate / 4) * (256.0 / 241.0) * (239.0 / 226.0)

_rate = addRate("FOIC4.16-RS")
_rate.lines = 16
_rate.baudRate = (_dictRates["OC-1920"].baudRate / 4) * (256.0 / 241.0) * (239.0 / 226.0)

_rate = addRate("FOIC1.2-RS")
_rate.lines = 2
_rate.baudRate = (_dictRates["OC-1920"].baudRate / 2) * (256.0 / 241.0) * (239.0 / 226.0)

_rate = addRate("FOIC2.4-RS")
_rate.lines = 4
_rate.baudRate = (_dictRates["OC-1920"].baudRate / 2) * (256.0 / 241.0) * (239.0 / 226.0)

_rate = addRate("FOIC4.8-RS")
_rate.lines = 4
_rate.baudRate = (_dictRates["OC-1920"].baudRate / 2) * (256.0 / 241.0) * (239.0 / 226.0)

_rate = addRate("ODU0")
_rate.baudRate = _dictRates["OC-24"].baudRate

_rate = addRate("ODU1")
_rate.baudRate = _dictRates["OC-48"].baudRate * (239.0 / 238.0)

_rate = addRate("ODU2", "", dictParams={"xfpRate": "99"})
_rate.baudRate = _dictRates["OC-192"].baudRate * (239.0 / 237.0)

_rate = addRate("ODU3")
_rate.baudRate = _dictRates["OC-768"].baudRate * (239.0 / 236.0)

_rate = addRate("ODU4")
_rate.baudRate = _dictRates["OC-1920"].baudRate * (239.0 / 227.0)

_rate = addRate("ODUC1")
_rate.baudRate = _dictRates["OC-1920"].baudRate * (239.0 / 226.0)

_rate = addRate("ODUC2")
_rate.baudRate = _dictRates["OC-1920"].baudRate * (2 * 239.0 / 226.0)

_rate = addRate("ODUC4")
_rate.baudRate = _dictRates["OC-1920"].baudRate * (4 * 239.0 / 226.0)

_rate = addRate("ODU2e")
_rate.baudRate = _dictRates["10GBASE-SR"].baudRate * (239.0 / 237.0)

_rate = addRate("InfiniBand_SDR", "", coding="8B/10B")
_rate.baudRate = 2000.0 * (10.0 / 8.0)

_rate = addRate("InfiniBand_DDR", "", coding="8B/10B")
_rate.baudRate = 4000.0 * (10.0 / 8.0)

_rate = addRate("InfiniBand_QDR", "", coding="8B/10B")
_rate.baudRate = 8000.0 * (10.0 / 8.0)

_rate = addRate("InfiniBand_FDR10", "", coding="64B/66B")
_rate.baudRate = 10000.0 * (66.0 / 64.0)

_rate = addRate("InfiniBand_FDR", "", coding="64B/66B")
_rate.baudRate = (1350000.0 / 99) * (66.0 / 64.0)

_rate = addRate("InfiniBand_EDR", "", coding="64B/66B")
_rate.baudRate = 25000.0 * (66.0 / 64.0)

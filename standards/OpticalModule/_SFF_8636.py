#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._constants import *
from . import _utils as u

class SFF_8636(object):

  # SFF-8636 rev 2.11 Table 6-4
  # yapf: disable
  dict_base_01_compliance = {
      0x00: "NA",
      0x01: "SFF-8436 Rev 4.8 or earlier",
      0x02: "SFF-8436 Rev 4.8 or earlier, except that this byte and Bytes 186-189 are as defined in SFF-8636",
      0x03: "SFF-8636 Rev 1.3 or earlier",
      0x04: "SFF-8636 Rev 1.4",
      0x05: "SFF-8636 Rev 1.5",
      0x06: "SFF-8636 Rev 2.0",
      0x07: "SFF-8636 Rev 2.5, 2.6 and 2.7",
      0x08: "SFF-8636 Rev 2.8, 2.9 and 2.10",
  }
  # yapf: enable

  # SFF-8636 rev 2.11 Table 6-12
  dict_base_87_rate_select_v1 = {
      0: "rate < 2.2 GBd",
      1: "2.2 GBd < rate < 6.6 GBd",
      2: "rate > 6.6 GBd",
      3: "reserved",
  }
  dict_base_87_rate_select_v2 = {
      0: "rate < 12 GBd",
      1: "12 GBd < rate < 24 GBd",
      2: "24 GBd < rate < 26 GBd",
      3: "rate > 26 GBd",
  }

  def __init__(self):
    self.base_data = 128 * [0xFF]
    self.up_00_data = 128 * [0xFF]  # ID
    self.infoBase = {}
    self.infoPg00 = {}

  def clear(self):
    self.base_data = 128 * [0xFF]
    self.up_00_data = 128 * [0xFF]  # ID
    self.infoBase = {}
    self.infoPg00 = {}

  def _parse_base_values(self):
    # SFF-8636 rev 2.11 Table 6-1
    self.infoBase["ModID"] = self.base_data[0]
    self.infoBase["Status"] = self.base_data[1:3]
    self.infoBase["ChStsIntFlags"] = self.base_data[3:6]
    self.infoBase["FreeSideIntFlags"] = self.base_data[6:9]
    self.infoBase["ChMonIntFlags"] = self.base_data[9:22]
    self.infoBase["FreeSideMon"] = self.base_data[22:34]
    self.infoBase["ChMon"] = self.base_data[34:82]
    self.infoBase["Ctrl"] = self.base_data[86:100]
    self.infoBase["FreeSideIntMask"] = self.base_data[100:107]
    self.infoBase["FreeSideProps"] = self.base_data[107:118]

  def _parse_base(self):
    tmp_parse = {"RevCompliance": self.dict_base_01_compliance[self.infoBase["Status"][0]]}
    bits = u.convertByte2Bits(self.infoBase["Status"][1])
    if (bits[2] == 0):
      tmp_parse["MemModel"] = "paged"
    else:
      tmp_parse["MemModel"] = "flat"
    tmp_parse["IntL"] = bits[1]
    tmp_parse["DataNotReady"] = bits[0]
    self.infoBase["Status"] = tmp_parse

    tmp_parse = {
        "Temp": u.convertTemp(self.infoBase["FreeSideMon"][0:2]),
        "VCC": u.convertVoltage(self.infoBase["FreeSideMon"][4:6]),
    }
    self.infoBase["FreeSideMon"] = tmp_parse

    tmp_parse = {
        "RX1Pow": u.convertPower(self.infoBase["ChMon"][0:2]),
        "RX2Pow": u.convertPower(self.infoBase["ChMon"][2:4]),
        "RX3Pow": u.convertPower(self.infoBase["ChMon"][4:6]),
        "RX4Pow": u.convertPower(self.infoBase["ChMon"][6:8]),
        "TX1Bias": u.convertBias(self.infoBase["ChMon"][8:10]),
        "TX2Bias": u.convertBias(self.infoBase["ChMon"][10:12]),
        "TX3Bias": u.convertBias(self.infoBase["ChMon"][12:14]),
        "TX4Bias": u.convertBias(self.infoBase["ChMon"][14:16]),
        "TX1Pow": u.convertPower(self.infoBase["ChMon"][16:18]),
        "TX2Pow": u.convertPower(self.infoBase["ChMon"][18:20]),
        "TX3Pow": u.convertPower(self.infoBase["ChMon"][20:22]),
        "TX4Pow": u.convertPower(self.infoBase["ChMon"][22:24]),
    }
    self.infoBase["ChMon"] = tmp_parse

    tmp_parse = {}
    bits = u.convertByte2Bits(self.infoBase["Ctrl"][0])
    tmp_parse["TX4Dis"] = bits[3]
    tmp_parse["TX3Dis"] = bits[2]
    tmp_parse["TX2Dis"] = bits[1]
    tmp_parse["TX1Dis"] = bits[0]
    bits = u.convertByte2Bits(self.infoBase["Ctrl"][1])
    tmp_parse["RX4RateSel"] = 2 * bits[7] + bits[6]
    tmp_parse["RX3RateSel"] = 2 * bits[5] + bits[4]
    tmp_parse["RX2RateSel"] = 2 * bits[3] + bits[2]
    tmp_parse["RX1RateSel"] = 2 * bits[1] + bits[0]
    bits = u.convertByte2Bits(self.infoBase["Ctrl"][3])
    tmp_parse["TX4RateSel"] = 2 * bits[7] + bits[6]
    tmp_parse["TX3RateSel"] = 2 * bits[5] + bits[4]
    tmp_parse["TX2RateSel"] = 2 * bits[3] + bits[2]
    tmp_parse["TX1RateSel"] = 2 * bits[1] + bits[0]
    bits = u.convertByte2Bits(self.infoBase["Ctrl"][7])
    tmp_parse["SWRst"] = bits[7]
    tmp_parse["HPwrCls8En"] = bits[3]
    tmp_parse["HPwrCls7-5En"] = bits[2]
    tmp_parse["PwrSet"] = bits[1]
    tmp_parse["PwrOverride"] = bits[0]
    bits = u.convertByte2Bits(self.infoBase["Ctrl"][12])
    tmp_parse["TX4CDR"] = bits[7]
    tmp_parse["TX3CDR"] = bits[6]
    tmp_parse["TX2CDR"] = bits[5]
    tmp_parse["TX1CDR"] = bits[4]
    tmp_parse["RX4CDR"] = bits[3]
    tmp_parse["RX3CDR"] = bits[2]
    tmp_parse["RX2CDR"] = bits[1]
    tmp_parse["RX1CDR"] = bits[0]
    bits = u.convertByte2Bits(self.infoBase["Ctrl"][13])
    tmp_parse["LP/TXDis"] = bits[1]
    tmp_parse["IntL/LOS"] = bits[0]
    self.infoBase["Ctrl"] = tmp_parse

    tmp_parse = {
        "MaxPwr": (self.infoBase["FreeSideProps"][0] * 0.1),
        "PropDelay": u.convertU16(self.infoBase["FreeSideProps"][1:3])
    }
    lst_bit_names = ["", "", "", "", "FarSideManaged", "", "", ""]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["FreeSideProps"][3], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    if (bits[4:8][::-1] == [0, 0, 0, 0]):
      tmp_parse["AdvLowPwrMode"] = "> 1.5 W"
    elif (bits[4:8][::-1] == [0, 0, 0, 1]):
      tmp_parse["AdvLowPwrMode"] = "< 1 W"
    elif (bits[4:8][::-1] == [0, 0, 1, 0]):
      tmp_parse["AdvLowPwrMode"] = "< 0.75 W"
    elif (bits[4:8][::-1] == [0, 0, 1, 1]):
      tmp_parse["AdvLowPwrMode"] = "< 0.5 W"
    else:
      tmp_parse["AdvLowPwrMode"] = "???"
    if (bits[0:3][::-1] == [0, 0, 0]):
      tmp_parse["MinOpVoltage"] = 3.3
    elif (bits[0:3][::-1] == [0, 0, 1]):
      tmp_parse["MinOpVoltage"] = 2.5
    elif (bits[0:3][::-1] == [0, 1, 0]):
      tmp_parse["MinOpVoltage"] = 1.8
    else:
      tmp_parse["MinOpVoltage"] = "???"
    tmp_parse["FarEndImpl"] = (self.infoBase["FreeSideProps"][6] >> 8) & 0x7
    tmp_parse["NearEndImpl"] = self.infoBase["FreeSideProps"][6] & 0x7
    tmp_parse["TXTurnOnMaxDuration"] = self.infoBase["FreeSideProps"][7] >> 16
    tmp_parse["DPInitMaxDuration"] = self.infoBase["FreeSideProps"][7] & 0x0F
    # yapf: disable
    tmp_parse["ModSelWaitTime"] = (self.infoBase["FreeSideProps"][8] & 0x0F) << (self.infoBase["FreeSideProps"][8] >> 16)
    # yapf: enable
    tmp_parse["SecExtSpecCompliance"] = self.infoBase["FreeSideProps"][9]
    tmp_parse["XceiverSubtype"] = dictXceiverSubType_QSFPDD[
        self.infoBase["FreeSideProps"][10] >> 16]
    tmp_parse["FiberFaceType"] = dictFiberFaceType[self.infoBase["FreeSideProps"][10] & 0x3]
    self.infoBase["FreeSideProps"] = tmp_parse

  def _parse_up_00_values(self):
    # SFF-8636 rev 2.11 Table 6-1
    self.infoPg00["ModID"] = self.up_00_data[0]
    self.infoPg00["ExtID"] = self.up_00_data[1]
    self.infoPg00["Connector"] = self.up_00_data[2]
    self.infoPg00["SpecCompliance"] = self.up_00_data[3:11]
    self.infoPg00["Encoding"] = self.up_00_data[11]
    self.infoPg00["SigRate"] = self.up_00_data[12]
    self.infoPg00["ExtRate"] = self.up_00_data[13]
    self.infoBase["LenSMF_km"] = self.base_data[14]
    self.infoBase["Length_OM3"] = self.base_data[15]
    self.infoBase["LenOM2"] = self.base_data[16]
    self.infoBase["LenOM1"] = self.base_data[17]
    self.infoBase["Length_cable"] = self.base_data[18]
    self.infoBase["Technology"] = self.base_data[19]
    self.infoPg00["VendorName"] = self.up_00_data[20:36]
    self.infoBase["ExtModule"] = self.base_data[36]
    self.infoPg00["VendorOUI"] = self.up_00_data[37:40]
    self.infoPg00["VendorPN"] = self.up_00_data[40:56]
    self.infoPg00["VendorRev"] = self.up_00_data[56:57]
    self.infoPg00["Wavelength"] = self.up_00_data[58:60]
    self.infoPg00["WavelengthTol"] = self.up_00_data[60:62]
    self.infoPg00["MaxCaseTemp"] = self.up_00_data[62]
    self.infoPg00["LinkCodes"] = self.up_00_data[64]
    self.infoPg00["Opts"] = self.up_00_data[65:68]
    self.infoPg00["VendorSN"] = self.up_00_data[68:84]
    self.infoPg00["VendorDate"] = self.up_00_data[84:92]
    self.infoPg00["DiagsMonType"] = self.up_00_data[92]
    self.infoPg00["EnhancedOpts"] = self.up_00_data[93]
    self.infoPg00["BaudRate"] = self.up_00_data[94]

  def _parse_up_00_vendor(self):
    self.infoPg00["VendorName"] = u.parseString(self.infoPg00["VendorName"])
    self.infoPg00["VendorOUI"] = u.parseOUI(self.infoPg00["VendorOUI"])
    self.infoPg00["VendorPN"] = u.parseString(self.infoPg00["VendorPN"])
    self.infoPg00["VendorRev"] = u.parseString(self.infoPg00["VendorRev"])
    self.infoPg00["VendorSN"] = u.parseString(self.infoPg00["VendorSN"])
    self.infoPg00["VendorDate"] = u.parseString(self.infoPg00["VendorDate"])

  def _parse_up_00(self):
    # TODO:
    self._parse_up_00_vendor()

  def parse_base(self, bRaw=False):
    if (len(self.base_data) != 128):
      logger.error("wrong size for base ({:d} bytes)".format(len(self.base_data)))
      return

    self._parse_base_values()
    if (not bRaw):
      self._parse_base()

  def parse_up_00(self, bRaw=False):
    if (len(self.up_00_data) != 128):
      logger.error("wrong size for page 0 ({:d} bytes)".format(len(self.up_00_data)))
      return

    self._parse_up_00_values()
    if (not bRaw):
      self._parse_up_00()

    crc = 0
    for i in range(0, 63):
      crc += self.up_00_data[i]
    if ((crc % 256) == self.up_00_data[63]):
      self.infoPg00["CRC_BASE"] = "OK"
    else:
      self.infoPg00["CRC_BASE"] = "FAIL"

    crc = 0
    for i in range(64, 95):
      crc += self.up_00_data[i]
    if ((crc % 256) == self.up_00_data[95]):
      self.infoPg00["CRC_EXT"] = "OK"
    else:
      self.infoPg00["CRC_EXT"] = "FAIL"

  def showInfo_base(self):
    logger.info("-- base")
    for k, v in sorted(self.infoBase.items()):
      logger.info("{:<16}: {}".format(k, v))

  def showInfo_up_00(self):
    logger.info("-- page 0x00")
    for k, v in sorted(self.infoPg00.items()):
      logger.info("{:<16}: {}".format(k, v))

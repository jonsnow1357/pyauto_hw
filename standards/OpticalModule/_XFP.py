#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._constants import *
from . import _utils as u

class XFPModule(object):

  A0_81_Power_Class = {
      0: "1 (1.5W max)",
      1: "2 (2.5W max)",
      2: "3 (3.5W max)",
      3: "4 (>3.5W)",
  }

  A0_8B_encoding = {
      0x01: "reserved",
      0x02: "reserved",
      0x04: "reserved",
      0x08: "RZ",
      0x10: "NRZ",
      0x20: "SONET scrambled",
      0x40: "8b/10b",
      0x80: "64b/66b",
  }

  A0_93_technology = {
      0: "850nm VCSEL",
      1: "1310nm VCSEL",
      2: "1550nm VCSEL",
      3: "1310nm FP",
      4: "1310nm DFB",
      5: "1550nm DFB",
      6: "1310nm EML",
      7: "1550nm EML",
      8: "Cu or others",
      9: "reserved",
      10: "reserved",
      11: "reserved",
      12: "reserved",
      13: "reserved",
      14: "reserved",
      15: "reserved",
  }

  A0_DE_auxiliary_AD = {
      0: "not implmented",
      1: "APD Bias Voltage",
      2: "reserved",
      3: "TEC Current",
      4: "Laser Temp",
      5: "Laser Wavelength",
      6: "+5V0 Voltage",
      7: "+3V3 Voltage",
      8: "+1V8 Voltage",
      9: "-5V2 Voltage",
      10: "+5V0 Current",
      11: "+3V3 Current",
      12: "not implmented",
      13: "not implmented",
      14: "+1V8 Current",
      15: "-5V2 Current",
  }

  def __init__(self):
    self.base_data = 128 * [0xFF]
    self.up_00_data = 128 * [0xFF]  # reserved
    self.up_01_data = 128 * [0xFF]  # ID, Vendor Specific Data
    self.infoBase = {}
    self.infoPg01 = {}

  def clear(self):
    self.base_data = 128 * [0xFF]
    self.up_00_data = 128 * [0xFF]  # reserved
    self.up_01_data = 128 * [0xFF]  # ID, Vendor Specific Data
    self.infoBase = {}
    self.infoPg01 = {}

  def _parse_base_values(self):
    # INF-8077 Table 31
    self.infoBase["ModID"] = self.base_data[0]
    self.infoBase["Sig_Conditioner"] = self.base_data[1]
    self.infoBase["Thresholds"] = self.base_data[2:58]
    self.infoBase["VPS_Ctrl"] = self.base_data[58:60]
    self.infoBase["BER"] = self.base_data[70:72]
    self.infoBase["Wavelength_Ctrl"] = self.base_data[72:76]
    self.infoBase["FEC_Ctrl"] = self.base_data[76:80]
    self.infoBase["Flags"] = self.base_data[80:96]
    self.infoBase["AD_Values"] = self.base_data[96:110]
    self.infoBase["StsCtrl"] = self.base_data[110:112]
    self.infoBase["StsCtrl"] = self.base_data[110:112]
    self.infoBase["PageSel"] = self.base_data[127]

  def _parse_base(self):
    if (self.infoBase["ModID"] in dictIdentifier.keys()):
      self.infoBase["ModID"] = dictIdentifier[self.infoBase["ModID"]]

    tmp_parse = {"Data Rate": (9.5 + (0.2 * (self.infoBase["Sig_Conditioner"] >> 4)))}
    bits = u.convertByte2Bits(self.infoBase["Sig_Conditioner"])
    if (bits[2] == 1):
      tmp_parse["Line Loopback"] = True
    else:
      tmp_parse["Line Loopback"] = False
    if (bits[1] == 1):
      tmp_parse["XFI Loopback"] = True
    else:
      tmp_parse["XFI Loopback"] = False
    if (bits[0] == 1):
      tmp_parse["REFCLK Mode"] = "Sync"
    else:
      tmp_parse["REFCLK Mode"] = "Async"
    if (len(tmp_parse) > 0):
      self.infoBase["Sig_Conditioner"] = tmp_parse

    # TODO: self.infoBase["VPS_Ctrl"]
    # TODO: self.infoBase["BER"]
    # TODO: self.infoBase["Wavelength_Ctrl"]
    # TODO: self.infoBase["FEC_Ctrl"]

    tmp_parse = {}
    lst_bit_names = [
        "TXDis", "Soft TXDis", "MOD_NR", "P_DOWN", "Soft P_DOWN", "!IRQ", "RXLOS",
        "AD NOT Ready"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["StsCtrl"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = ["TX_NR", "TX_Fault", "TX_CDR_LOL", "RX_NR", "RX_CDR_LOL", "", "", ""]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["StsCtrl"][1], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoBase["StsCtrl"] = tmp_parse

  def _parse_base_threshold(self):
    # yapf: disable
    lstTh = [
        "Temp_High_Alarm: {:.2f}C".format(u.convertTemp(self.infoBase["Thresholds"][0:2])),
        "Temp_Low_Alarm: {:.2f}C".format(u.convertTemp(self.infoBase["Thresholds"][2:4])),
        "Temp_High_Warn: {:.2f}C".format(u.convertTemp(self.infoBase["Thresholds"][4:6])),
        "Temp_Low_Warn: {:.2f}C".format(u.convertTemp(self.infoBase["Thresholds"][6:8])),
        "Bias_High_Alarm: {:.3f}mA".format(u.convertBias(self.infoBase["Thresholds"][16:18])),
        "Bias_Low_Alarm: {:.3f}mA".format(u.convertBias(self.infoBase["Thresholds"][18:20])),
        "Bias_High_Warn: {:.3f}mA".format(u.convertBias(self.infoBase["Thresholds"][20:22])),
        "Bias_Low_Warn: {:.3f}mA".format(u.convertBias(self.infoBase["Thresholds"][22:24])),
        #"TX_Pwr_High_Alarm: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][24:26])),
        #"TX_Pwr_Low_Alarm: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][26:28])),
        #"TX_Pwr_High_Warn: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][28:30])),
        #"TX_Pwr_Low_Warn: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][30:32])),
        #"RX_Pwr_High_Alarm: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][32:34])),
        #"RX_Pwr_Low_Alarm: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][34:36])),
        #"RX_Pwr_High_Warn: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][36:38])),
        #"RX_Pwr_Low_Warn: {:.3f}mW".format(u.convertPower(self.infoBase["Thresholds"][38:40])),
        "TX_Pwr_High_Alarm: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][24:26])),
        "TX_Pwr_Low_Alarm: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][26:28])),
        "TX_Pwr_High_Warn: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][28:30])),
        "TX_Pwr_Low_Warn: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][30:32])),
        "RX_Pwr_High_Alarm: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][32:34])),
        "RX_Pwr_Low_Alarm: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][34:36])),
        "RX_Pwr_High_Warn: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][36:38])),
        "RX_Pwr_Low_Warn: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["Thresholds"][38:40])),
    ]
    # yapf: enable
    self.infoBase["Thresholds"] = lstTh

  def _parse_base_flags(self):
    tmp_parse = {}
    lst_bit_names = [
        "Temp_High_Alarm", "Temp_Low_Alarm", "", "", "TX_Bias_High_Alarm",
        "TX_Bias_Low_Alarm", "TX_Pwr_High_Alarm", "TX_Pwr_High_Alarm"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "RX_Pwr_High_Alarm", "RX_Pwr_Low_Alarm", "AUX1_High_Alarm", "AUX1_Low_Alarm",
        "AUX2_High_Alarm", "AUX2_Low_Alarm", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][1], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "Temp_High_Warn", "Temp_Low_Warn", "", "", "TX_Bias_High_Warn", "TX_Bias_Low_Warn",
        "TX_Pwr_High_Warn", "TX_Pwr_High_Warn"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][2], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "RX_Pwr_High_Warn", "RX_Pwr_Low_Warn", "AUX1_High_Warn", "AUX1_Low_Warn",
        "AUX2_High_Warn", "AUX2_Low_Warn", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][3], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "TX_NR", "TX_Fault", "TX_CDR_LOL", "RX_NR", "RXLOS", "RX_CDR_LOL", "MOD_NR",
        "Reset_Complete"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][4], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "APD_Supply_Fault", "TEC_Fault", "Wavelength_Unlocked", "", "", "", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][5], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "VCC5_High_Alarm", "VCC5_Low_Alarm", "VCC3_High_Alarm", "VCC3_Low_Alarm",
        "VCC2_High_Alarm", "VCC2_Low_Alarm", "Vee5_High_Alarm", "Vee5_Low_Alarm"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][6], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "VCC5_High_Warn", "VCC5_Low_Warn", "VCC3_High_Warn", "VCC3_Low_Warn",
        "VCC2_High_Warn", "VCC2_Low_Warn", "Vee5_High_Warn", "Vee5_Low_Warn"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Flags"][7], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoBase["Flags"] = tmp_parse

  def _parse_base_diagnostic(self):
    tmp_parse = [
        "Temp: {:.2f}C".format(u.convertTemp(self.infoBase["AD_Values"][0:2])),
        "TX_Bias: {:.3f}mA".format(u.convertBias(self.infoBase["AD_Values"][4:6])),
        "TX_Pwr: {:.3f}mW".format(u.convertPower(self.infoBase["AD_Values"][6:8])),
        "RX_Pwr: {:.3f}mW".format(u.convertPower(self.infoBase["AD_Values"][8:10])),
        #"TX_Pwr: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["AD_Values"][6:8])),
        #"RX_Pwr: {:.3f}dBm".format(u.convertPower_dBm(self.infoBase["AD_Values"][8:10])),
    ]

    self.infoBase["AD_Values"] = tmp_parse

  def parse_base(self, bRaw=False):
    if (len(self.base_data) != 128):
      logger.error("wrong size for base ({:d} bytes)".format(len(self.base_data)))
      return

    self._parse_base_values()
    if (not bRaw):
      self._parse_base()
      self._parse_base_threshold()
      self._parse_base_diagnostic()
      self._parse_base_flags()

  def _parse_up_01_values(self):
    # INF-8077 Table 64
    self.infoPg01["ModID"] = self.up_01_data[0]
    self.infoPg01["ExtId"] = self.up_01_data[1]
    self.infoPg01["Connector"] = self.up_01_data[2]
    self.infoPg01["Transceiver"] = self.up_01_data[3:11]
    self.infoPg01["Encoding"] = self.up_01_data[11]
    self.infoPg01["BR_Min"] = self.up_01_data[12]
    self.infoPg01["BR_Max"] = self.up_01_data[13]
    self.infoPg01["LenSMF_km"] = self.up_01_data[14]
    self.infoPg01["Length_E50um"] = self.up_01_data[15]
    self.infoPg01["Length_50um"] = self.up_01_data[16]
    self.infoPg01["Length_62.5um"] = self.up_01_data[17]
    self.infoPg01["Length_cable"] = self.up_01_data[18]
    self.infoPg01["Technology"] = self.up_01_data[19]
    self.infoPg01["VendorName"] = self.up_01_data[20:36]
    self.infoPg01["CDR_Support"] = self.up_01_data[36]
    self.infoPg01["VendorOUI"] = self.up_01_data[37:40]
    self.infoPg01["VendorPN"] = self.up_01_data[40:56]
    self.infoPg01["VendorRev"] = self.up_01_data[56:58]
    self.infoPg01["Wavelength"] = self.up_01_data[58:60]
    self.infoPg01["Wavelength_Tol"] = self.up_01_data[60:62]
    self.infoPg01["Max_Case_Temp"] = self.up_01_data[62]
    self.infoPg01["Power_Supply"] = self.up_01_data[64:68]
    self.infoPg01["VendorSN"] = self.up_01_data[68:84]
    self.infoPg01["VendorDate"] = self.up_01_data[84:92]
    self.infoPg01["DiagsMonType"] = self.up_01_data[92]
    self.infoPg01["EnhancedOpts"] = self.up_01_data[93]
    self.infoPg01["Aux_Monitoring"] = self.up_01_data[94]

  def _parse_up_01_vendor(self):
    self.infoPg01["VendorName"] = u.parseString(self.infoPg01["VendorName"])
    self.infoPg01["VendorOUI"] = u.parseOUI(self.infoPg01["VendorOUI"])
    self.infoPg01["VendorPN"] = u.parseString(self.infoPg01["VendorPN"])
    self.infoPg01["VendorRev"] = u.parseString(self.infoPg01["VendorRev"])
    self.infoPg01["VendorSN"] = u.parseString(self.infoPg01["VendorSN"])
    self.infoPg01["VendorDate"] = u.parseString(self.infoPg01["VendorDate"])

  def _parse_up_01(self):
    if (self.infoPg01["ModID"] in dictIdentifier.keys()):
      self.infoPg01["ModID"] = dictIdentifier[self.infoPg01["ModID"]]

    tmp_parse = {
        "Options": [],
        "Power Class": self.A0_81_Power_Class[self.infoPg01["ExtId"] >> 6]
    }
    bits = u.convertByte2Bits(self.infoPg01["ExtId"])
    if (bits[5] == 1):
      tmp_parse["Options"].append("NO CDR")
    else:
      tmp_parse["Options"].append("CDR")
    if (bits[4] == 1):
      tmp_parse["Options"].append("NO TXREFCLK")
    else:
      tmp_parse["Options"].append("TXREFCLK")
    if (bits[3] == 1):
      tmp_parse["Options"].append("CLEI")
    else:
      tmp_parse["Options"].append("NO CLEI")
    if (len(tmp_parse) > 0):
      self.infoPg01["ExtId"] = tmp_parse

    if (self.infoPg01["Connector"] in dictConnector.keys()):
      self.infoPg01["Connector"] = dictConnector[self.infoPg01["Connector"]]

    # INF-8077 Table 49
    tmp_parse = {}
    lst_bit_names = [
        "10GBASE-SR", "10GBASE-LR", "10GBASE-ER", "10GBASE-LRM", "10GBASE-SW", "10GBASE-LW",
        "10GBASE-EW", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["Transceiver"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "1200-MX-SN-I", "1200-SM-LL-L", "10GFC ER 1550nm", "10GFC IR 1300nm FP", "", "", "",
        ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["Transceiver"][1], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "1000BASE-SX/1xFC MMF", "1000BASE-LX/1xFC SMF", "2xFC MMF", "2xFC SMF", "OC-48 SR",
        "OC-48 IR", "OC-48 LR", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["Transceiver"][3], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "SONET I-64.1r", "SONET I-64.1", "SONET I-64.2r", "SONET I-64.2", "SONET I-64.3",
        "SONET I-64.5", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["Transceiver"][4], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "SONET S-64.1", "SONET S-64.2a", "SONET S-64.2b", "SONET S-64.3a", "SONET S-64.3b",
        "SONET S-64.5a", "SONET S-64.5b", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["Transceiver"][5], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "SONET L-64.1", "SONET L-64.2a", "SONET L-64.2b", "SONET L-64.2c", "SONET L-64.3",
        "G.959.1 P1L1-2D2", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["Transceiver"][6], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = ["SONET V-64.2a", "SONET V-64.2b", "SONET V-64.3", "", "", "", "", ""]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["Transceiver"][7], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoPg01["Transceiver"] = tmp_parse

    if (self.infoPg01["Encoding"] in self.A0_8B_encoding.keys()):
      self.infoPg01["Encoding"] = self.A0_8B_encoding[self.infoPg01["Encoding"]]

    self.infoPg01["BR_Max"] = "{:d}Mbps".format(self.infoPg01["BR_Max"] * 100)
    self.infoPg01["BR_Min"] = "{:d}Mbps".format(self.infoPg01["BR_Min"] * 100)

    self.infoPg01["LenSMF_km"] = "{:d}km".format(self.infoPg01["LenSMF_km"])
    self.infoPg01["Length_E50um"] = "{:d}m".format(self.infoPg01["Length_E50um"] * 2 * 100)
    self.infoPg01["Length_50um"] = "{:d}m".format(self.infoPg01["Length_50um"] * 10)
    self.infoPg01["Length_62.5um"] = "{:d}m".format(self.infoPg01["Length_62.5um"] * 10)
    self.infoPg01["Length_cable"] = "{:d}m".format(self.infoPg01["Length_cable"])

    tmp_parse = [self.A0_93_technology[self.infoPg01["Technology"] >> 4]]
    bits = u.convertByte2Bits(self.infoPg01["Technology"])
    if (bits[3] == 1):
      tmp_parse.append("Active Wavelength Ctrl")
    if (bits[2] == 1):
      tmp_parse.append("Cooled TX")
    else:
      tmp_parse.append("Uncooled TX")
    if (bits[1] == 1):
      tmp_parse.append("APD Detector")
    else:
      tmp_parse.append("PIN Detector")
    if (bits[0] == 1):
      tmp_parse.append("Tunable TX")
    if (len(tmp_parse) > 0):
      self.infoPg01["Technology"] = tmp_parse

    tmp_parse = {"CDR": [], "Loopback Support": []}
    bits = u.convertByte2Bits(self.infoPg01["CDR_Support"])
    if (bits[7] == 1):
      tmp_parse["CDR"].append("9.95Gbps")
    if (bits[6] == 1):
      tmp_parse["CDR"].append("10.3Gbps")
    if (bits[5] == 1):
      tmp_parse["CDR"].append("10.5Gbps")
    if (bits[4] == 1):
      tmp_parse["CDR"].append("10.7Gbps")
    if (bits[3] == 1):
      tmp_parse["CDR"].append("11.1Gbps")
    if (bits[1] == 1):
      tmp_parse["Loopback Support"].append("Line")
    if (bits[0] == 1):
      tmp_parse["Loopback Support"].append("XFI")
    if (len(tmp_parse) > 0):
      self.infoPg01["CDR_Support"] = tmp_parse

    tmp = ((self.infoPg01["Wavelength"][0] << 8) + self.infoPg01["Wavelength"][1]) // 20
    self.infoPg01["Wavelength"] = "{:d}nm".format(tmp)
    tmp = ((self.infoPg01["Wavelength_Tol"][0] << 8)
           + self.infoPg01["Wavelength_Tol"][1]) // 200
    self.infoPg01["Wavelength_Tol"] = "+/-{:d}nm".format(tmp)
    self.infoPg01["Max_Case_Temp"] = "{:d}C".format(self.infoPg01["Max_Case_Temp"])

    tmp_parse = {
        "MaxPower": self.infoPg01["Power_Supply"][0] * 20.0e-3,
        "MaxPower_PD": self.infoPg01["Power_Supply"][1] * 10.0e-3,
        "MaxCur_+5V0": (self.infoPg01["Power_Supply"][2] // 16) * 50.0e-3,
        "MaxCur_+3V3": (self.infoPg01["Power_Supply"][2] % 16) * 100.0e-3,
        "MaxCur_+1V8": (self.infoPg01["Power_Supply"][3] // 16) * 100.0e-3,
        "MaxCur_-5V2": (self.infoPg01["Power_Supply"][3] % 16) * 50.0e-3,
    }
    if (len(tmp_parse) > 0):
      self.infoPg01["Power_Supply"] = tmp_parse

    tmp_parse = []
    bits = u.convertByte2Bits(self.infoPg01["DiagsMonType"])
    if (bits[4] == 1):
      tmp_parse.append("BER Support")
    if (bits[3] == 1):
      tmp_parse.append("RX Power Measurement: AVG")
    else:
      tmp_parse.append("RX Power Measurement: OMA")
    if (len(tmp_parse) > 0):
      self.infoPg01["DiagsMonType"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "VPS Support", "Soft TXDis", "Soft P_DOWN", "VPS LV Regulator",
        "VPS Bypass Regulator", "Active FEC Ctrl", "Tunable", "CMU Support Mode"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoPg01["EnhancedOpts"], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoPg01["EnhancedOpts"] = tmp_parse

    tmp_parse = {}
    tmp = self.infoPg01["Aux_Monitoring"] // 16
    tmp_parse["Aux1"] = self.A0_DE_auxiliary_AD[tmp]
    tmp = self.infoPg01["Aux_Monitoring"] % 16
    tmp_parse["Aux2"] = self.A0_DE_auxiliary_AD[tmp]
    if (len(tmp_parse) > 0):
      self.infoPg01["Aux_Monitoring"] = tmp_parse

    self._parse_up_01_vendor()

  def parse_up_00(self, bRaw=False):
    if (len(self.up_00_data) != 128):
      logger.error("wrong size for page 0 ({:d} bytes)".format(len(self.up_00_data)))
      return

    # TODO:

  def parse_up_01(self, bRaw=False):
    if (len(self.up_01_data) != 128):
      logger.error("wrong size for page 1 ({:d} bytes)".format(len(self.up_01_data)))
      return

    self._parse_up_01_values()
    if (not bRaw):
      self._parse_up_01()

    crc = 0
    for i in range(0, 63):
      crc += self.up_01_data[i]
    if ((crc % 256) == self.up_01_data[63]):
      self.infoPg01["CRC_BASE"] = "OK"
    else:
      self.infoPg01["CRC_BASE"] = "FAIL"

    crc = 0
    for i in range(64, 95):
      crc += self.up_01_data[i]
    if ((crc % 256) == self.up_01_data[95]):
      self.infoPg01["CRC_EXT"] = "OK"
    else:
      self.infoPg01["CRC_EXT"] = "FAIL"

  def showInfo_base(self):
    logger.info("-- base")
    for k, v in sorted(self.infoBase.items()):
      logger.info("{:<16}: {}".format(k, v))
    for k, v in sorted(self.infoBase.items()):
      logger.info("{:<16}: {}".format(k, v))

  def showInfo_up_00(self):
    #logger.info("-- page 0x00")
    pass  # TODO:

  def showInfo_up_01(self):
    logger.info("-- page 0x01")
    for k, v in sorted(self.infoPg01.items()):
      logger.info("{:<16}: {}".format(k, v))

#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import itertools

logger = logging.getLogger("lib")
from ._constants import *
from . import _utils as u

class SFF_8472(object):
  # SFF-8472 rev 12.4 Table 5-3
  dict_base_01_ModID_extended = {
      0x00: "GBIC not specified",
      0x01: "GBIC MOD_DEF_1",
      0x02: "GBIC MOD_DEF_2",
      0x03: "GBIC MOD_DEF_3",
      0x04: "two-wire interface",
      0x05: "GBIC MOD_DEF_5",
      0x06: "GBIC MOD_DEF_6",
      0x07: "GBIC MOD_DEF_7",
  }

  # SFF-8472 rev 12.4 Table 5-6
  dict_base_13_rate_identifier = {
      0x00: "unspecified",
      0x01: "SFF-8079",
      0x02: "SFF-8431 RX Rate_Sel only",
      0x03: "unspecified",
      0x04: "SFF-8431 TX Rate_Sel only",
      0x05: "unspecified",
      0x06: "SFF-8431 Independent RX, TX Rate_Sel",
      0x07: "unspecified",
      0x08: "FC-PI-5 RX Rate_Sel only",
      0x09: "unspecified",
      0x0A: "FC-PI-5 Independent RX, TX Rate_Sel",
      0x0B: "unspecified",
      0x0C: "FC-PI-6 Independent RX, TX Rate_Sel",
      0x0D: "unspecified",
      0x0E: "10G RX and TX Rate_Sel",
      0x0F: "unspecified",
      0x10: "FC-PI-7 Independent RX, TX Rate_Sel",
      0x11: "unspecified",
      0x20: "based on PMDs",
  }

  # SFF-8472 rev 12.4 Table 8-8
  dict_base_94_compliance = {
      0x00: "unspecified",
      0x01: "SFF-8472 Rev 9.3",
      0x02: "SFF-8472 Rev 9.5",
      0x03: "SFF-8472 Rev 10.2",
      0x04: "SFF-8472 Rev 10.4",
      0x05: "SFF-8472 Rev 11.0",
      0x06: "SFF-8472 Rev 11.3",
      0x07: "SFF-8472 Rev 11.4",
      0x08: "SFF-8472 Rev 12.3",
      0x09: "SFF-8472 Rev 12.4",
  }

  def __init__(self):
    self.base_data = 128 * [0xFF]
    self.ext_data = 128 * [0xFF]
    self.infoBase = {}
    self.infoExt = {}

  def clear(self):
    self.base_data = 128 * [0xFF]
    self.ext_data = 128 * [0xFF]
    self.infoBase = {}
    self.infoExt = {}

  def updateDiagMon(self, lstVal):
    if (len(lstVal) != 24):
      logger.error("wrong size for list ({:d} bytes)".format(len(lstVal)))
      return

    self.ext_data = self.ext_data[:96] + lstVal + self.ext_data[120:]

  def _parse_base_values(self):
    # SFF-8472 rev 12.4 Table 4-1
    self.infoBase["ModID"] = self.base_data[0]
    self.infoBase["ExtId"] = self.base_data[1]
    self.infoBase["Connector"] = self.base_data[2]
    # yapf: disable
    self.infoBase["Transceiver"] = self.base_data[3:11] + [self.base_data[36]] + [self.base_data[62]]
    # yapf: enable
    self.infoBase["Encoding"] = self.base_data[11]
    self.infoBase["BaudRate"] = self.base_data[12]
    self.infoBase["RateID"] = self.base_data[13]
    self.infoBase["LenSMF_km"] = self.base_data[14]
    self.infoBase["LenSMF"] = self.base_data[15]
    self.infoBase["LenOM2"] = self.base_data[16]
    self.infoBase["LenOM1"] = self.base_data[17]
    self.infoBase["LenOM4orCu"] = self.base_data[18]
    self.infoBase["LenOM3orCu"] = self.base_data[19]
    self.infoBase["VendorName"] = self.base_data[20:36]
    self.infoBase["VendorOUI"] = self.base_data[37:40]
    self.infoBase["VendorPN"] = self.base_data[40:56]
    self.infoBase["VendorRev"] = self.base_data[56:60]
    self.infoBase["Wavelength"] = self.base_data[60:62]

    self.infoBase["Opts"] = self.base_data[64:66]
    self.infoBase["SigRateMax"] = self.base_data[66]
    self.infoBase["SigRateMin"] = self.base_data[67]
    self.infoBase["VendorSN"] = self.base_data[68:84]
    self.infoBase["VendorDate"] = self.base_data[84:92]
    self.infoBase["DiagsMonType"] = self.base_data[92]
    self.infoBase["EnhancedOpts"] = self.base_data[93]
    self.infoBase["Compliance"] = self.base_data[94]

  def _parse_base_transceiver(self):
    # SFF-8472 rev 12.4 Table 5-3
    tmp_parse = {}
    lst_bit_names = [
        "10GBASE-ER", "10GBASE-LRM", "10GBASE-LR", "10GBASE-SR", "Infiniband_1X_SX",
        "Infiniband_1X_LX", "Infiniband_1X_Cu_active", "Infiniband_1X_Cu_passive"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Transceiver"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "ESCON_MMF_1310nm_LED", "ESCON_SMF_1310nm_laser", "", "", "", "", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Transceiver"][1], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    reachId = 2 * bits[4] + bits[3]
    SONET_SR = ["SR", None, "SR-1", None]
    SONET_IR = [None, "IR-2", "IR-1", None]
    SONET_LR = [None, "LR-2", "LR-1", "LR-3"]
    if (bits[5] == 1):
      tmp_parse["OC-192 {}".format(SONET_SR[reachId])] = True
    if (bits[2] == 1):
      tmp_parse["OC-48 {}".format(SONET_LR[reachId])] = True
    if (bits[1] == 1):
      tmp_parse["OC-48 {}".format(SONET_IR[reachId])] = True
    if (bits[0] == 1):
      tmp_parse["OC-48 {}".format(SONET_SR[reachId])] = True

    bits = u.convertByte2Bits(self.infoBase["Transceiver"][2])
    if (bits[6] == 1):
      tmp_parse["OC-12 single mode {}".format(SONET_LR[reachId])] = True
    if (bits[5] == 1):
      tmp_parse["OC-12 single mode {}".format(SONET_IR[reachId])] = True
    if (bits[4] == 1):
      tmp_parse["OC-12 {}".format(SONET_SR[reachId])] = True
    if (bits[2] == 1):
      tmp_parse["OC-3 single mode {}".format(SONET_LR[reachId])] = True
    if (bits[1] == 1):
      tmp_parse["OC-3 single mode {}".format(SONET_IR[reachId])] = True
    if (bits[0] == 1):
      tmp_parse["OC-3 {}".format(SONET_SR[reachId])] = True

    lst_bit_names = [
        "BASE-PX", "BASE-BX10", "100BASE-FX", "100BASE-LX/LX10", "1000BASE-T",
        "1000BASE-CX", "1000BASE-LX", "1000BASE-SX"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Transceiver"][3], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    FC_length = []
    FC_tech = []
    FC_media = []
    FC_speed = []
    lst_bit_info = ["V", "S", "I", "L", "M", None, None, None]
    bits = u.convertByte2Bits(self.infoBase["Transceiver"][4])
    for t in itertools.compress(lst_bit_info[::-1], bits):
      if (t is not None):
        FC_length.append(t)

    lst_bit_info = [None, None, None, None, None, "SA", "LC", "EL"]
    bits = u.convertByte2Bits(self.infoBase["Transceiver"][4])
    for t in itertools.compress(lst_bit_info[::-1], bits):
      if (t is not None):
        FC_tech.append(t)

    lst_bit_info = ["EL", "SN", "SL", "LL", None, None, None, None]
    bits = u.convertByte2Bits(self.infoBase["Transceiver"][5])
    for t in itertools.compress(lst_bit_info[::-1], bits):
      if (t is not None):
        FC_tech.append(t)

    lst_bit_info = ["TW", "TP", "MI", "TV", "M6", "MSE", None, "SM"]
    bits = u.convertByte2Bits(self.infoBase["Transceiver"][6])
    for t in itertools.compress(lst_bit_info[::-1], bits):
      if (t is not None):
        FC_media.append(t)

    lst_bit_info = ["1200M", "800M", "1600M", "400M", "3200M", "200M", None, "100M"]
    bits = u.convertByte2Bits(self.infoBase["Transceiver"][7])
    for t in itertools.compress(lst_bit_info[::-1], bits):
      if (t is not None):
        FC_speed.append(t)
    if (bits[1] == 1):
      lst_bit_info = [None, None, None, None, None, None, None, "64G"]
      bits = u.convertByte2Bits(self.infoBase["Transceiver"][9])
      for t in itertools.compress(lst_bit_info[::-1], bits):
        if (t is not None):
          FC_speed.append(t)

    FC_desc = "FC-{}-{}-{}-{}".format(",".join(FC_speed), ",".join(FC_length),
                                      ",".join(FC_media), ",".join(FC_tech))
    tmp_parse[FC_desc] = True

    tmp_parse[dictExtSpecCompliance[self.infoBase["Transceiver"][8]]] = True
    self.infoBase["Transceiver"] = tmp_parse

  def _parse_base_wavelength(self):
    # SFF-8472 rev 12.4 Section 8-1
    bits = u.convertByte2Bits(self.base_data[8])
    SFPp_active_cable = bits[3]
    SFPp_passive_cable = bits[2]

    if ((SFPp_active_cable == 0) and (SFPp_passive_cable == 0)):
      #print("DBG", self.infoBase["Wavelength"])
      wv = 256 * self.infoBase["Wavelength"][0] + self.infoBase["Wavelength"][1]
      self.infoBase["Wavelength"] = "{:d}nm".format(wv)
    elif ((SFPp_active_cable == 1) and (SFPp_passive_cable == 0)):
      tmp_parse = {}

      lst_bit_names = [
          "", "", "", "", "FC-PI-4_limiting", "SFF-8431_limiting", "FC-PI-4_Appx_H",
          "SFF-8431_Appx_E"
      ]
      bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Wavelength"][1],
                                               lst_bit_names)
      tmp_parse.update(dict_bit_info)
      self.infoBase["Wavelength"] = tmp_parse
    elif ((SFPp_active_cable == 0) and (SFPp_passive_cable == 1)):
      tmp_parse = {}

      lst_bit_names = [
          "", "", "SFF-8461", "SFF-8461", "SFF-8461", "SFF-8461", "FC-PI-4_Appx_H",
          "SFF-8431_Appx_E"
      ]
      bits, dict_bit_info = u.convertByte2Dict(self.infoBase["Wavelength"][1],
                                               lst_bit_names)
      tmp_parse.update(dict_bit_info)
      self.infoBase["Wavelength"] = tmp_parse
      self.infoBase["Wavelength"] = tmp_parse
    else:
      msg = "CANNOT parse wavelength"
      logger.error(msg)
      raise RuntimeError(msg)

  def _parse_base_vendor(self):
    self.infoBase["VendorName"] = u.parseString(self.infoBase["VendorName"])
    self.infoBase["VendorOUI"] = u.parseOUI(self.infoBase["VendorOUI"])
    self.infoBase["VendorPN"] = u.parseString(self.infoBase["VendorPN"])
    self.infoBase["VendorRev"] = u.parseString(self.infoBase["VendorRev"])
    self.infoBase["VendorSN"] = u.parseString(self.infoBase["VendorSN"])
    self.infoBase["VendorDate"] = u.parseString(self.infoBase["VendorDate"])

  def _parse_base(self):
    if (self.infoBase["ModID"] in dictIdentifier.keys()):
      self.infoBase["ModID"] = dictIdentifier[self.infoBase["ModID"]]

    self.infoBase["ExtId"] = self.dict_base_01_ModID_extended[self.infoBase["ExtId"]]

    if (self.infoBase["Connector"] in dictConnector.keys()):
      self.infoBase["Connector"] = dictConnector[self.infoBase["Connector"]]

    self._parse_base_transceiver()

    if (self.infoBase["Encoding"] in dictEncoding_SFF_8472.keys()):
      self.infoBase["Encoding"] = dictEncoding_SFF_8472[self.infoBase["Encoding"]]

    self.infoBase["BaudRate"] = "{:d}Mbps".format(self.infoBase["BaudRate"] * 100)

    self.infoBase["RateID"] = self.dict_base_13_rate_identifier[self.infoBase["RateID"]]

    self.infoBase["LenSMF_km"] = "{:d}km".format(self.infoBase["LenSMF_km"])
    self.infoBase["LenSMF"] = "{:d}m".format(self.infoBase["LenSMF"] * 100)
    self.infoBase["LenOM2"] = "{:d}m".format(self.infoBase["LenOM2"] * 10)
    self.infoBase["LenOM1"] = "{:d}m".format(self.infoBase["LenOM1"] * 10)
    self.infoBase["LenOM4orCu"] = "{:d}m".format(self.infoBase["LenOM4orCu"])
    self.infoBase["LenOM3orCu"] = "{:d}m".format(self.infoBase["LenOM3orCu"] * 10)

    self._parse_base_wavelength()

    tmp_parse = {}

    bits = u.convertByte2Bits(self.infoBase["Opts"][0])
    if (bits[4] == 1):
      tmp_parse["MemModel"] = "paged"
    else:
      tmp_parse["MemModel"] = "flat"
    if (bits[3] == 1):
      tmp_parse["Retimer/CDR"] = True
    else:
      tmp_parse["Retimer/CDR"] = False
    if (bits[2] == 1):
      tmp_parse["Laser"] = "cooled"
    else:
      tmp_parse["Laser"] = "unspecified"
    if (bits[0] == 1):
      tmp_parse["RXOut"] = "linear"
    else:
      tmp_parse["RXOut"] = "unspecfied/PAM4"
    if (bits[1] == 0):
      tmp_parse["PwrLvl"] = 1
    else:
      if (bits[5] == 0):
        tmp_parse["PwrLvl"] = 2
      else:
        if (bits[6] == 0):
          tmp_parse["PwrLvl"] = 3
        else:
          tmp_parse["PwrLvl"] = 4
    bits = u.convertByte2Bits(self.infoBase["Opts"][1])
    if (bits[7] == 1):
      tmp_parse["RXDecisionThreshold"] = True
    else:
      tmp_parse["RXDecisionThreshold"] = False
    if (bits[7] == 1):
      tmp_parse["TXTunable"] = True
    else:
      tmp_parse["TXTunable"] = False
    if (bits[5] == 1):
      tmp_parse["RATE_SELECT"] = True
    else:
      tmp_parse["RATE_SELECT"] = False
    if (bits[4] == 1):
      tmp_parse["TXDis"] = True
    else:
      tmp_parse["TXDis"] = False
    if (bits[3] == 1):
      tmp_parse["TX_FAULT"] = True
    else:
      tmp_parse["TX_FAULT"] = False
    LOS = 2 * bits[2] + bits[1]
    if (bits[2] == 1):
      tmp_parse["LOS"] = "inverted"
    if (bits[1] == 1):
      tmp_parse["LOS"] = True
    else:
      tmp_parse["LOS"] = False
    self.infoBase["Opts"] = tmp_parse

    self.infoBase["SigRateMax"] = "{:d}%".format(self.infoBase["SigRateMax"])
    self.infoBase["SigRateMin"] = "{:d}%".format(self.infoBase["SigRateMin"])

    tmp_parse = {}
    lst_bit_names = [
        "LegacyDiags", "DiagsImplemented", "InternalCal", "ExternalCal", "",
        "AddressChange", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["DiagsMonType"], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if (bits[3] == 0):
      tmp_parse["RXPwrMeas"] = "OMA"
    else:
      tmp_parse["RXPwrMeas"] = "avg"
    self.infoBase["DiagsMonType"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "AlarmWarnFlags", "SoftTXDis", "SoftTX_FAULT", "SoftRXLOS", "SoftRATE_SELECT",
        "AppSelControl", "RateSelControl", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBase["EnhancedOpts"], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    self.infoBase["EnhancedOpts"] = tmp_parse

    if (self.infoBase["Compliance"] in self.dict_base_94_compliance.keys()):
      self.infoBase["Compliance"] = self.dict_base_94_compliance[
          self.infoBase["Compliance"]]

    self._parse_base_vendor()

  def _parse_ext_values(self):
    # SFF-8472 rev 12.4 Table 3.1a
    self.infoExt["Thresholds"] = self.ext_data[0:40]
    self.infoExt["OptThresholds"] = self.ext_data[40:56]
    self.infoExt["ExtCalibration"] = self.ext_data[56:92]
    self.infoExt["Diags"] = self.ext_data[96:106]
    self.infoExt["OptDiags"] = self.ext_data[106:110]
    self.infoExt["StsCtrl"] = self.ext_data[110]
    self.infoExt["AlarmFlags"] = self.ext_data[112:114]
    self.infoExt["TXInEqCtrl"] = self.ext_data[114]
    self.infoExt["RXOutEmphCtrl"] = self.ext_data[115]
    self.infoExt["WarnFlags"] = self.ext_data[116:118]
    self.infoExt["ExtStsCtrl"] = self.ext_data[118:120]

  def _parse_ext_threshold(self):
    lstTh = [
        "Temp_High_Alarm: {:.2f}C".format(u.convertTemp(self.infoExt["Thresholds"][0:2])),
        "Temp_Low_Alarm: {:.2f}C".format(u.convertTemp(self.infoExt["Thresholds"][2:4])),
        "Temp_High_Warn: {:.2f}C".format(u.convertTemp(self.infoExt["Thresholds"][4:6])),
        "Temp_Low_Warn: {:.2f}C".format(u.convertTemp(self.infoExt["Thresholds"][6:8])),
        "Volt_High_Alarm: {:.2f}V".format(u.convertVoltage(
            self.infoExt["Thresholds"][8:10])),
        "Volt_Low_Alarm: {:.2f}V".format(u.convertVoltage(
            self.infoExt["Thresholds"][10:12])),
        "Volt_High_Warn: {:.2f}V".format(u.convertVoltage(
            self.infoExt["Thresholds"][12:14])),
        "Volt_Low_Warn: {:.2f}V".format(u.convertVoltage(
            self.infoExt["Thresholds"][14:16])),
        "Bias_High_Alarm: {:.3f}mA".format(u.convertBias(
            self.infoExt["Thresholds"][16:18])),
        "Bias_Low_Alarm: {:.3f}mA".format(u.convertBias(self.infoExt["Thresholds"][18:20])),
        "Bias_High_Warn: {:.3f}mA".format(u.convertBias(self.infoExt["Thresholds"][20:22])),
        "Bias_Low_Warn: {:.3f}mA".format(u.convertBias(self.infoExt["Thresholds"][22:24])),
        #"TX_Pwr_High_Alarm: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][24:26])),
        #"TX_Pwr_Low_Alarm: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][26:28])),
        #"TX_Pwr_High_Warn: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][28:30])),
        #"TX_Pwr_Low_Warn: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][30:32])),
        #"RX_Pwr_High_Alarm: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][32:34])),
        #"RX_Pwr_Low_Alarm: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][34:36])),
        #"RX_Pwr_High_Warn: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][36:38])),
        #"RX_Pwr_Low_Warn: {:.3f}mW".format(u.convertPower(self.infoDiag["Thresholds"][38:40])),
        "TX_Pwr_High_Alarm: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][24:26])),
        "TX_Pwr_Low_Alarm: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][26:28])),
        "TX_Pwr_High_Warn: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][28:30])),
        "TX_Pwr_Low_Warn: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][30:32])),
        "RX_Pwr_High_Alarm: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][32:34])),
        "RX_Pwr_Low_Alarm: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][34:36])),
        "RX_Pwr_High_Warn: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][36:38])),
        "RX_Pwr_Low_Warn: {:.3f}dBm".format(
            u.convertPower_dBm(self.infoExt["Thresholds"][38:40])),
    ]

    self.infoExt["Thresholds"] = lstTh

  def _parse_ext_diagnostic(self):
    tmp_parse = [
        "Temp: {:.2f}C".format(u.convertTemp(self.infoExt["Diags"][0:2])),
        "Voltage: {:.2f}V".format(u.convertVoltage(self.infoExt["Diags"][2:4])),
        "TX_Bias: {:.3f}mA".format(u.convertBias(self.infoExt["Diags"][4:6])),
        "TX_Pwr: {:.3f}mW".format(u.convertPower(self.infoExt["Diags"][6:8])),
        "RX_Pwr: {:.3f}mW".format(u.convertPower(self.infoExt["Diags"][8:10])),
        #"TX_Pwr: {:.3f}dBm".format(u.convertPower_dBm(self.infoDiag["Diags"][6:8])),
        #"RX_Pwr: {:.3f}dBm".format(u.convertPower_dBm(self.infoDiag["Diags"][8:10])),
    ]

    self.infoExt["Diags"] = tmp_parse

  def _parse_ext(self):
    tmp_parse = {}
    lst_bit_names = [
        "TXDis", "Soft_TXDis", "RS1", "RS0", "Soft_RS0", "TXFault", "RXLOS", "DataNotReady"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoExt["StsCtrl"], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    self.infoExt["StsCtrl"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "Temp_High", "Temp_Low", "VCC_High", "VCC_Low", "TX_Bias_High", "TX_Bias_Low",
        "TX_Pwr_High", "TX_Pwr_Low"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoExt["AlarmFlags"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "RX_Pwr_High", "RX_Pwr_Low", "Laser_Temp_High", "Laser_Temp_Low", "TEC_High",
        "TEC_Low", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoExt["AlarmFlags"][1], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    self.infoExt["AlarmFlags"] = tmp_parse

    # TODO: self.infoExt["TXInEqCtrl"] = self.ext_data[114]
    # TODO: self.infoExt["RXOutEmphCtrl"] = self.ext_data[115]

    tmp_parse = {}
    lst_bit_names = [
        "Temp_High", "Temp_Low", "VCC_High", "VCC_Low", "TX_Bias_High", "TX_Bias_Low",
        "TX_Pwr_High", "TX_Pwr_Low"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoExt["WarnFlags"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "RX_Pwr_High", "RX_Pwr_Low", "Laser_Temp_High", "Laser_temp_Low", "TEC_High",
        "TEC_Low", "", ""
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoExt["WarnFlags"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    self.infoExt["WarnFlags"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "", "", "", "TXAdaptiveInputEQ", "Soft_RS1", "PwrLvl4En", "PwrLvlState", "PwrLvlSel"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoExt["ExtStsCtrl"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "", "", "", "PAM4TXMode", "PAM4RXMode", "64GFCMode", "TXCDRUnlocked",
        "RXCDRUnlocked"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoExt["ExtStsCtrl"][1], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    self.infoExt["ExtStsCtrl"] = tmp_parse

  def parse_base(self, bRaw=False):
    if (len(self.base_data) != 128):
      logger.error("wrong size for base ({:d} bytes)".format(len(self.base_data)))
      return

    self._parse_base_values()
    if (not bRaw):
      self._parse_base()

    crc = 0
    for i in range(0, 63):
      crc += self.base_data[i]
    if ((crc % 256) == self.base_data[63]):
      self.infoBase["CRC"] = "OK"
    else:
      self.infoBase["CRC"] = "FAIL"

    crc = 0
    for i in range(64, 95):
      crc += self.base_data[i]
    if ((crc % 256) == self.base_data[95]):
      self.infoBase["CRC"] = "OK"
    else:
      self.infoBase["CRC"] = "FAIL"

  def parse_ext(self, bRaw=False):
    if (len(self.ext_data) != 128):
      logger.error("wrong size for ext ({:d} bytes)".format(len(self.ext_data)))
      return

    self._parse_ext_values()
    if (not bRaw):
      self._parse_ext_threshold()
      self._parse_ext_diagnostic()
      self._parse_ext()

    crc = 0
    for i in range(0, 95):
      crc += self.ext_data[i]
    if ((crc % 256) == self.ext_data[95]):
      self.infoExt["CRC"] = "OK"
    else:
      self.infoExt["CRC"] = "FAIL"

  def showInfo_base(self):
    logger.info("-- base")
    for k, v in sorted(self.infoBase.items()):
      logger.info("{:<16}: {}".format(k, v))

  def showInfo_ext(self):
    logger.info("-- extension")
    for k, v in sorted(self.infoExt.items()):
      logger.info("{:<16}: {}".format(k, v))

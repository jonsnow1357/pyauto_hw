#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

import math
#import csv
import typing
import json
import itertools

logger = logging.getLogger("lib")
from ._constants import *
import pyauto_base.fs
import pyauto_base.bin

def convertByte2Bits(n: int) -> list[str]:
  bits = [int(t) for t in list("{0:08b}".format(n))]
  return bits[::-1]

def convertByte2Dict(byteVal: int, lstBitNames: list[str]):
  bits = [int(t) for t in list("{0:08b}".format(byteVal))]
  #print("DBG", byteVal, "->", bits)
  # yapf: disable
  dict_res = dict([(t, True) for t in itertools.compress(lstBitNames, bits) if ((t is not None) and (len(t) > 0))])
  # yapf: enable
  #print("DBG", dict_res)
  return [bits[::-1], dict_res]

def convertS8(byteVal: int) -> int:
  if ((byteVal < 0) or (byteVal > 255)):
    raise RuntimeError

  if (byteVal > 127):
    return (256 - byteVal)
  else:
    return byteVal

def convertU16(lstBytes: list[int]) -> int:
  if (len(lstBytes) != 2):
    raise RuntimeError
  for b in lstBytes:
    if ((b < 0) or (b > 255)):
      raise RuntimeError

  return ((lstBytes[0] * 256) + lstBytes[1])

def convertS16(lstBytes: list[int]) -> int:
  if (len(lstBytes) != 2):
    raise RuntimeError
  for b in lstBytes:
    if ((b < 0) or (b > 255)):
      raise RuntimeError

  tmp = "0x{:02x}{:02x}".format(lstBytes[0], lstBytes[1])
  return pyauto_base.bin.hex2int_2c(tmp, 16)

def convertTemp(lstBytes: list[int]) -> float:
  """
  returns temperature in C
  """
  return round((convertS16(lstBytes) / 256.0), 3)

def convertVoltage(lstBytes: list[int], lsb: float = 0.1e-3) -> float:
  """
  returns voltage in V
  """
  return round((convertU16(lstBytes) * lsb), 3)

def convertBias(lstBytes: list[int], lsb: float = 2.0e-3) -> float:
  """
  returns bias current in mA
  """
  return round((convertU16(lstBytes) * lsb), 3)

def convertPower(lstBytes: list[int], lsb: float = 0.1e-3) -> float:
  """
  returns power in mW
  """
  return round((convertU16(lstBytes) * lsb), 3)

def convertPower_dBm(lstBytes: list[int], lsb: float = 0.1e-3) -> float:
  """
  returns power in dBm
  """
  tmp = convertPower(lstBytes, lsb=lsb)
  if (tmp == 0.0):
    return float("-inf")
  else:
    return (10.0 * math.log10(tmp))

def parseString(lstBytes):
  return "".join([chr(t) for t in lstBytes])

def parseOUI(lstBytes):
  return "{:02x}:{:02x}:{:02x}".format(lstBytes[2], lstBytes[1], lstBytes[0])

def parseAppSel(lstBytes, mediaType):
  if (mediaType == 0x00):
    return lstBytes
  elif (mediaType == 0x01):
    media_if = dictMediaIfID_MMF[lstBytes[1]]
  elif (mediaType == 0x02):
    media_if = dictMediaIfID_SMF[lstBytes[1]]
  elif (mediaType == 0x03):
    media_if = dictMediaIfID_passive[lstBytes[1]]
  elif (mediaType == 0x04):
    media_if = dictMediaIfID_active[lstBytes[1]]
  elif (mediaType == 0x05):
    media_if = dictMediaIfID_BASE_T[lstBytes[1]]
  else:
    raise NotImplementedError
  host_if = dictHostIfID[lstBytes[0]]

  host_ln = lstBytes[2] // 16
  media_ln = lstBytes[2] & 0x0F

  res = "{} [{} ln] -> {} [{} ln], HostLnOpts 0x{:02x}".format(host_if, host_ln, media_if,
                                                               media_ln, lstBytes[3])
  return res

def CRC(lstBytes, idxStart, idxStop):
  crc = 0
  for i in range(idxStart, idxStop):
    crc += lstBytes[i]
  return (crc % 256)

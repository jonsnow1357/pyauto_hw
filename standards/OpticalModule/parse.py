#!/usr/bin/env python
# template: app_log
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""app"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
import logging.config
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv
import argparse
import json

logging.config.fileConfig("logging.cfg")
logger = logging.getLogger("app")
import pyauto_base.fs
import pyauto_hw.standards.OpticalModule as opt_mod

def _check(path):
  locs = []
  with open(path, "r") as f_in:
    for i, ln in enumerate(f_in):
      page = json.loads(ln.strip("\n\r"))
      if ((set(page.keys()) != {"location", "data"})):
        msg = "INCORRECT entry: {}".format(page)
        logger.error(msg)
        raise RuntimeError(msg)
      if (len(page["data"]) != 128):
        msg = "INCORRECT entry length: {} - {}".format(page["location"], len(page["data"]))
        logger.error(msg)
        raise RuntimeError(msg)
      if (i == 0):
        mod_id = page["data"][0]
      locs.append(page["location"])

  logger.info(opt_mod.dictIdentifier[mod_id])
  if (mod_id == 0x03):  # SFF-8472
    if (set(locs) != {"base", "ext"}):
      msg = "INCORRECT locations: {}".format(locs)
      logger.error(msg)
      raise RuntimeError(msg)
  elif (mod_id == 0x06):  # XFP
    if (set(locs) != {"base", "up_01"}):
      msg = "INCORRECT locations: {}".format(locs)
      logger.error(msg)
      raise RuntimeError(msg)
  elif (mod_id == 0x0E):  # CFP
    if (set(locs) != {"NVR1"}):
      msg = "INCORRECT locations: {}".format(locs)
      logger.error(msg)
      raise RuntimeError(msg)
  elif (mod_id in (0x0D, 0x11)):  # SFF-8636
    if (set(locs) != {"base", "up_00"}):
      msg = "INCORRECT locations: {}".format(locs)
      logger.error(msg)
      raise RuntimeError(msg)
  elif (mod_id in (0x18, 0x19)):  # CMIS
    if (set(locs) != {"base", "up_00_00", "up_00_01", "up_00_04"}):
      msg = "INCORRECT locations: {}".format(locs)
      logger.error(msg)
      raise RuntimeError(msg)
  else:
    msg = "unsupported id {}".format(mod_id)
    logger.error(msg)
    raise NotImplementedError(msg)

def _parse(path):
  dict_pages = {}
  with open(path, "r") as f_in:
    for i, ln in enumerate(f_in):
      page = json.loads(ln.strip("\n\r"))
      if (i == 0):
        mod_id = page["data"][0]
      dict_pages[page["location"]] = page["data"]

  if (mod_id == 0x03):
    sfp = opt_mod.SFF_8472()
    sfp.base_data = dict_pages["base"]
    sfp.ext_data = dict_pages["ext"]
    sfp.parse_base()
    sfp.parse_ext()
    sfp.showInfo_base()
    sfp.showInfo_ext()
  elif (mod_id == 0x06):
    xfp = opt_mod.XFPModule()
    xfp.base_data = dict_pages["base"]
    xfp.up_01_data = dict_pages["up_01"]
    xfp.parse_base()
    xfp.parse_up_00()
    xfp.parse_up_01()
    xfp.showInfo_base()
    xfp.showInfo_up_00()
    xfp.showInfo_up_01()
  elif (mod_id == 0x0E):
    cfp = opt_mod.CFPModule()
    cfp.NVR1_data = dict_pages["NVR1"]
    cfp.parse_NVR1()
    cfp.showInfo_NVR1()
  elif (mod_id in (0x0D, 0x11)):
    mod = opt_mod.SFF_8636()
    mod.base_data = dict_pages["base"]
    mod.up_00_data = dict_pages["up_00"]
    mod.parse_base()
    mod.parse_up_00()
    mod.showInfo_base()
    mod.showInfo_up_00()
  elif (mod_id in (0x18, 0x19)):
    cmis = opt_mod.CMIS()
    cmis.base_data = dict_pages["base"]
    cmis.up_00_00_data = dict_pages["up_00_00"]
    cmis.up_00_01_data = dict_pages["up_00_01"]
    cmis.up_00_04_data = dict_pages["up_00_04"]
    cmis.parse_base()
    cmis.parse_up_00_00()
    cmis.parse_up_00_01()
    cmis.parse_up_00_04()
    cmis.showInfo_base()
    cmis.showInfo_up_00_00()
    cmis.showInfo_up_00_01()
    cmis.showInfo_up_00_04()
  else:
    msg = "unsupported id {}".format(mod_id)
    logger.error(msg)
    raise NotImplementedError(msg)

def mainApp():
  pyauto_base.fs.chkPath_File(cliArgs["path"])

  _check(cliArgs["path"])
  _parse(cliArgs["path"])

if (__name__ == "__main__"):
  modName = os.path.basename(__file__)
  modName = ".".join(modName.split(".")[:-1])

  #print("[{}] {}".format(modName, sys.prefix))
  #print("[{}] {}".format(modName, sys.exec_prefix))
  #print("[{}] {}".format(modName, sys.path))
  #for arg in sys.argv:
  #  print("[{}] {}".format(modName, arg))

  #appDir = sys.path[0]  # folder where the script was invoked
  #appDir = os.getcwd()  # current folder
  #appCfgPath = os.path.join(appDir, (modName + ".cfg"))
  #print("[{}] {}".format(modName, appDir))
  #print("[{}] {}".format(modName, appCfgPath))
  #os.chdir(appDir)

  #pyauto_base.misc.changeLoggerName("{}.log".format(modName))

  appDesc = "parse Optical Module information"
  parser = argparse.ArgumentParser(description=appDesc)
  parser.add_argument("path", help="JSON file")
  #parser.add_argument("-f", "--cfg", default=appCfgPath,
  #                    help="configuration file path")
  #parser.add_argument("-l", "--list", action="store_true", default=False,
  #                    help="list config file options")
  #parser.add_argument("-x", "--extra",
  #                    choices=("", ""),
  #                    help="extra parameters")

  cliArgs = vars(parser.parse_args())
  #logger.info(cliArgs)

  #parser.print_help()
  mainApp()

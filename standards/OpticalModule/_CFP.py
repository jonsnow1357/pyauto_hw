#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._constants import *
from . import _utils as u
import pyauto_base.bin

class CFPModule(object):
  NVR1_01_Power_Class = {
      0: "1 (<8W)",
      1: "2 (<16W)",
      2: "3 (<32W)",
      3: "4 (<64W)",
  }

  NVR1_01_Lane_Ratio = {
      0: "1:n (Mux Type)",
      1: "n:m (Gearbox Type)",
      2: "n:n (Parallel)",
      3: "reserved",
  }

  NVR1_01_WDM = {
      0: "Non-WDM",
      1: "CWDM",
      2: "LANWDM",
      3: "DWDM (200G Grid)",
      4: "DWDM (100G Grid)",
      5: "DWDM (50G Grid)",
      6: "DWDM (25G Grid)",
      7: "Other WDM",
  }

  NVR1_02_Connector = {
      0x00: "unspecified",
      0x01: "SC",
      0x07: "LC",
      0x08: "MT-RJ",
      0x09: "MPO",
  }

  NVR1_03_AppCode_ETH = {
      0x01: ["100GE SMF 10km", "100GE-LR4"],
      0x02: ["100GE SMF 40km", "100GE-ER4"],
      0x03: ["100GE MMF 100m OM3", "100GE-SR10"],
      0x05: ["40GE SMF 10km", "40GE-LR4"],
      0x07: ["40GE MMF 100m OM3", "40GE-SR4"],
      0x0D: ["40GE-CR4 Cu"],
      0x0E: ["100GE-CR10 Cu"],
      0x0F: ["40G BASE-FR"],
  }

  NVR1_04_AppCode_FC = {}

  NVR1_05_AppCode_Cu = {}

  NVR1_06_AppCode_Sonet = {
      0x01: ["VSR2000-3R2"],
      0x02: ["VSR2000-3R3"],
      0x03: ["VSR2000-3R5"],
  }

  NVR1_07_AppCode_OTN = {
      0x01: ["VSR2000-3R2F"],
      0x02: ["VSR2000-3R3F"],
      0x03: ["VSR2000-3R5F"],
      0x04: ["VSR2000-3L2F"],
      0x05: ["VSR2000-3L3F"],
      0x06: ["VSR2000-3RL5"],
      0x07: ["C4S1-2D1 (OTL3.4)"],
      0x08: ["4I1-9D1F (OTL4.4)"],
      0x09: ["P1I1-3D1 (NRZ 40G 1300nm, 10km)"],
  }

  NVR1_0A_Media_Type = {
      0: "SMF",
      1: "MMF (OM3)",
      2: "reserved",
      3: "Cu",
  }

  NVR1_18_Laser_Source = {
      0: "VCSEL",
      1: "FP",
      2: "DFB",
      3: "DBR",
      4: "Cu",
      5: "External Cavity",
  }

  NVR1_18_TX_Modulation = {
      0: "DML",
      1: "EML",
      2: "InP-MZ",
      3: "LN-MZ",
      4: "Cu",
  }

  NVR1_19_Detector_Type = {
      0: "undefined/Coherent Detector",
      1: "PIN Detector",
      2: "APD Detector",
      3: "Optical Amp + PIN Detector",
  }

  NVR1_1A_Modulation = {
      0: "undefined",
      1: "NRZ",
      2: "RZ",
      3: "reserved",
  }

  NVR1_1A_Signal_Coding = {
      0: "Non-PSK",
      1: "ODB",
      2: "DPSK",
      3: "QPSK",
      4: "DQPSK",
      5: "DPQPSK",
      11: "16QAM",
      12: "64QAM",
      13: "256QAM",
  }

  NVR1_74_SignalSpec_Host = {
      0x00: "unspecified",
      0x01: "CAUI",
      0x02: "XLAUI",
      0x03: "SFI5.2",
      0x04: "SFI-S",
      0x05: "OTL3.4",
      0x06: "OTL4.10",
      0x07: "OTL4.4",
      0x08: "STL256.4",
  }

  def __init__(self):
    self.NVR1_data = []
    self.NVR2_data = []
    self.infoNVR1 = {}
    self.infoNVR2 = {}

  def clear(self):
    self.NVR1_data = []
    self.NVR2_data = []
    self.infoNVR1 = {}
    self.infoNVR2 = {}

  def _parse_NVR1_values(self):
    # CFP MSA Table 19
    self.infoNVR1["ModID"] = self.NVR1_data[0]
    self.infoNVR1["ExtId"] = self.NVR1_data[1]
    self.infoNVR1["Connector"] = self.NVR1_data[2]
    self.infoNVR1["AppCodes"] = self.NVR1_data[3:8]
    self.infoNVR1["AdditionalRates"] = self.NVR1_data[8]
    self.infoNVR1["Lanes"] = self.NVR1_data[9]
    self.infoNVR1["MediaProperties"] = self.NVR1_data[10]
    self.infoNVR1["BR_Max_Net_Lane"] = self.NVR1_data[11]
    self.infoNVR1["BR_Max_Host_Lane"] = self.NVR1_data[12]
    self.infoNVR1["LenSMF"] = self.NVR1_data[13]
    self.infoNVR1["Length_MMF"] = self.NVR1_data[14]
    self.infoNVR1["Length_cable"] = self.NVR1_data[15]
    self.infoNVR1["SpectralChar_TX"] = self.NVR1_data[16:18]
    self.infoNVR1["Wavelength_Min"] = self.NVR1_data[18:20]
    self.infoNVR1["Wavelength_Max"] = self.NVR1_data[20:22]
    self.infoNVR1["OpticalWidth_Max"] = self.NVR1_data[22:24]
    self.infoNVR1["DeviceTechnology"] = self.NVR1_data[24:26]
    self.infoNVR1["SignalCode"] = self.NVR1_data[26]
    self.infoNVR1["OpticalOutPwrConn_Max"] = self.NVR1_data[27]
    self.infoNVR1["OpticalInPwrLane_Max"] = self.NVR1_data[28]
    self.infoNVR1["Pwr_Max"] = self.NVR1_data[29]
    self.infoNVR1["LOPWR_Max"] = self.NVR1_data[30]
    self.infoNVR1["CaseTemperatureRange"] = self.NVR1_data[31:33]

    self.infoNVR1["VendorName"] = self.NVR1_data[33:49]
    self.infoNVR1["VendorOUI"] = self.NVR1_data[49:52]
    self.infoNVR1["VendorPN"] = self.NVR1_data[52:68]
    self.infoNVR1["VendorSN"] = self.NVR1_data[68:84]
    self.infoNVR1["VendorDate"] = self.NVR1_data[84:92]
    self.infoNVR1["Vendor_Lot"] = self.NVR1_data[92:94]
    self.infoNVR1["CLEI"] = self.NVR1_data[94:104]

    self.infoNVR1["MSA_HW_Rev"] = self.NVR1_data[104]
    self.infoNVR1["MSA_IF_Rev"] = self.NVR1_data[105]
    self.infoNVR1["Mod_HW_Ver"] = self.NVR1_data[106:108]
    self.infoNVR1["Mod_FW_Ver"] = self.NVR1_data[108:110]
    self.infoNVR1["Mod_FW_B_Ver"] = self.NVR1_data[123:125]

    self.infoNVR1["DiagsMonType"] = self.NVR1_data[110]
    self.infoNVR1["Diag_Mon_Capability"] = self.NVR1_data[111:113]
    self.infoNVR1["EnhancedOpts"] = [self.NVR1_data[113], self.NVR1_data[120]]
    self.infoNVR1["Time_HIPWR_Max"] = self.NVR1_data[114]
    self.infoNVR1["Time_TXON_Max"] = self.NVR1_data[115]
    self.infoNVR1["SignalSpec_Host"] = self.NVR1_data[116]
    self.infoNVR1["Heatsink"] = self.NVR1_data[117]
    self.infoNVR1["Time_TXOFF_Max"] = self.NVR1_data[118]
    self.infoNVR1["Time_LOPWR_Max"] = self.NVR1_data[119]
    self.infoNVR1["MonClkOpts_TX"] = self.NVR1_data[121]
    self.infoNVR1["MonClkOpts_RX"] = self.NVR1_data[122]

  def _parse_NVR1_vendor(self):
    self.infoNVR1["VendorName"] = u.parseString(self.infoNVR1["VendorName"])
    self.infoNVR1["VendorOUI"] = u.parseOUI(self.infoNVR1["VendorOUI"])
    self.infoNVR1["VendorPN"] = u.parseString(self.infoNVR1["VendorPN"])
    self.infoNVR1["VendorSN"] = u.parseString(self.infoNVR1["VendorSN"])
    self.infoNVR1["VendorDate"] = u.parseString(self.infoNVR1["VendorDate"])
    self.infoNVR1["Vendor_Lot"] = u.parseString(self.infoNVR1["Vendor_Lot"])
    if ((self.infoNVR1["ModID"] == "CFP") and (self.infoNVR1["ExtId"]["CLEI"])):
      self.infoNVR1["CLEI"] = u.parseString(self.infoNVR1["CLEI"])

  def _parse_NVR1(self):
    if (self.infoNVR1["ModID"] in dictIdentifier.keys()):
      self.infoNVR1["ModID"] = dictIdentifier[self.infoNVR1["ModID"]]

    if (self.infoNVR1["ModID"] == "CFP"):  # ignore for 100GLH
      tmp_parse = {}
      tmp = (self.infoNVR1["ExtId"] & 0xC0) // 64
      tmp_parse["Power Class"] = self.NVR1_01_Power_Class[tmp]
      tmp = (self.infoNVR1["ExtId"] & 0x30) // 16
      tmp_parse["Lane Ratio"] = self.NVR1_01_Lane_Ratio[tmp]
      tmp = (self.infoNVR1["ExtId"] & 0x0E) // 2
      tmp_parse["WDM Type"] = self.NVR1_01_WDM[tmp]
      if (pyauto_base.bin.getBit(0, self.infoNVR1["ExtId"]) == 1):
        tmp_parse["CLEI"] = True
      else:
        tmp_parse["CLEI"] = False
      if (tmp_parse != {}):
        self.infoNVR1["ExtId"] = tmp_parse

    if (self.infoNVR1["Connector"] in self.NVR1_02_Connector.keys()):
      self.infoNVR1["Connector"] = self.NVR1_02_Connector[self.infoNVR1["Connector"]]

    tmp_parse = []
    if (self.infoNVR1["AppCodes"][0] in self.NVR1_03_AppCode_ETH.keys()):
      tmp_parse += self.NVR1_03_AppCode_ETH[self.infoNVR1["AppCodes"][0]]
    if (self.infoNVR1["AppCodes"][3] in self.NVR1_06_AppCode_Sonet.keys()):
      tmp_parse += self.NVR1_06_AppCode_Sonet[self.infoNVR1["AppCodes"][3]]
    if (self.infoNVR1["AppCodes"][4] in self.NVR1_07_AppCode_OTN.keys()):
      tmp_parse += self.NVR1_07_AppCode_OTN[self.infoNVR1["AppCodes"][4]]
    if (len(tmp_parse) > 0):
      self.infoNVR1["AppCodes"] = tmp_parse

    tmp_parse = []
    bits = u.convertByte2Bits(self.infoNVR1["AdditionalRates"])
    if (self.infoNVR1["ModID"] == "MSA-100GLH"):
      if (bits[6] == 1):
        tmp_parse.append("OTU4 with EFEC")
      if (bits[5] == 1):
        tmp_parse.append("OTU3 with EFEC")
    if (bits[4] == 1):
      tmp_parse.append("111.8 Gbps")
    if (bits[3] == 1):
      tmp_parse.append("103.125 Gbps")
    if (bits[2] == 1):
      tmp_parse.append("41.25 Gbps")
    if (bits[1] == 1):
      tmp_parse.append("43 Gbps")
    if (bits[0] == 1):
      tmp_parse.append("39.8 Gbps")
    if (len(tmp_parse) > 0):
      self.infoNVR1["AdditionalRates"] = tmp_parse

    tmp = self.infoNVR1["Lanes"] % 16
    nHost = 16 if (tmp == 0) else tmp
    tmp = self.infoNVR1["Lanes"] // 16
    nNet = 16 if (tmp == 0) else tmp
    self.infoNVR1["Lanes"] = {"Network": nNet, "Host": nHost}

    tmp_parse = []
    tmp = (self.infoNVR1["MediaProperties"] & 0xC0) // 64
    bits = u.convertByte2Bits(self.infoNVR1["MediaProperties"])
    tmp_parse.append(self.NVR1_0A_Media_Type[tmp])
    if (bits[5] == 0):
      tmp_parse.append("one-way")
    else:
      tmp_parse.append("bidir")
    if (bits[4] == 1):
      tmp_parse.append("Optical MUX/DEMUX")
    else:
      tmp_parse.append("NO Optical MUX/DEMUX")
    nAct = self.infoNVR1["MediaProperties"] % 16
    nAct = 16 if (nAct == 0) else nAct
    tmp_parse.append("{:d} active lanes".format(nAct))
    if (len(tmp_parse) > 0):
      self.infoNVR1["MediaProperties"] = tmp_parse

    if (self.infoNVR1["ModID"] == "CFP"):  # ignore for 100GLH
      self.infoNVR1["BR_Max_Net_Lane"] = "{:.2f}Gbps".format(
          self.infoNVR1["BR_Max_Net_Lane"] * 0.2)
    self.infoNVR1["BR_Max_Host_Lane"] = "{:.2f}Gbps".format(
        self.infoNVR1["BR_Max_Host_Lane"] * 0.2)

    self.infoNVR1["LenSMF"] = "{:d}km".format(self.infoNVR1["LenSMF"])
    self.infoNVR1["Length_MMF"] = "{:d}m".format(self.infoNVR1["Length_MMF"] * 10)
    self.infoNVR1["Length_cable"] = "{:d}m".format(self.infoNVR1["Length_cable"])

    tmp_parse = {}
    tmp = self.infoNVR1["SpectralChar_TX"][0] & 0x1F
    tmp_parse["Active TX Fibers"] = tmp
    tmp = self.infoNVR1["SpectralChar_TX"][1] & 0x1F
    tmp_parse["Wavelengths Per Active Fiber"] = tmp
    if (len(tmp_parse) > 0):
      self.infoNVR1["SpectralChar_TX"] = tmp_parse

    # yapf: disable
    tmp = (256 * self.infoNVR1["Wavelength_Min"][0]) + self.infoNVR1["Wavelength_Min"][1]
    self.infoNVR1["Wavelength_Min"] = "{:.4f}nm".format(tmp * 0.025)
    tmp = (256 * self.infoNVR1["Wavelength_Max"][0]) + self.infoNVR1["Wavelength_Max"][1]
    self.infoNVR1["Wavelength_Max"] = "{:.4f}nm".format(tmp * 0.025)
    tmp = (256 * self.infoNVR1["OpticalWidth_Max"][0]) + self.infoNVR1["OpticalWidth_Max"][1]
    # yapf: enable
    self.infoNVR1["OpticalWidth_Max"] = "{:d}ppm".format(tmp)

    tmp_parse = []
    tmp = (self.infoNVR1["DeviceTechnology"][0] & 0xF0) // 16
    tmp_parse.append(self.NVR1_18_Laser_Source[tmp])
    tmp = (self.infoNVR1["DeviceTechnology"][0] & 0x0F) // 16
    bits = u.convertByte2Bits(self.infoNVR1["DeviceTechnology"][1])
    tmp_parse.append(self.NVR1_18_TX_Modulation[tmp])
    if (bits[7] == 1):
      tmp_parse.append("Active Wavelength Control")
    if (bits[6] == 1):
      tmp_parse.append("Cooled TX")
    else:
      tmp_parse.append("Un-cooled TX")
    if (bits[5] == 1):
      tmp_parse.append("Tunable TX")
    if (bits[4] == 1):
      tmp_parse.append("VOA Implemented")
    tmp = (self.infoNVR1["DeviceTechnology"][1] & 0x0C) // 4
    tmp_parse.append(self.NVR1_19_Detector_Type[tmp])
    if (bits[1] == 1):
      tmp_parse.append("CDR with EDC")
    else:
      tmp_parse.append("CDR without EDC")
    if (len(tmp_parse) > 0):
      self.infoNVR1["DeviceTechnology"] = tmp_parse

    tmp_parse = {}
    tmp = (self.infoNVR1["SignalCode"] & 0xC0) // 64
    tmp_parse["Modulation"] = self.NVR1_1A_Modulation[tmp]
    tmp = (self.infoNVR1["SignalCode"] & 0x3C) // 4
    tmp_parse["Signal Coding"] = self.NVR1_1A_Signal_Coding[tmp]
    if (len(tmp_parse) > 0):
      self.infoNVR1["SignalCode"] = tmp_parse

    # yapf: disable
    self.infoNVR1["OpticalOutPwrConn_Max"] = "{:d}uW".format(self.infoNVR1["OpticalOutPwrConn_Max"] * 100)
    self.infoNVR1["OpticalInPwrLane_Max"] = "{:d}uW".format(self.infoNVR1["OpticalInPwrLane_Max"] * 100)
    # yapf: enable

    if (self.infoNVR1["ModID"] == "CFP"):  # ignore for 100GLH
      self.infoNVR1["Pwr_Max"] = "{:d}mW".format(self.infoNVR1["Pwr_Max"] * 200)
      self.infoNVR1["LOPWR_Max"] = "{:d}mW".format(self.infoNVR1["LOPWR_Max"] * 20)

    # yapf: disable
    tmp_parse = [
        "{:d}C".format(pyauto_base.bin.hex2int_2c(hex(self.infoNVR1["CaseTemperatureRange"][1]))),
        "{:d}C".format(pyauto_base.bin.hex2int_2c(hex(self.infoNVR1["CaseTemperatureRange"][0]))),
    ]
    # yapf: enable
    self.infoNVR1["CaseTemperatureRange"] = tmp_parse

    self.infoNVR1["MSA_HW_Rev"] = "{:.1f}".format(self.infoNVR1["MSA_HW_Rev"] / 10.0)
    self.infoNVR1["MSA_IF_Rev"] = "{:.1f}".format(self.infoNVR1["MSA_IF_Rev"] / 10.0)

    # yapf: disable
    self.infoNVR1["Mod_HW_Ver"] = "{}.{}".format(self.infoNVR1["Mod_HW_Ver"][0], self.infoNVR1["Mod_HW_Ver"][1])
    self.infoNVR1["Mod_FW_Ver"] = "{}.{}".format(self.infoNVR1["Mod_FW_Ver"][0], self.infoNVR1["Mod_FW_Ver"][1])
    if (self.infoNVR1["ModID"] == "MSA-100GLH"):
      self.infoNVR1["Mod_FW_B_Ver"] = "{}.{}".format(self.infoNVR1["Mod_FW_B_Ver"][0], self.infoNVR1["Mod_FW_B_Ver"][1])
    # yapf: enable

    tmp_parse = []
    bits = u.convertByte2Bits(self.infoNVR1["DiagsMonType"])
    if (bits[3] == 1):
      tmp_parse.append("RX Power Measurement: AVG")
    else:
      tmp_parse.append("RX Power Measurement: OMA")
    if (bits[2] == 1):
      tmp_parse.append("TX Power Measurement: AVG")
    else:
      tmp_parse.append("TX Power Measurement: OMA")
    if (len(tmp_parse) > 0):
      self.infoNVR1["DiagsMonType"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "", "", "", "", "", "TX SOA bias current", "TX supply voltage", "TX supply voltage"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoNVR1["Diag_Mon_Capability"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "", "", "", "", "Network RX power", "Network laser output power",
        "Network laser bias current", "Network laser temperature"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoNVR1["Diag_Mon_Capability"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoNVR1["Diag_Mon_Capability"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "Host Loopback", "Host PRBS", "Host Emphasis", "Network Loopback", "Network PRBS",
        "FEC Decision Threshold Voltage", "FEC Decision Phase",
        "Unidirectional TX/RX Operation"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoNVR1["EnhancedOpts"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)

    lst_bit_names = [
        "", "", "", "Active Decision Voltage and Phase", "RX FIFO Reset",
        "RX FIFO Auto-Reset", "TX FIFO Reset", "TX FIFO Auto-Reset"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoNVR1["EnhancedOpts"][1], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoNVR1["EnhancedOpts"] = tmp_parse

    tmp = self.infoNVR1["Time_HIPWR_Max"]
    self.infoNVR1["Time_HIPWR_Max"] = "1s" if (tmp == 0) else "{:d}s".format(tmp)
    tmp = self.infoNVR1["Time_TXON_Max"]
    self.infoNVR1["Time_TXON_Max"] = "1s" if (tmp == 0) else "{:d}s".format(tmp)
    tmp = self.infoNVR1["Time_TXOFF_Max"]
    self.infoNVR1["Time_TXOFF_Max"] = "1s" if (tmp == 0) else "{:d}s".format(tmp)
    tmp = self.infoNVR1["Time_LOPWR_Max"]
    self.infoNVR1["Time_LOPWR_Max"] = "1s" if (tmp == 0) else "{:d}s".format(tmp)

    if (self.infoNVR1["SignalSpec_Host"] in self.NVR1_74_SignalSpec_Host.keys()):
      self.infoNVR1["SignalSpec_Host"] = self.NVR1_74_SignalSpec_Host[
          self.infoNVR1["SignalSpec_Host"]]

    tmp_parse = []
    bits = u.convertByte2Bits(self.infoNVR1["Heatsink"])
    if (bits[0] == 1):
      tmp_parse.append("Integrated")
    else:
      tmp_parse.append("Flat Top")
    if (len(tmp_parse) > 0):
      self.infoNVR1["Heatsink"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "1/16 Host Rate", "1/16 Network Rate", "1/64 Host Rate", "1/64 Network Rate", None,
        "1/8 Network Rate", None, None
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoNVR1["MonClkOpts_TX"], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoNVR1["MonClkOpts_TX"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "1/16 Host Rate", "1/16 Network Rate", "1/64 Host Rate", "1/64 Network Rate", None,
        "1/8 Network Rate", None, None
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoNVR1["MonClkOpts_RX"], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if (len(tmp_parse) > 0):
      self.infoNVR1["MonClkOpts_RX"] = tmp_parse

    self._parse_NVR1_vendor()

  def parse_NVR1(self, bRaw=False):
    if (len(self.NVR1_data) != 128):
      logger.error("wrong size for NVR1 ({:d} bytes)".format(len(self.NVR1_data)))
      return

    self._parse_NVR1_values()
    if (not bRaw):
      self._parse_NVR1()

    crc = 0
    for i in range(0, 127):
      crc += self.NVR1_data[i]
    if ((crc % 256) == self.NVR1_data[127]):
      self.infoNVR1["CRC"] = "OK"
    else:
      self.infoNVR1["CRC"] = "FAIL"

  def showInfo_NVR1(self):
    logger.info("-- NVR1")
    for k, v in sorted(self.infoNVR1.items()):
      logger.info("{:<24}: {}".format(k, v))

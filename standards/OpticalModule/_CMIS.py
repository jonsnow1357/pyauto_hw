#!/usr/bin/env python
# template: library
# SPDX-License-Identifier: MIT or GPL-3.0-or-later
"""library"""

#import site #http://docs.python.org/library/site.html
import sys
import os
#import platform
import logging
#import re
#import time
#import datetime

#sys.path.append("./")
#sys.path.append("../")

#import math
#import csv

logger = logging.getLogger("lib")
from ._constants import *
from . import _utils as u

class CMIS(object):

  dict_base_41_fault = {
      0x00: "none",
      0x01: "TEC runaway",
      0x02: "Data mem. corrupted",
      0x03: "Prog mem. corrupted",
  }

  dict_base_85_media_type = {
      0x00: "undefined",
      0x01: "MMF",
      0x02: "SMF",
      0x03: "passive",
      0x04: "active",
      0x05: "BASE-T",
  }

  dict_up_00_00_212_media_tech = {
      0x00: "850 nm VCSEL",
      0x01: "1310 nm VCSEL",
      0x02: "1550 nm VCSEL",
      0x03: "1310 nm FP",
      0x04: "1310 nm DFB",
      0x05: "1550 nm DFB",
      0x06: "1310 nm EML",
      0x07: "1550 nm EML",
      0x08: "others",
      0x09: "1490 nm DFB",
      0x0A: "Cu cable unequalized",
      0x0B: "Cu cable passive equalized",
      0x0C: "Cu cable, near and far end limiting active equalizers",
      0x0D: "Cu cable, far end limiting active equalizers",
      0x0E: "Cu cable, near end limiting active equalizers",
      0x0F: "Cu cable, linear active equalizers",
      0x10: "C-band tunable laser",
      0x11: "L-band tunable laser",
  }

  dict_up_00_01_144_state_duration = {
      0x0: "T < 1 ms",
      0x1: "1 ms < T < 5 ms",
      0x2: "5 ms < T < 10 ms",
      0x3: "10 ms < T < 50 ms",
      0x4: "50 ms < T < 100 ms",
      0x5: "100 ms < T < 500 ms",
      0x6: "500 ms < T < 1 s",
      0x7: "1 s < T < 5 s",
      0x8: "5 s < T < 10 s",
      0x9: "10 s < T < 1 min",
      0xA: "1 min < T < 5 min",
      0xB: "5 min < T < 10 min",
      0xC: "10 min < T < 50 min",
      0xD: "50 min <= T",
      0xE: "reserved",
      0xF: "reserved",
  }

  def __init__(self):
    self.base_data = 128 * [0xFF]
    self.up_00_00_data = 128 * [0xFF]
    self.up_00_01_data = 128 * [0xFF]
    self.up_00_04_data = 128 * [0xFF]
    self.infoBase = {}
    self.infoBk00Pg00 = {}
    self.infoBk00Pg01 = {}
    self.infoBk00Pg04 = {}
    self._media_type = None

  def clear(self):
    self.base_data = 128 * [0xFF]
    self.up_00_00_data = 128 * [0xFF]
    self.up_00_01_data = 128 * [0xFF]
    self.up_00_04_data = 128 * [0xFF]
    self.infoBase = {}
    self.infoBk00Pg00 = {}
    self.infoBk00Pg01 = {}
    self.infoBk00Pg04 = {}
    self._media_type = None

  def _parse_base_values(self):
    # CMIS rev 5.2 Table 8-5
    self.infoBase["ModID"] = self.base_data[0]
    self.infoBase["CMISRev"] = self.base_data[1]
    self.infoBase["ModCaps"] = self.base_data[2]
    self.infoBase["ModState"] = self.base_data[3]
    self.infoBase["LaneFlags"] = self.base_data[4:8]
    self.infoBase["ModFlags"] = self.base_data[8:14]
    self.infoBase["ModMon"] = self.base_data[14:26]
    self.infoBase["ModCtrl"] = self.base_data[26:31]
    self.infoBase["ModMasks"] = self.base_data[31:37]
    self.infoBase["CDBStatus"] = self.base_data[37:39]
    self.infoBase["ModFW"] = self.base_data[39:41]
    self.infoBase["ModFault"] = self.base_data[41]
    self.infoBase["MediaType"] = self.base_data[85]
    self.infoBase["AppSel1"] = self.base_data[86:90]
    self.infoBase["AppSel2"] = self.base_data[90:94]
    self.infoBase["AppSel3"] = self.base_data[94:98]
    self.infoBase["AppSel4"] = self.base_data[98:102]
    self.infoBase["AppSel5"] = self.base_data[102:106]
    self.infoBase["AppSel6"] = self.base_data[106:110]
    self.infoBase["AppSel7"] = self.base_data[110:114]
    self.infoBase["AppSel8"] = self.base_data[114:118]

  def _parse_base(self):
    # yapf: disable
    self.infoBase["CMISRev"] = "{}.{}".format((self.infoBase["CMISRev"] // 16), (self.infoBase["CMISRev"] & 0x0f))
    # yapf: enable

    tmp_parse = {}
    bits = u.convertByte2Bits(self.infoBase["ModCaps"])
    if (bits[7] == 1):
      tmp_parse["MemModel"] = "flat"
    else:
      tmp_parse["MemModel"] = "paged"
    if (bits[6] == 1):
      tmp_parse["Config"] = "step-by-step"
    else:
      tmp_parse["Config"] = "intervention-free"
    if (((bits[3] * 2) + bits[2]) == 0):
      tmp_parse["MaxSpeed"] = "400k"
    elif (((bits[3] * 2) + bits[2]) == 1):
      tmp_parse["MaxSpeed"] = "1M"
    self.infoBase["ModCaps"] = tmp_parse

    tmp_parse = ["", ""]
    if ((self.infoBase["ModState"] // 2) == 1):
      tmp_parse[0] = "LowPwr"
    elif ((self.infoBase["ModState"] // 2) == 2):
      tmp_parse[0] = "PwrUp"
    elif ((self.infoBase["ModState"] // 2) == 3):
      tmp_parse[0] = "Ready"
    elif ((self.infoBase["ModState"] // 2) == 4):
      tmp_parse[0] = "PwrDown"
    elif ((self.infoBase["ModState"] // 2) == 5):
      tmp_parse[0] = "Fault"
    else:
      tmp_parse[0] = "UNDEF"
    if ((self.infoBase["ModState"] % 2) == 1):
      tmp_parse[1] = "(INT)"
    self.infoBase["ModState"] = " ".join(tmp_parse)

    tmp_parse = {
        "Temp": u.convertTemp(self.infoBase["ModMon"][0:2]),
        "VCC": u.convertVoltage(self.infoBase["ModMon"][2:4]),
    }
    self.infoBase["ModMon"] = tmp_parse

    self._media_type = self.infoBase["MediaType"]
    self.infoBase["MediaType"] = self.dict_base_85_media_type[self.infoBase["MediaType"]]

    # yapf: disable
    self.infoBase["ModFW"] = "{}.{}".format(self.infoBase["ModFW"][0], self.infoBase["ModFW"][1])
    # yapf: enable

    self.infoBase["ModFault"] = self.dict_base_41_fault[self.infoBase["ModFault"]]

    self.infoBase["AppSel1"] = u.parseAppSel(self.infoBase["AppSel1"], self._media_type)
    self.infoBase["AppSel2"] = u.parseAppSel(self.infoBase["AppSel2"], self._media_type)
    self.infoBase["AppSel3"] = u.parseAppSel(self.infoBase["AppSel3"], self._media_type)
    self.infoBase["AppSel4"] = u.parseAppSel(self.infoBase["AppSel4"], self._media_type)
    self.infoBase["AppSel5"] = u.parseAppSel(self.infoBase["AppSel5"], self._media_type)
    self.infoBase["AppSel6"] = u.parseAppSel(self.infoBase["AppSel6"], self._media_type)
    self.infoBase["AppSel7"] = u.parseAppSel(self.infoBase["AppSel7"], self._media_type)
    self.infoBase["AppSel8"] = u.parseAppSel(self.infoBase["AppSel8"], self._media_type)

  def _parse_up_00_00_values(self):
    # CMIS rev 5.2 Table 8-23
    self.infoBk00Pg00["ModID"] = self.up_00_00_data[0]
    self.infoBk00Pg00["VendorName"] = self.up_00_00_data[1:17]
    self.infoBk00Pg00["VendorOUI"] = self.up_00_00_data[17:20]
    self.infoBk00Pg00["VendorPN"] = self.up_00_00_data[20:36]
    self.infoBk00Pg00["VendorRev"] = self.up_00_00_data[36:38]
    self.infoBk00Pg00["VendorSN"] = self.up_00_00_data[38:54]
    self.infoBk00Pg00["VendorDate"] = self.up_00_00_data[54:62]
    self.infoBk00Pg00["CLEI"] = self.up_00_00_data[62:72]
    self.infoBk00Pg00["ModPwr"] = self.up_00_00_data[72:74]
    self.infoBk00Pg00["CableLen"] = self.up_00_00_data[74]
    self.infoBk00Pg00["Connector"] = self.up_00_00_data[75]
    self.infoBk00Pg00["CableAtt"] = self.up_00_00_data[76:82]
    self.infoBk00Pg00["MediaInfo"] = self.up_00_00_data[82]
    self.infoBk00Pg00["CableInfo"] = self.up_00_00_data[83]
    self.infoBk00Pg00["MediaTech"] = self.up_00_00_data[84]

  def _parse_up_00_00_vendor(self):
    self.infoBk00Pg00["VendorName"] = u.parseString(self.infoBk00Pg00["VendorName"])
    self.infoBk00Pg00["VendorOUI"] = u.parseOUI(self.infoBk00Pg00["VendorOUI"])
    self.infoBk00Pg00["VendorPN"] = u.parseString(self.infoBk00Pg00["VendorPN"])
    self.infoBk00Pg00["VendorRev"] = u.parseString(self.infoBk00Pg00["VendorRev"])
    self.infoBk00Pg00["VendorSN"] = u.parseString(self.infoBk00Pg00["VendorSN"])
    self.infoBk00Pg00["VendorDate"] = u.parseString(self.infoBk00Pg00["VendorDate"])
    self.infoBk00Pg00["CLEI"] = u.parseString(self.infoBk00Pg00["CLEI"])

  def _parse_up_00_00(self):
    tmp_parse = {
        "class": ((self.infoBk00Pg00["ModPwr"][0] // 32) + 1),
        "max": self.infoBk00Pg00["ModPwr"][1] * 0.25
    }
    self.infoBk00Pg00["ModPwr"] = tmp_parse

    if (self.infoBk00Pg00["Connector"] in dictConnector.keys()):
      self.infoBk00Pg00["Connector"] = dictConnector[self.infoBk00Pg00["Connector"]]

    self.infoBk00Pg00["MediaInfo"] = "0x{:02x}".format(self.infoBk00Pg00["MediaInfo"])

    if (self.infoBk00Pg00["MediaTech"] in self.dict_up_00_00_212_media_tech.keys()):
      self.infoBk00Pg00["MediaTech"] = self.dict_up_00_00_212_media_tech[
          self.infoBk00Pg00["MediaTech"]]

    self._parse_up_00_00_vendor()

  def _parse_up_00_01_values(self):
    # CMIS rev 5.2 Table 8-37
    self.infoBk00Pg01["ModFW"] = self.up_00_01_data[0:4]
    self.infoBk00Pg01["LinkLength"] = self.up_00_01_data[4:10]
    self.infoBk00Pg01["Wavelength"] = self.up_00_01_data[10:14]
    self.infoBk00Pg01["SuppPages"] = self.up_00_01_data[14]
    self.infoBk00Pg01["DurationAdv"] = self.up_00_01_data[15:17]
    self.infoBk00Pg01["ModCharac"] = self.up_00_01_data[17:27]
    self.infoBk00Pg01["SuppCtrl"] = self.up_00_01_data[27:29]
    self.infoBk00Pg01["SuppFlags"] = self.up_00_01_data[29:31]
    self.infoBk00Pg01["SuppMon"] = self.up_00_01_data[31:33]
    self.infoBk00Pg01["SuppSICtrl"] = self.up_00_01_data[33:35]
    self.infoBk00Pg01["SuppCDB"] = self.up_00_01_data[35:39]
    self.infoBk00Pg01["ExtraDurationAdv"] = self.up_00_01_data[39:42]
    self.infoBk00Pg01["AppSelMediaLnOpts"] = self.up_00_01_data[48:63]
    self.infoBk00Pg01["AppSel9"] = self.up_00_01_data[95:99]
    self.infoBk00Pg01["AppSel10"] = self.up_00_01_data[99:103]
    self.infoBk00Pg01["AppSel11"] = self.up_00_01_data[103:107]
    self.infoBk00Pg01["AppSel12"] = self.up_00_01_data[107:111]
    self.infoBk00Pg01["AppSel13"] = self.up_00_01_data[111:115]
    self.infoBk00Pg01["AppSel14"] = self.up_00_01_data[115:119]
    self.infoBk00Pg01["AppSel15"] = self.up_00_01_data[119:123]

  def _parse_up_00_01(self):
    tmp_parse = {
        "InactiveFW":
        "{}.{}".format(self.infoBk00Pg01["ModFW"][0], self.infoBk00Pg01["ModFW"][1]),
        "HWRev":
        "{}.{}".format(self.infoBk00Pg01["ModFW"][2], self.infoBk00Pg01["ModFW"][3]),
    }
    self.infoBk00Pg01["ModFW"] = tmp_parse

    # yapf: disable
    tmp_parse = {
        "SMF":
        "{}km".format((self.infoBk00Pg01["LinkLength"][0] // 32) * (self.infoBk00Pg01["LinkLength"][0] & 0x1F)),
        "OM5":
        "{}m".format(self.infoBk00Pg01["LinkLength"][1] * 2),
        "OM4":
        "{}m".format(self.infoBk00Pg01["LinkLength"][2] * 2),
        "OM3":
        "{}m".format(self.infoBk00Pg01["LinkLength"][3] * 2),
        "OM2":
        "{}m".format(self.infoBk00Pg01["LinkLength"][4]),
    }
    # yapf: enable
    self.infoBk00Pg01["LinkLength"] = tmp_parse

    tmp_parse = [
        u.convertU16(self.infoBk00Pg01["Wavelength"][0:2]) * 0.05,
        u.convertU16(self.infoBk00Pg01["Wavelength"][2:4]) * 0.0005,
    ]
    self.infoBk00Pg01["Wavelength"] = "{} +/- {}nm".format(tmp_parse[0], tmp_parse[1])

    tmp_parse = {}
    lst_bit_names = ["NetworkPath", "VDM", "Diagss", "", "Page 05h", "Page 03h", "", ""]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppPages"], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if ((bits[1] == 0) and (bits[0] == 0)):
      tmp_parse["Pages 10-2Fh Bank 0"] = True
    elif ((bits[1] == 0) and (bits[0] == 1)):
      tmp_parse["Pages 10-2Fh Banks 0-1"] = True
    elif ((bits[1] == 1) and (bits[0] == 0)):
      tmp_parse["Pages 10-2Fh Banks 0-3"] = True
    self.infoBk00Pg01["SuppPages"] = tmp_parse

    tmp_parse = {}
    e = self.infoBk00Pg01["DurationAdv"][0] // 16
    m = self.infoBk00Pg01["DurationAdv"][0] & 0x0F
    tmp_parse["ModSelWait"] = "{}us".format(m * (2**e))
    if (tmp_parse["ModSelWait"] == "0us"):
      tmp_parse["ModSelWait"] = "NA"
    tmp_parse["DPDeinit"] = self.dict_up_00_01_144_state_duration[
        self.infoBk00Pg01["DurationAdv"][0] // 16]
    tmp_parse["DPinit"] = self.dict_up_00_01_144_state_duration[
        self.infoBk00Pg01["DurationAdv"][0] & 0x0F]
    self.infoBk00Pg01["DurationAdv"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = ["CooledTX", "", "", "ePPS", "Timing", "Aux3Mon", "Aux2Mon", "Aux1Mon"]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["ModCharac"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    if ((bits[6] == 0) and (bits[5] == 0)):
      tmp_parse["TXLanes_1-8_Sync"] = True
    elif ((bits[6] == 0) and (bits[5] == 1)):
      tmp_parse["TXLanes_1-4_5-8_Sync"] = True
    elif ((bits[6] == 1) and (bits[5] == 0)):
      tmp_parse["TXLanes_1-2_3-4_5-6_7-8_Sync"] = True
    else:
      tmp_parse["TXLanes_Async"] = True
    tmp_parse["ModTempMax"] = u.convertS8(self.infoBk00Pg01["ModCharac"][1])
    tmp_parse["ModTempMin"] = u.convertS8(self.infoBk00Pg01["ModCharac"][2])
    v = u.convertU16(self.infoBk00Pg01["ModCharac"][3:5]) * 10
    tmp_parse["PropDelay"] = "NA" if (v == 0) else "{} ns".format(v)
    v = self.infoBk00Pg01["ModCharac"][5] * 0.02
    tmp_parse["ModVoltageMin"] = "NA" if (v == 0) else v
    bits = u.convertByte2Bits(self.infoBk00Pg01["ModCharac"][6])
    if (bits[7] == 0):
      tmp_parse["OpticalDetector"] = "PIN"
    else:
      tmp_parse["OpticalDetector"] = "APD"
    if ((bits[6] == 0) and (bits[5] == 0)):
      tmp_parse["RXOutEq"] = "NA"
    elif ((bits[6] == 0) and (bits[5] == 1)):
      tmp_parse["RXOutEq"] = "steady-state"
    elif ((bits[6] == 1) and (bits[5] == 0)):
      tmp_parse["RXOutEq"] = "Avg of pk-pk and steady-state"
    else:
      tmp_parse["RXOutEq"] = "reserved"
    if (bits[4] == 0):
      tmp_parse["RXPwrMeas"] = "OMA"
    else:
      tmp_parse["RXPwrMeas"] = "avg"
    if (bits[3] == 0):
      tmp_parse["RXLOS"] = "OMA"
    else:
      tmp_parse["RXLOS"] = "avg"
    if (bits[2] == 0):
      tmp_parse["RXLOSisFast"] = False
    else:
      tmp_parse["RXLOSisFast"] = True
    if (bits[1] == 0):
      tmp_parse["TXDISisFast"] = False
    else:
      tmp_parse["TXDISisFast"] = True
    if (bits[0] == 0):
      tmp_parse["TXDIS"] = "per lane"
    else:
      tmp_parse["TXDIS"] = "per module"
    tmp_parse["CDRPowerSavedPerLane"] = (self.infoBk00Pg01["ModCharac"][7] * 0.01)
    lst_bit_names = ["RXOutLvl3", "RXOutLvl2", "RXOutLvl1", "RXOutLvl0", "", "", "", ""]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["ModCharac"][8], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    tmp_parse["TXInEqMax"] = (self.infoBk00Pg01["ModCharac"][8] & 0x0F)
    tmp_parse["RXOutEqPostCursorMax"] = (self.infoBk00Pg01["ModCharac"][9] // 16)
    tmp_parse["RXOutEqPreCursorMax"] = (self.infoBk00Pg01["ModCharac"][9] & 0x0F)
    self.infoBk00Pg01["ModCharac"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "WavelenghtIsControllable", "TXIsTunable", "", "", "ForcedSquelchTX",
        "AutoSquelchDisableTX", "OutDisableTX", "InPolarityFlipTX"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppCtrl"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    if ((bits[5] == 0) and (bits[4] == 0)):
      tmp_parse["SquelchMethodTX"] = "NA"
    elif ((bits[5] == 0) and (bits[4] == 1)):
      tmp_parse["SquelchMethodTX"] = "OMA"
    elif ((bits[5] == 1) and (bits[4] == 0)):
      tmp_parse["SquelchMethodTX"] = "avg"
    else:
      tmp_parse["SquelchMethodTX"] = "ModCtrl in Page 00h"
    lst_bit_names = [
        "BankBcast", "", "", "", "", "AutoSquelchDisableRX", "OutDisableRX",
        "InPolarityFlipRX"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppCtrl"][1], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    self.infoBk00Pg01["SuppCtrl"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "", "", "", "", "AdaptiveInputEqFailFlagTX", "CDRLOLFlagTX", "LOSFlagTX",
        "LOSFlagTX"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppFlags"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    lst_bit_names = ["", "", "", "", "", "CDRLOLFlagRX", "LOSFlagRX", ""]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppFlags"][1], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    self.infoBk00Pg01["SuppFlags"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "", "", "CustomMon", "Aux3Mon", "Aux2Mon", "Aux1Mon", "VCCMon", "TempMon"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppMon"][0], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    lst_bit_names = [
        "", "", "", "", "", "RXOpticalPowerMon", "TXOpticalPowerMon", "TXBiasMon"
    ]
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppMon"][1], lst_bit_names)
    tmp_parse.update(dict_bit_info)
    if ((bits[4] == 0) and (bits[3] == 0)):
      tmp_parse["TXBiasCurrentScaling"] = 1
    elif ((bits[4] == 0) and (bits[3] == 0)):
      tmp_parse["TXBiasCurrentScaling"] = 2
    elif ((bits[4] == 0) and (bits[3] == 0)):
      tmp_parse["TXBiasCurrentScaling"] = 4
    else:
      tmp_parse["TXBiasCurrentScaling"] = "reserved"
    self.infoBk00Pg01["SuppMon"] = tmp_parse

    tmp_parse = {}
    lst_bit_names = [
        "", "", "", "TXInputEqFreeze", "TXInputAdaptiveEq", "TXInputEqFixedManualControl",
        "TXCDRBypassControl", "TXCDR"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppSICtrl"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    if ((bits[6] == 0) and (bits[5] == 0)):
      tmp_parse["TXInputEqRecallBuffers"] = "NA"
    elif ((bits[6] == 0) and (bits[5] == 0)):
      tmp_parse["TXInputEqRecallBuffers"] = 1
    elif ((bits[6] == 0) and (bits[5] == 0)):
      tmp_parse["TXInputEqRecallBuffers"] = 2
    else:
      tmp_parse["TXInputEqRecallBuffers"] = "reserved"
    lst_bit_names = [
        "", "UnidirReconfig", "StagedSet1", "", "", "RXOutputAmplitudeControl",
        "RXCDRBypassControl", "RXCDR"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg01["SuppSICtrl"][1], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    if ((bits[4] == 0) and (bits[3] == 0)):
      tmp_parse["RXOutputEqControl"] = "NA"
    elif ((bits[4] == 0) and (bits[3] == 0)):
      tmp_parse["RXOutputEqControl"] = "pre-cursor"
    elif ((bits[4] == 0) and (bits[3] == 0)):
      tmp_parse["RXOutputEqControl"] = "post-cursor"
    else:
      tmp_parse["RXOutputEqControl"] = "pre- and post-cursor"
    self.infoBk00Pg01["SuppSICtrl"] = tmp_parse

    # TODO: self.infoBk00Pg01["SuppCDB"]

    # yapf: disable
    tmp_parse = {
        "PwrDwn": self.dict_up_00_01_144_state_duration[self.infoBk00Pg01["ExtraDurationAdv"][0] // 16],
        "PwrUp": self.dict_up_00_01_144_state_duration[self.infoBk00Pg01["ExtraDurationAdv"][0] & 0x0F],
        "DPTXTurnOff": self.dict_up_00_01_144_state_duration[self.infoBk00Pg01["ExtraDurationAdv"][1] // 16],
        "DPTXTurnOn": self.dict_up_00_01_144_state_duration[self.infoBk00Pg01["ExtraDurationAdv"][1] & 0x0F],
    }
    # yapf: enable
    self.infoBk00Pg01["ExtraDurationAdv"] = tmp_parse

    self.infoBk00Pg01["AppSelMediaLnOpts"] = [
        "0x{:02x}".format(t) for t in self.infoBk00Pg01["AppSelMediaLnOpts"]
    ]

    # yapf: disable
    self.infoBk00Pg01["AppSel9"] = u.parseAppSel(self.infoBk00Pg01["AppSel9"], self._media_type)
    self.infoBk00Pg01["AppSel10"] = u.parseAppSel(self.infoBk00Pg01["AppSel10"], self._media_type)
    self.infoBk00Pg01["AppSel11"] = u.parseAppSel(self.infoBk00Pg01["AppSel11"], self._media_type)
    self.infoBk00Pg01["AppSel12"] = u.parseAppSel(self.infoBk00Pg01["AppSel12"], self._media_type)
    self.infoBk00Pg01["AppSel13"] = u.parseAppSel(self.infoBk00Pg01["AppSel13"], self._media_type)
    self.infoBk00Pg01["AppSel14"] = u.parseAppSel(self.infoBk00Pg01["AppSel14"], self._media_type)
    self.infoBk00Pg01["AppSel15"] = u.parseAppSel(self.infoBk00Pg01["AppSel15"], self._media_type)
    # yapf: enable

  def _parse_up_00_04_values(self):
    # CMIS rev 5.2 Table 8-58
    self.infoBk00Pg04["WavelengthGrids"] = self.up_00_04_data[0:2]
    self.infoBk00Pg04["ChNoRanges"] = self.up_00_04_data[2:62]
    self.infoBk00Pg04["FineTuning"] = self.up_00_04_data[62:70]
    self.infoBk00Pg04["OutPwr"] = self.up_00_04_data[70:74]

  def _parse_up_00_04(self):
    tmp_parse = {}
    lst_bit_names = [
        "Grid75GHz", "Grid33GHz", "Grid100GHz", "Grid50GHz", "Grid25GHz", "Grid12p5GHz",
        "Grid6p25GHz", "Grid3p125GHz"
    ]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg04["WavelengthGrids"][0], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    lst_bit_names = ["FineTuning", "", "", "", "", "", "", ""]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg04["WavelengthGrids"][1], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    self.infoBk00Pg04["WavelengthGrids"] = tmp_parse

    tmp_parse = {}
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][0:2])
    if (s != 0):
      tmp_parse["LowCH3p125GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][2:4])
    if (s != 0):
      tmp_parse["HighCH3p125GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][4:6])
    if (s != 0):
      tmp_parse["LowCH6p25GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][6:8])
    if (s != 0):
      tmp_parse["HighCH6p25GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][8:10])
    if (s != 0):
      tmp_parse["LowCH12p5GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][10:12])
    if (s != 0):
      tmp_parse["HighCH12p5GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][12:14])
    if (s != 0):
      tmp_parse["LowCH25GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][14:16])
    if (s != 0):
      tmp_parse["HighCH25GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][16:18])
    if (s != 0):
      tmp_parse["LowCH50GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][18:20])
    if (s != 0):
      tmp_parse["HighCH50GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][20:22])
    if (s != 0):
      tmp_parse["LowCH100GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][22:24])
    if (s != 0):
      tmp_parse["HighCH100GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][24:26])
    if (s != 0):
      tmp_parse["LowCH33GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][26:28])
    if (s != 0):
      tmp_parse["HighCH33GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][28:30])
    if (s != 0):
      tmp_parse["LowCH75GHz"] = s
    s = u.convertS16(self.infoBk00Pg04["ChNoRanges"][30:32])
    if (s != 0):
      tmp_parse["HighCH75GHz"] = s
    self.infoBk00Pg04["ChNoRanges"] = tmp_parse

    tmp_parse = {}
    s = u.convertU16(self.infoBk00Pg04["FineTuning"][0:2])
    tmp_parse["FineTuningResolution"] = "{} GHz".format(s * 0.001)
    s = u.convertS16(self.infoBk00Pg04["FineTuning"][2:4])
    tmp_parse["FineTuningLowOffset"] = "{} GHz".format(s * 0.001)
    s = u.convertS16(self.infoBk00Pg04["FineTuning"][4:6])
    tmp_parse["FineTuningHighOffset"] = "{} GHz".format(s * 0.001)
    lst_bit_names = ["ProgOutPwrPerLane", "", "", "", "", "", "", ""]
    # yapf: disable
    bits, dict_bit_info = u.convertByte2Dict(self.infoBk00Pg04["FineTuning"][6], lst_bit_names)
    # yapf: enable
    tmp_parse.update(dict_bit_info)
    self.infoBk00Pg04["FineTuning"] = tmp_parse

    tmp_parse = {}
    s = u.convertS16(self.infoBk00Pg04["OutPwr"][0:2])
    tmp_parse["ProgOutPwrMin"] = "{} dBm".format(s * 0.01)
    s = u.convertS16(self.infoBk00Pg04["OutPwr"][2:4])
    tmp_parse["ProgOutPwrMax"] = "{} dBm".format(s * 0.01)
    self.infoBk00Pg04["OutPwr"] = tmp_parse

  def parse_base(self, bRaw=False):
    if (len(self.base_data) != 128):
      logger.error("wrong size for base ({:d} bytes)".format(len(self.base_data)))
      return

    self._parse_base_values()
    if (not bRaw):
      self._parse_base()

  def parse_up_00_00(self, bRaw=False):
    if (len(self.up_00_00_data) != 128):
      logger.error("wrong size for bank 0 page 0 ({:d} bytes)".format(
          len(self.up_00_00_data)))
      return

    self._parse_up_00_00_values()
    if (not bRaw):
      self._parse_up_00_00()

    crc = u.CRC(self.up_00_00_data, 0, 94)
    if (crc == self.up_00_00_data[94]):
      self.infoBk00Pg00["CRC"] = "OK"
    else:
      self.infoBk00Pg00["CRC"] = "FAIL"

  def parse_up_00_01(self, bRaw=False):
    if (len(self.up_00_01_data) != 128):
      logger.error("wrong size for bank 0 page 1 ({:d} bytes)".format(
          len(self.up_00_01_data)))
      return

    self._parse_up_00_01_values()
    if (not bRaw):
      self._parse_up_00_01()

    crc = u.CRC(self.up_00_01_data, 2, 127)
    if (crc == self.up_00_01_data[127]):
      self.infoBk00Pg01["CRC"] = "OK"
    else:
      self.infoBk00Pg01["CRC"] = "FAIL"

  def parse_up_00_04(self, bRaw=False):
    if (len(self.up_00_04_data) != 128):
      logger.error("wrong size for bank 0 page 4 ({:d} bytes)".format(
          len(self.up_00_04_data)))
      return

    self._parse_up_00_04_values()
    if (not bRaw):
      self._parse_up_00_04()

    crc = u.CRC(self.up_00_04_data, 0, 127)
    if (crc == self.up_00_04_data[127]):
      self.infoBk00Pg04["CRC"] = "OK"
    else:
      self.infoBk00Pg04["CRC"] = "FAIL"

  def showInfo_base(self):
    logger.info("-- base")
    for k, v in sorted(self.infoBase.items()):
      logger.info("{:<18}: {}".format(k, v))

  def showInfo_up_00_00(self):
    logger.info("-- bank 0x00 page 0x00")
    for k, v in sorted(self.infoBk00Pg00.items()):
      logger.info("{:<18}: {}".format(k, v))

  def showInfo_up_00_01(self):
    logger.info("-- bank 0x00 page 0x01")
    for k, v in sorted(self.infoBk00Pg01.items()):
      logger.info("{:<18}: {}".format(k, v))

  def showInfo_up_00_04(self):
    logger.info("-- bank 0x00 page 0x04")
    for k, v in sorted(self.infoBk00Pg04.items()):
      logger.info("{:<18}: {}".format(k, v))
